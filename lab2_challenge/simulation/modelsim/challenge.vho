-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.1 Build 190 01/19/2015 SJ Full Version"

-- DATE "10/02/2016 17:23:23"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	challenge IS
    PORT (
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	VGA_R : BUFFER std_logic_vector(9 DOWNTO 0);
	VGA_G : BUFFER std_logic_vector(9 DOWNTO 0);
	VGA_B : BUFFER std_logic_vector(9 DOWNTO 0);
	VGA_HS : BUFFER std_logic;
	VGA_VS : BUFFER std_logic;
	VGA_BLANK : BUFFER std_logic;
	VGA_SYNC : BUFFER std_logic;
	VGA_CLK : BUFFER std_logic;
	LEDR : BUFFER std_logic_vector(9 DOWNTO 0)
	);
END challenge;

-- Design Ports Information
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[8]	=>  Location: PIN_AK12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[9]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[8]	=>  Location: PIN_AF8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[9]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[8]	=>  Location: PIN_AK4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[9]	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_BLANK	=>  Location: PIN_AG13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_SYNC	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF challenge IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_VGA_R : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_BLANK : std_logic;
SIGNAL ww_VGA_SYNC : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dp|Mult0~136_AX_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \dp|Mult0~136_AY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult0~136_BX_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \dp|Mult0~136_BY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult0~136_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \dp|Mult0~477_AX_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult0~477_AY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult0~477_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \dp|Mult1~20_AX_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult1~20_AY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult1~20_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \dp|Mult1~405_AX_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \dp|Mult1~405_AY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult1~405_BX_bus\ : std_logic_vector(13 DOWNTO 0);
SIGNAL \dp|Mult1~405_BY_bus\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \dp|Mult1~405_RESULTA_bus\ : std_logic_vector(63 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|Mult0~150\ : std_logic;
SIGNAL \dp|Mult0~151\ : std_logic;
SIGNAL \dp|Mult0~152\ : std_logic;
SIGNAL \dp|Mult0~153\ : std_logic;
SIGNAL \dp|Mult0~154\ : std_logic;
SIGNAL \dp|Mult0~155\ : std_logic;
SIGNAL \dp|Mult0~156\ : std_logic;
SIGNAL \dp|Mult0~157\ : std_logic;
SIGNAL \dp|Mult0~158\ : std_logic;
SIGNAL \dp|Mult0~159\ : std_logic;
SIGNAL \dp|Mult0~160\ : std_logic;
SIGNAL \dp|Mult0~161\ : std_logic;
SIGNAL \dp|Mult0~162\ : std_logic;
SIGNAL \dp|Mult0~163\ : std_logic;
SIGNAL \dp|Mult0~164\ : std_logic;
SIGNAL \dp|Mult0~165\ : std_logic;
SIGNAL \dp|Mult0~166\ : std_logic;
SIGNAL \dp|Mult0~167\ : std_logic;
SIGNAL \dp|Mult0~168\ : std_logic;
SIGNAL \dp|Mult0~169\ : std_logic;
SIGNAL \dp|Mult0~170\ : std_logic;
SIGNAL \dp|Mult0~171\ : std_logic;
SIGNAL \dp|Mult0~172\ : std_logic;
SIGNAL \dp|Mult0~173\ : std_logic;
SIGNAL \dp|Mult0~174\ : std_logic;
SIGNAL \dp|Mult0~175\ : std_logic;
SIGNAL \dp|Mult0~176\ : std_logic;
SIGNAL \dp|Mult0~177\ : std_logic;
SIGNAL \dp|Mult0~178\ : std_logic;
SIGNAL \dp|Mult0~179\ : std_logic;
SIGNAL \dp|Mult0~180\ : std_logic;
SIGNAL \dp|Mult0~181\ : std_logic;
SIGNAL \dp|Mult0~182\ : std_logic;
SIGNAL \dp|Mult0~183\ : std_logic;
SIGNAL \dp|Mult0~184\ : std_logic;
SIGNAL \dp|Mult0~185\ : std_logic;
SIGNAL \dp|Mult0~186\ : std_logic;
SIGNAL \dp|Mult0~187\ : std_logic;
SIGNAL \dp|Mult0~188\ : std_logic;
SIGNAL \dp|Mult0~189\ : std_logic;
SIGNAL \dp|Mult0~190\ : std_logic;
SIGNAL \dp|Mult0~191\ : std_logic;
SIGNAL \dp|Mult0~192\ : std_logic;
SIGNAL \dp|Mult0~193\ : std_logic;
SIGNAL \dp|Mult0~194\ : std_logic;
SIGNAL \dp|Mult0~195\ : std_logic;
SIGNAL \dp|Mult0~196\ : std_logic;
SIGNAL \dp|Mult0~197\ : std_logic;
SIGNAL \dp|Mult0~198\ : std_logic;
SIGNAL \dp|Mult0~199\ : std_logic;
SIGNAL \dp|Mult0~509\ : std_logic;
SIGNAL \dp|Mult0~510\ : std_logic;
SIGNAL \dp|Mult0~511\ : std_logic;
SIGNAL \dp|Mult0~512\ : std_logic;
SIGNAL \dp|Mult0~513\ : std_logic;
SIGNAL \dp|Mult0~514\ : std_logic;
SIGNAL \dp|Mult0~515\ : std_logic;
SIGNAL \dp|Mult0~516\ : std_logic;
SIGNAL \dp|Mult0~517\ : std_logic;
SIGNAL \dp|Mult0~518\ : std_logic;
SIGNAL \dp|Mult0~519\ : std_logic;
SIGNAL \dp|Mult0~520\ : std_logic;
SIGNAL \dp|Mult0~521\ : std_logic;
SIGNAL \dp|Mult0~522\ : std_logic;
SIGNAL \dp|Mult0~523\ : std_logic;
SIGNAL \dp|Mult0~524\ : std_logic;
SIGNAL \dp|Mult0~525\ : std_logic;
SIGNAL \dp|Mult0~526\ : std_logic;
SIGNAL \dp|Mult0~527\ : std_logic;
SIGNAL \dp|Mult0~528\ : std_logic;
SIGNAL \dp|Mult0~529\ : std_logic;
SIGNAL \dp|Mult0~530\ : std_logic;
SIGNAL \dp|Mult0~531\ : std_logic;
SIGNAL \dp|Mult0~532\ : std_logic;
SIGNAL \dp|Mult0~533\ : std_logic;
SIGNAL \dp|Mult0~534\ : std_logic;
SIGNAL \dp|Mult0~535\ : std_logic;
SIGNAL \dp|Mult0~536\ : std_logic;
SIGNAL \dp|Mult0~537\ : std_logic;
SIGNAL \dp|Mult0~538\ : std_logic;
SIGNAL \dp|Mult0~539\ : std_logic;
SIGNAL \dp|Mult0~540\ : std_logic;
SIGNAL \dp|Mult1~52\ : std_logic;
SIGNAL \dp|Mult1~53\ : std_logic;
SIGNAL \dp|Mult1~54\ : std_logic;
SIGNAL \dp|Mult1~55\ : std_logic;
SIGNAL \dp|Mult1~56\ : std_logic;
SIGNAL \dp|Mult1~57\ : std_logic;
SIGNAL \dp|Mult1~58\ : std_logic;
SIGNAL \dp|Mult1~59\ : std_logic;
SIGNAL \dp|Mult1~60\ : std_logic;
SIGNAL \dp|Mult1~61\ : std_logic;
SIGNAL \dp|Mult1~62\ : std_logic;
SIGNAL \dp|Mult1~63\ : std_logic;
SIGNAL \dp|Mult1~64\ : std_logic;
SIGNAL \dp|Mult1~65\ : std_logic;
SIGNAL \dp|Mult1~66\ : std_logic;
SIGNAL \dp|Mult1~67\ : std_logic;
SIGNAL \dp|Mult1~68\ : std_logic;
SIGNAL \dp|Mult1~69\ : std_logic;
SIGNAL \dp|Mult1~70\ : std_logic;
SIGNAL \dp|Mult1~71\ : std_logic;
SIGNAL \dp|Mult1~72\ : std_logic;
SIGNAL \dp|Mult1~73\ : std_logic;
SIGNAL \dp|Mult1~74\ : std_logic;
SIGNAL \dp|Mult1~75\ : std_logic;
SIGNAL \dp|Mult1~76\ : std_logic;
SIGNAL \dp|Mult1~77\ : std_logic;
SIGNAL \dp|Mult1~78\ : std_logic;
SIGNAL \dp|Mult1~79\ : std_logic;
SIGNAL \dp|Mult1~80\ : std_logic;
SIGNAL \dp|Mult1~81\ : std_logic;
SIGNAL \dp|Mult1~82\ : std_logic;
SIGNAL \dp|Mult1~83\ : std_logic;
SIGNAL \dp|Mult1~419\ : std_logic;
SIGNAL \dp|Mult1~420\ : std_logic;
SIGNAL \dp|Mult1~421\ : std_logic;
SIGNAL \dp|Mult1~422\ : std_logic;
SIGNAL \dp|Mult1~423\ : std_logic;
SIGNAL \dp|Mult1~424\ : std_logic;
SIGNAL \dp|Mult1~425\ : std_logic;
SIGNAL \dp|Mult1~426\ : std_logic;
SIGNAL \dp|Mult1~427\ : std_logic;
SIGNAL \dp|Mult1~428\ : std_logic;
SIGNAL \dp|Mult1~429\ : std_logic;
SIGNAL \dp|Mult1~430\ : std_logic;
SIGNAL \dp|Mult1~431\ : std_logic;
SIGNAL \dp|Mult1~432\ : std_logic;
SIGNAL \dp|Mult1~433\ : std_logic;
SIGNAL \dp|Mult1~434\ : std_logic;
SIGNAL \dp|Mult1~435\ : std_logic;
SIGNAL \dp|Mult1~436\ : std_logic;
SIGNAL \dp|Mult1~437\ : std_logic;
SIGNAL \dp|Mult1~438\ : std_logic;
SIGNAL \dp|Mult1~439\ : std_logic;
SIGNAL \dp|Mult1~440\ : std_logic;
SIGNAL \dp|Mult1~441\ : std_logic;
SIGNAL \dp|Mult1~442\ : std_logic;
SIGNAL \dp|Mult1~443\ : std_logic;
SIGNAL \dp|Mult1~444\ : std_logic;
SIGNAL \dp|Mult1~445\ : std_logic;
SIGNAL \dp|Mult1~446\ : std_logic;
SIGNAL \dp|Mult1~447\ : std_logic;
SIGNAL \dp|Mult1~448\ : std_logic;
SIGNAL \dp|Mult1~449\ : std_logic;
SIGNAL \dp|Mult1~450\ : std_logic;
SIGNAL \dp|Mult1~451\ : std_logic;
SIGNAL \dp|Mult1~452\ : std_logic;
SIGNAL \dp|Mult1~453\ : std_logic;
SIGNAL \dp|Mult1~454\ : std_logic;
SIGNAL \dp|Mult1~455\ : std_logic;
SIGNAL \dp|Mult1~456\ : std_logic;
SIGNAL \dp|Mult1~457\ : std_logic;
SIGNAL \dp|Mult1~458\ : std_logic;
SIGNAL \dp|Mult1~459\ : std_logic;
SIGNAL \dp|Mult1~460\ : std_logic;
SIGNAL \dp|Mult1~461\ : std_logic;
SIGNAL \dp|Mult1~462\ : std_logic;
SIGNAL \dp|Mult1~463\ : std_logic;
SIGNAL \dp|Mult1~464\ : std_logic;
SIGNAL \dp|Mult1~465\ : std_logic;
SIGNAL \dp|Mult1~466\ : std_logic;
SIGNAL \dp|Mult1~467\ : std_logic;
SIGNAL \dp|Mult1~468\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \sm|current_state[2]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|Decoder0~0_combout\ : std_logic;
SIGNAL \dp|yf[4]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|initx~0_combout\ : std_logic;
SIGNAL \dp|Add2~17_sumout\ : std_logic;
SIGNAL \dp|yf~6_combout\ : std_logic;
SIGNAL \dp|Add2~18\ : std_logic;
SIGNAL \dp|Add2~21_sumout\ : std_logic;
SIGNAL \dp|yf~7_combout\ : std_logic;
SIGNAL \dp|Add2~22\ : std_logic;
SIGNAL \dp|Add2~25_sumout\ : std_logic;
SIGNAL \dp|yf~8_combout\ : std_logic;
SIGNAL \dp|yf[2]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add2~26\ : std_logic;
SIGNAL \dp|Add2~13_sumout\ : std_logic;
SIGNAL \dp|yf~5_combout\ : std_logic;
SIGNAL \dp|Add2~14\ : std_logic;
SIGNAL \dp|Add2~9_sumout\ : std_logic;
SIGNAL \dp|yf~4_combout\ : std_logic;
SIGNAL \dp|yf~3_combout\ : std_logic;
SIGNAL \dp|Add2~10\ : std_logic;
SIGNAL \dp|Add2~5_sumout\ : std_logic;
SIGNAL \dp|Equal2~2_combout\ : std_logic;
SIGNAL \dp|yf~0_combout\ : std_logic;
SIGNAL \dp|yf~1_combout\ : std_logic;
SIGNAL \dp|yf~2_combout\ : std_logic;
SIGNAL \dp|Add2~6\ : std_logic;
SIGNAL \dp|Add2~1_sumout\ : std_logic;
SIGNAL \dp|Equal2~1_combout\ : std_logic;
SIGNAL \dp|Equal2~0_combout\ : std_logic;
SIGNAL \dp|Equal2~3_combout\ : std_logic;
SIGNAL \dp|ydone~q\ : std_logic;
SIGNAL \sm|WideOr0~0_combout\ : std_logic;
SIGNAL \dp|Add3~1_sumout\ : std_logic;
SIGNAL \dp|xf~0_combout\ : std_logic;
SIGNAL \dp|xf[0]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add3~2\ : std_logic;
SIGNAL \dp|Add3~5_sumout\ : std_logic;
SIGNAL \dp|xf~1_combout\ : std_logic;
SIGNAL \dp|xf[1]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add3~6\ : std_logic;
SIGNAL \dp|Add3~9_sumout\ : std_logic;
SIGNAL \dp|xf~2_combout\ : std_logic;
SIGNAL \dp|xf[2]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add3~10\ : std_logic;
SIGNAL \dp|Add3~13_sumout\ : std_logic;
SIGNAL \dp|xf~3_combout\ : std_logic;
SIGNAL \dp|Add3~14\ : std_logic;
SIGNAL \dp|Add3~17_sumout\ : std_logic;
SIGNAL \dp|xf~4_combout\ : std_logic;
SIGNAL \dp|Add3~18\ : std_logic;
SIGNAL \dp|Add3~29_sumout\ : std_logic;
SIGNAL \dp|xf~7_combout\ : std_logic;
SIGNAL \dp|Add3~30\ : std_logic;
SIGNAL \dp|Add3~25_sumout\ : std_logic;
SIGNAL \dp|xf~6_combout\ : std_logic;
SIGNAL \dp|Add3~26\ : std_logic;
SIGNAL \dp|Add3~21_sumout\ : std_logic;
SIGNAL \dp|xf~5_combout\ : std_logic;
SIGNAL \dp|Equal3~0_combout\ : std_logic;
SIGNAL \dp|Equal3~1_combout\ : std_logic;
SIGNAL \dp|Equal3~2_combout\ : std_logic;
SIGNAL \dp|xdone~q\ : std_logic;
SIGNAL \dp|Add4~2_combout\ : std_logic;
SIGNAL \dp|Add4~0_combout\ : std_logic;
SIGNAL \dp|xf~8_combout\ : std_logic;
SIGNAL \dp|xf~10_combout\ : std_logic;
SIGNAL \dp|Add4~3_combout\ : std_logic;
SIGNAL \dp|xf~9_combout\ : std_logic;
SIGNAL \dp|Add4~4_combout\ : std_logic;
SIGNAL \dp|Add4~1_combout\ : std_logic;
SIGNAL \dp|Mult0~506\ : std_logic;
SIGNAL \dp|yf~9_combout\ : std_logic;
SIGNAL \dp|Add5~1_combout\ : std_logic;
SIGNAL \dp|Add5~5_combout\ : std_logic;
SIGNAL \dp|Add5~0_combout\ : std_logic;
SIGNAL \dp|Add5~2_combout\ : std_logic;
SIGNAL \dp|Add5~3_combout\ : std_logic;
SIGNAL \dp|Add5~4_combout\ : std_logic;
SIGNAL \dp|Mult1~416\ : std_logic;
SIGNAL \dp|Mult1~49\ : std_logic;
SIGNAL \dp|Mult1~48\ : std_logic;
SIGNAL \dp|Mult1~415\ : std_logic;
SIGNAL \dp|Mult0~505\ : std_logic;
SIGNAL \dp|Mult1~47\ : std_logic;
SIGNAL \dp|Mult1~414\ : std_logic;
SIGNAL \dp|Mult0~504\ : std_logic;
SIGNAL \dp|Mult0~503\ : std_logic;
SIGNAL \dp|Mult1~413\ : std_logic;
SIGNAL \dp|Mult1~46\ : std_logic;
SIGNAL \dp|Mult1~45\ : std_logic;
SIGNAL \dp|Mult0~502\ : std_logic;
SIGNAL \dp|Mult1~412\ : std_logic;
SIGNAL \dp|Mult1~44\ : std_logic;
SIGNAL \dp|Mult1~411\ : std_logic;
SIGNAL \dp|Mult0~501\ : std_logic;
SIGNAL \dp|Mult0~500\ : std_logic;
SIGNAL \dp|Mult1~43\ : std_logic;
SIGNAL \dp|Mult1~410\ : std_logic;
SIGNAL \dp|Mult0~499\ : std_logic;
SIGNAL \dp|Mult1~42\ : std_logic;
SIGNAL \dp|Mult1~409\ : std_logic;
SIGNAL \dp|Mult1~41\ : std_logic;
SIGNAL \dp|Mult1~408\ : std_logic;
SIGNAL \dp|Mult0~498\ : std_logic;
SIGNAL \dp|Mult1~40\ : std_logic;
SIGNAL \dp|Mult0~497\ : std_logic;
SIGNAL \dp|Mult1~407\ : std_logic;
SIGNAL \dp|Mult0~496\ : std_logic;
SIGNAL \dp|Mult1~39\ : std_logic;
SIGNAL \dp|Mult1~406\ : std_logic;
SIGNAL \dp|Mult0~495\ : std_logic;
SIGNAL \dp|Mult1~405_resulta\ : std_logic;
SIGNAL \dp|Mult1~38\ : std_logic;
SIGNAL \dp|Mult1~383\ : std_logic;
SIGNAL \dp|Mult1~384\ : std_logic;
SIGNAL \dp|Mult1~367\ : std_logic;
SIGNAL \dp|Mult1~368\ : std_logic;
SIGNAL \dp|Mult1~355\ : std_logic;
SIGNAL \dp|Mult1~356\ : std_logic;
SIGNAL \dp|Mult1~359\ : std_logic;
SIGNAL \dp|Mult1~360\ : std_logic;
SIGNAL \dp|Mult1~363\ : std_logic;
SIGNAL \dp|Mult1~364\ : std_logic;
SIGNAL \dp|Mult1~387\ : std_logic;
SIGNAL \dp|Mult1~388\ : std_logic;
SIGNAL \dp|Mult1~371\ : std_logic;
SIGNAL \dp|Mult1~372\ : std_logic;
SIGNAL \dp|Mult1~375\ : std_logic;
SIGNAL \dp|Mult1~376\ : std_logic;
SIGNAL \dp|Mult1~379\ : std_logic;
SIGNAL \dp|Mult1~380\ : std_logic;
SIGNAL \dp|Mult1~391\ : std_logic;
SIGNAL \dp|Mult1~392\ : std_logic;
SIGNAL \dp|Mult1~395\ : std_logic;
SIGNAL \dp|Mult1~396\ : std_logic;
SIGNAL \dp|Mult1~1_sumout\ : std_logic;
SIGNAL \dp|Mult0~147\ : std_logic;
SIGNAL \dp|Mult1~394_sumout\ : std_logic;
SIGNAL \dp|Mult0~146\ : std_logic;
SIGNAL \dp|Mult1~390_sumout\ : std_logic;
SIGNAL \dp|Mult0~145\ : std_logic;
SIGNAL \dp|Mult0~144\ : std_logic;
SIGNAL \dp|Mult1~378_sumout\ : std_logic;
SIGNAL \dp|Mult1~374_sumout\ : std_logic;
SIGNAL \dp|Mult0~143\ : std_logic;
SIGNAL \dp|Mult0~142\ : std_logic;
SIGNAL \dp|Mult1~370_sumout\ : std_logic;
SIGNAL \dp|Mult0~141\ : std_logic;
SIGNAL \dp|Mult1~386_sumout\ : std_logic;
SIGNAL \dp|Mult1~362_sumout\ : std_logic;
SIGNAL \dp|Mult0~140\ : std_logic;
SIGNAL \dp|Mult0~139\ : std_logic;
SIGNAL \dp|Mult1~358_sumout\ : std_logic;
SIGNAL \dp|Mult1~354_sumout\ : std_logic;
SIGNAL \dp|Mult0~138\ : std_logic;
SIGNAL \dp|Mult1~366_sumout\ : std_logic;
SIGNAL \dp|Mult0~137\ : std_logic;
SIGNAL \dp|Mult1~382_sumout\ : std_logic;
SIGNAL \dp|Mult0~136_resulta\ : std_logic;
SIGNAL \dp|Mult1~37\ : std_logic;
SIGNAL \dp|Mult0~494\ : std_logic;
SIGNAL \dp|Mult1~36\ : std_logic;
SIGNAL \dp|Mult0~493\ : std_logic;
SIGNAL \dp|Mult1~35\ : std_logic;
SIGNAL \dp|Mult0~492\ : std_logic;
SIGNAL \dp|Mult0~491\ : std_logic;
SIGNAL \dp|Mult1~34\ : std_logic;
SIGNAL \dp|Mult0~490\ : std_logic;
SIGNAL \dp|Mult1~33\ : std_logic;
SIGNAL \dp|Mult0~489\ : std_logic;
SIGNAL \dp|Mult1~32\ : std_logic;
SIGNAL \dp|Mult0~488\ : std_logic;
SIGNAL \dp|Mult1~31\ : std_logic;
SIGNAL \dp|Mult1~30\ : std_logic;
SIGNAL \dp|Mult0~487\ : std_logic;
SIGNAL \dp|Mult1~29\ : std_logic;
SIGNAL \dp|Mult0~486\ : std_logic;
SIGNAL \dp|Mult1~28\ : std_logic;
SIGNAL \dp|Mult0~485\ : std_logic;
SIGNAL \dp|Mult1~27\ : std_logic;
SIGNAL \dp|Mult0~484\ : std_logic;
SIGNAL \dp|Mult1~26\ : std_logic;
SIGNAL \dp|Mult0~483\ : std_logic;
SIGNAL \dp|Mult1~25\ : std_logic;
SIGNAL \dp|Mult0~482\ : std_logic;
SIGNAL \dp|Mult1~24\ : std_logic;
SIGNAL \dp|Mult0~481\ : std_logic;
SIGNAL \dp|Mult1~23\ : std_logic;
SIGNAL \dp|Mult0~480\ : std_logic;
SIGNAL \dp|Mult1~22\ : std_logic;
SIGNAL \dp|Mult0~479\ : std_logic;
SIGNAL \dp|Mult0~478\ : std_logic;
SIGNAL \dp|Mult1~21\ : std_logic;
SIGNAL \dp|Mult0~477_resulta\ : std_logic;
SIGNAL \dp|Mult1~20_resulta\ : std_logic;
SIGNAL \dp|Mult0~66\ : std_logic;
SIGNAL \dp|Mult0~70\ : std_logic;
SIGNAL \dp|Mult0~62\ : std_logic;
SIGNAL \dp|Mult0~50\ : std_logic;
SIGNAL \dp|Mult0~54\ : std_logic;
SIGNAL \dp|Mult0~58\ : std_logic;
SIGNAL \dp|Mult0~42\ : std_logic;
SIGNAL \dp|Mult0~30\ : std_logic;
SIGNAL \dp|Mult0~34\ : std_logic;
SIGNAL \dp|Mult0~38\ : std_logic;
SIGNAL \dp|Mult0~90\ : std_logic;
SIGNAL \dp|Mult0~94\ : std_logic;
SIGNAL \dp|Mult0~98\ : std_logic;
SIGNAL \dp|Mult0~102\ : std_logic;
SIGNAL \dp|Mult0~106\ : std_logic;
SIGNAL \dp|Mult0~46\ : std_logic;
SIGNAL \dp|Mult0~14\ : std_logic;
SIGNAL \dp|Mult0~110\ : std_logic;
SIGNAL \dp|Mult0~114\ : std_logic;
SIGNAL \dp|Mult0~74\ : std_logic;
SIGNAL \dp|Mult0~18\ : std_logic;
SIGNAL \dp|Mult0~22\ : std_logic;
SIGNAL \dp|Mult0~26\ : std_logic;
SIGNAL \dp|Mult0~118\ : std_logic;
SIGNAL \dp|Mult0~78\ : std_logic;
SIGNAL \dp|Mult0~82\ : std_logic;
SIGNAL \dp|Mult0~86\ : std_logic;
SIGNAL \dp|Mult0~122\ : std_logic;
SIGNAL \dp|Mult0~126\ : std_logic;
SIGNAL \dp|Mult0~1_sumout\ : std_logic;
SIGNAL \dp|Mult0~148\ : std_logic;
SIGNAL \dp|Mult1~50\ : std_logic;
SIGNAL \dp|Mult1~417\ : std_logic;
SIGNAL \dp|Mult0~507\ : std_logic;
SIGNAL \dp|Mult1~2\ : std_logic;
SIGNAL \dp|Mult1~3\ : std_logic;
SIGNAL \dp|Mult1~5_sumout\ : std_logic;
SIGNAL \dp|Mult0~2\ : std_logic;
SIGNAL \dp|Mult0~5_sumout\ : std_logic;
SIGNAL \dp|Mult0~149\ : std_logic;
SIGNAL \dp|Mult1~51\ : std_logic;
SIGNAL \dp|Mult0~508\ : std_logic;
SIGNAL \dp|Mult1~418\ : std_logic;
SIGNAL \dp|Mult1~6\ : std_logic;
SIGNAL \dp|Mult1~7\ : std_logic;
SIGNAL \dp|Mult1~9_sumout\ : std_logic;
SIGNAL \dp|Mult0~6\ : std_logic;
SIGNAL \dp|Mult0~9_sumout\ : std_logic;
SIGNAL \dp|Mult0~21_sumout\ : std_logic;
SIGNAL \dp|Mult0~17_sumout\ : std_logic;
SIGNAL \dp|Mult0~25_sumout\ : std_logic;
SIGNAL \dp|Mult0~37_sumout\ : std_logic;
SIGNAL \dp|Mult0~29_sumout\ : std_logic;
SIGNAL \dp|Mult0~33_sumout\ : std_logic;
SIGNAL \dp|Mult0~49_sumout\ : std_logic;
SIGNAL \dp|Mult0~69_sumout\ : std_logic;
SIGNAL \dp|Mult0~65_sumout\ : std_logic;
SIGNAL \dp|Mult0~61_sumout\ : std_logic;
SIGNAL \dp|Mult0~57_sumout\ : std_logic;
SIGNAL \dp|Mult0~53_sumout\ : std_logic;
SIGNAL \dp|LessThan0~0_combout\ : std_logic;
SIGNAL \dp|Mult0~41_sumout\ : std_logic;
SIGNAL \dp|Mult0~45_sumout\ : std_logic;
SIGNAL \dp|cond~0_combout\ : std_logic;
SIGNAL \dp|Mult0~13_sumout\ : std_logic;
SIGNAL \dp|cond~1_combout\ : std_logic;
SIGNAL \dp|Mult0~109_sumout\ : std_logic;
SIGNAL \dp|Mult0~121_sumout\ : std_logic;
SIGNAL \dp|Mult0~117_sumout\ : std_logic;
SIGNAL \dp|Mult0~113_sumout\ : std_logic;
SIGNAL \dp|Mult0~125_sumout\ : std_logic;
SIGNAL \dp|cond~4_combout\ : std_logic;
SIGNAL \dp|Mult0~77_sumout\ : std_logic;
SIGNAL \dp|Mult0~73_sumout\ : std_logic;
SIGNAL \dp|Mult0~81_sumout\ : std_logic;
SIGNAL \dp|Mult0~89_sumout\ : std_logic;
SIGNAL \dp|Mult0~105_sumout\ : std_logic;
SIGNAL \dp|Mult0~101_sumout\ : std_logic;
SIGNAL \dp|Mult0~93_sumout\ : std_logic;
SIGNAL \dp|Mult0~97_sumout\ : std_logic;
SIGNAL \dp|cond~2_combout\ : std_logic;
SIGNAL \dp|Mult0~85_sumout\ : std_logic;
SIGNAL \dp|cond~3_combout\ : std_logic;
SIGNAL \dp|cond~5_combout\ : std_logic;
SIGNAL \dp|cond~q\ : std_logic;
SIGNAL \sm|current_state[3]~0_combout\ : std_logic;
SIGNAL \sm|Mux1~0_combout\ : std_logic;
SIGNAL \sm|current_state[3]~1_combout\ : std_logic;
SIGNAL \sm|current_state[3]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|Mux2~0_combout\ : std_logic;
SIGNAL \sm|Decoder0~1_combout\ : std_logic;
SIGNAL \dp|yp[4]~5_combout\ : std_logic;
SIGNAL \dp|Add0~21_sumout\ : std_logic;
SIGNAL \dp|Add0~22\ : std_logic;
SIGNAL \dp|Add0~25_sumout\ : std_logic;
SIGNAL \sm|initxp~0_combout\ : std_logic;
SIGNAL \dp|Equal0~0_combout\ : std_logic;
SIGNAL \dp|Add0~26\ : std_logic;
SIGNAL \dp|Add0~17_sumout\ : std_logic;
SIGNAL \dp|yp~4_combout\ : std_logic;
SIGNAL \dp|Add0~18\ : std_logic;
SIGNAL \dp|Add0~13_sumout\ : std_logic;
SIGNAL \dp|Add0~14\ : std_logic;
SIGNAL \dp|Add0~9_sumout\ : std_logic;
SIGNAL \dp|Add0~10\ : std_logic;
SIGNAL \dp|Add0~5_sumout\ : std_logic;
SIGNAL \dp|Add0~6\ : std_logic;
SIGNAL \dp|Add0~1_sumout\ : std_logic;
SIGNAL \dp|yp~0_combout\ : std_logic;
SIGNAL \dp|yp~2_combout\ : std_logic;
SIGNAL \dp|yp~1_combout\ : std_logic;
SIGNAL \dp|yp~3_combout\ : std_logic;
SIGNAL \dp|Equal0~1_combout\ : std_logic;
SIGNAL \dp|ypdone~q\ : std_logic;
SIGNAL \sm|WideOr1~0_combout\ : std_logic;
SIGNAL \dp|Add1~1_sumout\ : std_logic;
SIGNAL \dp|xp~6_combout\ : std_logic;
SIGNAL \dp|Add1~2\ : std_logic;
SIGNAL \dp|Add1~5_sumout\ : std_logic;
SIGNAL \dp|Add1~6\ : std_logic;
SIGNAL \dp|Add1~9_sumout\ : std_logic;
SIGNAL \dp|Add1~10\ : std_logic;
SIGNAL \dp|Add1~13_sumout\ : std_logic;
SIGNAL \dp|Add1~14\ : std_logic;
SIGNAL \dp|Add1~17_sumout\ : std_logic;
SIGNAL \dp|Add1~18\ : std_logic;
SIGNAL \dp|Add1~21_sumout\ : std_logic;
SIGNAL \dp|xp~5_combout\ : std_logic;
SIGNAL \dp|xp~3_combout\ : std_logic;
SIGNAL \dp|Equal1~0_combout\ : std_logic;
SIGNAL \dp|xp~4_combout\ : std_logic;
SIGNAL \dp|Add1~22\ : std_logic;
SIGNAL \dp|Add1~29_sumout\ : std_logic;
SIGNAL \dp|Add1~30\ : std_logic;
SIGNAL \dp|Add1~25_sumout\ : std_logic;
SIGNAL \dp|Equal1~1_combout\ : std_logic;
SIGNAL \dp|xp~2_combout\ : std_logic;
SIGNAL \dp|Equal1~2_combout\ : std_logic;
SIGNAL \dp|xpdone~q\ : std_logic;
SIGNAL \sm|Mux2~1_combout\ : std_logic;
SIGNAL \sm|Mux2~2_combout\ : std_logic;
SIGNAL \sm|current_state[1]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|Mux3~0_combout\ : std_logic;
SIGNAL \sm|Mux3~1_combout\ : std_logic;
SIGNAL \sm|current_state[0]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|plot~0_combout\ : std_logic;
SIGNAL \dp|Equal4~0_combout\ : std_logic;
SIGNAL \dp|yp~7_combout\ : std_logic;
SIGNAL \dp|yp~6_combout\ : std_logic;
SIGNAL \dp|xp~7_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~6\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~7\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|writeEn~0_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \vga_u0|controller|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~6\ : std_logic;
SIGNAL \vga_u0|controller|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~38\ : std_logic;
SIGNAL \vga_u0|controller|Add0~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~18\ : std_logic;
SIGNAL \vga_u0|controller|Add0~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~34\ : std_logic;
SIGNAL \vga_u0|controller|Add0~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~30\ : std_logic;
SIGNAL \vga_u0|controller|Add0~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~14\ : std_logic;
SIGNAL \vga_u0|controller|Add0~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~26\ : std_logic;
SIGNAL \vga_u0|controller|Add0~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~22\ : std_logic;
SIGNAL \vga_u0|controller|Add0~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~10\ : std_logic;
SIGNAL \vga_u0|controller|Add0~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~6\ : std_logic;
SIGNAL \vga_u0|controller|Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~2\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~3\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~5_sumout\ : std_logic;
SIGNAL \sm|WideOr2~0_combout\ : std_logic;
SIGNAL \dp|xp~0_combout\ : std_logic;
SIGNAL \dp|xp~1_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|LessThan7~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \vga_u0|controller|VGA_R[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_G[0]~0_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_B[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS~feeder_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK~feeder_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK~q\ : std_logic;
SIGNAL \dp|x\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \vga_u0|controller|yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|yp\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \sm|current_state\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|xp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|xf\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|yf\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_yp\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add2~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_y\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ : std_logic;
SIGNAL \dp|ALT_INV_yf[2]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_xf[2]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_xf[1]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_xf[0]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_yf[4]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[3]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[2]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[1]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[0]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_yf~9_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~10_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~9_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~8_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yf~8_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Decoder0~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cond~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal3~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal3~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|ALT_INV_Equal3~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~6_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~5_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cond~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cond~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cond~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cond~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal2~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal2~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal2~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yf\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal1~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~5_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xf~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~418\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~417\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~416\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~415\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~414\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~413\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~412\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~411\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~410\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~409\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~408\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~407\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~406\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~405_resulta\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~394_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~390_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~386_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~382_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~378_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~374_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~370_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~366_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~362_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~358_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~354_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~51\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~50\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~49\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~48\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~47\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~46\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~45\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~44\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~43\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~42\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~41\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~40\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~39\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~38\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~37\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~36\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~35\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~34\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~33\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~32\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~31\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~30\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~29\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~28\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~27\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~26\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~25\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~24\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~23\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~22\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~21\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~20_resulta\ : std_logic;
SIGNAL \sm|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_WideOr1~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_initxp~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yf~3_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Decoder0~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_initx~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yf~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yf~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[3]~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Mux2~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ypdone~q\ : std_logic;
SIGNAL \sm|ALT_INV_Mux2~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Mux3~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xdone~q\ : std_logic;
SIGNAL \dp|ALT_INV_ydone~q\ : std_logic;
SIGNAL \dp|ALT_INV_cond~q\ : std_logic;
SIGNAL \dp|ALT_INV_xpdone~q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_BLANK1~q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_HS1~q\ : std_logic;
SIGNAL \vga_u0|ALT_INV_writeEn~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_plot~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_current_state\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~508\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~507\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~506\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~505\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~504\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~503\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~502\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~501\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~500\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~499\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~498\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~497\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~496\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~495\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~494\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~493\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~492\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~491\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~490\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~489\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~488\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~487\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~486\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~485\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~484\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~483\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~482\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~481\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~480\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~479\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~478\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~477_resulta\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~149\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~148\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~147\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~146\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~145\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~144\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~143\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~142\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~141\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~140\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~139\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~138\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~137\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~136_resulta\ : std_logic;
SIGNAL \dp|ALT_INV_Mult1~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~125_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~121_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~117_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~113_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~109_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~105_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~101_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~97_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~93_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~89_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~85_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~81_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~77_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~73_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~69_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~65_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~61_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~57_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~53_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~49_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~45_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~41_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~37_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~33_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Mult0~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_xp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_x\ : std_logic_vector(6 DOWNTO 5);
SIGNAL \dp|ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add2~9_sumout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
VGA_R <= ww_VGA_R;
VGA_G <= ww_VGA_G;
VGA_B <= ww_VGA_B;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_BLANK <= ww_VGA_BLANK;
VGA_SYNC <= ww_VGA_SYNC;
VGA_CLK <= ww_VGA_CLK;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\(0) <= \sm|WideOr2~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter[3]~DUPLICATE_q\ & 
\vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\(0) <= \sm|WideOr2~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ <= (\sm|WideOr2~0_combout\ & \sm|WideOr2~0_combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & \dp|x\(3) & \dp|x\(2) & \dp|x\(1) & 
\dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter[3]~DUPLICATE_q\ & \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(0);
\vga_u0|VideoMemory|auto_generated|ram_block1a8\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\(0) <= \sm|WideOr2~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\(0) <= \sm|WideOr2~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ <= (gnd & \~GND~combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & \dp|x\(3) & \dp|x\(2) & \dp|x\(1) & 
\dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter[3]~DUPLICATE_q\ & \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter[3]~DUPLICATE_q\ & 
\vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter[6]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);

\dp|Mult0~136_AX_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\);

\dp|Mult0~136_AY_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~4_combout\ & \dp|Add4~3_combout\ & \dp|Add4~2_combout\ & NOT \dp|xf~4_combout\ & \dp|xf~3_combout\ & \dp|xf~2_combout\ & \dp|xf~1_combout\ & \dp|xf~0_combout\);

\dp|Mult0~136_BX_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\);

\dp|Mult0~136_BY_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~4_combout\ & \dp|Add4~3_combout\ & \dp|Add4~2_combout\ & NOT \dp|xf~4_combout\ & \dp|xf~3_combout\ & \dp|xf~2_combout\ & \dp|xf~1_combout\ & \dp|xf~0_combout\);

\dp|Mult0~136_resulta\ <= \dp|Mult0~136_RESULTA_bus\(0);
\dp|Mult0~137\ <= \dp|Mult0~136_RESULTA_bus\(1);
\dp|Mult0~138\ <= \dp|Mult0~136_RESULTA_bus\(2);
\dp|Mult0~139\ <= \dp|Mult0~136_RESULTA_bus\(3);
\dp|Mult0~140\ <= \dp|Mult0~136_RESULTA_bus\(4);
\dp|Mult0~141\ <= \dp|Mult0~136_RESULTA_bus\(5);
\dp|Mult0~142\ <= \dp|Mult0~136_RESULTA_bus\(6);
\dp|Mult0~143\ <= \dp|Mult0~136_RESULTA_bus\(7);
\dp|Mult0~144\ <= \dp|Mult0~136_RESULTA_bus\(8);
\dp|Mult0~145\ <= \dp|Mult0~136_RESULTA_bus\(9);
\dp|Mult0~146\ <= \dp|Mult0~136_RESULTA_bus\(10);
\dp|Mult0~147\ <= \dp|Mult0~136_RESULTA_bus\(11);
\dp|Mult0~148\ <= \dp|Mult0~136_RESULTA_bus\(12);
\dp|Mult0~149\ <= \dp|Mult0~136_RESULTA_bus\(13);
\dp|Mult0~150\ <= \dp|Mult0~136_RESULTA_bus\(14);
\dp|Mult0~151\ <= \dp|Mult0~136_RESULTA_bus\(15);
\dp|Mult0~152\ <= \dp|Mult0~136_RESULTA_bus\(16);
\dp|Mult0~153\ <= \dp|Mult0~136_RESULTA_bus\(17);
\dp|Mult0~154\ <= \dp|Mult0~136_RESULTA_bus\(18);
\dp|Mult0~155\ <= \dp|Mult0~136_RESULTA_bus\(19);
\dp|Mult0~156\ <= \dp|Mult0~136_RESULTA_bus\(20);
\dp|Mult0~157\ <= \dp|Mult0~136_RESULTA_bus\(21);
\dp|Mult0~158\ <= \dp|Mult0~136_RESULTA_bus\(22);
\dp|Mult0~159\ <= \dp|Mult0~136_RESULTA_bus\(23);
\dp|Mult0~160\ <= \dp|Mult0~136_RESULTA_bus\(24);
\dp|Mult0~161\ <= \dp|Mult0~136_RESULTA_bus\(25);
\dp|Mult0~162\ <= \dp|Mult0~136_RESULTA_bus\(26);
\dp|Mult0~163\ <= \dp|Mult0~136_RESULTA_bus\(27);
\dp|Mult0~164\ <= \dp|Mult0~136_RESULTA_bus\(28);
\dp|Mult0~165\ <= \dp|Mult0~136_RESULTA_bus\(29);
\dp|Mult0~166\ <= \dp|Mult0~136_RESULTA_bus\(30);
\dp|Mult0~167\ <= \dp|Mult0~136_RESULTA_bus\(31);
\dp|Mult0~168\ <= \dp|Mult0~136_RESULTA_bus\(32);
\dp|Mult0~169\ <= \dp|Mult0~136_RESULTA_bus\(33);
\dp|Mult0~170\ <= \dp|Mult0~136_RESULTA_bus\(34);
\dp|Mult0~171\ <= \dp|Mult0~136_RESULTA_bus\(35);
\dp|Mult0~172\ <= \dp|Mult0~136_RESULTA_bus\(36);
\dp|Mult0~173\ <= \dp|Mult0~136_RESULTA_bus\(37);
\dp|Mult0~174\ <= \dp|Mult0~136_RESULTA_bus\(38);
\dp|Mult0~175\ <= \dp|Mult0~136_RESULTA_bus\(39);
\dp|Mult0~176\ <= \dp|Mult0~136_RESULTA_bus\(40);
\dp|Mult0~177\ <= \dp|Mult0~136_RESULTA_bus\(41);
\dp|Mult0~178\ <= \dp|Mult0~136_RESULTA_bus\(42);
\dp|Mult0~179\ <= \dp|Mult0~136_RESULTA_bus\(43);
\dp|Mult0~180\ <= \dp|Mult0~136_RESULTA_bus\(44);
\dp|Mult0~181\ <= \dp|Mult0~136_RESULTA_bus\(45);
\dp|Mult0~182\ <= \dp|Mult0~136_RESULTA_bus\(46);
\dp|Mult0~183\ <= \dp|Mult0~136_RESULTA_bus\(47);
\dp|Mult0~184\ <= \dp|Mult0~136_RESULTA_bus\(48);
\dp|Mult0~185\ <= \dp|Mult0~136_RESULTA_bus\(49);
\dp|Mult0~186\ <= \dp|Mult0~136_RESULTA_bus\(50);
\dp|Mult0~187\ <= \dp|Mult0~136_RESULTA_bus\(51);
\dp|Mult0~188\ <= \dp|Mult0~136_RESULTA_bus\(52);
\dp|Mult0~189\ <= \dp|Mult0~136_RESULTA_bus\(53);
\dp|Mult0~190\ <= \dp|Mult0~136_RESULTA_bus\(54);
\dp|Mult0~191\ <= \dp|Mult0~136_RESULTA_bus\(55);
\dp|Mult0~192\ <= \dp|Mult0~136_RESULTA_bus\(56);
\dp|Mult0~193\ <= \dp|Mult0~136_RESULTA_bus\(57);
\dp|Mult0~194\ <= \dp|Mult0~136_RESULTA_bus\(58);
\dp|Mult0~195\ <= \dp|Mult0~136_RESULTA_bus\(59);
\dp|Mult0~196\ <= \dp|Mult0~136_RESULTA_bus\(60);
\dp|Mult0~197\ <= \dp|Mult0~136_RESULTA_bus\(61);
\dp|Mult0~198\ <= \dp|Mult0~136_RESULTA_bus\(62);
\dp|Mult0~199\ <= \dp|Mult0~136_RESULTA_bus\(63);

\dp|Mult0~477_AX_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~4_combout\ & \dp|Add4~3_combout\ & \dp|Add4~2_combout\ & NOT \dp|xf~4_combout\ & \dp|xf~3_combout\ & \dp|xf~2_combout\ & \dp|xf~1_combout\ & \dp|xf~0_combout\);

\dp|Mult0~477_AY_bus\ <= (\dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & \dp|Add4~1_combout\ & 
\dp|Add4~4_combout\ & \dp|Add4~3_combout\ & \dp|Add4~2_combout\ & NOT \dp|xf~4_combout\ & \dp|xf~3_combout\ & \dp|xf~2_combout\ & \dp|xf~1_combout\ & \dp|xf~0_combout\);

\dp|Mult0~477_resulta\ <= \dp|Mult0~477_RESULTA_bus\(0);
\dp|Mult0~478\ <= \dp|Mult0~477_RESULTA_bus\(1);
\dp|Mult0~479\ <= \dp|Mult0~477_RESULTA_bus\(2);
\dp|Mult0~480\ <= \dp|Mult0~477_RESULTA_bus\(3);
\dp|Mult0~481\ <= \dp|Mult0~477_RESULTA_bus\(4);
\dp|Mult0~482\ <= \dp|Mult0~477_RESULTA_bus\(5);
\dp|Mult0~483\ <= \dp|Mult0~477_RESULTA_bus\(6);
\dp|Mult0~484\ <= \dp|Mult0~477_RESULTA_bus\(7);
\dp|Mult0~485\ <= \dp|Mult0~477_RESULTA_bus\(8);
\dp|Mult0~486\ <= \dp|Mult0~477_RESULTA_bus\(9);
\dp|Mult0~487\ <= \dp|Mult0~477_RESULTA_bus\(10);
\dp|Mult0~488\ <= \dp|Mult0~477_RESULTA_bus\(11);
\dp|Mult0~489\ <= \dp|Mult0~477_RESULTA_bus\(12);
\dp|Mult0~490\ <= \dp|Mult0~477_RESULTA_bus\(13);
\dp|Mult0~491\ <= \dp|Mult0~477_RESULTA_bus\(14);
\dp|Mult0~492\ <= \dp|Mult0~477_RESULTA_bus\(15);
\dp|Mult0~493\ <= \dp|Mult0~477_RESULTA_bus\(16);
\dp|Mult0~494\ <= \dp|Mult0~477_RESULTA_bus\(17);
\dp|Mult0~495\ <= \dp|Mult0~477_RESULTA_bus\(18);
\dp|Mult0~496\ <= \dp|Mult0~477_RESULTA_bus\(19);
\dp|Mult0~497\ <= \dp|Mult0~477_RESULTA_bus\(20);
\dp|Mult0~498\ <= \dp|Mult0~477_RESULTA_bus\(21);
\dp|Mult0~499\ <= \dp|Mult0~477_RESULTA_bus\(22);
\dp|Mult0~500\ <= \dp|Mult0~477_RESULTA_bus\(23);
\dp|Mult0~501\ <= \dp|Mult0~477_RESULTA_bus\(24);
\dp|Mult0~502\ <= \dp|Mult0~477_RESULTA_bus\(25);
\dp|Mult0~503\ <= \dp|Mult0~477_RESULTA_bus\(26);
\dp|Mult0~504\ <= \dp|Mult0~477_RESULTA_bus\(27);
\dp|Mult0~505\ <= \dp|Mult0~477_RESULTA_bus\(28);
\dp|Mult0~506\ <= \dp|Mult0~477_RESULTA_bus\(29);
\dp|Mult0~507\ <= \dp|Mult0~477_RESULTA_bus\(30);
\dp|Mult0~508\ <= \dp|Mult0~477_RESULTA_bus\(31);
\dp|Mult0~509\ <= \dp|Mult0~477_RESULTA_bus\(32);
\dp|Mult0~510\ <= \dp|Mult0~477_RESULTA_bus\(33);
\dp|Mult0~511\ <= \dp|Mult0~477_RESULTA_bus\(34);
\dp|Mult0~512\ <= \dp|Mult0~477_RESULTA_bus\(35);
\dp|Mult0~513\ <= \dp|Mult0~477_RESULTA_bus\(36);
\dp|Mult0~514\ <= \dp|Mult0~477_RESULTA_bus\(37);
\dp|Mult0~515\ <= \dp|Mult0~477_RESULTA_bus\(38);
\dp|Mult0~516\ <= \dp|Mult0~477_RESULTA_bus\(39);
\dp|Mult0~517\ <= \dp|Mult0~477_RESULTA_bus\(40);
\dp|Mult0~518\ <= \dp|Mult0~477_RESULTA_bus\(41);
\dp|Mult0~519\ <= \dp|Mult0~477_RESULTA_bus\(42);
\dp|Mult0~520\ <= \dp|Mult0~477_RESULTA_bus\(43);
\dp|Mult0~521\ <= \dp|Mult0~477_RESULTA_bus\(44);
\dp|Mult0~522\ <= \dp|Mult0~477_RESULTA_bus\(45);
\dp|Mult0~523\ <= \dp|Mult0~477_RESULTA_bus\(46);
\dp|Mult0~524\ <= \dp|Mult0~477_RESULTA_bus\(47);
\dp|Mult0~525\ <= \dp|Mult0~477_RESULTA_bus\(48);
\dp|Mult0~526\ <= \dp|Mult0~477_RESULTA_bus\(49);
\dp|Mult0~527\ <= \dp|Mult0~477_RESULTA_bus\(50);
\dp|Mult0~528\ <= \dp|Mult0~477_RESULTA_bus\(51);
\dp|Mult0~529\ <= \dp|Mult0~477_RESULTA_bus\(52);
\dp|Mult0~530\ <= \dp|Mult0~477_RESULTA_bus\(53);
\dp|Mult0~531\ <= \dp|Mult0~477_RESULTA_bus\(54);
\dp|Mult0~532\ <= \dp|Mult0~477_RESULTA_bus\(55);
\dp|Mult0~533\ <= \dp|Mult0~477_RESULTA_bus\(56);
\dp|Mult0~534\ <= \dp|Mult0~477_RESULTA_bus\(57);
\dp|Mult0~535\ <= \dp|Mult0~477_RESULTA_bus\(58);
\dp|Mult0~536\ <= \dp|Mult0~477_RESULTA_bus\(59);
\dp|Mult0~537\ <= \dp|Mult0~477_RESULTA_bus\(60);
\dp|Mult0~538\ <= \dp|Mult0~477_RESULTA_bus\(61);
\dp|Mult0~539\ <= \dp|Mult0~477_RESULTA_bus\(62);
\dp|Mult0~540\ <= \dp|Mult0~477_RESULTA_bus\(63);

\dp|Mult1~20_AX_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~4_combout\ & \dp|Add5~3_combout\ & \dp|Add5~2_combout\ & \dp|Add5~0_combout\ & NOT \dp|yf~8_combout\ & \dp|yf~7_combout\ & \dp|yf~6_combout\);

\dp|Mult1~20_AY_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~4_combout\ & \dp|Add5~3_combout\ & \dp|Add5~2_combout\ & \dp|Add5~0_combout\ & NOT \dp|yf~8_combout\ & \dp|yf~7_combout\ & \dp|yf~6_combout\);

\dp|Mult1~20_resulta\ <= \dp|Mult1~20_RESULTA_bus\(0);
\dp|Mult1~21\ <= \dp|Mult1~20_RESULTA_bus\(1);
\dp|Mult1~22\ <= \dp|Mult1~20_RESULTA_bus\(2);
\dp|Mult1~23\ <= \dp|Mult1~20_RESULTA_bus\(3);
\dp|Mult1~24\ <= \dp|Mult1~20_RESULTA_bus\(4);
\dp|Mult1~25\ <= \dp|Mult1~20_RESULTA_bus\(5);
\dp|Mult1~26\ <= \dp|Mult1~20_RESULTA_bus\(6);
\dp|Mult1~27\ <= \dp|Mult1~20_RESULTA_bus\(7);
\dp|Mult1~28\ <= \dp|Mult1~20_RESULTA_bus\(8);
\dp|Mult1~29\ <= \dp|Mult1~20_RESULTA_bus\(9);
\dp|Mult1~30\ <= \dp|Mult1~20_RESULTA_bus\(10);
\dp|Mult1~31\ <= \dp|Mult1~20_RESULTA_bus\(11);
\dp|Mult1~32\ <= \dp|Mult1~20_RESULTA_bus\(12);
\dp|Mult1~33\ <= \dp|Mult1~20_RESULTA_bus\(13);
\dp|Mult1~34\ <= \dp|Mult1~20_RESULTA_bus\(14);
\dp|Mult1~35\ <= \dp|Mult1~20_RESULTA_bus\(15);
\dp|Mult1~36\ <= \dp|Mult1~20_RESULTA_bus\(16);
\dp|Mult1~37\ <= \dp|Mult1~20_RESULTA_bus\(17);
\dp|Mult1~38\ <= \dp|Mult1~20_RESULTA_bus\(18);
\dp|Mult1~39\ <= \dp|Mult1~20_RESULTA_bus\(19);
\dp|Mult1~40\ <= \dp|Mult1~20_RESULTA_bus\(20);
\dp|Mult1~41\ <= \dp|Mult1~20_RESULTA_bus\(21);
\dp|Mult1~42\ <= \dp|Mult1~20_RESULTA_bus\(22);
\dp|Mult1~43\ <= \dp|Mult1~20_RESULTA_bus\(23);
\dp|Mult1~44\ <= \dp|Mult1~20_RESULTA_bus\(24);
\dp|Mult1~45\ <= \dp|Mult1~20_RESULTA_bus\(25);
\dp|Mult1~46\ <= \dp|Mult1~20_RESULTA_bus\(26);
\dp|Mult1~47\ <= \dp|Mult1~20_RESULTA_bus\(27);
\dp|Mult1~48\ <= \dp|Mult1~20_RESULTA_bus\(28);
\dp|Mult1~49\ <= \dp|Mult1~20_RESULTA_bus\(29);
\dp|Mult1~50\ <= \dp|Mult1~20_RESULTA_bus\(30);
\dp|Mult1~51\ <= \dp|Mult1~20_RESULTA_bus\(31);
\dp|Mult1~52\ <= \dp|Mult1~20_RESULTA_bus\(32);
\dp|Mult1~53\ <= \dp|Mult1~20_RESULTA_bus\(33);
\dp|Mult1~54\ <= \dp|Mult1~20_RESULTA_bus\(34);
\dp|Mult1~55\ <= \dp|Mult1~20_RESULTA_bus\(35);
\dp|Mult1~56\ <= \dp|Mult1~20_RESULTA_bus\(36);
\dp|Mult1~57\ <= \dp|Mult1~20_RESULTA_bus\(37);
\dp|Mult1~58\ <= \dp|Mult1~20_RESULTA_bus\(38);
\dp|Mult1~59\ <= \dp|Mult1~20_RESULTA_bus\(39);
\dp|Mult1~60\ <= \dp|Mult1~20_RESULTA_bus\(40);
\dp|Mult1~61\ <= \dp|Mult1~20_RESULTA_bus\(41);
\dp|Mult1~62\ <= \dp|Mult1~20_RESULTA_bus\(42);
\dp|Mult1~63\ <= \dp|Mult1~20_RESULTA_bus\(43);
\dp|Mult1~64\ <= \dp|Mult1~20_RESULTA_bus\(44);
\dp|Mult1~65\ <= \dp|Mult1~20_RESULTA_bus\(45);
\dp|Mult1~66\ <= \dp|Mult1~20_RESULTA_bus\(46);
\dp|Mult1~67\ <= \dp|Mult1~20_RESULTA_bus\(47);
\dp|Mult1~68\ <= \dp|Mult1~20_RESULTA_bus\(48);
\dp|Mult1~69\ <= \dp|Mult1~20_RESULTA_bus\(49);
\dp|Mult1~70\ <= \dp|Mult1~20_RESULTA_bus\(50);
\dp|Mult1~71\ <= \dp|Mult1~20_RESULTA_bus\(51);
\dp|Mult1~72\ <= \dp|Mult1~20_RESULTA_bus\(52);
\dp|Mult1~73\ <= \dp|Mult1~20_RESULTA_bus\(53);
\dp|Mult1~74\ <= \dp|Mult1~20_RESULTA_bus\(54);
\dp|Mult1~75\ <= \dp|Mult1~20_RESULTA_bus\(55);
\dp|Mult1~76\ <= \dp|Mult1~20_RESULTA_bus\(56);
\dp|Mult1~77\ <= \dp|Mult1~20_RESULTA_bus\(57);
\dp|Mult1~78\ <= \dp|Mult1~20_RESULTA_bus\(58);
\dp|Mult1~79\ <= \dp|Mult1~20_RESULTA_bus\(59);
\dp|Mult1~80\ <= \dp|Mult1~20_RESULTA_bus\(60);
\dp|Mult1~81\ <= \dp|Mult1~20_RESULTA_bus\(61);
\dp|Mult1~82\ <= \dp|Mult1~20_RESULTA_bus\(62);
\dp|Mult1~83\ <= \dp|Mult1~20_RESULTA_bus\(63);

\dp|Mult1~405_AX_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\);

\dp|Mult1~405_AY_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~4_combout\ & \dp|Add5~3_combout\ & \dp|Add5~2_combout\ & \dp|Add5~0_combout\ & NOT \dp|yf~8_combout\ & \dp|yf~7_combout\ & \dp|yf~6_combout\);

\dp|Mult1~405_BX_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\);

\dp|Mult1~405_BY_bus\ <= (\dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & \dp|Add5~5_combout\ & 
\dp|Add5~5_combout\ & \dp|Add5~4_combout\ & \dp|Add5~3_combout\ & \dp|Add5~2_combout\ & \dp|Add5~0_combout\ & NOT \dp|yf~8_combout\ & \dp|yf~7_combout\ & \dp|yf~6_combout\);

\dp|Mult1~405_resulta\ <= \dp|Mult1~405_RESULTA_bus\(0);
\dp|Mult1~406\ <= \dp|Mult1~405_RESULTA_bus\(1);
\dp|Mult1~407\ <= \dp|Mult1~405_RESULTA_bus\(2);
\dp|Mult1~408\ <= \dp|Mult1~405_RESULTA_bus\(3);
\dp|Mult1~409\ <= \dp|Mult1~405_RESULTA_bus\(4);
\dp|Mult1~410\ <= \dp|Mult1~405_RESULTA_bus\(5);
\dp|Mult1~411\ <= \dp|Mult1~405_RESULTA_bus\(6);
\dp|Mult1~412\ <= \dp|Mult1~405_RESULTA_bus\(7);
\dp|Mult1~413\ <= \dp|Mult1~405_RESULTA_bus\(8);
\dp|Mult1~414\ <= \dp|Mult1~405_RESULTA_bus\(9);
\dp|Mult1~415\ <= \dp|Mult1~405_RESULTA_bus\(10);
\dp|Mult1~416\ <= \dp|Mult1~405_RESULTA_bus\(11);
\dp|Mult1~417\ <= \dp|Mult1~405_RESULTA_bus\(12);
\dp|Mult1~418\ <= \dp|Mult1~405_RESULTA_bus\(13);
\dp|Mult1~419\ <= \dp|Mult1~405_RESULTA_bus\(14);
\dp|Mult1~420\ <= \dp|Mult1~405_RESULTA_bus\(15);
\dp|Mult1~421\ <= \dp|Mult1~405_RESULTA_bus\(16);
\dp|Mult1~422\ <= \dp|Mult1~405_RESULTA_bus\(17);
\dp|Mult1~423\ <= \dp|Mult1~405_RESULTA_bus\(18);
\dp|Mult1~424\ <= \dp|Mult1~405_RESULTA_bus\(19);
\dp|Mult1~425\ <= \dp|Mult1~405_RESULTA_bus\(20);
\dp|Mult1~426\ <= \dp|Mult1~405_RESULTA_bus\(21);
\dp|Mult1~427\ <= \dp|Mult1~405_RESULTA_bus\(22);
\dp|Mult1~428\ <= \dp|Mult1~405_RESULTA_bus\(23);
\dp|Mult1~429\ <= \dp|Mult1~405_RESULTA_bus\(24);
\dp|Mult1~430\ <= \dp|Mult1~405_RESULTA_bus\(25);
\dp|Mult1~431\ <= \dp|Mult1~405_RESULTA_bus\(26);
\dp|Mult1~432\ <= \dp|Mult1~405_RESULTA_bus\(27);
\dp|Mult1~433\ <= \dp|Mult1~405_RESULTA_bus\(28);
\dp|Mult1~434\ <= \dp|Mult1~405_RESULTA_bus\(29);
\dp|Mult1~435\ <= \dp|Mult1~405_RESULTA_bus\(30);
\dp|Mult1~436\ <= \dp|Mult1~405_RESULTA_bus\(31);
\dp|Mult1~437\ <= \dp|Mult1~405_RESULTA_bus\(32);
\dp|Mult1~438\ <= \dp|Mult1~405_RESULTA_bus\(33);
\dp|Mult1~439\ <= \dp|Mult1~405_RESULTA_bus\(34);
\dp|Mult1~440\ <= \dp|Mult1~405_RESULTA_bus\(35);
\dp|Mult1~441\ <= \dp|Mult1~405_RESULTA_bus\(36);
\dp|Mult1~442\ <= \dp|Mult1~405_RESULTA_bus\(37);
\dp|Mult1~443\ <= \dp|Mult1~405_RESULTA_bus\(38);
\dp|Mult1~444\ <= \dp|Mult1~405_RESULTA_bus\(39);
\dp|Mult1~445\ <= \dp|Mult1~405_RESULTA_bus\(40);
\dp|Mult1~446\ <= \dp|Mult1~405_RESULTA_bus\(41);
\dp|Mult1~447\ <= \dp|Mult1~405_RESULTA_bus\(42);
\dp|Mult1~448\ <= \dp|Mult1~405_RESULTA_bus\(43);
\dp|Mult1~449\ <= \dp|Mult1~405_RESULTA_bus\(44);
\dp|Mult1~450\ <= \dp|Mult1~405_RESULTA_bus\(45);
\dp|Mult1~451\ <= \dp|Mult1~405_RESULTA_bus\(46);
\dp|Mult1~452\ <= \dp|Mult1~405_RESULTA_bus\(47);
\dp|Mult1~453\ <= \dp|Mult1~405_RESULTA_bus\(48);
\dp|Mult1~454\ <= \dp|Mult1~405_RESULTA_bus\(49);
\dp|Mult1~455\ <= \dp|Mult1~405_RESULTA_bus\(50);
\dp|Mult1~456\ <= \dp|Mult1~405_RESULTA_bus\(51);
\dp|Mult1~457\ <= \dp|Mult1~405_RESULTA_bus\(52);
\dp|Mult1~458\ <= \dp|Mult1~405_RESULTA_bus\(53);
\dp|Mult1~459\ <= \dp|Mult1~405_RESULTA_bus\(54);
\dp|Mult1~460\ <= \dp|Mult1~405_RESULTA_bus\(55);
\dp|Mult1~461\ <= \dp|Mult1~405_RESULTA_bus\(56);
\dp|Mult1~462\ <= \dp|Mult1~405_RESULTA_bus\(57);
\dp|Mult1~463\ <= \dp|Mult1~405_RESULTA_bus\(58);
\dp|Mult1~464\ <= \dp|Mult1~405_RESULTA_bus\(59);
\dp|Mult1~465\ <= \dp|Mult1~405_RESULTA_bus\(60);
\dp|Mult1~466\ <= \dp|Mult1~405_RESULTA_bus\(61);
\dp|Mult1~467\ <= \dp|Mult1~405_RESULTA_bus\(62);
\dp|Mult1~468\ <= \dp|Mult1~405_RESULTA_bus\(63);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\(6);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\
& \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\);
\dp|ALT_INV_Add0~9_sumout\ <= NOT \dp|Add0~9_sumout\;
\dp|ALT_INV_yp\(4) <= NOT \dp|yp\(4);
\dp|ALT_INV_Add2~5_sumout\ <= NOT \dp|Add2~5_sumout\;
\dp|ALT_INV_Add0~5_sumout\ <= NOT \dp|Add0~5_sumout\;
\dp|ALT_INV_yp\(5) <= NOT \dp|yp\(5);
\dp|ALT_INV_Add2~1_sumout\ <= NOT \dp|Add2~1_sumout\;
\dp|ALT_INV_Add0~1_sumout\ <= NOT \dp|Add0~1_sumout\;
\dp|ALT_INV_yp\(6) <= NOT \dp|yp\(6);
\vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~5_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~1_sumout\;
\dp|ALT_INV_y\(3) <= NOT \dp|y\(3);
\dp|ALT_INV_y\(4) <= NOT \dp|y\(4);
\dp|ALT_INV_y\(5) <= NOT \dp|y\(5);
\dp|ALT_INV_y\(6) <= NOT \dp|y\(6);
\vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|user_input_translator|Add1~5_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|user_input_translator|Add1~1_sumout\;
\vga_u0|controller|ALT_INV_yCounter\(0) <= NOT \vga_u0|controller|yCounter\(0);
\vga_u0|controller|ALT_INV_yCounter\(1) <= NOT \vga_u0|controller|yCounter\(1);
\vga_u0|controller|ALT_INV_yCounter\(2) <= NOT \vga_u0|controller|yCounter\(2);
\vga_u0|controller|ALT_INV_yCounter\(3) <= NOT \vga_u0|controller|yCounter\(3);
\vga_u0|controller|ALT_INV_yCounter\(4) <= NOT \vga_u0|controller|yCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(0) <= NOT \vga_u0|controller|xCounter\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a8\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\;
\vga_u0|controller|ALT_INV_xCounter\(2) <= NOT \vga_u0|controller|xCounter\(2);
\vga_u0|controller|ALT_INV_xCounter\(3) <= NOT \vga_u0|controller|xCounter\(3);
\vga_u0|controller|ALT_INV_xCounter\(5) <= NOT \vga_u0|controller|xCounter\(5);
\vga_u0|controller|ALT_INV_xCounter\(6) <= NOT \vga_u0|controller|xCounter\(6);
\vga_u0|controller|ALT_INV_xCounter\(1) <= NOT \vga_u0|controller|xCounter\(1);
\vga_u0|controller|ALT_INV_xCounter\(4) <= NOT \vga_u0|controller|xCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(7) <= NOT \vga_u0|controller|xCounter\(7);
\vga_u0|controller|ALT_INV_xCounter\(8) <= NOT \vga_u0|controller|xCounter\(8);
\vga_u0|controller|ALT_INV_xCounter\(9) <= NOT \vga_u0|controller|xCounter\(9);
\vga_u0|controller|ALT_INV_yCounter\(5) <= NOT \vga_u0|controller|yCounter\(5);
\vga_u0|controller|ALT_INV_yCounter\(6) <= NOT \vga_u0|controller|yCounter\(6);
\vga_u0|controller|ALT_INV_yCounter\(7) <= NOT \vga_u0|controller|yCounter\(7);
\vga_u0|controller|ALT_INV_yCounter\(8) <= NOT \vga_u0|controller|yCounter\(8);
\vga_u0|controller|ALT_INV_yCounter\(9) <= NOT \vga_u0|controller|yCounter\(9);
\dp|ALT_INV_yf[2]~DUPLICATE_q\ <= NOT \dp|yf[2]~DUPLICATE_q\;
\dp|ALT_INV_xf[2]~DUPLICATE_q\ <= NOT \dp|xf[2]~DUPLICATE_q\;
\dp|ALT_INV_xf[1]~DUPLICATE_q\ <= NOT \dp|xf[1]~DUPLICATE_q\;
\dp|ALT_INV_xf[0]~DUPLICATE_q\ <= NOT \dp|xf[0]~DUPLICATE_q\;
\dp|ALT_INV_yf[4]~DUPLICATE_q\ <= NOT \dp|yf[4]~DUPLICATE_q\;
\sm|ALT_INV_current_state[3]~DUPLICATE_q\ <= NOT \sm|current_state[3]~DUPLICATE_q\;
\sm|ALT_INV_current_state[2]~DUPLICATE_q\ <= NOT \sm|current_state[2]~DUPLICATE_q\;
\sm|ALT_INV_current_state[1]~DUPLICATE_q\ <= NOT \sm|current_state[1]~DUPLICATE_q\;
\sm|ALT_INV_current_state[0]~DUPLICATE_q\ <= NOT \sm|current_state[0]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[3]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[2]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[2]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[3]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[3]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[6]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[7]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[7]~DUPLICATE_q\;
\dp|ALT_INV_yf~9_combout\ <= NOT \dp|yf~9_combout\;
\dp|ALT_INV_Add5~1_combout\ <= NOT \dp|Add5~1_combout\;
\dp|ALT_INV_Add4~0_combout\ <= NOT \dp|Add4~0_combout\;
\dp|ALT_INV_xf~10_combout\ <= NOT \dp|xf~10_combout\;
\dp|ALT_INV_xf~9_combout\ <= NOT \dp|xf~9_combout\;
\dp|ALT_INV_xf~8_combout\ <= NOT \dp|xf~8_combout\;
\dp|ALT_INV_yf~8_combout\ <= NOT \dp|yf~8_combout\;
\dp|ALT_INV_Equal0~0_combout\ <= NOT \dp|Equal0~0_combout\;
\sm|ALT_INV_Decoder0~1_combout\ <= NOT \sm|Decoder0~1_combout\;
\dp|ALT_INV_yp~4_combout\ <= NOT \dp|yp~4_combout\;
\dp|ALT_INV_cond~4_combout\ <= NOT \dp|cond~4_combout\;
\dp|ALT_INV_Equal3~2_combout\ <= NOT \dp|Equal3~2_combout\;
\dp|ALT_INV_Equal3~1_combout\ <= NOT \dp|Equal3~1_combout\;
\dp|ALT_INV_xf\(5) <= NOT \dp|xf\(5);
\dp|ALT_INV_Equal3~0_combout\ <= NOT \dp|Equal3~0_combout\;
\dp|ALT_INV_xf~6_combout\ <= NOT \dp|xf~6_combout\;
\dp|ALT_INV_xf\(6) <= NOT \dp|xf\(6);
\dp|ALT_INV_xf~5_combout\ <= NOT \dp|xf~5_combout\;
\dp|ALT_INV_xf\(7) <= NOT \dp|xf\(7);
\dp|ALT_INV_cond~3_combout\ <= NOT \dp|cond~3_combout\;
\dp|ALT_INV_cond~2_combout\ <= NOT \dp|cond~2_combout\;
\dp|ALT_INV_cond~1_combout\ <= NOT \dp|cond~1_combout\;
\dp|ALT_INV_cond~0_combout\ <= NOT \dp|cond~0_combout\;
\dp|ALT_INV_LessThan0~0_combout\ <= NOT \dp|LessThan0~0_combout\;
\dp|ALT_INV_Equal2~3_combout\ <= NOT \dp|Equal2~3_combout\;
\dp|ALT_INV_Equal2~2_combout\ <= NOT \dp|Equal2~2_combout\;
\dp|ALT_INV_Equal2~1_combout\ <= NOT \dp|Equal2~1_combout\;
\dp|ALT_INV_yf\(2) <= NOT \dp|yf\(2);
\dp|ALT_INV_Equal2~0_combout\ <= NOT \dp|Equal2~0_combout\;
\dp|ALT_INV_yf\(0) <= NOT \dp|yf\(0);
\dp|ALT_INV_yf\(1) <= NOT \dp|yf\(1);
\dp|ALT_INV_Equal1~1_combout\ <= NOT \dp|Equal1~1_combout\;
\dp|ALT_INV_Equal1~0_combout\ <= NOT \dp|Equal1~0_combout\;
\dp|ALT_INV_xp~5_combout\ <= NOT \dp|xp~5_combout\;
\vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ <= NOT \vga_u0|controller|VGA_VS1~0_combout\;
\vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ <= NOT \vga_u0|controller|VGA_HS1~0_combout\;
\dp|ALT_INV_xf~4_combout\ <= NOT \dp|xf~4_combout\;
\dp|ALT_INV_xf\(4) <= NOT \dp|xf\(4);
\dp|ALT_INV_xp~4_combout\ <= NOT \dp|xp~4_combout\;
\dp|ALT_INV_xf~3_combout\ <= NOT \dp|xf~3_combout\;
\dp|ALT_INV_xf\(3) <= NOT \dp|xf\(3);
\dp|ALT_INV_xp~3_combout\ <= NOT \dp|xp~3_combout\;
\dp|ALT_INV_xf~2_combout\ <= NOT \dp|xf~2_combout\;
\dp|ALT_INV_xf\(2) <= NOT \dp|xf\(2);
\dp|ALT_INV_xp~2_combout\ <= NOT \dp|xp~2_combout\;
\dp|ALT_INV_xf\(1) <= NOT \dp|xf\(1);
\dp|ALT_INV_Mult1~418\ <= NOT \dp|Mult1~418\;
\dp|ALT_INV_Mult1~417\ <= NOT \dp|Mult1~417\;
\dp|ALT_INV_Mult1~416\ <= NOT \dp|Mult1~416\;
\dp|ALT_INV_Mult1~415\ <= NOT \dp|Mult1~415\;
\dp|ALT_INV_Mult1~414\ <= NOT \dp|Mult1~414\;
\dp|ALT_INV_Mult1~413\ <= NOT \dp|Mult1~413\;
\dp|ALT_INV_Mult1~412\ <= NOT \dp|Mult1~412\;
\dp|ALT_INV_Mult1~411\ <= NOT \dp|Mult1~411\;
\dp|ALT_INV_Mult1~410\ <= NOT \dp|Mult1~410\;
\dp|ALT_INV_Mult1~409\ <= NOT \dp|Mult1~409\;
\dp|ALT_INV_Mult1~408\ <= NOT \dp|Mult1~408\;
\dp|ALT_INV_Mult1~407\ <= NOT \dp|Mult1~407\;
\dp|ALT_INV_Mult1~406\ <= NOT \dp|Mult1~406\;
\dp|ALT_INV_Mult1~405_resulta\ <= NOT \dp|Mult1~405_resulta\;
\dp|ALT_INV_Mult1~394_sumout\ <= NOT \dp|Mult1~394_sumout\;
\dp|ALT_INV_Mult1~390_sumout\ <= NOT \dp|Mult1~390_sumout\;
\dp|ALT_INV_Mult1~386_sumout\ <= NOT \dp|Mult1~386_sumout\;
\dp|ALT_INV_Mult1~382_sumout\ <= NOT \dp|Mult1~382_sumout\;
\dp|ALT_INV_Mult1~378_sumout\ <= NOT \dp|Mult1~378_sumout\;
\dp|ALT_INV_Mult1~374_sumout\ <= NOT \dp|Mult1~374_sumout\;
\dp|ALT_INV_Mult1~370_sumout\ <= NOT \dp|Mult1~370_sumout\;
\dp|ALT_INV_Mult1~366_sumout\ <= NOT \dp|Mult1~366_sumout\;
\dp|ALT_INV_Mult1~362_sumout\ <= NOT \dp|Mult1~362_sumout\;
\dp|ALT_INV_Mult1~358_sumout\ <= NOT \dp|Mult1~358_sumout\;
\dp|ALT_INV_Mult1~354_sumout\ <= NOT \dp|Mult1~354_sumout\;
\dp|ALT_INV_Mult1~51\ <= NOT \dp|Mult1~51\;
\dp|ALT_INV_Mult1~50\ <= NOT \dp|Mult1~50\;
\dp|ALT_INV_Mult1~49\ <= NOT \dp|Mult1~49\;
\dp|ALT_INV_Mult1~48\ <= NOT \dp|Mult1~48\;
\dp|ALT_INV_Mult1~47\ <= NOT \dp|Mult1~47\;
\dp|ALT_INV_Mult1~46\ <= NOT \dp|Mult1~46\;
\dp|ALT_INV_Mult1~45\ <= NOT \dp|Mult1~45\;
\dp|ALT_INV_Mult1~44\ <= NOT \dp|Mult1~44\;
\dp|ALT_INV_Mult1~43\ <= NOT \dp|Mult1~43\;
\dp|ALT_INV_Mult1~42\ <= NOT \dp|Mult1~42\;
\dp|ALT_INV_Mult1~41\ <= NOT \dp|Mult1~41\;
\dp|ALT_INV_Mult1~40\ <= NOT \dp|Mult1~40\;
\dp|ALT_INV_Mult1~39\ <= NOT \dp|Mult1~39\;
\dp|ALT_INV_Mult1~38\ <= NOT \dp|Mult1~38\;
\dp|ALT_INV_Mult1~37\ <= NOT \dp|Mult1~37\;
\dp|ALT_INV_Mult1~36\ <= NOT \dp|Mult1~36\;
\dp|ALT_INV_Mult1~35\ <= NOT \dp|Mult1~35\;
\dp|ALT_INV_Mult1~34\ <= NOT \dp|Mult1~34\;
\dp|ALT_INV_Mult1~33\ <= NOT \dp|Mult1~33\;
\dp|ALT_INV_Mult1~32\ <= NOT \dp|Mult1~32\;
\dp|ALT_INV_Mult1~31\ <= NOT \dp|Mult1~31\;
\dp|ALT_INV_Mult1~30\ <= NOT \dp|Mult1~30\;
\dp|ALT_INV_Mult1~29\ <= NOT \dp|Mult1~29\;
\dp|ALT_INV_Mult1~28\ <= NOT \dp|Mult1~28\;
\dp|ALT_INV_Mult1~27\ <= NOT \dp|Mult1~27\;
\dp|ALT_INV_Mult1~26\ <= NOT \dp|Mult1~26\;
\dp|ALT_INV_Mult1~25\ <= NOT \dp|Mult1~25\;
\dp|ALT_INV_Mult1~24\ <= NOT \dp|Mult1~24\;
\dp|ALT_INV_Mult1~23\ <= NOT \dp|Mult1~23\;
\dp|ALT_INV_Mult1~22\ <= NOT \dp|Mult1~22\;
\dp|ALT_INV_Mult1~21\ <= NOT \dp|Mult1~21\;
\dp|ALT_INV_Mult1~20_resulta\ <= NOT \dp|Mult1~20_resulta\;
\dp|ALT_INV_xf\(0) <= NOT \dp|xf\(0);
\sm|ALT_INV_WideOr0~0_combout\ <= NOT \sm|WideOr0~0_combout\;
\sm|ALT_INV_WideOr1~0_combout\ <= NOT \sm|WideOr1~0_combout\;
\sm|ALT_INV_initxp~0_combout\ <= NOT \sm|initxp~0_combout\;
\dp|ALT_INV_yf\(3) <= NOT \dp|yf\(3);
\dp|ALT_INV_yp~3_combout\ <= NOT \dp|yp~3_combout\;
\dp|ALT_INV_yf\(4) <= NOT \dp|yf\(4);
\dp|ALT_INV_yp~2_combout\ <= NOT \dp|yp~2_combout\;
\dp|ALT_INV_yf~3_combout\ <= NOT \dp|yf~3_combout\;
\dp|ALT_INV_yf\(5) <= NOT \dp|yf\(5);
\sm|ALT_INV_Decoder0~0_combout\ <= NOT \sm|Decoder0~0_combout\;
\sm|ALT_INV_initx~0_combout\ <= NOT \sm|initx~0_combout\;
\dp|ALT_INV_yp~1_combout\ <= NOT \dp|yp~1_combout\;
\dp|ALT_INV_yf~1_combout\ <= NOT \dp|yf~1_combout\;
\dp|ALT_INV_yf\(6) <= NOT \dp|yf\(6);
\dp|ALT_INV_yf~0_combout\ <= NOT \dp|yf~0_combout\;
\dp|ALT_INV_yp~0_combout\ <= NOT \dp|yp~0_combout\;
\sm|ALT_INV_current_state[3]~0_combout\ <= NOT \sm|current_state[3]~0_combout\;
\sm|ALT_INV_Mux2~1_combout\ <= NOT \sm|Mux2~1_combout\;
\dp|ALT_INV_ypdone~q\ <= NOT \dp|ypdone~q\;
\sm|ALT_INV_Mux2~0_combout\ <= NOT \sm|Mux2~0_combout\;
\sm|ALT_INV_Mux3~0_combout\ <= NOT \sm|Mux3~0_combout\;
\dp|ALT_INV_xdone~q\ <= NOT \dp|xdone~q\;
\dp|ALT_INV_ydone~q\ <= NOT \dp|ydone~q\;
\dp|ALT_INV_cond~q\ <= NOT \dp|cond~q\;
\dp|ALT_INV_xpdone~q\ <= NOT \dp|xpdone~q\;
\vga_u0|controller|ALT_INV_VGA_BLANK1~q\ <= NOT \vga_u0|controller|VGA_BLANK1~q\;
\vga_u0|controller|ALT_INV_VGA_HS1~q\ <= NOT \vga_u0|controller|VGA_HS1~q\;
\vga_u0|ALT_INV_writeEn~0_combout\ <= NOT \vga_u0|writeEn~0_combout\;
\sm|ALT_INV_plot~0_combout\ <= NOT \sm|plot~0_combout\;
\vga_u0|controller|ALT_INV_always1~1_combout\ <= NOT \vga_u0|controller|always1~1_combout\;
\vga_u0|controller|ALT_INV_always1~0_combout\ <= NOT \vga_u0|controller|always1~0_combout\;
\vga_u0|controller|ALT_INV_Equal0~1_combout\ <= NOT \vga_u0|controller|Equal0~1_combout\;
\vga_u0|controller|ALT_INV_Equal0~0_combout\ <= NOT \vga_u0|controller|Equal0~0_combout\;
\sm|ALT_INV_current_state\(3) <= NOT \sm|current_state\(3);
\sm|ALT_INV_current_state\(2) <= NOT \sm|current_state\(2);
\sm|ALT_INV_current_state\(1) <= NOT \sm|current_state\(1);
\sm|ALT_INV_current_state\(0) <= NOT \sm|current_state\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1);
\vga_u0|controller|ALT_INV_on_screen~1_combout\ <= NOT \vga_u0|controller|on_screen~1_combout\;
\vga_u0|controller|ALT_INV_on_screen~0_combout\ <= NOT \vga_u0|controller|on_screen~0_combout\;
\vga_u0|controller|ALT_INV_LessThan7~0_combout\ <= NOT \vga_u0|controller|LessThan7~0_combout\;
\dp|ALT_INV_Mult0~508\ <= NOT \dp|Mult0~508\;
\dp|ALT_INV_Mult0~507\ <= NOT \dp|Mult0~507\;
\dp|ALT_INV_Mult0~506\ <= NOT \dp|Mult0~506\;
\dp|ALT_INV_Mult0~505\ <= NOT \dp|Mult0~505\;
\dp|ALT_INV_Mult0~504\ <= NOT \dp|Mult0~504\;
\dp|ALT_INV_Mult0~503\ <= NOT \dp|Mult0~503\;
\dp|ALT_INV_Mult0~502\ <= NOT \dp|Mult0~502\;
\dp|ALT_INV_Mult0~501\ <= NOT \dp|Mult0~501\;
\dp|ALT_INV_Mult0~500\ <= NOT \dp|Mult0~500\;
\dp|ALT_INV_Mult0~499\ <= NOT \dp|Mult0~499\;
\dp|ALT_INV_Mult0~498\ <= NOT \dp|Mult0~498\;
\dp|ALT_INV_Mult0~497\ <= NOT \dp|Mult0~497\;
\dp|ALT_INV_Mult0~496\ <= NOT \dp|Mult0~496\;
\dp|ALT_INV_Mult0~495\ <= NOT \dp|Mult0~495\;
\dp|ALT_INV_Mult0~494\ <= NOT \dp|Mult0~494\;
\dp|ALT_INV_Mult0~493\ <= NOT \dp|Mult0~493\;
\dp|ALT_INV_Mult0~492\ <= NOT \dp|Mult0~492\;
\dp|ALT_INV_Mult0~491\ <= NOT \dp|Mult0~491\;
\dp|ALT_INV_Mult0~490\ <= NOT \dp|Mult0~490\;
\dp|ALT_INV_Mult0~489\ <= NOT \dp|Mult0~489\;
\dp|ALT_INV_Mult0~488\ <= NOT \dp|Mult0~488\;
\dp|ALT_INV_Mult0~487\ <= NOT \dp|Mult0~487\;
\dp|ALT_INV_Mult0~486\ <= NOT \dp|Mult0~486\;
\dp|ALT_INV_Mult0~485\ <= NOT \dp|Mult0~485\;
\dp|ALT_INV_Mult0~484\ <= NOT \dp|Mult0~484\;
\dp|ALT_INV_Mult0~483\ <= NOT \dp|Mult0~483\;
\dp|ALT_INV_Mult0~482\ <= NOT \dp|Mult0~482\;
\dp|ALT_INV_Mult0~481\ <= NOT \dp|Mult0~481\;
\dp|ALT_INV_Mult0~480\ <= NOT \dp|Mult0~480\;
\dp|ALT_INV_Mult0~479\ <= NOT \dp|Mult0~479\;
\dp|ALT_INV_Mult0~478\ <= NOT \dp|Mult0~478\;
\dp|ALT_INV_Mult0~477_resulta\ <= NOT \dp|Mult0~477_resulta\;
\dp|ALT_INV_Mult1~9_sumout\ <= NOT \dp|Mult1~9_sumout\;
\dp|ALT_INV_Mult1~5_sumout\ <= NOT \dp|Mult1~5_sumout\;
\dp|ALT_INV_Mult0~149\ <= NOT \dp|Mult0~149\;
\dp|ALT_INV_Mult0~148\ <= NOT \dp|Mult0~148\;
\dp|ALT_INV_Mult0~147\ <= NOT \dp|Mult0~147\;
\dp|ALT_INV_Mult0~146\ <= NOT \dp|Mult0~146\;
\dp|ALT_INV_Mult0~145\ <= NOT \dp|Mult0~145\;
\dp|ALT_INV_Mult0~144\ <= NOT \dp|Mult0~144\;
\dp|ALT_INV_Mult0~143\ <= NOT \dp|Mult0~143\;
\dp|ALT_INV_Mult0~142\ <= NOT \dp|Mult0~142\;
\dp|ALT_INV_Mult0~141\ <= NOT \dp|Mult0~141\;
\dp|ALT_INV_Mult0~140\ <= NOT \dp|Mult0~140\;
\dp|ALT_INV_Mult0~139\ <= NOT \dp|Mult0~139\;
\dp|ALT_INV_Mult0~138\ <= NOT \dp|Mult0~138\;
\dp|ALT_INV_Mult0~137\ <= NOT \dp|Mult0~137\;
\dp|ALT_INV_Mult0~136_resulta\ <= NOT \dp|Mult0~136_resulta\;
\dp|ALT_INV_Mult1~1_sumout\ <= NOT \dp|Mult1~1_sumout\;
\dp|ALT_INV_Add0~25_sumout\ <= NOT \dp|Add0~25_sumout\;
\dp|ALT_INV_Add0~21_sumout\ <= NOT \dp|Add0~21_sumout\;
\dp|ALT_INV_yp\(0) <= NOT \dp|yp\(0);
\dp|ALT_INV_yp\(1) <= NOT \dp|yp\(1);
\dp|ALT_INV_Add0~17_sumout\ <= NOT \dp|Add0~17_sumout\;
\dp|ALT_INV_yp\(2) <= NOT \dp|yp\(2);
\dp|ALT_INV_Add3~29_sumout\ <= NOT \dp|Add3~29_sumout\;
\dp|ALT_INV_Add3~25_sumout\ <= NOT \dp|Add3~25_sumout\;
\dp|ALT_INV_Add3~21_sumout\ <= NOT \dp|Add3~21_sumout\;
\dp|ALT_INV_Mult0~125_sumout\ <= NOT \dp|Mult0~125_sumout\;
\dp|ALT_INV_Mult0~121_sumout\ <= NOT \dp|Mult0~121_sumout\;
\dp|ALT_INV_Mult0~117_sumout\ <= NOT \dp|Mult0~117_sumout\;
\dp|ALT_INV_Mult0~113_sumout\ <= NOT \dp|Mult0~113_sumout\;
\dp|ALT_INV_Mult0~109_sumout\ <= NOT \dp|Mult0~109_sumout\;
\dp|ALT_INV_Mult0~105_sumout\ <= NOT \dp|Mult0~105_sumout\;
\dp|ALT_INV_Mult0~101_sumout\ <= NOT \dp|Mult0~101_sumout\;
\dp|ALT_INV_Mult0~97_sumout\ <= NOT \dp|Mult0~97_sumout\;
\dp|ALT_INV_Mult0~93_sumout\ <= NOT \dp|Mult0~93_sumout\;
\dp|ALT_INV_Mult0~89_sumout\ <= NOT \dp|Mult0~89_sumout\;
\dp|ALT_INV_Mult0~85_sumout\ <= NOT \dp|Mult0~85_sumout\;
\dp|ALT_INV_Mult0~81_sumout\ <= NOT \dp|Mult0~81_sumout\;
\dp|ALT_INV_Mult0~77_sumout\ <= NOT \dp|Mult0~77_sumout\;
\dp|ALT_INV_Mult0~73_sumout\ <= NOT \dp|Mult0~73_sumout\;
\dp|ALT_INV_Mult0~69_sumout\ <= NOT \dp|Mult0~69_sumout\;
\dp|ALT_INV_Mult0~65_sumout\ <= NOT \dp|Mult0~65_sumout\;
\dp|ALT_INV_Mult0~61_sumout\ <= NOT \dp|Mult0~61_sumout\;
\dp|ALT_INV_Mult0~57_sumout\ <= NOT \dp|Mult0~57_sumout\;
\dp|ALT_INV_Mult0~53_sumout\ <= NOT \dp|Mult0~53_sumout\;
\dp|ALT_INV_Mult0~49_sumout\ <= NOT \dp|Mult0~49_sumout\;
\dp|ALT_INV_Mult0~45_sumout\ <= NOT \dp|Mult0~45_sumout\;
\dp|ALT_INV_Mult0~41_sumout\ <= NOT \dp|Mult0~41_sumout\;
\dp|ALT_INV_Mult0~37_sumout\ <= NOT \dp|Mult0~37_sumout\;
\dp|ALT_INV_Mult0~33_sumout\ <= NOT \dp|Mult0~33_sumout\;
\dp|ALT_INV_Mult0~29_sumout\ <= NOT \dp|Mult0~29_sumout\;
\dp|ALT_INV_Add2~25_sumout\ <= NOT \dp|Add2~25_sumout\;
\dp|ALT_INV_Add2~21_sumout\ <= NOT \dp|Add2~21_sumout\;
\dp|ALT_INV_Add2~17_sumout\ <= NOT \dp|Add2~17_sumout\;
\dp|ALT_INV_Mult0~25_sumout\ <= NOT \dp|Mult0~25_sumout\;
\dp|ALT_INV_Mult0~21_sumout\ <= NOT \dp|Mult0~21_sumout\;
\dp|ALT_INV_Mult0~17_sumout\ <= NOT \dp|Mult0~17_sumout\;
\dp|ALT_INV_Mult0~13_sumout\ <= NOT \dp|Mult0~13_sumout\;
\dp|ALT_INV_Mult0~9_sumout\ <= NOT \dp|Mult0~9_sumout\;
\dp|ALT_INV_Mult0~5_sumout\ <= NOT \dp|Mult0~5_sumout\;
\dp|ALT_INV_Mult0~1_sumout\ <= NOT \dp|Mult0~1_sumout\;
\dp|ALT_INV_Add1~29_sumout\ <= NOT \dp|Add1~29_sumout\;
\dp|ALT_INV_Add1~25_sumout\ <= NOT \dp|Add1~25_sumout\;
\dp|ALT_INV_xp\(6) <= NOT \dp|xp\(6);
\dp|ALT_INV_xp\(7) <= NOT \dp|xp\(7);
\dp|ALT_INV_Add1~21_sumout\ <= NOT \dp|Add1~21_sumout\;
\dp|ALT_INV_xp\(5) <= NOT \dp|xp\(5);
\dp|ALT_INV_y\(2) <= NOT \dp|y\(2);
\dp|ALT_INV_x\(6) <= NOT \dp|x\(6);
\dp|ALT_INV_y\(1) <= NOT \dp|y\(1);
\dp|ALT_INV_x\(5) <= NOT \dp|x\(5);
\dp|ALT_INV_y\(0) <= NOT \dp|y\(0);
\dp|ALT_INV_Add3~17_sumout\ <= NOT \dp|Add3~17_sumout\;
\dp|ALT_INV_Add1~17_sumout\ <= NOT \dp|Add1~17_sumout\;
\dp|ALT_INV_xp\(4) <= NOT \dp|xp\(4);
\dp|ALT_INV_Add3~13_sumout\ <= NOT \dp|Add3~13_sumout\;
\dp|ALT_INV_Add1~13_sumout\ <= NOT \dp|Add1~13_sumout\;
\dp|ALT_INV_xp\(3) <= NOT \dp|xp\(3);
\dp|ALT_INV_Add3~9_sumout\ <= NOT \dp|Add3~9_sumout\;
\dp|ALT_INV_Add1~9_sumout\ <= NOT \dp|Add1~9_sumout\;
\dp|ALT_INV_xp\(2) <= NOT \dp|xp\(2);
\dp|ALT_INV_Add3~5_sumout\ <= NOT \dp|Add3~5_sumout\;
\dp|ALT_INV_Add1~5_sumout\ <= NOT \dp|Add1~5_sumout\;
\dp|ALT_INV_xp\(1) <= NOT \dp|xp\(1);
\dp|ALT_INV_Add3~1_sumout\ <= NOT \dp|Add3~1_sumout\;
\dp|ALT_INV_Add1~1_sumout\ <= NOT \dp|Add1~1_sumout\;
\dp|ALT_INV_xp\(0) <= NOT \dp|xp\(0);
\dp|ALT_INV_Add2~13_sumout\ <= NOT \dp|Add2~13_sumout\;
\dp|ALT_INV_Add0~13_sumout\ <= NOT \dp|Add0~13_sumout\;
\dp|ALT_INV_yp\(3) <= NOT \dp|yp\(3);
\dp|ALT_INV_Add2~9_sumout\ <= NOT \dp|Add2~9_sumout\;

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X36_Y0_N36
\VGA_R[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(8));

-- Location: IOOBUF_X12_Y81_N2
\VGA_R[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(9));

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X10_Y0_N76
\VGA_G[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(8));

-- Location: IOOBUF_X12_Y0_N19
\VGA_G[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(9));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X22_Y0_N53
\VGA_B[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(8));

-- Location: IOOBUF_X22_Y81_N36
\VGA_B[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(9));

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X26_Y0_N59
\VGA_BLANK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_BLANK~q\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK);

-- Location: IOOBUF_X12_Y81_N53
\VGA_SYNC~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_VGA_SYNC);

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \sm|current_state[0]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \sm|current_state[1]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \sm|current_state[2]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \sm|current_state[3]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G5
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: FF_X27_Y47_N32
\sm|current_state[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux3~1_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(0));

-- Location: FF_X27_Y47_N16
\sm|current_state[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux1~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[2]~DUPLICATE_q\);

-- Location: FF_X27_Y47_N52
\sm|current_state[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|current_state[3]~1_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(3));

-- Location: FF_X27_Y47_N10
\sm|current_state[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux2~2_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(1));

-- Location: MLABCELL_X28_Y47_N6
\sm|Decoder0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Decoder0~0_combout\ = ( !\sm|current_state\(1) & ( (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state\(1),
	combout => \sm|Decoder0~0_combout\);

-- Location: FF_X28_Y47_N20
\dp|yf[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~4_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf[4]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y47_N0
\sm|initx~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|initx~0_combout\ = ( !\sm|current_state[1]~DUPLICATE_q\ & ( (\sm|current_state[2]~DUPLICATE_q\ & !\sm|current_state[3]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	combout => \sm|initx~0_combout\);

-- Location: MLABCELL_X28_Y47_N30
\dp|Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~17_sumout\ = SUM(( \dp|yf\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add2~18\ = CARRY(( \dp|yf\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_yf\(0),
	cin => GND,
	sumout => \dp|Add2~17_sumout\,
	cout => \dp|Add2~18\);

-- Location: MLABCELL_X28_Y47_N24
\dp|yf~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~6_combout\ = ( \sm|Decoder0~0_combout\ ) # ( !\sm|Decoder0~0_combout\ & ( (!\sm|initx~0_combout\ & ((\dp|yf\(0)))) # (\sm|initx~0_combout\ & (\dp|Add2~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add2~17_sumout\,
	datad => \dp|ALT_INV_yf\(0),
	dataf => \sm|ALT_INV_Decoder0~0_combout\,
	combout => \dp|yf~6_combout\);

-- Location: FF_X28_Y47_N26
\dp|yf[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yf~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(0));

-- Location: MLABCELL_X28_Y47_N33
\dp|Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~21_sumout\ = SUM(( \dp|yf\(1) ) + ( GND ) + ( \dp|Add2~18\ ))
-- \dp|Add2~22\ = CARRY(( \dp|yf\(1) ) + ( GND ) + ( \dp|Add2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yf\(1),
	cin => \dp|Add2~18\,
	sumout => \dp|Add2~21_sumout\,
	cout => \dp|Add2~22\);

-- Location: MLABCELL_X28_Y47_N15
\dp|yf~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~7_combout\ = (!\sm|Decoder0~0_combout\ & ((!\sm|initx~0_combout\ & ((\dp|yf\(1)))) # (\sm|initx~0_combout\ & (\dp|Add2~21_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010010001100000001001000110000000100100011000000010010001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initx~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add2~21_sumout\,
	datad => \dp|ALT_INV_yf\(1),
	combout => \dp|yf~7_combout\);

-- Location: FF_X28_Y47_N23
\dp|yf[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~7_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(1));

-- Location: MLABCELL_X28_Y47_N36
\dp|Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~25_sumout\ = SUM(( \dp|yf[2]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add2~22\ ))
-- \dp|Add2~26\ = CARRY(( \dp|yf[2]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yf[2]~DUPLICATE_q\,
	cin => \dp|Add2~22\,
	sumout => \dp|Add2~25_sumout\,
	cout => \dp|Add2~26\);

-- Location: FF_X28_Y47_N56
\dp|yf[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~8_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(2));

-- Location: MLABCELL_X28_Y47_N12
\dp|yf~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~8_combout\ = ((!\sm|initx~0_combout\ & ((\dp|yf\(2)))) # (\sm|initx~0_combout\ & (\dp|Add2~25_sumout\))) # (\sm|Decoder0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011110111111001101111011111100110111101111110011011110111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initx~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add2~25_sumout\,
	datad => \dp|ALT_INV_yf\(2),
	combout => \dp|yf~8_combout\);

-- Location: FF_X28_Y47_N55
\dp|yf[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~8_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf[2]~DUPLICATE_q\);

-- Location: MLABCELL_X28_Y47_N39
\dp|Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~13_sumout\ = SUM(( \dp|yf\(3) ) + ( GND ) + ( \dp|Add2~26\ ))
-- \dp|Add2~14\ = CARRY(( \dp|yf\(3) ) + ( GND ) + ( \dp|Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_yf\(3),
	cin => \dp|Add2~26\,
	sumout => \dp|Add2~13_sumout\,
	cout => \dp|Add2~14\);

-- Location: LABCELL_X29_Y49_N51
\dp|yf~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~5_combout\ = ( \dp|Add2~13_sumout\ & ( ((\sm|initx~0_combout\) # (\dp|yf\(3))) # (\sm|Decoder0~0_combout\) ) ) # ( !\dp|Add2~13_sumout\ & ( ((\dp|yf\(3) & !\sm|initx~0_combout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101010101010111110101010101011111111111110101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_yf\(3),
	datad => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_Add2~13_sumout\,
	combout => \dp|yf~5_combout\);

-- Location: FF_X28_Y47_N17
\dp|yf[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~5_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(3));

-- Location: MLABCELL_X28_Y47_N42
\dp|Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~9_sumout\ = SUM(( \dp|yf[4]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add2~14\ ))
-- \dp|Add2~10\ = CARRY(( \dp|yf[4]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_yf[4]~DUPLICATE_q\,
	cin => \dp|Add2~14\,
	sumout => \dp|Add2~9_sumout\,
	cout => \dp|Add2~10\);

-- Location: LABCELL_X29_Y49_N48
\dp|yf~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~4_combout\ = ( \dp|yf\(4) & ( ((!\sm|initx~0_combout\) # (\dp|Add2~9_sumout\)) # (\sm|Decoder0~0_combout\) ) ) # ( !\dp|yf\(4) & ( ((\dp|Add2~9_sumout\ & \sm|initx~0_combout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101011111010101010101111111111111010111111111111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add2~9_sumout\,
	datad => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_yf\(4),
	combout => \dp|yf~4_combout\);

-- Location: FF_X28_Y47_N19
\dp|yf[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~4_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(4));

-- Location: MLABCELL_X28_Y47_N0
\dp|yf~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~3_combout\ = (!\sm|Decoder0~0_combout\ & ((!\sm|initx~0_combout\ & (\dp|yf\(5))) # (\sm|initx~0_combout\ & ((\dp|Add2~5_sumout\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001001100000010000100110000001000010011000000100001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initx~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_yf\(5),
	datad => \dp|ALT_INV_Add2~5_sumout\,
	combout => \dp|yf~3_combout\);

-- Location: FF_X28_Y47_N14
\dp|yf[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~3_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(5));

-- Location: MLABCELL_X28_Y47_N45
\dp|Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~5_sumout\ = SUM(( \dp|yf\(5) ) + ( GND ) + ( \dp|Add2~10\ ))
-- \dp|Add2~6\ = CARRY(( \dp|yf\(5) ) + ( GND ) + ( \dp|Add2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_yf\(5),
	cin => \dp|Add2~10\,
	sumout => \dp|Add2~5_sumout\,
	cout => \dp|Add2~6\);

-- Location: LABCELL_X30_Y47_N48
\dp|Equal2~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal2~2_combout\ = ( !\sm|Decoder0~0_combout\ & ( \dp|yf\(5) & ( (!\sm|initx~0_combout\) # ((!\dp|Add2~9_sumout\) # (\dp|Add2~5_sumout\)) ) ) ) # ( !\sm|Decoder0~0_combout\ & ( !\dp|yf\(5) & ( (!\sm|initx~0_combout\ & (!\dp|yf\(4))) # 
-- (\sm|initx~0_combout\ & (((!\dp|Add2~9_sumout\) # (\dp|Add2~5_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011100010111011000000000000000011111100111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf\(4),
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add2~9_sumout\,
	datad => \dp|ALT_INV_Add2~5_sumout\,
	datae => \sm|ALT_INV_Decoder0~0_combout\,
	dataf => \dp|ALT_INV_yf\(5),
	combout => \dp|Equal2~2_combout\);

-- Location: LABCELL_X27_Y47_N36
\dp|yf~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~0_combout\ = ( \sm|current_state\(0) & ( (!\sm|current_state[1]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & \sm|current_state[2]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \dp|yf~0_combout\);

-- Location: LABCELL_X27_Y47_N42
\dp|yf~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~1_combout\ = ( \sm|current_state[2]~DUPLICATE_q\ & ( (\dp|yf\(6) & ((\sm|current_state[3]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\))) ) ) # ( !\sm|current_state[2]~DUPLICATE_q\ & ( \dp|yf\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000001111110000000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \dp|ALT_INV_yf\(6),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \dp|yf~1_combout\);

-- Location: LABCELL_X27_Y47_N21
\dp|yf~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~2_combout\ = ((\dp|yf~0_combout\ & \dp|Add2~1_sumout\)) # (\dp|yf~1_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011111000011110101111100001111010111110000111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf~0_combout\,
	datac => \dp|ALT_INV_yf~1_combout\,
	datad => \dp|ALT_INV_Add2~1_sumout\,
	combout => \dp|yf~2_combout\);

-- Location: FF_X28_Y47_N5
\dp|yf[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|yf~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yf\(6));

-- Location: MLABCELL_X28_Y47_N48
\dp|Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add2~1_sumout\ = SUM(( \dp|yf\(6) ) + ( GND ) + ( \dp|Add2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_yf\(6),
	cin => \dp|Add2~6\,
	sumout => \dp|Add2~1_sumout\);

-- Location: MLABCELL_X28_Y48_N18
\dp|Equal2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal2~1_combout\ = ( \sm|initx~0_combout\ & ( \dp|Add2~13_sumout\ & ( (!\sm|Decoder0~0_combout\ & !\dp|Add2~25_sumout\) ) ) ) # ( !\sm|initx~0_combout\ & ( \dp|Add2~13_sumout\ & ( (!\dp|yf[2]~DUPLICATE_q\ & (\dp|yf\(3) & !\sm|Decoder0~0_combout\)) ) 
-- ) ) # ( !\sm|initx~0_combout\ & ( !\dp|Add2~13_sumout\ & ( (!\dp|yf[2]~DUPLICATE_q\ & (\dp|yf\(3) & !\sm|Decoder0~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000000000000000000000100000001000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf[2]~DUPLICATE_q\,
	datab => \dp|ALT_INV_yf\(3),
	datac => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_Add2~25_sumout\,
	datae => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_Add2~13_sumout\,
	combout => \dp|Equal2~1_combout\);

-- Location: MLABCELL_X28_Y48_N12
\dp|Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal2~0_combout\ = ( \dp|yf\(0) & ( \dp|Add2~21_sumout\ & ( (!\dp|Add2~17_sumout\ & (!\sm|Decoder0~0_combout\ & \sm|initx~0_combout\)) ) ) ) # ( !\dp|yf\(0) & ( \dp|Add2~21_sumout\ & ( (!\sm|Decoder0~0_combout\ & ((!\sm|initx~0_combout\ & 
-- (\dp|yf\(1))) # (\sm|initx~0_combout\ & ((!\dp|Add2~17_sumout\))))) ) ) ) # ( !\dp|yf\(0) & ( !\dp|Add2~21_sumout\ & ( (\dp|yf\(1) & (!\sm|Decoder0~0_combout\ & !\sm|initx~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000000000000000000001010000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf\(1),
	datab => \dp|ALT_INV_Add2~17_sumout\,
	datac => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \sm|ALT_INV_initx~0_combout\,
	datae => \dp|ALT_INV_yf\(0),
	dataf => \dp|ALT_INV_Add2~21_sumout\,
	combout => \dp|Equal2~0_combout\);

-- Location: LABCELL_X27_Y47_N24
\dp|Equal2~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal2~3_combout\ = ( \dp|yf~0_combout\ & ( \dp|yf~1_combout\ & ( (!\dp|Equal2~2_combout\ & (\dp|Equal2~1_combout\ & \dp|Equal2~0_combout\)) ) ) ) # ( !\dp|yf~0_combout\ & ( \dp|yf~1_combout\ & ( (!\dp|Equal2~2_combout\ & (\dp|Equal2~1_combout\ & 
-- \dp|Equal2~0_combout\)) ) ) ) # ( \dp|yf~0_combout\ & ( !\dp|yf~1_combout\ & ( (!\dp|Equal2~2_combout\ & (\dp|Add2~1_sumout\ & (\dp|Equal2~1_combout\ & \dp|Equal2~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Equal2~2_combout\,
	datab => \dp|ALT_INV_Add2~1_sumout\,
	datac => \dp|ALT_INV_Equal2~1_combout\,
	datad => \dp|ALT_INV_Equal2~0_combout\,
	datae => \dp|ALT_INV_yf~0_combout\,
	dataf => \dp|ALT_INV_yf~1_combout\,
	combout => \dp|Equal2~3_combout\);

-- Location: FF_X27_Y47_N26
\dp|ydone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Equal2~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ydone~q\);

-- Location: FF_X31_Y47_N35
\dp|xf[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~6_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(6));

-- Location: LABCELL_X31_Y47_N57
\sm|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr0~0_combout\ = ( \sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state\(3) & ((!\sm|current_state[1]~DUPLICATE_q\) # (!\sm|current_state[0]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111100000000001111110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \sm|WideOr0~0_combout\);

-- Location: FF_X30_Y47_N59
\dp|xf[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(2));

-- Location: LABCELL_X31_Y47_N0
\dp|Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~1_sumout\ = SUM(( \dp|xf[0]~DUPLICATE_q\ ) + ( VCC ) + ( !VCC ))
-- \dp|Add3~2\ = CARRY(( \dp|xf[0]~DUPLICATE_q\ ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_xf[0]~DUPLICATE_q\,
	cin => GND,
	sumout => \dp|Add3~1_sumout\,
	cout => \dp|Add3~2\);

-- Location: FF_X30_Y47_N41
\dp|xf[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(0));

-- Location: LABCELL_X30_Y47_N45
\dp|xf~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~0_combout\ = ( \dp|xf\(0) & ( ((!\sm|WideOr0~0_combout\) # (\dp|Add3~1_sumout\)) # (\sm|initx~0_combout\) ) ) # ( !\dp|xf\(0) & ( (\sm|WideOr0~0_combout\ & ((\dp|Add3~1_sumout\) # (\sm|initx~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100001111000000110000111111110011111111111111001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \sm|ALT_INV_WideOr0~0_combout\,
	datad => \dp|ALT_INV_Add3~1_sumout\,
	dataf => \dp|ALT_INV_xf\(0),
	combout => \dp|xf~0_combout\);

-- Location: FF_X30_Y47_N40
\dp|xf[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf[0]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y47_N3
\dp|Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~5_sumout\ = SUM(( \dp|xf[1]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~2\ ))
-- \dp|Add3~6\ = CARRY(( \dp|xf[1]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xf[1]~DUPLICATE_q\,
	cin => \dp|Add3~2\,
	sumout => \dp|Add3~5_sumout\,
	cout => \dp|Add3~6\);

-- Location: FF_X30_Y47_N23
\dp|xf[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~1_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(1));

-- Location: LABCELL_X30_Y47_N54
\dp|xf~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~1_combout\ = ( \sm|WideOr0~0_combout\ & ( (\sm|initx~0_combout\) # (\dp|Add3~5_sumout\) ) ) # ( !\sm|WideOr0~0_combout\ & ( \dp|xf\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111011101110111011100001111000011110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add3~5_sumout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xf\(1),
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	combout => \dp|xf~1_combout\);

-- Location: FF_X30_Y47_N22
\dp|xf[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~1_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf[1]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y47_N6
\dp|Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~9_sumout\ = SUM(( \dp|xf[2]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~6\ ))
-- \dp|Add3~10\ = CARRY(( \dp|xf[2]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xf[2]~DUPLICATE_q\,
	cin => \dp|Add3~6\,
	sumout => \dp|Add3~9_sumout\,
	cout => \dp|Add3~10\);

-- Location: LABCELL_X30_Y47_N33
\dp|xf~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~2_combout\ = ( \dp|Add3~9_sumout\ & ( (!\sm|WideOr0~0_combout\ & ((\dp|xf\(2)))) # (\sm|WideOr0~0_combout\ & (!\sm|initx~0_combout\)) ) ) # ( !\dp|Add3~9_sumout\ & ( (\dp|xf\(2) & !\sm|WideOr0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111110011000000111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xf\(2),
	datad => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~9_sumout\,
	combout => \dp|xf~2_combout\);

-- Location: FF_X30_Y47_N58
\dp|xf[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf[2]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y47_N9
\dp|Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~13_sumout\ = SUM(( \dp|xf\(3) ) + ( GND ) + ( \dp|Add3~10\ ))
-- \dp|Add3~14\ = CARRY(( \dp|xf\(3) ) + ( GND ) + ( \dp|Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xf\(3),
	cin => \dp|Add3~10\,
	sumout => \dp|Add3~13_sumout\,
	cout => \dp|Add3~14\);

-- Location: LABCELL_X30_Y47_N30
\dp|xf~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~3_combout\ = ( \dp|xf\(3) & ( (!\sm|WideOr0~0_combout\) # ((!\sm|initx~0_combout\ & \dp|Add3~13_sumout\)) ) ) # ( !\dp|xf\(3) & ( (!\sm|initx~0_combout\ & (\dp|Add3~13_sumout\ & \sm|WideOr0~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110011111111000011001111111100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add3~13_sumout\,
	datad => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_xf\(3),
	combout => \dp|xf~3_combout\);

-- Location: FF_X30_Y47_N8
\dp|xf[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~3_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(3));

-- Location: LABCELL_X31_Y47_N12
\dp|Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~17_sumout\ = SUM(( \dp|xf\(4) ) + ( GND ) + ( \dp|Add3~14\ ))
-- \dp|Add3~18\ = CARRY(( \dp|xf\(4) ) + ( GND ) + ( \dp|Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_xf\(4),
	cin => \dp|Add3~14\,
	sumout => \dp|Add3~17_sumout\,
	cout => \dp|Add3~18\);

-- Location: LABCELL_X30_Y47_N3
\dp|xf~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~4_combout\ = ( \sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ ) ) # ( !\sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ & ( \dp|xf\(4) ) ) ) # ( \sm|WideOr0~0_combout\ & ( !\dp|Add3~17_sumout\ & ( \sm|initx~0_combout\ ) ) ) # ( 
-- !\sm|WideOr0~0_combout\ & ( !\dp|Add3~17_sumout\ & ( \dp|xf\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111100110011001100111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_xf\(4),
	datac => \sm|ALT_INV_initx~0_combout\,
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~17_sumout\,
	combout => \dp|xf~4_combout\);

-- Location: FF_X30_Y47_N14
\dp|xf[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~4_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(4));

-- Location: LABCELL_X31_Y47_N15
\dp|Add3~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~29_sumout\ = SUM(( \dp|xf\(5) ) + ( GND ) + ( \dp|Add3~18\ ))
-- \dp|Add3~30\ = CARRY(( \dp|xf\(5) ) + ( GND ) + ( \dp|Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_xf\(5),
	cin => \dp|Add3~18\,
	sumout => \dp|Add3~29_sumout\,
	cout => \dp|Add3~30\);

-- Location: LABCELL_X30_Y47_N9
\dp|xf~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~7_combout\ = (!\sm|WideOr0~0_combout\ & (\dp|xf\(5))) # (\sm|WideOr0~0_combout\ & (((\dp|Add3~29_sumout\) # (\sm|initx~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001101011111010100110101111101010011010111110101001101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf\(5),
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \sm|ALT_INV_WideOr0~0_combout\,
	datad => \dp|ALT_INV_Add3~29_sumout\,
	combout => \dp|xf~7_combout\);

-- Location: FF_X31_Y47_N41
\dp|xf[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~7_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(5));

-- Location: LABCELL_X31_Y47_N18
\dp|Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~25_sumout\ = SUM(( \dp|xf\(6) ) + ( GND ) + ( \dp|Add3~30\ ))
-- \dp|Add3~26\ = CARRY(( \dp|xf\(6) ) + ( GND ) + ( \dp|Add3~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_xf\(6),
	cin => \dp|Add3~30\,
	sumout => \dp|Add3~25_sumout\,
	cout => \dp|Add3~26\);

-- Location: LABCELL_X30_Y47_N6
\dp|xf~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~6_combout\ = ( \dp|Add3~25_sumout\ & ( (!\sm|WideOr0~0_combout\ & ((\dp|xf\(6)))) # (\sm|WideOr0~0_combout\ & (!\sm|initx~0_combout\)) ) ) # ( !\dp|Add3~25_sumout\ & ( (\dp|xf\(6) & !\sm|WideOr0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111110011000000111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xf\(6),
	datad => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~25_sumout\,
	combout => \dp|xf~6_combout\);

-- Location: FF_X30_Y47_N50
\dp|xf[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|xf~5_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xf\(7));

-- Location: LABCELL_X31_Y47_N21
\dp|Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~21_sumout\ = SUM(( \dp|xf\(7) ) + ( GND ) + ( \dp|Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_xf\(7),
	cin => \dp|Add3~26\,
	sumout => \dp|Add3~21_sumout\);

-- Location: LABCELL_X30_Y47_N42
\dp|xf~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~5_combout\ = ( \sm|initx~0_combout\ & ( (!\sm|WideOr0~0_combout\ & \dp|xf\(7)) ) ) # ( !\sm|initx~0_combout\ & ( (!\sm|WideOr0~0_combout\ & (\dp|xf\(7))) # (\sm|WideOr0~0_combout\ & ((\dp|Add3~21_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001011111000010100101111100001010000010100000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr0~0_combout\,
	datac => \dp|ALT_INV_xf\(7),
	datad => \dp|ALT_INV_Add3~21_sumout\,
	dataf => \sm|ALT_INV_initx~0_combout\,
	combout => \dp|xf~5_combout\);

-- Location: LABCELL_X30_Y47_N18
\dp|Equal3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal3~0_combout\ = ( \sm|WideOr0~0_combout\ & ( \dp|xf\(0) & ( (\dp|Add3~5_sumout\ & (!\sm|initx~0_combout\ & !\dp|Add3~1_sumout\)) ) ) ) # ( \sm|WideOr0~0_combout\ & ( !\dp|xf\(0) & ( (\dp|Add3~5_sumout\ & (!\sm|initx~0_combout\ & 
-- !\dp|Add3~1_sumout\)) ) ) ) # ( !\sm|WideOr0~0_combout\ & ( !\dp|xf\(0) & ( \dp|xf[1]~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111010000000100000000000000000000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add3~5_sumout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add3~1_sumout\,
	datad => \dp|ALT_INV_xf[1]~DUPLICATE_q\,
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_xf\(0),
	combout => \dp|Equal3~0_combout\);

-- Location: LABCELL_X30_Y47_N36
\dp|Equal3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal3~1_combout\ = ( !\sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ & ( (\dp|xf\(5) & !\dp|xf\(4)) ) ) ) # ( \sm|WideOr0~0_combout\ & ( !\dp|Add3~17_sumout\ & ( (\dp|Add3~29_sumout\ & !\sm|initx~0_combout\) ) ) ) # ( !\sm|WideOr0~0_combout\ & ( 
-- !\dp|Add3~17_sumout\ & ( (\dp|xf\(5) & !\dp|xf\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000010001000100010000001111000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add3~29_sumout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xf\(5),
	datad => \dp|ALT_INV_xf\(4),
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~17_sumout\,
	combout => \dp|Equal3~1_combout\);

-- Location: LABCELL_X30_Y47_N12
\dp|Equal3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal3~2_combout\ = ( \dp|Equal3~1_combout\ & ( \dp|xf~2_combout\ & ( (\dp|xf~6_combout\ & (!\dp|xf~5_combout\ & (\dp|Equal3~0_combout\ & \dp|xf~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf~6_combout\,
	datab => \dp|ALT_INV_xf~5_combout\,
	datac => \dp|ALT_INV_Equal3~0_combout\,
	datad => \dp|ALT_INV_xf~3_combout\,
	datae => \dp|ALT_INV_Equal3~1_combout\,
	dataf => \dp|ALT_INV_xf~2_combout\,
	combout => \dp|Equal3~2_combout\);

-- Location: FF_X30_Y47_N5
\dp|xdone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \dp|Equal3~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xdone~q\);

-- Location: LABCELL_X30_Y47_N27
\dp|Add4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~2_combout\ = ( \sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ & ( (\dp|Add3~29_sumout\) # (\sm|initx~0_combout\) ) ) ) # ( !\sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ & ( !\dp|xf\(5) $ (\dp|xf\(4)) ) ) ) # ( \sm|WideOr0~0_combout\ & ( 
-- !\dp|Add3~17_sumout\ & ( (!\dp|Add3~29_sumout\) # (\sm|initx~0_combout\) ) ) ) # ( !\sm|WideOr0~0_combout\ & ( !\dp|Add3~17_sumout\ & ( !\dp|xf\(5) $ (\dp|xf\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001100110011001111111110000111110011001100110010000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf\(5),
	datab => \dp|ALT_INV_xf\(4),
	datac => \sm|ALT_INV_initx~0_combout\,
	datad => \dp|ALT_INV_Add3~29_sumout\,
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~17_sumout\,
	combout => \dp|Add4~2_combout\);

-- Location: LABCELL_X31_Y47_N24
\dp|Add4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~0_combout\ = ( !\sm|WideOr0~0_combout\ & ( \dp|Add3~17_sumout\ & ( (!\dp|xf\(5) & !\dp|xf\(4)) ) ) ) # ( \sm|WideOr0~0_combout\ & ( !\dp|Add3~17_sumout\ & ( (!\dp|Add3~29_sumout\ & !\sm|initx~0_combout\) ) ) ) # ( !\sm|WideOr0~0_combout\ & ( 
-- !\dp|Add3~17_sumout\ & ( (!\dp|xf\(5) & !\dp|xf\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000110011000000000010100000101000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf\(5),
	datab => \dp|ALT_INV_Add3~29_sumout\,
	datac => \dp|ALT_INV_xf\(4),
	datad => \sm|ALT_INV_initx~0_combout\,
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_Add3~17_sumout\,
	combout => \dp|Add4~0_combout\);

-- Location: LABCELL_X31_Y47_N42
\dp|xf~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~8_combout\ = ( !\sm|initx~0_combout\ & ( \sm|WideOr0~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \sm|ALT_INV_initx~0_combout\,
	combout => \dp|xf~8_combout\);

-- Location: LABCELL_X31_Y47_N51
\dp|xf~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~10_combout\ = ( !\sm|WideOr0~0_combout\ & ( \dp|xf\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_xf\(6),
	combout => \dp|xf~10_combout\);

-- Location: LABCELL_X31_Y47_N45
\dp|Add4~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~3_combout\ = ( \dp|Add3~25_sumout\ & ( !\dp|Add4~0_combout\ $ (((\dp|xf~10_combout\) # (\dp|xf~8_combout\))) ) ) # ( !\dp|Add3~25_sumout\ & ( !\dp|Add4~0_combout\ $ (\dp|xf~10_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101001010101101010100101010110100101010101011010010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add4~0_combout\,
	datac => \dp|ALT_INV_xf~8_combout\,
	datad => \dp|ALT_INV_xf~10_combout\,
	dataf => \dp|ALT_INV_Add3~25_sumout\,
	combout => \dp|Add4~3_combout\);

-- Location: LABCELL_X31_Y47_N54
\dp|xf~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xf~9_combout\ = ( \dp|xf\(7) & ( !\sm|WideOr0~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \sm|ALT_INV_WideOr0~0_combout\,
	dataf => \dp|ALT_INV_xf\(7),
	combout => \dp|xf~9_combout\);

-- Location: LABCELL_X31_Y47_N36
\dp|Add4~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~4_combout\ = ( \dp|Add3~21_sumout\ & ( \dp|Add3~25_sumout\ & ( (!\dp|xf~8_combout\ & (!\dp|xf~9_combout\ $ (((!\dp|Add4~0_combout\ & \dp|xf~10_combout\))))) # (\dp|xf~8_combout\ & (((!\dp|Add4~0_combout\)))) ) ) ) # ( !\dp|Add3~21_sumout\ & ( 
-- \dp|Add3~25_sumout\ & ( !\dp|xf~9_combout\ $ (((!\dp|Add4~0_combout\ & ((\dp|xf~10_combout\) # (\dp|xf~8_combout\))))) ) ) ) # ( \dp|Add3~21_sumout\ & ( !\dp|Add3~25_sumout\ & ( (!\dp|xf~9_combout\ & (!\dp|xf~8_combout\ $ (((!\dp|Add4~0_combout\ & 
-- \dp|xf~10_combout\))))) # (\dp|xf~9_combout\ & (((!\dp|Add4~0_combout\ & \dp|xf~10_combout\)))) ) ) ) # ( !\dp|Add3~21_sumout\ & ( !\dp|Add3~25_sumout\ & ( !\dp|xf~9_combout\ $ (((!\dp|Add4~0_combout\ & \dp|xf~10_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101001011010100010000111100010011010010110101011100001111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf~9_combout\,
	datab => \dp|ALT_INV_xf~8_combout\,
	datac => \dp|ALT_INV_Add4~0_combout\,
	datad => \dp|ALT_INV_xf~10_combout\,
	datae => \dp|ALT_INV_Add3~21_sumout\,
	dataf => \dp|ALT_INV_Add3~25_sumout\,
	combout => \dp|Add4~4_combout\);

-- Location: LABCELL_X31_Y47_N30
\dp|Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~1_combout\ = ( \dp|Add3~21_sumout\ & ( \dp|Add3~25_sumout\ & ( (!\dp|xf~9_combout\ & (!\dp|xf~8_combout\ & ((!\dp|xf~10_combout\) # (\dp|Add4~0_combout\)))) ) ) ) # ( !\dp|Add3~21_sumout\ & ( \dp|Add3~25_sumout\ & ( (!\dp|xf~9_combout\ & 
-- (((!\dp|xf~8_combout\ & !\dp|xf~10_combout\)) # (\dp|Add4~0_combout\))) ) ) ) # ( \dp|Add3~21_sumout\ & ( !\dp|Add3~25_sumout\ & ( (!\dp|xf~9_combout\ & (!\dp|xf~8_combout\ & ((!\dp|xf~10_combout\) # (\dp|Add4~0_combout\)))) ) ) ) # ( !\dp|Add3~21_sumout\ 
-- & ( !\dp|Add3~25_sumout\ & ( (!\dp|xf~9_combout\ & ((!\dp|xf~10_combout\) # (\dp|Add4~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101000001010100010000000100010001010000010101000100000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xf~9_combout\,
	datab => \dp|ALT_INV_xf~8_combout\,
	datac => \dp|ALT_INV_Add4~0_combout\,
	datad => \dp|ALT_INV_xf~10_combout\,
	datae => \dp|ALT_INV_Add3~21_sumout\,
	dataf => \dp|ALT_INV_Add3~25_sumout\,
	combout => \dp|Add4~1_combout\);

-- Location: DSP_X32_Y49_N0
\dp|Mult0~477\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 18,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 18,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m18x18_full",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \dp|Mult0~477_AX_bus\,
	ay => \dp|Mult0~477_AY_bus\,
	resulta => \dp|Mult0~477_RESULTA_bus\);

-- Location: MLABCELL_X28_Y47_N9
\dp|yf~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yf~9_combout\ = ( \dp|Add2~9_sumout\ & ( (\sm|initx~0_combout\) # (\dp|yf[4]~DUPLICATE_q\) ) ) # ( !\dp|Add2~9_sumout\ & ( (\dp|yf[4]~DUPLICATE_q\ & !\sm|initx~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010100000101000001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf[4]~DUPLICATE_q\,
	datac => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_Add2~9_sumout\,
	combout => \dp|yf~9_combout\);

-- Location: MLABCELL_X28_Y47_N27
\dp|Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~1_combout\ = ( \dp|Add2~13_sumout\ & ( (!\sm|initx~0_combout\ & (\dp|yf\(2) & (\dp|yf\(3)))) # (\sm|initx~0_combout\ & (((\dp|Add2~25_sumout\)))) ) ) # ( !\dp|Add2~13_sumout\ & ( (\dp|yf\(2) & (!\sm|initx~0_combout\ & \dp|yf\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010000000100001101110000010000110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf\(2),
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_yf\(3),
	datad => \dp|ALT_INV_Add2~25_sumout\,
	dataf => \dp|ALT_INV_Add2~13_sumout\,
	combout => \dp|Add5~1_combout\);

-- Location: MLABCELL_X28_Y47_N57
\dp|Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~5_combout\ = ( \dp|Add5~1_combout\ & ( \dp|Add2~1_sumout\ & ( (!\dp|yf~0_combout\ & (!\dp|yf~1_combout\ & ((!\dp|yf~9_combout\) # (!\dp|yf~3_combout\)))) ) ) ) # ( !\dp|Add5~1_combout\ & ( \dp|Add2~1_sumout\ & ( (!\dp|yf~0_combout\ & 
-- !\dp|yf~1_combout\) ) ) ) # ( \dp|Add5~1_combout\ & ( !\dp|Add2~1_sumout\ & ( (!\dp|yf~1_combout\ & ((!\dp|yf~9_combout\) # (!\dp|yf~3_combout\))) ) ) ) # ( !\dp|Add5~1_combout\ & ( !\dp|Add2~1_sumout\ & ( !\dp|yf~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100000010001000100010001000100010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf~0_combout\,
	datab => \dp|ALT_INV_yf~1_combout\,
	datac => \dp|ALT_INV_yf~9_combout\,
	datad => \dp|ALT_INV_yf~3_combout\,
	datae => \dp|ALT_INV_Add5~1_combout\,
	dataf => \dp|ALT_INV_Add2~1_sumout\,
	combout => \dp|Add5~5_combout\);

-- Location: MLABCELL_X28_Y48_N54
\dp|Add5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~0_combout\ = ( \sm|initx~0_combout\ & ( \dp|Add2~13_sumout\ & ( (!\sm|Decoder0~0_combout\ & !\dp|Add2~25_sumout\) ) ) ) # ( !\sm|initx~0_combout\ & ( \dp|Add2~13_sumout\ & ( (!\sm|Decoder0~0_combout\ & (!\dp|yf[2]~DUPLICATE_q\ $ (!\dp|yf\(3)))) ) 
-- ) ) # ( \sm|initx~0_combout\ & ( !\dp|Add2~13_sumout\ & ( (!\sm|Decoder0~0_combout\ & \dp|Add2~25_sumout\) ) ) ) # ( !\sm|initx~0_combout\ & ( !\dp|Add2~13_sumout\ & ( (!\sm|Decoder0~0_combout\ & (!\dp|yf[2]~DUPLICATE_q\ $ (!\dp|yf\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110000001100000000000001111000001100000011000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf[2]~DUPLICATE_q\,
	datab => \dp|ALT_INV_yf\(3),
	datac => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_Add2~25_sumout\,
	datae => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_Add2~13_sumout\,
	combout => \dp|Add5~0_combout\);

-- Location: MLABCELL_X28_Y47_N3
\dp|Add5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~2_combout\ = ( \dp|yf[4]~DUPLICATE_q\ & ( (!\sm|Decoder0~0_combout\ & (!\dp|Add5~1_combout\ $ (((\sm|initx~0_combout\ & !\dp|Add2~9_sumout\))))) ) ) # ( !\dp|yf[4]~DUPLICATE_q\ & ( (!\sm|Decoder0~0_combout\ & (!\dp|Add5~1_combout\ $ 
-- (((!\sm|initx~0_combout\) # (!\dp|Add2~9_sumout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010011001000000001001100100010001100010000001000110001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initx~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add2~9_sumout\,
	datad => \dp|ALT_INV_Add5~1_combout\,
	dataf => \dp|ALT_INV_yf[4]~DUPLICATE_q\,
	combout => \dp|Add5~2_combout\);

-- Location: MLABCELL_X28_Y47_N21
\dp|Add5~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~3_combout\ = ( \dp|Add2~5_sumout\ & ( \dp|yf~9_combout\ & ( (!\dp|Add5~1_combout\ $ (((!\dp|yf\(5) & !\sm|initx~0_combout\)))) # (\sm|Decoder0~0_combout\) ) ) ) # ( !\dp|Add2~5_sumout\ & ( \dp|yf~9_combout\ & ( (!\dp|Add5~1_combout\ $ 
-- (((!\dp|yf\(5)) # (\sm|initx~0_combout\)))) # (\sm|Decoder0~0_combout\) ) ) ) # ( \dp|Add2~5_sumout\ & ( !\dp|yf~9_combout\ & ( ((\sm|initx~0_combout\) # (\dp|yf\(5))) # (\sm|Decoder0~0_combout\) ) ) ) # ( !\dp|Add2~5_sumout\ & ( !\dp|yf~9_combout\ & ( 
-- ((\dp|yf\(5) & !\sm|initx~0_combout\)) # (\sm|Decoder0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100110011001111111111111101111011011101110111101110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add5~1_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_yf\(5),
	datad => \sm|ALT_INV_initx~0_combout\,
	datae => \dp|ALT_INV_Add2~5_sumout\,
	dataf => \dp|ALT_INV_yf~9_combout\,
	combout => \dp|Add5~3_combout\);

-- Location: LABCELL_X27_Y47_N54
\dp|Add5~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~4_combout\ = ( \dp|yf~9_combout\ & ( \dp|Add5~1_combout\ & ( !\dp|yf~3_combout\ $ ((((\dp|yf~0_combout\ & \dp|Add2~1_sumout\)) # (\dp|yf~1_combout\))) ) ) ) # ( !\dp|yf~9_combout\ & ( \dp|Add5~1_combout\ & ( (!\dp|yf~1_combout\ & 
-- ((!\dp|yf~0_combout\) # (!\dp|Add2~1_sumout\))) ) ) ) # ( \dp|yf~9_combout\ & ( !\dp|Add5~1_combout\ & ( (!\dp|yf~1_combout\ & ((!\dp|yf~0_combout\) # (!\dp|Add2~1_sumout\))) ) ) ) # ( !\dp|yf~9_combout\ & ( !\dp|Add5~1_combout\ & ( (!\dp|yf~1_combout\ & 
-- ((!\dp|yf~0_combout\) # (!\dp|Add2~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011000000110011001100000011001100110000001001100110010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yf~3_combout\,
	datab => \dp|ALT_INV_yf~1_combout\,
	datac => \dp|ALT_INV_yf~0_combout\,
	datad => \dp|ALT_INV_Add2~1_sumout\,
	datae => \dp|ALT_INV_yf~9_combout\,
	dataf => \dp|ALT_INV_Add5~1_combout\,
	combout => \dp|Add5~4_combout\);

-- Location: DSP_X32_Y47_N0
\dp|Mult1~405\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 14,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 18,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 14,
	by_clock => "none",
	by_use_scan_in => "false",
	by_width => 18,
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m18x18_sumof2",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \dp|Mult1~405_AX_bus\,
	ay => \dp|Mult1~405_AY_bus\,
	bx => \dp|Mult1~405_BX_bus\,
	by => \dp|Mult1~405_BY_bus\,
	resulta => \dp|Mult1~405_RESULTA_bus\);

-- Location: DSP_X32_Y45_N0
\dp|Mult1~20\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 18,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 18,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	by_clock => "none",
	by_use_scan_in => "false",
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m18x18_full",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \dp|Mult1~20_AX_bus\,
	ay => \dp|Mult1~20_AY_bus\,
	resulta => \dp|Mult1~20_RESULTA_bus\);

-- Location: LABCELL_X31_Y46_N0
\dp|Mult1~382\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~382_sumout\ = SUM(( !\dp|Mult0~495\ $ (!\dp|Mult1~405_resulta\ $ (\dp|Mult1~38\)) ) + ( !VCC ) + ( !VCC ))
-- \dp|Mult1~383\ = CARRY(( !\dp|Mult0~495\ $ (!\dp|Mult1~405_resulta\ $ (\dp|Mult1~38\)) ) + ( !VCC ) + ( !VCC ))
-- \dp|Mult1~384\ = SHARE((!\dp|Mult0~495\ & (\dp|Mult1~405_resulta\ & \dp|Mult1~38\)) # (\dp|Mult0~495\ & ((\dp|Mult1~38\) # (\dp|Mult1~405_resulta\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~495\,
	datac => \dp|ALT_INV_Mult1~405_resulta\,
	datad => \dp|ALT_INV_Mult1~38\,
	cin => GND,
	sharein => GND,
	sumout => \dp|Mult1~382_sumout\,
	cout => \dp|Mult1~383\,
	shareout => \dp|Mult1~384\);

-- Location: LABCELL_X31_Y46_N3
\dp|Mult1~366\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~366_sumout\ = SUM(( !\dp|Mult0~496\ $ (!\dp|Mult1~39\ $ (\dp|Mult1~406\)) ) + ( \dp|Mult1~384\ ) + ( \dp|Mult1~383\ ))
-- \dp|Mult1~367\ = CARRY(( !\dp|Mult0~496\ $ (!\dp|Mult1~39\ $ (\dp|Mult1~406\)) ) + ( \dp|Mult1~384\ ) + ( \dp|Mult1~383\ ))
-- \dp|Mult1~368\ = SHARE((!\dp|Mult0~496\ & (\dp|Mult1~39\ & \dp|Mult1~406\)) # (\dp|Mult0~496\ & ((\dp|Mult1~406\) # (\dp|Mult1~39\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~496\,
	datac => \dp|ALT_INV_Mult1~39\,
	datad => \dp|ALT_INV_Mult1~406\,
	cin => \dp|Mult1~383\,
	sharein => \dp|Mult1~384\,
	sumout => \dp|Mult1~366_sumout\,
	cout => \dp|Mult1~367\,
	shareout => \dp|Mult1~368\);

-- Location: LABCELL_X31_Y46_N6
\dp|Mult1~354\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~354_sumout\ = SUM(( !\dp|Mult1~40\ $ (!\dp|Mult0~497\ $ (\dp|Mult1~407\)) ) + ( \dp|Mult1~368\ ) + ( \dp|Mult1~367\ ))
-- \dp|Mult1~355\ = CARRY(( !\dp|Mult1~40\ $ (!\dp|Mult0~497\ $ (\dp|Mult1~407\)) ) + ( \dp|Mult1~368\ ) + ( \dp|Mult1~367\ ))
-- \dp|Mult1~356\ = SHARE((!\dp|Mult1~40\ & (\dp|Mult0~497\ & \dp|Mult1~407\)) # (\dp|Mult1~40\ & ((\dp|Mult1~407\) # (\dp|Mult0~497\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~40\,
	datac => \dp|ALT_INV_Mult0~497\,
	datad => \dp|ALT_INV_Mult1~407\,
	cin => \dp|Mult1~367\,
	sharein => \dp|Mult1~368\,
	sumout => \dp|Mult1~354_sumout\,
	cout => \dp|Mult1~355\,
	shareout => \dp|Mult1~356\);

-- Location: LABCELL_X31_Y46_N9
\dp|Mult1~358\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~358_sumout\ = SUM(( !\dp|Mult1~41\ $ (!\dp|Mult1~408\ $ (\dp|Mult0~498\)) ) + ( \dp|Mult1~356\ ) + ( \dp|Mult1~355\ ))
-- \dp|Mult1~359\ = CARRY(( !\dp|Mult1~41\ $ (!\dp|Mult1~408\ $ (\dp|Mult0~498\)) ) + ( \dp|Mult1~356\ ) + ( \dp|Mult1~355\ ))
-- \dp|Mult1~360\ = SHARE((!\dp|Mult1~41\ & (\dp|Mult1~408\ & \dp|Mult0~498\)) # (\dp|Mult1~41\ & ((\dp|Mult0~498\) # (\dp|Mult1~408\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~41\,
	datac => \dp|ALT_INV_Mult1~408\,
	datad => \dp|ALT_INV_Mult0~498\,
	cin => \dp|Mult1~355\,
	sharein => \dp|Mult1~356\,
	sumout => \dp|Mult1~358_sumout\,
	cout => \dp|Mult1~359\,
	shareout => \dp|Mult1~360\);

-- Location: LABCELL_X31_Y46_N12
\dp|Mult1~362\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~362_sumout\ = SUM(( !\dp|Mult0~499\ $ (!\dp|Mult1~42\ $ (\dp|Mult1~409\)) ) + ( \dp|Mult1~360\ ) + ( \dp|Mult1~359\ ))
-- \dp|Mult1~363\ = CARRY(( !\dp|Mult0~499\ $ (!\dp|Mult1~42\ $ (\dp|Mult1~409\)) ) + ( \dp|Mult1~360\ ) + ( \dp|Mult1~359\ ))
-- \dp|Mult1~364\ = SHARE((!\dp|Mult0~499\ & (\dp|Mult1~42\ & \dp|Mult1~409\)) # (\dp|Mult0~499\ & ((\dp|Mult1~409\) # (\dp|Mult1~42\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~499\,
	datac => \dp|ALT_INV_Mult1~42\,
	datad => \dp|ALT_INV_Mult1~409\,
	cin => \dp|Mult1~359\,
	sharein => \dp|Mult1~360\,
	sumout => \dp|Mult1~362_sumout\,
	cout => \dp|Mult1~363\,
	shareout => \dp|Mult1~364\);

-- Location: LABCELL_X31_Y46_N15
\dp|Mult1~386\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~386_sumout\ = SUM(( !\dp|Mult0~500\ $ (!\dp|Mult1~43\ $ (\dp|Mult1~410\)) ) + ( \dp|Mult1~364\ ) + ( \dp|Mult1~363\ ))
-- \dp|Mult1~387\ = CARRY(( !\dp|Mult0~500\ $ (!\dp|Mult1~43\ $ (\dp|Mult1~410\)) ) + ( \dp|Mult1~364\ ) + ( \dp|Mult1~363\ ))
-- \dp|Mult1~388\ = SHARE((!\dp|Mult0~500\ & (\dp|Mult1~43\ & \dp|Mult1~410\)) # (\dp|Mult0~500\ & ((\dp|Mult1~410\) # (\dp|Mult1~43\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~500\,
	datac => \dp|ALT_INV_Mult1~43\,
	datad => \dp|ALT_INV_Mult1~410\,
	cin => \dp|Mult1~363\,
	sharein => \dp|Mult1~364\,
	sumout => \dp|Mult1~386_sumout\,
	cout => \dp|Mult1~387\,
	shareout => \dp|Mult1~388\);

-- Location: LABCELL_X31_Y46_N18
\dp|Mult1~370\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~370_sumout\ = SUM(( !\dp|Mult1~44\ $ (!\dp|Mult1~411\ $ (\dp|Mult0~501\)) ) + ( \dp|Mult1~388\ ) + ( \dp|Mult1~387\ ))
-- \dp|Mult1~371\ = CARRY(( !\dp|Mult1~44\ $ (!\dp|Mult1~411\ $ (\dp|Mult0~501\)) ) + ( \dp|Mult1~388\ ) + ( \dp|Mult1~387\ ))
-- \dp|Mult1~372\ = SHARE((!\dp|Mult1~44\ & (\dp|Mult1~411\ & \dp|Mult0~501\)) # (\dp|Mult1~44\ & ((\dp|Mult0~501\) # (\dp|Mult1~411\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~44\,
	datac => \dp|ALT_INV_Mult1~411\,
	datad => \dp|ALT_INV_Mult0~501\,
	cin => \dp|Mult1~387\,
	sharein => \dp|Mult1~388\,
	sumout => \dp|Mult1~370_sumout\,
	cout => \dp|Mult1~371\,
	shareout => \dp|Mult1~372\);

-- Location: LABCELL_X31_Y46_N21
\dp|Mult1~374\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~374_sumout\ = SUM(( !\dp|Mult1~45\ $ (!\dp|Mult0~502\ $ (\dp|Mult1~412\)) ) + ( \dp|Mult1~372\ ) + ( \dp|Mult1~371\ ))
-- \dp|Mult1~375\ = CARRY(( !\dp|Mult1~45\ $ (!\dp|Mult0~502\ $ (\dp|Mult1~412\)) ) + ( \dp|Mult1~372\ ) + ( \dp|Mult1~371\ ))
-- \dp|Mult1~376\ = SHARE((!\dp|Mult1~45\ & (\dp|Mult0~502\ & \dp|Mult1~412\)) # (\dp|Mult1~45\ & ((\dp|Mult1~412\) # (\dp|Mult0~502\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~45\,
	datac => \dp|ALT_INV_Mult0~502\,
	datad => \dp|ALT_INV_Mult1~412\,
	cin => \dp|Mult1~371\,
	sharein => \dp|Mult1~372\,
	sumout => \dp|Mult1~374_sumout\,
	cout => \dp|Mult1~375\,
	shareout => \dp|Mult1~376\);

-- Location: LABCELL_X31_Y46_N24
\dp|Mult1~378\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~378_sumout\ = SUM(( !\dp|Mult0~503\ $ (!\dp|Mult1~413\ $ (\dp|Mult1~46\)) ) + ( \dp|Mult1~376\ ) + ( \dp|Mult1~375\ ))
-- \dp|Mult1~379\ = CARRY(( !\dp|Mult0~503\ $ (!\dp|Mult1~413\ $ (\dp|Mult1~46\)) ) + ( \dp|Mult1~376\ ) + ( \dp|Mult1~375\ ))
-- \dp|Mult1~380\ = SHARE((!\dp|Mult0~503\ & (\dp|Mult1~413\ & \dp|Mult1~46\)) # (\dp|Mult0~503\ & ((\dp|Mult1~46\) # (\dp|Mult1~413\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~503\,
	datac => \dp|ALT_INV_Mult1~413\,
	datad => \dp|ALT_INV_Mult1~46\,
	cin => \dp|Mult1~375\,
	sharein => \dp|Mult1~376\,
	sumout => \dp|Mult1~378_sumout\,
	cout => \dp|Mult1~379\,
	shareout => \dp|Mult1~380\);

-- Location: LABCELL_X31_Y46_N27
\dp|Mult1~390\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~390_sumout\ = SUM(( !\dp|Mult1~47\ $ (!\dp|Mult1~414\ $ (\dp|Mult0~504\)) ) + ( \dp|Mult1~380\ ) + ( \dp|Mult1~379\ ))
-- \dp|Mult1~391\ = CARRY(( !\dp|Mult1~47\ $ (!\dp|Mult1~414\ $ (\dp|Mult0~504\)) ) + ( \dp|Mult1~380\ ) + ( \dp|Mult1~379\ ))
-- \dp|Mult1~392\ = SHARE((!\dp|Mult1~47\ & (\dp|Mult1~414\ & \dp|Mult0~504\)) # (\dp|Mult1~47\ & ((\dp|Mult0~504\) # (\dp|Mult1~414\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~47\,
	datac => \dp|ALT_INV_Mult1~414\,
	datad => \dp|ALT_INV_Mult0~504\,
	cin => \dp|Mult1~379\,
	sharein => \dp|Mult1~380\,
	sumout => \dp|Mult1~390_sumout\,
	cout => \dp|Mult1~391\,
	shareout => \dp|Mult1~392\);

-- Location: LABCELL_X31_Y46_N30
\dp|Mult1~394\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~394_sumout\ = SUM(( !\dp|Mult1~48\ $ (!\dp|Mult1~415\ $ (\dp|Mult0~505\)) ) + ( \dp|Mult1~392\ ) + ( \dp|Mult1~391\ ))
-- \dp|Mult1~395\ = CARRY(( !\dp|Mult1~48\ $ (!\dp|Mult1~415\ $ (\dp|Mult0~505\)) ) + ( \dp|Mult1~392\ ) + ( \dp|Mult1~391\ ))
-- \dp|Mult1~396\ = SHARE((!\dp|Mult1~48\ & (\dp|Mult1~415\ & \dp|Mult0~505\)) # (\dp|Mult1~48\ & ((\dp|Mult0~505\) # (\dp|Mult1~415\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~48\,
	datac => \dp|ALT_INV_Mult1~415\,
	datad => \dp|ALT_INV_Mult0~505\,
	cin => \dp|Mult1~391\,
	sharein => \dp|Mult1~392\,
	sumout => \dp|Mult1~394_sumout\,
	cout => \dp|Mult1~395\,
	shareout => \dp|Mult1~396\);

-- Location: LABCELL_X31_Y46_N33
\dp|Mult1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~1_sumout\ = SUM(( !\dp|Mult0~506\ $ (!\dp|Mult1~416\ $ (\dp|Mult1~49\)) ) + ( \dp|Mult1~396\ ) + ( \dp|Mult1~395\ ))
-- \dp|Mult1~2\ = CARRY(( !\dp|Mult0~506\ $ (!\dp|Mult1~416\ $ (\dp|Mult1~49\)) ) + ( \dp|Mult1~396\ ) + ( \dp|Mult1~395\ ))
-- \dp|Mult1~3\ = SHARE((!\dp|Mult0~506\ & (\dp|Mult1~416\ & \dp|Mult1~49\)) # (\dp|Mult0~506\ & ((\dp|Mult1~49\) # (\dp|Mult1~416\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~506\,
	datac => \dp|ALT_INV_Mult1~416\,
	datad => \dp|ALT_INV_Mult1~49\,
	cin => \dp|Mult1~395\,
	sharein => \dp|Mult1~396\,
	sumout => \dp|Mult1~1_sumout\,
	cout => \dp|Mult1~2\,
	shareout => \dp|Mult1~3\);

-- Location: DSP_X32_Y51_N0
\dp|Mult0~136\ : cyclonev_mac
-- pragma translate_off
GENERIC MAP (
	accumulate_clock => "none",
	ax_clock => "none",
	ax_width => 14,
	ay_scan_in_clock => "none",
	ay_scan_in_width => 18,
	ay_use_scan_in => "false",
	az_clock => "none",
	bx_clock => "none",
	bx_width => 14,
	by_clock => "none",
	by_use_scan_in => "false",
	by_width => 18,
	bz_clock => "none",
	coef_a_0 => 0,
	coef_a_1 => 0,
	coef_a_2 => 0,
	coef_a_3 => 0,
	coef_a_4 => 0,
	coef_a_5 => 0,
	coef_a_6 => 0,
	coef_a_7 => 0,
	coef_b_0 => 0,
	coef_b_1 => 0,
	coef_b_2 => 0,
	coef_b_3 => 0,
	coef_b_4 => 0,
	coef_b_5 => 0,
	coef_b_6 => 0,
	coef_b_7 => 0,
	coef_sel_a_clock => "none",
	coef_sel_b_clock => "none",
	delay_scan_out_ay => "false",
	delay_scan_out_by => "false",
	enable_double_accum => "false",
	load_const_clock => "none",
	load_const_value => 0,
	mode_sub_location => 0,
	negate_clock => "none",
	operand_source_max => "input",
	operand_source_may => "input",
	operand_source_mbx => "input",
	operand_source_mby => "input",
	operation_mode => "m18x18_sumof2",
	output_clock => "none",
	preadder_subtract_a => "false",
	preadder_subtract_b => "false",
	result_a_width => 64,
	signed_max => "false",
	signed_may => "false",
	signed_mbx => "false",
	signed_mby => "false",
	sub_clock => "none",
	use_chainadder => "false")
-- pragma translate_on
PORT MAP (
	sub => GND,
	negate => GND,
	ax => \dp|Mult0~136_AX_bus\,
	ay => \dp|Mult0~136_AY_bus\,
	bx => \dp|Mult0~136_BX_bus\,
	by => \dp|Mult0~136_BY_bus\,
	resulta => \dp|Mult0~136_RESULTA_bus\);

-- Location: LABCELL_X30_Y49_N0
\dp|Mult0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~65_sumout\ = SUM(( \dp|Mult1~20_resulta\ ) + ( \dp|Mult0~477_resulta\ ) + ( !VCC ))
-- \dp|Mult0~66\ = CARRY(( \dp|Mult1~20_resulta\ ) + ( \dp|Mult0~477_resulta\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~477_resulta\,
	datad => \dp|ALT_INV_Mult1~20_resulta\,
	cin => GND,
	sumout => \dp|Mult0~65_sumout\,
	cout => \dp|Mult0~66\);

-- Location: LABCELL_X30_Y49_N3
\dp|Mult0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~69_sumout\ = SUM(( \dp|Mult0~478\ ) + ( \dp|Mult1~21\ ) + ( \dp|Mult0~66\ ))
-- \dp|Mult0~70\ = CARRY(( \dp|Mult0~478\ ) + ( \dp|Mult1~21\ ) + ( \dp|Mult0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~478\,
	datac => \dp|ALT_INV_Mult1~21\,
	cin => \dp|Mult0~66\,
	sumout => \dp|Mult0~69_sumout\,
	cout => \dp|Mult0~70\);

-- Location: LABCELL_X30_Y49_N6
\dp|Mult0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~61_sumout\ = SUM(( \dp|Mult0~479\ ) + ( \dp|Mult1~22\ ) + ( \dp|Mult0~70\ ))
-- \dp|Mult0~62\ = CARRY(( \dp|Mult0~479\ ) + ( \dp|Mult1~22\ ) + ( \dp|Mult0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~22\,
	datad => \dp|ALT_INV_Mult0~479\,
	cin => \dp|Mult0~70\,
	sumout => \dp|Mult0~61_sumout\,
	cout => \dp|Mult0~62\);

-- Location: LABCELL_X30_Y49_N9
\dp|Mult0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~49_sumout\ = SUM(( \dp|Mult0~480\ ) + ( \dp|Mult1~23\ ) + ( \dp|Mult0~62\ ))
-- \dp|Mult0~50\ = CARRY(( \dp|Mult0~480\ ) + ( \dp|Mult1~23\ ) + ( \dp|Mult0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~23\,
	datac => \dp|ALT_INV_Mult0~480\,
	cin => \dp|Mult0~62\,
	sumout => \dp|Mult0~49_sumout\,
	cout => \dp|Mult0~50\);

-- Location: LABCELL_X30_Y49_N12
\dp|Mult0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~53_sumout\ = SUM(( \dp|Mult0~481\ ) + ( \dp|Mult1~24\ ) + ( \dp|Mult0~50\ ))
-- \dp|Mult0~54\ = CARRY(( \dp|Mult0~481\ ) + ( \dp|Mult1~24\ ) + ( \dp|Mult0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~24\,
	datac => \dp|ALT_INV_Mult0~481\,
	cin => \dp|Mult0~50\,
	sumout => \dp|Mult0~53_sumout\,
	cout => \dp|Mult0~54\);

-- Location: LABCELL_X30_Y49_N15
\dp|Mult0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~57_sumout\ = SUM(( \dp|Mult0~482\ ) + ( \dp|Mult1~25\ ) + ( \dp|Mult0~54\ ))
-- \dp|Mult0~58\ = CARRY(( \dp|Mult0~482\ ) + ( \dp|Mult1~25\ ) + ( \dp|Mult0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~25\,
	datad => \dp|ALT_INV_Mult0~482\,
	cin => \dp|Mult0~54\,
	sumout => \dp|Mult0~57_sumout\,
	cout => \dp|Mult0~58\);

-- Location: LABCELL_X30_Y49_N18
\dp|Mult0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~41_sumout\ = SUM(( \dp|Mult0~483\ ) + ( \dp|Mult1~26\ ) + ( \dp|Mult0~58\ ))
-- \dp|Mult0~42\ = CARRY(( \dp|Mult0~483\ ) + ( \dp|Mult1~26\ ) + ( \dp|Mult0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~26\,
	datad => \dp|ALT_INV_Mult0~483\,
	cin => \dp|Mult0~58\,
	sumout => \dp|Mult0~41_sumout\,
	cout => \dp|Mult0~42\);

-- Location: LABCELL_X30_Y49_N21
\dp|Mult0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~29_sumout\ = SUM(( \dp|Mult0~484\ ) + ( \dp|Mult1~27\ ) + ( \dp|Mult0~42\ ))
-- \dp|Mult0~30\ = CARRY(( \dp|Mult0~484\ ) + ( \dp|Mult1~27\ ) + ( \dp|Mult0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~27\,
	datad => \dp|ALT_INV_Mult0~484\,
	cin => \dp|Mult0~42\,
	sumout => \dp|Mult0~29_sumout\,
	cout => \dp|Mult0~30\);

-- Location: LABCELL_X30_Y49_N24
\dp|Mult0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~33_sumout\ = SUM(( \dp|Mult0~485\ ) + ( \dp|Mult1~28\ ) + ( \dp|Mult0~30\ ))
-- \dp|Mult0~34\ = CARRY(( \dp|Mult0~485\ ) + ( \dp|Mult1~28\ ) + ( \dp|Mult0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~28\,
	datad => \dp|ALT_INV_Mult0~485\,
	cin => \dp|Mult0~30\,
	sumout => \dp|Mult0~33_sumout\,
	cout => \dp|Mult0~34\);

-- Location: LABCELL_X30_Y49_N27
\dp|Mult0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~37_sumout\ = SUM(( \dp|Mult0~486\ ) + ( \dp|Mult1~29\ ) + ( \dp|Mult0~34\ ))
-- \dp|Mult0~38\ = CARRY(( \dp|Mult0~486\ ) + ( \dp|Mult1~29\ ) + ( \dp|Mult0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~29\,
	datad => \dp|ALT_INV_Mult0~486\,
	cin => \dp|Mult0~34\,
	sumout => \dp|Mult0~37_sumout\,
	cout => \dp|Mult0~38\);

-- Location: LABCELL_X30_Y49_N30
\dp|Mult0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~89_sumout\ = SUM(( \dp|Mult0~487\ ) + ( \dp|Mult1~30\ ) + ( \dp|Mult0~38\ ))
-- \dp|Mult0~90\ = CARRY(( \dp|Mult0~487\ ) + ( \dp|Mult1~30\ ) + ( \dp|Mult0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~30\,
	datac => \dp|ALT_INV_Mult0~487\,
	cin => \dp|Mult0~38\,
	sumout => \dp|Mult0~89_sumout\,
	cout => \dp|Mult0~90\);

-- Location: LABCELL_X30_Y49_N33
\dp|Mult0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~93_sumout\ = SUM(( \dp|Mult0~488\ ) + ( \dp|Mult1~31\ ) + ( \dp|Mult0~90\ ))
-- \dp|Mult0~94\ = CARRY(( \dp|Mult0~488\ ) + ( \dp|Mult1~31\ ) + ( \dp|Mult0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_Mult0~488\,
	dataf => \dp|ALT_INV_Mult1~31\,
	cin => \dp|Mult0~90\,
	sumout => \dp|Mult0~93_sumout\,
	cout => \dp|Mult0~94\);

-- Location: LABCELL_X30_Y49_N36
\dp|Mult0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~97_sumout\ = SUM(( \dp|Mult0~489\ ) + ( \dp|Mult1~32\ ) + ( \dp|Mult0~94\ ))
-- \dp|Mult0~98\ = CARRY(( \dp|Mult0~489\ ) + ( \dp|Mult1~32\ ) + ( \dp|Mult0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~489\,
	datac => \dp|ALT_INV_Mult1~32\,
	cin => \dp|Mult0~94\,
	sumout => \dp|Mult0~97_sumout\,
	cout => \dp|Mult0~98\);

-- Location: LABCELL_X30_Y49_N39
\dp|Mult0~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~101_sumout\ = SUM(( \dp|Mult0~490\ ) + ( \dp|Mult1~33\ ) + ( \dp|Mult0~98\ ))
-- \dp|Mult0~102\ = CARRY(( \dp|Mult0~490\ ) + ( \dp|Mult1~33\ ) + ( \dp|Mult0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~490\,
	datac => \dp|ALT_INV_Mult1~33\,
	cin => \dp|Mult0~98\,
	sumout => \dp|Mult0~101_sumout\,
	cout => \dp|Mult0~102\);

-- Location: LABCELL_X30_Y49_N42
\dp|Mult0~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~105_sumout\ = SUM(( \dp|Mult0~491\ ) + ( \dp|Mult1~34\ ) + ( \dp|Mult0~102\ ))
-- \dp|Mult0~106\ = CARRY(( \dp|Mult0~491\ ) + ( \dp|Mult1~34\ ) + ( \dp|Mult0~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~491\,
	datac => \dp|ALT_INV_Mult1~34\,
	cin => \dp|Mult0~102\,
	sumout => \dp|Mult0~105_sumout\,
	cout => \dp|Mult0~106\);

-- Location: LABCELL_X30_Y49_N45
\dp|Mult0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~45_sumout\ = SUM(( \dp|Mult1~35\ ) + ( \dp|Mult0~492\ ) + ( \dp|Mult0~106\ ))
-- \dp|Mult0~46\ = CARRY(( \dp|Mult1~35\ ) + ( \dp|Mult0~492\ ) + ( \dp|Mult0~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~35\,
	dataf => \dp|ALT_INV_Mult0~492\,
	cin => \dp|Mult0~106\,
	sumout => \dp|Mult0~45_sumout\,
	cout => \dp|Mult0~46\);

-- Location: LABCELL_X30_Y49_N48
\dp|Mult0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~13_sumout\ = SUM(( \dp|Mult0~493\ ) + ( \dp|Mult1~36\ ) + ( \dp|Mult0~46\ ))
-- \dp|Mult0~14\ = CARRY(( \dp|Mult0~493\ ) + ( \dp|Mult1~36\ ) + ( \dp|Mult0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~36\,
	datad => \dp|ALT_INV_Mult0~493\,
	cin => \dp|Mult0~46\,
	sumout => \dp|Mult0~13_sumout\,
	cout => \dp|Mult0~14\);

-- Location: LABCELL_X30_Y49_N51
\dp|Mult0~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~109_sumout\ = SUM(( \dp|Mult0~494\ ) + ( \dp|Mult1~37\ ) + ( \dp|Mult0~14\ ))
-- \dp|Mult0~110\ = CARRY(( \dp|Mult0~494\ ) + ( \dp|Mult1~37\ ) + ( \dp|Mult0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~37\,
	datad => \dp|ALT_INV_Mult0~494\,
	cin => \dp|Mult0~14\,
	sumout => \dp|Mult0~109_sumout\,
	cout => \dp|Mult0~110\);

-- Location: LABCELL_X30_Y49_N54
\dp|Mult0~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~113_sumout\ = SUM(( \dp|Mult0~136_resulta\ ) + ( \dp|Mult1~382_sumout\ ) + ( \dp|Mult0~110\ ))
-- \dp|Mult0~114\ = CARRY(( \dp|Mult0~136_resulta\ ) + ( \dp|Mult1~382_sumout\ ) + ( \dp|Mult0~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~382_sumout\,
	datad => \dp|ALT_INV_Mult0~136_resulta\,
	cin => \dp|Mult0~110\,
	sumout => \dp|Mult0~113_sumout\,
	cout => \dp|Mult0~114\);

-- Location: LABCELL_X30_Y49_N57
\dp|Mult0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~73_sumout\ = SUM(( \dp|Mult0~137\ ) + ( \dp|Mult1~366_sumout\ ) + ( \dp|Mult0~114\ ))
-- \dp|Mult0~74\ = CARRY(( \dp|Mult0~137\ ) + ( \dp|Mult1~366_sumout\ ) + ( \dp|Mult0~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~366_sumout\,
	datad => \dp|ALT_INV_Mult0~137\,
	cin => \dp|Mult0~114\,
	sumout => \dp|Mult0~73_sumout\,
	cout => \dp|Mult0~74\);

-- Location: LABCELL_X30_Y48_N0
\dp|Mult0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~17_sumout\ = SUM(( \dp|Mult0~138\ ) + ( \dp|Mult1~354_sumout\ ) + ( \dp|Mult0~74\ ))
-- \dp|Mult0~18\ = CARRY(( \dp|Mult0~138\ ) + ( \dp|Mult1~354_sumout\ ) + ( \dp|Mult0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~354_sumout\,
	datad => \dp|ALT_INV_Mult0~138\,
	cin => \dp|Mult0~74\,
	sumout => \dp|Mult0~17_sumout\,
	cout => \dp|Mult0~18\);

-- Location: LABCELL_X30_Y48_N3
\dp|Mult0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~21_sumout\ = SUM(( \dp|Mult1~358_sumout\ ) + ( \dp|Mult0~139\ ) + ( \dp|Mult0~18\ ))
-- \dp|Mult0~22\ = CARRY(( \dp|Mult1~358_sumout\ ) + ( \dp|Mult0~139\ ) + ( \dp|Mult0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~139\,
	datac => \dp|ALT_INV_Mult1~358_sumout\,
	cin => \dp|Mult0~18\,
	sumout => \dp|Mult0~21_sumout\,
	cout => \dp|Mult0~22\);

-- Location: LABCELL_X30_Y48_N6
\dp|Mult0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~25_sumout\ = SUM(( \dp|Mult0~140\ ) + ( \dp|Mult1~362_sumout\ ) + ( \dp|Mult0~22\ ))
-- \dp|Mult0~26\ = CARRY(( \dp|Mult0~140\ ) + ( \dp|Mult1~362_sumout\ ) + ( \dp|Mult0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~362_sumout\,
	datad => \dp|ALT_INV_Mult0~140\,
	cin => \dp|Mult0~22\,
	sumout => \dp|Mult0~25_sumout\,
	cout => \dp|Mult0~26\);

-- Location: LABCELL_X30_Y48_N9
\dp|Mult0~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~117_sumout\ = SUM(( \dp|Mult1~386_sumout\ ) + ( \dp|Mult0~141\ ) + ( \dp|Mult0~26\ ))
-- \dp|Mult0~118\ = CARRY(( \dp|Mult1~386_sumout\ ) + ( \dp|Mult0~141\ ) + ( \dp|Mult0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~141\,
	datac => \dp|ALT_INV_Mult1~386_sumout\,
	cin => \dp|Mult0~26\,
	sumout => \dp|Mult0~117_sumout\,
	cout => \dp|Mult0~118\);

-- Location: LABCELL_X30_Y48_N12
\dp|Mult0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~77_sumout\ = SUM(( \dp|Mult1~370_sumout\ ) + ( \dp|Mult0~142\ ) + ( \dp|Mult0~118\ ))
-- \dp|Mult0~78\ = CARRY(( \dp|Mult1~370_sumout\ ) + ( \dp|Mult0~142\ ) + ( \dp|Mult0~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~142\,
	datac => \dp|ALT_INV_Mult1~370_sumout\,
	cin => \dp|Mult0~118\,
	sumout => \dp|Mult0~77_sumout\,
	cout => \dp|Mult0~78\);

-- Location: LABCELL_X30_Y48_N15
\dp|Mult0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~81_sumout\ = SUM(( \dp|Mult0~143\ ) + ( \dp|Mult1~374_sumout\ ) + ( \dp|Mult0~78\ ))
-- \dp|Mult0~82\ = CARRY(( \dp|Mult0~143\ ) + ( \dp|Mult1~374_sumout\ ) + ( \dp|Mult0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~374_sumout\,
	datad => \dp|ALT_INV_Mult0~143\,
	cin => \dp|Mult0~78\,
	sumout => \dp|Mult0~81_sumout\,
	cout => \dp|Mult0~82\);

-- Location: LABCELL_X30_Y48_N18
\dp|Mult0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~85_sumout\ = SUM(( \dp|Mult1~378_sumout\ ) + ( \dp|Mult0~144\ ) + ( \dp|Mult0~82\ ))
-- \dp|Mult0~86\ = CARRY(( \dp|Mult1~378_sumout\ ) + ( \dp|Mult0~144\ ) + ( \dp|Mult0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult0~144\,
	datad => \dp|ALT_INV_Mult1~378_sumout\,
	cin => \dp|Mult0~82\,
	sumout => \dp|Mult0~85_sumout\,
	cout => \dp|Mult0~86\);

-- Location: LABCELL_X30_Y48_N21
\dp|Mult0~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~121_sumout\ = SUM(( \dp|Mult0~145\ ) + ( \dp|Mult1~390_sumout\ ) + ( \dp|Mult0~86\ ))
-- \dp|Mult0~122\ = CARRY(( \dp|Mult0~145\ ) + ( \dp|Mult1~390_sumout\ ) + ( \dp|Mult0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~390_sumout\,
	datad => \dp|ALT_INV_Mult0~145\,
	cin => \dp|Mult0~86\,
	sumout => \dp|Mult0~121_sumout\,
	cout => \dp|Mult0~122\);

-- Location: LABCELL_X30_Y48_N24
\dp|Mult0~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~125_sumout\ = SUM(( \dp|Mult1~394_sumout\ ) + ( \dp|Mult0~146\ ) + ( \dp|Mult0~122\ ))
-- \dp|Mult0~126\ = CARRY(( \dp|Mult1~394_sumout\ ) + ( \dp|Mult0~146\ ) + ( \dp|Mult0~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~394_sumout\,
	datac => \dp|ALT_INV_Mult0~146\,
	cin => \dp|Mult0~122\,
	sumout => \dp|Mult0~125_sumout\,
	cout => \dp|Mult0~126\);

-- Location: LABCELL_X30_Y48_N27
\dp|Mult0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~1_sumout\ = SUM(( \dp|Mult1~1_sumout\ ) + ( \dp|Mult0~147\ ) + ( \dp|Mult0~126\ ))
-- \dp|Mult0~2\ = CARRY(( \dp|Mult1~1_sumout\ ) + ( \dp|Mult0~147\ ) + ( \dp|Mult0~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Mult1~1_sumout\,
	dataf => \dp|ALT_INV_Mult0~147\,
	cin => \dp|Mult0~126\,
	sumout => \dp|Mult0~1_sumout\,
	cout => \dp|Mult0~2\);

-- Location: LABCELL_X31_Y46_N36
\dp|Mult1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~5_sumout\ = SUM(( !\dp|Mult1~50\ $ (!\dp|Mult1~417\ $ (\dp|Mult0~507\)) ) + ( \dp|Mult1~3\ ) + ( \dp|Mult1~2\ ))
-- \dp|Mult1~6\ = CARRY(( !\dp|Mult1~50\ $ (!\dp|Mult1~417\ $ (\dp|Mult0~507\)) ) + ( \dp|Mult1~3\ ) + ( \dp|Mult1~2\ ))
-- \dp|Mult1~7\ = SHARE((!\dp|Mult1~50\ & (\dp|Mult1~417\ & \dp|Mult0~507\)) # (\dp|Mult1~50\ & ((\dp|Mult0~507\) # (\dp|Mult1~417\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult1~50\,
	datac => \dp|ALT_INV_Mult1~417\,
	datad => \dp|ALT_INV_Mult0~507\,
	cin => \dp|Mult1~2\,
	sharein => \dp|Mult1~3\,
	sumout => \dp|Mult1~5_sumout\,
	cout => \dp|Mult1~6\,
	shareout => \dp|Mult1~7\);

-- Location: LABCELL_X30_Y48_N30
\dp|Mult0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~5_sumout\ = SUM(( \dp|Mult1~5_sumout\ ) + ( \dp|Mult0~148\ ) + ( \dp|Mult0~2\ ))
-- \dp|Mult0~6\ = CARRY(( \dp|Mult1~5_sumout\ ) + ( \dp|Mult0~148\ ) + ( \dp|Mult0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~148\,
	datac => \dp|ALT_INV_Mult1~5_sumout\,
	cin => \dp|Mult0~2\,
	sumout => \dp|Mult0~5_sumout\,
	cout => \dp|Mult0~6\);

-- Location: LABCELL_X31_Y46_N39
\dp|Mult1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult1~9_sumout\ = SUM(( !\dp|Mult1~51\ $ (!\dp|Mult0~508\ $ (\dp|Mult1~418\)) ) + ( \dp|Mult1~7\ ) + ( \dp|Mult1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult1~51\,
	datac => \dp|ALT_INV_Mult0~508\,
	datad => \dp|ALT_INV_Mult1~418\,
	cin => \dp|Mult1~6\,
	sharein => \dp|Mult1~7\,
	sumout => \dp|Mult1~9_sumout\);

-- Location: LABCELL_X30_Y48_N33
\dp|Mult0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Mult0~9_sumout\ = SUM(( \dp|Mult1~9_sumout\ ) + ( \dp|Mult0~149\ ) + ( \dp|Mult0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~149\,
	datac => \dp|ALT_INV_Mult1~9_sumout\,
	cin => \dp|Mult0~6\,
	sumout => \dp|Mult0~9_sumout\);

-- Location: LABCELL_X31_Y48_N30
\dp|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|LessThan0~0_combout\ = ( !\dp|Mult0~57_sumout\ & ( !\dp|Mult0~53_sumout\ & ( (!\dp|Mult0~49_sumout\ & ((!\dp|Mult0~61_sumout\) # ((!\dp|Mult0~69_sumout\ & !\dp|Mult0~65_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~49_sumout\,
	datab => \dp|ALT_INV_Mult0~69_sumout\,
	datac => \dp|ALT_INV_Mult0~65_sumout\,
	datad => \dp|ALT_INV_Mult0~61_sumout\,
	datae => \dp|ALT_INV_Mult0~57_sumout\,
	dataf => \dp|ALT_INV_Mult0~53_sumout\,
	combout => \dp|LessThan0~0_combout\);

-- Location: LABCELL_X31_Y48_N36
\dp|cond~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~0_combout\ = ( \dp|Mult0~41_sumout\ & ( !\dp|Mult0~45_sumout\ & ( (!\dp|Mult0~37_sumout\) # ((!\dp|Mult0~29_sumout\) # (!\dp|Mult0~33_sumout\)) ) ) ) # ( !\dp|Mult0~41_sumout\ & ( !\dp|Mult0~45_sumout\ & ( (!\dp|Mult0~37_sumout\) # 
-- ((!\dp|Mult0~29_sumout\) # ((!\dp|Mult0~33_sumout\) # (\dp|LessThan0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111011111111111111101111111000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~37_sumout\,
	datab => \dp|ALT_INV_Mult0~29_sumout\,
	datac => \dp|ALT_INV_Mult0~33_sumout\,
	datad => \dp|ALT_INV_LessThan0~0_combout\,
	datae => \dp|ALT_INV_Mult0~41_sumout\,
	dataf => \dp|ALT_INV_Mult0~45_sumout\,
	combout => \dp|cond~0_combout\);

-- Location: LABCELL_X30_Y48_N42
\dp|cond~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~1_combout\ = ( \dp|cond~0_combout\ & ( !\dp|Mult0~13_sumout\ & ( (!\dp|Mult0~21_sumout\ & (!\dp|Equal2~3_combout\ & (!\dp|Mult0~17_sumout\ & !\dp|Mult0~25_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~21_sumout\,
	datab => \dp|ALT_INV_Equal2~3_combout\,
	datac => \dp|ALT_INV_Mult0~17_sumout\,
	datad => \dp|ALT_INV_Mult0~25_sumout\,
	datae => \dp|ALT_INV_cond~0_combout\,
	dataf => \dp|ALT_INV_Mult0~13_sumout\,
	combout => \dp|cond~1_combout\);

-- Location: LABCELL_X30_Y48_N54
\dp|cond~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~4_combout\ = ( !\dp|Mult0~113_sumout\ & ( !\dp|Mult0~125_sumout\ & ( (!\dp|Equal3~2_combout\ & (!\dp|Mult0~109_sumout\ & (!\dp|Mult0~121_sumout\ & !\dp|Mult0~117_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Equal3~2_combout\,
	datab => \dp|ALT_INV_Mult0~109_sumout\,
	datac => \dp|ALT_INV_Mult0~121_sumout\,
	datad => \dp|ALT_INV_Mult0~117_sumout\,
	datae => \dp|ALT_INV_Mult0~113_sumout\,
	dataf => \dp|ALT_INV_Mult0~125_sumout\,
	combout => \dp|cond~4_combout\);

-- Location: LABCELL_X31_Y49_N18
\dp|cond~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~2_combout\ = ( !\dp|Mult0~97_sumout\ & ( (!\dp|Mult0~89_sumout\ & (!\dp|Mult0~105_sumout\ & (!\dp|Mult0~101_sumout\ & !\dp|Mult0~93_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~89_sumout\,
	datab => \dp|ALT_INV_Mult0~105_sumout\,
	datac => \dp|ALT_INV_Mult0~101_sumout\,
	datad => \dp|ALT_INV_Mult0~93_sumout\,
	dataf => \dp|ALT_INV_Mult0~97_sumout\,
	combout => \dp|cond~2_combout\);

-- Location: LABCELL_X30_Y48_N48
\dp|cond~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~3_combout\ = ( \dp|cond~2_combout\ & ( !\dp|Mult0~85_sumout\ & ( (!\dp|Mult0~77_sumout\ & (!\dp|Mult0~73_sumout\ & !\dp|Mult0~81_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Mult0~77_sumout\,
	datac => \dp|ALT_INV_Mult0~73_sumout\,
	datad => \dp|ALT_INV_Mult0~81_sumout\,
	datae => \dp|ALT_INV_cond~2_combout\,
	dataf => \dp|ALT_INV_Mult0~85_sumout\,
	combout => \dp|cond~3_combout\);

-- Location: LABCELL_X30_Y48_N36
\dp|cond~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cond~5_combout\ = ( \dp|cond~4_combout\ & ( \dp|cond~3_combout\ & ( (!\dp|Mult0~1_sumout\ & (!\dp|Mult0~5_sumout\ & (!\dp|Mult0~9_sumout\ & \dp|cond~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Mult0~1_sumout\,
	datab => \dp|ALT_INV_Mult0~5_sumout\,
	datac => \dp|ALT_INV_Mult0~9_sumout\,
	datad => \dp|ALT_INV_cond~1_combout\,
	datae => \dp|ALT_INV_cond~4_combout\,
	dataf => \dp|ALT_INV_cond~3_combout\,
	combout => \dp|cond~5_combout\);

-- Location: FF_X30_Y48_N38
\dp|cond\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cond~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cond~q\);

-- Location: LABCELL_X27_Y47_N39
\sm|current_state[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|current_state[3]~0_combout\ = (\dp|ydone~q\ & (\sm|current_state[1]~DUPLICATE_q\ & (\dp|xdone~q\ & !\dp|cond~q\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000000000010000000000000001000000000000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ydone~q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \dp|ALT_INV_xdone~q\,
	datad => \dp|ALT_INV_cond~q\,
	combout => \sm|current_state[3]~0_combout\);

-- Location: LABCELL_X27_Y47_N15
\sm|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux1~0_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (((!\sm|current_state[3]~0_combout\ & \sm|current_state\(2))) # (\sm|current_state[1]~DUPLICATE_q\))) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( 
-- (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[3]~0_combout\ & \sm|current_state\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000000100010101000100010001010100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~0_combout\,
	datad => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|Mux1~0_combout\);

-- Location: FF_X27_Y47_N17
\sm|current_state[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux1~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(2));

-- Location: LABCELL_X27_Y47_N51
\sm|current_state[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|current_state[3]~1_combout\ = ( \sm|current_state[3]~0_combout\ & ( ((!\sm|current_state\(0) & \sm|current_state\(2))) # (\sm|current_state\(3)) ) ) # ( !\sm|current_state[3]~0_combout\ & ( \sm|current_state\(3) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001100111111110000110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state\(0),
	datac => \sm|ALT_INV_current_state\(2),
	datad => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[3]~0_combout\,
	combout => \sm|current_state[3]~1_combout\);

-- Location: FF_X27_Y47_N53
\sm|current_state[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|current_state[3]~1_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[3]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y47_N6
\sm|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux2~0_combout\ = ( \dp|xdone~q\ & ( (\sm|current_state[2]~DUPLICATE_q\ & ((\dp|cond~q\) # (\sm|current_state[0]~DUPLICATE_q\))) ) ) # ( !\dp|xdone~q\ & ( \sm|current_state[2]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100000011001100110000001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \dp|ALT_INV_cond~q\,
	dataf => \dp|ALT_INV_xdone~q\,
	combout => \sm|Mux2~0_combout\);

-- Location: LABCELL_X27_Y48_N54
\sm|Decoder0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Decoder0~1_combout\ = ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & !\sm|current_state[0]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \sm|Decoder0~1_combout\);

-- Location: LABCELL_X27_Y48_N57
\dp|yp[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp[4]~5_combout\ = ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state\(3) & !\sm|current_state\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \dp|yp[4]~5_combout\);

-- Location: FF_X28_Y48_N35
\dp|yp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~25_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(1));

-- Location: MLABCELL_X28_Y48_N30
\dp|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~21_sumout\ = SUM(( \dp|yp\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add0~22\ = CARRY(( \dp|yp\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(0),
	cin => GND,
	sumout => \dp|Add0~21_sumout\,
	cout => \dp|Add0~22\);

-- Location: FF_X28_Y48_N32
\dp|yp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~21_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(0));

-- Location: MLABCELL_X28_Y48_N33
\dp|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~25_sumout\ = SUM(( \dp|yp\(1) ) + ( GND ) + ( \dp|Add0~22\ ))
-- \dp|Add0~26\ = CARRY(( \dp|yp\(1) ) + ( GND ) + ( \dp|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(1),
	cin => \dp|Add0~22\,
	sumout => \dp|Add0~25_sumout\,
	cout => \dp|Add0~26\);

-- Location: LABCELL_X27_Y47_N12
\sm|initxp~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|initxp~0_combout\ = ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \sm|initxp~0_combout\);

-- Location: MLABCELL_X28_Y48_N24
\dp|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal0~0_combout\ = ( \sm|initxp~0_combout\ & ( \dp|Add0~21_sumout\ & ( (\dp|Add0~25_sumout\ & !\sm|Decoder0~1_combout\) ) ) ) # ( !\sm|initxp~0_combout\ & ( \dp|Add0~21_sumout\ & ( (\dp|yp\(1) & (!\sm|Decoder0~1_combout\ & \dp|yp\(0))) ) ) ) # ( 
-- !\sm|initxp~0_combout\ & ( !\dp|Add0~21_sumout\ & ( (\dp|yp\(1) & (!\sm|Decoder0~1_combout\ & \dp|yp\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000000000000000000001100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add0~25_sumout\,
	datab => \dp|ALT_INV_yp\(1),
	datac => \sm|ALT_INV_Decoder0~1_combout\,
	datad => \dp|ALT_INV_yp\(0),
	datae => \sm|ALT_INV_initxp~0_combout\,
	dataf => \dp|ALT_INV_Add0~21_sumout\,
	combout => \dp|Equal0~0_combout\);

-- Location: FF_X28_Y48_N38
\dp|yp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~17_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(2));

-- Location: MLABCELL_X28_Y48_N36
\dp|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~17_sumout\ = SUM(( \dp|yp\(2) ) + ( GND ) + ( \dp|Add0~26\ ))
-- \dp|Add0~18\ = CARRY(( \dp|yp\(2) ) + ( GND ) + ( \dp|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(2),
	cin => \dp|Add0~26\,
	sumout => \dp|Add0~17_sumout\,
	cout => \dp|Add0~18\);

-- Location: LABCELL_X27_Y48_N6
\dp|yp~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~4_combout\ = ( \dp|yp\(2) & ( \sm|current_state\(1) ) ) # ( \dp|yp\(2) & ( !\sm|current_state\(1) & ( (((\sm|current_state[0]~DUPLICATE_q\ & \dp|Add0~17_sumout\)) # (\sm|current_state[2]~DUPLICATE_q\)) # (\sm|current_state\(3)) ) ) ) # ( 
-- !\dp|yp\(2) & ( !\sm|current_state\(1) & ( (!\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & (\sm|current_state[0]~DUPLICATE_q\ & \dp|Add0~17_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001000011101110111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \dp|ALT_INV_Add0~17_sumout\,
	datae => \dp|ALT_INV_yp\(2),
	dataf => \sm|ALT_INV_current_state\(1),
	combout => \dp|yp~4_combout\);

-- Location: FF_X28_Y48_N49
\dp|yp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~1_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(6));

-- Location: MLABCELL_X28_Y48_N39
\dp|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~13_sumout\ = SUM(( \dp|yp\(3) ) + ( GND ) + ( \dp|Add0~18\ ))
-- \dp|Add0~14\ = CARRY(( \dp|yp\(3) ) + ( GND ) + ( \dp|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(3),
	cin => \dp|Add0~18\,
	sumout => \dp|Add0~13_sumout\,
	cout => \dp|Add0~14\);

-- Location: FF_X28_Y48_N41
\dp|yp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~13_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(3));

-- Location: MLABCELL_X28_Y48_N42
\dp|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~9_sumout\ = SUM(( \dp|yp\(4) ) + ( GND ) + ( \dp|Add0~14\ ))
-- \dp|Add0~10\ = CARRY(( \dp|yp\(4) ) + ( GND ) + ( \dp|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(4),
	cin => \dp|Add0~14\,
	sumout => \dp|Add0~9_sumout\,
	cout => \dp|Add0~10\);

-- Location: FF_X28_Y48_N43
\dp|yp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~9_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(4));

-- Location: MLABCELL_X28_Y48_N45
\dp|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~5_sumout\ = SUM(( \dp|yp\(5) ) + ( GND ) + ( \dp|Add0~10\ ))
-- \dp|Add0~6\ = CARRY(( \dp|yp\(5) ) + ( GND ) + ( \dp|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(5),
	cin => \dp|Add0~10\,
	sumout => \dp|Add0~5_sumout\,
	cout => \dp|Add0~6\);

-- Location: FF_X28_Y48_N47
\dp|yp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~5_sumout\,
	sclr => \sm|Decoder0~1_combout\,
	ena => \dp|yp[4]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(5));

-- Location: MLABCELL_X28_Y48_N48
\dp|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~1_sumout\ = SUM(( \dp|yp\(6) ) + ( GND ) + ( \dp|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(6),
	cin => \dp|Add0~6\,
	sumout => \dp|Add0~1_sumout\);

-- Location: LABCELL_X27_Y48_N0
\dp|yp~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~0_combout\ = ( \dp|Add0~1_sumout\ & ( \dp|yp\(6) & ( (((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( !\dp|Add0~1_sumout\ & ( \dp|yp\(6) & ( 
-- ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( \dp|Add0~1_sumout\ & ( !\dp|yp\(6) & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & (\sm|current_state[0]~DUPLICATE_q\ & 
-- !\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000001110111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \dp|ALT_INV_Add0~1_sumout\,
	dataf => \dp|ALT_INV_yp\(6),
	combout => \dp|yp~0_combout\);

-- Location: LABCELL_X27_Y48_N12
\dp|yp~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~2_combout\ = ( \dp|yp\(4) & ( \sm|current_state[2]~DUPLICATE_q\ ) ) # ( \dp|yp\(4) & ( !\sm|current_state[2]~DUPLICATE_q\ & ( (((\sm|current_state[0]~DUPLICATE_q\ & \dp|Add0~9_sumout\)) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( 
-- !\dp|yp\(4) & ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & (\sm|current_state[0]~DUPLICATE_q\ & \dp|Add0~9_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001000011101110111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \dp|ALT_INV_Add0~9_sumout\,
	datae => \dp|ALT_INV_yp\(4),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \dp|yp~2_combout\);

-- Location: LABCELL_X27_Y48_N36
\dp|yp~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~1_combout\ = ( \dp|Add0~5_sumout\ & ( \dp|yp\(5) & ( (((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( !\dp|Add0~5_sumout\ & ( \dp|yp\(5) & ( 
-- ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( \dp|Add0~5_sumout\ & ( !\dp|yp\(5) & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & (\sm|current_state[0]~DUPLICATE_q\ & 
-- !\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000001110111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \dp|ALT_INV_Add0~5_sumout\,
	dataf => \dp|ALT_INV_yp\(5),
	combout => \dp|yp~1_combout\);

-- Location: LABCELL_X27_Y48_N48
\dp|yp~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~3_combout\ = ( \dp|Add0~13_sumout\ & ( \dp|yp\(3) & ( (((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( !\dp|Add0~13_sumout\ & ( \dp|yp\(3) & ( 
-- ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( \dp|Add0~13_sumout\ & ( !\dp|yp\(3) & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & (\sm|current_state[0]~DUPLICATE_q\ & 
-- !\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000001110111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \dp|ALT_INV_Add0~13_sumout\,
	dataf => \dp|ALT_INV_yp\(3),
	combout => \dp|yp~3_combout\);

-- Location: LABCELL_X27_Y48_N18
\dp|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal0~1_combout\ = ( \dp|yp~1_combout\ & ( !\dp|yp~3_combout\ & ( (\dp|Equal0~0_combout\ & (\dp|yp~4_combout\ & (\dp|yp~0_combout\ & \dp|yp~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Equal0~0_combout\,
	datab => \dp|ALT_INV_yp~4_combout\,
	datac => \dp|ALT_INV_yp~0_combout\,
	datad => \dp|ALT_INV_yp~2_combout\,
	datae => \dp|ALT_INV_yp~1_combout\,
	dataf => \dp|ALT_INV_yp~3_combout\,
	combout => \dp|Equal0~1_combout\);

-- Location: FF_X27_Y48_N19
\dp|ypdone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ypdone~q\);

-- Location: LABCELL_X27_Y47_N48
\sm|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr1~0_combout\ = ( \sm|current_state[1]~DUPLICATE_q\ & ( (!\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\)) ) ) # ( !\sm|current_state[1]~DUPLICATE_q\ & ( 
-- (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	combout => \sm|WideOr1~0_combout\);

-- Location: LABCELL_X31_Y48_N0
\dp|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~1_sumout\ = SUM(( \dp|xp\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add1~2\ = CARRY(( \dp|xp\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(0),
	cin => GND,
	sumout => \dp|Add1~1_sumout\,
	cout => \dp|Add1~2\);

-- Location: LABCELL_X27_Y47_N18
\dp|xp~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~6_combout\ = ( \sm|WideOr1~0_combout\ & ( \sm|initxp~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initxp~0_combout\,
	dataf => \sm|ALT_INV_WideOr1~0_combout\,
	combout => \dp|xp~6_combout\);

-- Location: FF_X31_Y48_N2
\dp|xp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~1_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(0));

-- Location: LABCELL_X31_Y48_N3
\dp|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~5_sumout\ = SUM(( \dp|xp\(1) ) + ( GND ) + ( \dp|Add1~2\ ))
-- \dp|Add1~6\ = CARRY(( \dp|xp\(1) ) + ( GND ) + ( \dp|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(1),
	cin => \dp|Add1~2\,
	sumout => \dp|Add1~5_sumout\,
	cout => \dp|Add1~6\);

-- Location: FF_X31_Y48_N5
\dp|xp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~5_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(1));

-- Location: LABCELL_X31_Y48_N6
\dp|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~9_sumout\ = SUM(( \dp|xp\(2) ) + ( GND ) + ( \dp|Add1~6\ ))
-- \dp|Add1~10\ = CARRY(( \dp|xp\(2) ) + ( GND ) + ( \dp|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(2),
	cin => \dp|Add1~6\,
	sumout => \dp|Add1~9_sumout\,
	cout => \dp|Add1~10\);

-- Location: FF_X31_Y48_N7
\dp|xp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~9_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(2));

-- Location: LABCELL_X31_Y48_N9
\dp|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~13_sumout\ = SUM(( \dp|xp\(3) ) + ( GND ) + ( \dp|Add1~10\ ))
-- \dp|Add1~14\ = CARRY(( \dp|xp\(3) ) + ( GND ) + ( \dp|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(3),
	cin => \dp|Add1~10\,
	sumout => \dp|Add1~13_sumout\,
	cout => \dp|Add1~14\);

-- Location: FF_X31_Y48_N11
\dp|xp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~13_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(3));

-- Location: LABCELL_X31_Y48_N12
\dp|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~17_sumout\ = SUM(( \dp|xp\(4) ) + ( GND ) + ( \dp|Add1~14\ ))
-- \dp|Add1~18\ = CARRY(( \dp|xp\(4) ) + ( GND ) + ( \dp|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(4),
	cin => \dp|Add1~14\,
	sumout => \dp|Add1~17_sumout\,
	cout => \dp|Add1~18\);

-- Location: FF_X31_Y48_N13
\dp|xp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~17_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(4));

-- Location: LABCELL_X31_Y48_N15
\dp|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~21_sumout\ = SUM(( \dp|xp\(5) ) + ( GND ) + ( \dp|Add1~18\ ))
-- \dp|Add1~22\ = CARRY(( \dp|xp\(5) ) + ( GND ) + ( \dp|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(5),
	cin => \dp|Add1~18\,
	sumout => \dp|Add1~21_sumout\,
	cout => \dp|Add1~22\);

-- Location: FF_X31_Y48_N16
\dp|xp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~21_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(5));

-- Location: LABCELL_X33_Y48_N48
\dp|xp~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~5_combout\ = ( \dp|Add1~21_sumout\ & ( (!\sm|WideOr1~0_combout\ & ((\dp|xp\(5)))) # (\sm|WideOr1~0_combout\ & (!\sm|initxp~0_combout\)) ) ) # ( !\dp|Add1~21_sumout\ & ( (!\sm|WideOr1~0_combout\ & \dp|xp\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000101110001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_xp\(5),
	dataf => \dp|ALT_INV_Add1~21_sumout\,
	combout => \dp|xp~5_combout\);

-- Location: LABCELL_X33_Y48_N12
\dp|xp~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~3_combout\ = ( \dp|Add1~13_sumout\ & ( (!\sm|WideOr1~0_combout\ & ((\dp|xp\(3)))) # (\sm|WideOr1~0_combout\ & (!\sm|initxp~0_combout\)) ) ) # ( !\dp|Add1~13_sumout\ & ( (!\sm|WideOr1~0_combout\ & \dp|xp\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000101110001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_xp\(3),
	dataf => \dp|ALT_INV_Add1~13_sumout\,
	combout => \dp|xp~3_combout\);

-- Location: LABCELL_X33_Y48_N33
\dp|Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~0_combout\ = ( \sm|initxp~0_combout\ & ( \dp|xp\(0) & ( (\dp|xp\(1) & !\sm|WideOr1~0_combout\) ) ) ) # ( !\sm|initxp~0_combout\ & ( \dp|xp\(0) & ( (!\sm|WideOr1~0_combout\ & (((\dp|xp\(1))))) # (\sm|WideOr1~0_combout\ & (\dp|Add1~5_sumout\ & 
-- ((\dp|Add1~1_sumout\)))) ) ) ) # ( !\sm|initxp~0_combout\ & ( !\dp|xp\(0) & ( (\dp|Add1~5_sumout\ & (\sm|WideOr1~0_combout\ & \dp|Add1~1_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000000000110000001101010011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add1~5_sumout\,
	datab => \dp|ALT_INV_xp\(1),
	datac => \sm|ALT_INV_WideOr1~0_combout\,
	datad => \dp|ALT_INV_Add1~1_sumout\,
	datae => \sm|ALT_INV_initxp~0_combout\,
	dataf => \dp|ALT_INV_xp\(0),
	combout => \dp|Equal1~0_combout\);

-- Location: LABCELL_X33_Y48_N15
\dp|xp~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~4_combout\ = ( \dp|xp\(4) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initxp~0_combout\ & \dp|Add1~17_sumout\)) ) ) # ( !\dp|xp\(4) & ( (!\sm|initxp~0_combout\ & (\sm|WideOr1~0_combout\ & \dp|Add1~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001011001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_Add1~17_sumout\,
	dataf => \dp|ALT_INV_xp\(4),
	combout => \dp|xp~4_combout\);

-- Location: FF_X31_Y48_N22
\dp|xp[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~25_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(7));

-- Location: LABCELL_X31_Y48_N18
\dp|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~29_sumout\ = SUM(( \dp|xp\(6) ) + ( GND ) + ( \dp|Add1~22\ ))
-- \dp|Add1~30\ = CARRY(( \dp|xp\(6) ) + ( GND ) + ( \dp|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(6),
	cin => \dp|Add1~22\,
	sumout => \dp|Add1~29_sumout\,
	cout => \dp|Add1~30\);

-- Location: FF_X31_Y48_N20
\dp|xp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~29_sumout\,
	sclr => \dp|xp~6_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(6));

-- Location: LABCELL_X31_Y48_N21
\dp|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~25_sumout\ = SUM(( \dp|xp\(7) ) + ( GND ) + ( \dp|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(7),
	cin => \dp|Add1~30\,
	sumout => \dp|Add1~25_sumout\);

-- Location: LABCELL_X31_Y48_N54
\dp|Equal1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~1_combout\ = ( \sm|WideOr1~0_combout\ & ( \dp|Add1~29_sumout\ & ( (\dp|Add1~25_sumout\ & \sm|initxp~0_combout\) ) ) ) # ( !\sm|WideOr1~0_combout\ & ( \dp|Add1~29_sumout\ & ( (\dp|xp\(7) & !\dp|xp\(6)) ) ) ) # ( \sm|WideOr1~0_combout\ & ( 
-- !\dp|Add1~29_sumout\ & ( \dp|Add1~25_sumout\ ) ) ) # ( !\sm|WideOr1~0_combout\ & ( !\dp|Add1~29_sumout\ & ( (\dp|xp\(7) & !\dp|xp\(6)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000010101010101010100110011000000000000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add1~25_sumout\,
	datab => \dp|ALT_INV_xp\(7),
	datac => \sm|ALT_INV_initxp~0_combout\,
	datad => \dp|ALT_INV_xp\(6),
	datae => \sm|ALT_INV_WideOr1~0_combout\,
	dataf => \dp|ALT_INV_Add1~29_sumout\,
	combout => \dp|Equal1~1_combout\);

-- Location: LABCELL_X33_Y48_N39
\dp|xp~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~2_combout\ = ( \dp|Add1~9_sumout\ & ( (!\sm|WideOr1~0_combout\ & ((\dp|xp\(2)))) # (\sm|WideOr1~0_combout\ & (!\sm|initxp~0_combout\)) ) ) # ( !\dp|Add1~9_sumout\ & ( (!\sm|WideOr1~0_combout\ & \dp|xp\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000101110001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_xp\(2),
	dataf => \dp|ALT_INV_Add1~9_sumout\,
	combout => \dp|xp~2_combout\);

-- Location: LABCELL_X33_Y48_N54
\dp|Equal1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~2_combout\ = ( \dp|Equal1~1_combout\ & ( \dp|xp~2_combout\ & ( (!\dp|xp~5_combout\ & (\dp|xp~3_combout\ & (\dp|Equal1~0_combout\ & \dp|xp~4_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_xp~5_combout\,
	datab => \dp|ALT_INV_xp~3_combout\,
	datac => \dp|ALT_INV_Equal1~0_combout\,
	datad => \dp|ALT_INV_xp~4_combout\,
	datae => \dp|ALT_INV_Equal1~1_combout\,
	dataf => \dp|ALT_INV_xp~2_combout\,
	combout => \dp|Equal1~2_combout\);

-- Location: FF_X33_Y48_N55
\dp|xpdone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Equal1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xpdone~q\);

-- Location: LABCELL_X27_Y47_N33
\sm|Mux2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux2~1_combout\ = ( !\sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & ((!\dp|xpdone~q\) # (\dp|ypdone~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000001010000111100000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ypdone~q\,
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datad => \dp|ALT_INV_xpdone~q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|Mux2~1_combout\);

-- Location: LABCELL_X27_Y47_N9
\sm|Mux2~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux2~2_combout\ = ( \sm|Mux2~1_combout\ & ( !\sm|current_state[3]~DUPLICATE_q\ ) ) # ( !\sm|Mux2~1_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & ((!\sm|current_state\(1)) # (\sm|Mux2~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101000001010101010100000101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_Mux2~0_combout\,
	datad => \sm|ALT_INV_current_state\(1),
	dataf => \sm|ALT_INV_Mux2~1_combout\,
	combout => \sm|Mux2~2_combout\);

-- Location: FF_X27_Y47_N11
\sm|current_state[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux2~2_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[1]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y47_N3
\sm|Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux3~0_combout\ = ( \dp|xdone~q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (((!\dp|xpdone~q\)))) # (\sm|current_state[2]~DUPLICATE_q\ & (\dp|ydone~q\ & (!\dp|cond~q\))) ) ) # ( !\dp|xdone~q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & 
-- ((!\dp|xpdone~q\))) # (\sm|current_state[2]~DUPLICATE_q\ & (!\dp|cond~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000110000111111000011000011011100000100001101110000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ydone~q\,
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \dp|ALT_INV_cond~q\,
	datad => \dp|ALT_INV_xpdone~q\,
	dataf => \dp|ALT_INV_xdone~q\,
	combout => \sm|Mux3~0_combout\);

-- Location: LABCELL_X27_Y47_N30
\sm|Mux3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux3~1_combout\ = ( !\sm|current_state[3]~DUPLICATE_q\ & ( (\sm|current_state[1]~DUPLICATE_q\ & (!\sm|Mux3~0_combout\ & !\sm|current_state\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_Mux3~0_combout\,
	datad => \sm|ALT_INV_current_state\(0),
	dataf => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	combout => \sm|Mux3~1_combout\);

-- Location: FF_X27_Y47_N31
\sm|current_state[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux3~1_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[0]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y48_N42
\sm|plot~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|plot~0_combout\ = ( \sm|current_state[2]~DUPLICATE_q\ & ( (\sm|current_state[0]~DUPLICATE_q\ & (\sm|current_state\(1) & !\sm|current_state\(3))) ) ) # ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state[0]~DUPLICATE_q\ & 
-- (\sm|current_state\(1) & !\sm|current_state\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000000010000000100000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \sm|plot~0_combout\);

-- Location: LABCELL_X27_Y47_N45
\dp|Equal4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal4~0_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( ((!\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[2]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101111111011111110111111101111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \dp|Equal4~0_combout\);

-- Location: FF_X27_Y48_N2
\dp|y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~0_combout\,
	asdata => \dp|yf~2_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(6));

-- Location: FF_X27_Y48_N38
\dp|y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~1_combout\,
	asdata => \dp|yf~3_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(5));

-- Location: FF_X27_Y48_N14
\dp|y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~2_combout\,
	asdata => \dp|yf~4_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(4));

-- Location: FF_X27_Y48_N50
\dp|y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~3_combout\,
	asdata => \dp|yf~5_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(3));

-- Location: FF_X27_Y48_N8
\dp|y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~4_combout\,
	asdata => \dp|yf~8_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(2));

-- Location: LABCELL_X27_Y48_N30
\dp|yp~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~7_combout\ = ( \dp|Add0~25_sumout\ & ( \dp|yp\(1) & ( (((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( !\dp|Add0~25_sumout\ & ( \dp|yp\(1) & ( 
-- ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(1))) # (\sm|current_state\(3)) ) ) ) # ( \dp|Add0~25_sumout\ & ( !\dp|yp\(1) & ( (!\sm|current_state\(3) & (!\sm|current_state\(1) & (\sm|current_state[0]~DUPLICATE_q\ & 
-- !\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000001110111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \dp|ALT_INV_Add0~25_sumout\,
	dataf => \dp|ALT_INV_yp\(1),
	combout => \dp|yp~7_combout\);

-- Location: FF_X27_Y48_N31
\dp|y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~7_combout\,
	asdata => \dp|yf~7_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(1));

-- Location: LABCELL_X27_Y48_N24
\dp|yp~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~6_combout\ = ( \dp|yp\(0) & ( \sm|current_state[0]~DUPLICATE_q\ & ( (((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(3))) # (\sm|current_state\(1))) # (\dp|Add0~21_sumout\) ) ) ) # ( !\dp|yp\(0) & ( \sm|current_state[0]~DUPLICATE_q\ & ( 
-- (\dp|Add0~21_sumout\ & (!\sm|current_state\(1) & (!\sm|current_state\(3) & !\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(0) & ( !\sm|current_state[0]~DUPLICATE_q\ & ( ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(3))) # 
-- (\sm|current_state\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001111111111111101000000000000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add0~21_sumout\,
	datab => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(3),
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(0),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \dp|yp~6_combout\);

-- Location: FF_X27_Y48_N25
\dp|y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|yp~6_combout\,
	asdata => \dp|yf~6_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(0));

-- Location: LABCELL_X33_Y48_N36
\dp|xp~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~7_combout\ = ( \dp|xp\(6) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initxp~0_combout\ & \dp|Add1~29_sumout\)) ) ) # ( !\dp|xp\(6) & ( (!\sm|initxp~0_combout\ & (\sm|WideOr1~0_combout\ & \dp|Add1~29_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001011001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_Add1~29_sumout\,
	dataf => \dp|ALT_INV_xp\(6),
	combout => \dp|xp~7_combout\);

-- Location: FF_X33_Y48_N37
\dp|x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~7_combout\,
	asdata => \dp|xf~6_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(6));

-- Location: FF_X33_Y48_N49
\dp|x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~5_combout\,
	asdata => \dp|xf~7_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(5));

-- Location: LABCELL_X29_Y48_N0
\vga_u0|user_input_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~9_sumout\ = SUM(( !\dp|y\(0) $ (!\dp|x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~10\ = CARRY(( !\dp|y\(0) $ (!\dp|x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~11\ = SHARE((\dp|y\(0) & \dp|x\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_y\(0),
	datac => \dp|ALT_INV_x\(5),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|user_input_translator|Add1~9_sumout\,
	cout => \vga_u0|user_input_translator|Add1~10\,
	shareout => \vga_u0|user_input_translator|Add1~11\);

-- Location: LABCELL_X29_Y48_N3
\vga_u0|user_input_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~13_sumout\ = SUM(( !\dp|x\(6) $ (!\dp|y\(1)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~14\ = CARRY(( !\dp|x\(6) $ (!\dp|y\(1)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~15\ = SHARE((\dp|x\(6) & \dp|y\(1)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_x\(6),
	datac => \dp|ALT_INV_y\(1),
	cin => \vga_u0|user_input_translator|Add1~10\,
	sharein => \vga_u0|user_input_translator|Add1~11\,
	sumout => \vga_u0|user_input_translator|Add1~13_sumout\,
	cout => \vga_u0|user_input_translator|Add1~14\,
	shareout => \vga_u0|user_input_translator|Add1~15\);

-- Location: LABCELL_X29_Y48_N6
\vga_u0|user_input_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~17_sumout\ = SUM(( !\dp|y\(0) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~18\ = CARRY(( !\dp|y\(0) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~19\ = SHARE((\dp|y\(0) & \dp|y\(2)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_y\(0),
	datac => \dp|ALT_INV_y\(2),
	cin => \vga_u0|user_input_translator|Add1~14\,
	sharein => \vga_u0|user_input_translator|Add1~15\,
	sumout => \vga_u0|user_input_translator|Add1~17_sumout\,
	cout => \vga_u0|user_input_translator|Add1~18\,
	shareout => \vga_u0|user_input_translator|Add1~19\);

-- Location: LABCELL_X29_Y48_N9
\vga_u0|user_input_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~21_sumout\ = SUM(( !\dp|y\(3) $ (!\dp|y\(1)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~22\ = CARRY(( !\dp|y\(3) $ (!\dp|y\(1)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~23\ = SHARE((\dp|y\(3) & \dp|y\(1)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(3),
	datac => \dp|ALT_INV_y\(1),
	cin => \vga_u0|user_input_translator|Add1~18\,
	sharein => \vga_u0|user_input_translator|Add1~19\,
	sumout => \vga_u0|user_input_translator|Add1~21_sumout\,
	cout => \vga_u0|user_input_translator|Add1~22\,
	shareout => \vga_u0|user_input_translator|Add1~23\);

-- Location: LABCELL_X29_Y48_N12
\vga_u0|user_input_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~25_sumout\ = SUM(( !\dp|y\(4) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~26\ = CARRY(( !\dp|y\(4) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~27\ = SHARE((\dp|y\(4) & \dp|y\(2)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(4),
	datad => \dp|ALT_INV_y\(2),
	cin => \vga_u0|user_input_translator|Add1~22\,
	sharein => \vga_u0|user_input_translator|Add1~23\,
	sumout => \vga_u0|user_input_translator|Add1~25_sumout\,
	cout => \vga_u0|user_input_translator|Add1~26\,
	shareout => \vga_u0|user_input_translator|Add1~27\);

-- Location: LABCELL_X29_Y48_N15
\vga_u0|user_input_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~29_sumout\ = SUM(( !\dp|y\(3) $ (!\dp|y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~30\ = CARRY(( !\dp|y\(3) $ (!\dp|y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~31\ = SHARE((\dp|y\(3) & \dp|y\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(3),
	datac => \dp|ALT_INV_y\(5),
	cin => \vga_u0|user_input_translator|Add1~26\,
	sharein => \vga_u0|user_input_translator|Add1~27\,
	sumout => \vga_u0|user_input_translator|Add1~29_sumout\,
	cout => \vga_u0|user_input_translator|Add1~30\,
	shareout => \vga_u0|user_input_translator|Add1~31\);

-- Location: LABCELL_X29_Y48_N18
\vga_u0|user_input_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~33_sumout\ = SUM(( !\dp|y\(6) $ (!\dp|y\(4)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~34\ = CARRY(( !\dp|y\(6) $ (!\dp|y\(4)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~35\ = SHARE((\dp|y\(6) & \dp|y\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(6),
	datac => \dp|ALT_INV_y\(4),
	cin => \vga_u0|user_input_translator|Add1~30\,
	sharein => \vga_u0|user_input_translator|Add1~31\,
	sumout => \vga_u0|user_input_translator|Add1~33_sumout\,
	cout => \vga_u0|user_input_translator|Add1~34\,
	shareout => \vga_u0|user_input_translator|Add1~35\);

-- Location: LABCELL_X29_Y48_N21
\vga_u0|user_input_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~37_sumout\ = SUM(( \dp|y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~38\ = CARRY(( \dp|y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(5),
	cin => \vga_u0|user_input_translator|Add1~34\,
	sharein => \vga_u0|user_input_translator|Add1~35\,
	sumout => \vga_u0|user_input_translator|Add1~37_sumout\,
	cout => \vga_u0|user_input_translator|Add1~38\,
	shareout => \vga_u0|user_input_translator|Add1~39\);

-- Location: LABCELL_X29_Y48_N24
\vga_u0|user_input_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~5_sumout\ = SUM(( \dp|y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~6\ = CARRY(( \dp|y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~7\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(6),
	cin => \vga_u0|user_input_translator|Add1~38\,
	sharein => \vga_u0|user_input_translator|Add1~39\,
	sumout => \vga_u0|user_input_translator|Add1~5_sumout\,
	cout => \vga_u0|user_input_translator|Add1~6\,
	shareout => \vga_u0|user_input_translator|Add1~7\);

-- Location: LABCELL_X29_Y48_N27
\vga_u0|user_input_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~1_sumout\ = SUM(( GND ) + ( \vga_u0|user_input_translator|Add1~7\ ) + ( \vga_u0|user_input_translator|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|user_input_translator|Add1~6\,
	sharein => \vga_u0|user_input_translator|Add1~7\,
	sumout => \vga_u0|user_input_translator|Add1~1_sumout\);

-- Location: MLABCELL_X28_Y48_N3
\vga_u0|writeEn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~0_combout\ = ( \dp|y\(4) & ( (\dp|y\(6) & (\dp|y\(5) & \dp|y\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(6),
	datac => \dp|ALT_INV_y\(5),
	datad => \dp|ALT_INV_y\(3),
	dataf => \dp|ALT_INV_y\(4),
	combout => \vga_u0|writeEn~0_combout\);

-- Location: MLABCELL_X28_Y48_N6
\vga_u0|VideoMemory|auto_generated|decode2|w_anode118w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2) = ( \vga_u0|user_input_translator|Add1~5_sumout\ & ( (\sm|plot~0_combout\ & (!\vga_u0|user_input_translator|Add1~1_sumout\ & !\vga_u0|writeEn~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000100000000000100010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_plot~0_combout\,
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	datad => \vga_u0|ALT_INV_writeEn~0_combout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2));

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "gclk_far",
	nreset_invert => "true",
	output_clock_frequency => "300.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 4000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 10,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "fb_1",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 6,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 6,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 0,
	pll_m_cnt_prst => 1,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 1,
	pll_n_cnt_lo_div => 1,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "false",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	ecnc1test => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	tclk => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 6,
	dprio0_cnt_lo_div => 6,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "25.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	up0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	vco0ph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0));

-- Location: CLKCTRL_G6
\vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0),
	outclk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\);

-- Location: LABCELL_X31_Y49_N30
\vga_u0|controller|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add1~37_sumout\,
	cout => \vga_u0|controller|Add1~38\);

-- Location: LABCELL_X31_Y49_N54
\vga_u0|controller|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~5_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))
-- \vga_u0|controller|Add1~6\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|Add1~10\,
	sumout => \vga_u0|controller|Add1~5_sumout\,
	cout => \vga_u0|controller|Add1~6\);

-- Location: LABCELL_X31_Y49_N57
\vga_u0|controller|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	cin => \vga_u0|controller|Add1~6\,
	sumout => \vga_u0|controller|Add1~1_sumout\);

-- Location: MLABCELL_X25_Y49_N0
\vga_u0|controller|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~37_sumout\ = SUM(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add0~38\ = CARRY(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add0~37_sumout\,
	cout => \vga_u0|controller|Add0~38\);

-- Location: FF_X25_Y49_N2
\vga_u0|controller|xCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(0));

-- Location: MLABCELL_X25_Y49_N3
\vga_u0|controller|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~17_sumout\ = SUM(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))
-- \vga_u0|controller|Add0~18\ = CARRY(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	cin => \vga_u0|controller|Add0~38\,
	sumout => \vga_u0|controller|Add0~17_sumout\,
	cout => \vga_u0|controller|Add0~18\);

-- Location: FF_X25_Y49_N5
\vga_u0|controller|xCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(1));

-- Location: MLABCELL_X25_Y49_N6
\vga_u0|controller|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~33_sumout\ = SUM(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))
-- \vga_u0|controller|Add0~34\ = CARRY(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	cin => \vga_u0|controller|Add0~18\,
	sumout => \vga_u0|controller|Add0~33_sumout\,
	cout => \vga_u0|controller|Add0~34\);

-- Location: FF_X25_Y49_N8
\vga_u0|controller|xCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(2));

-- Location: MLABCELL_X25_Y49_N9
\vga_u0|controller|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~29_sumout\ = SUM(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))
-- \vga_u0|controller|Add0~30\ = CARRY(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	cin => \vga_u0|controller|Add0~34\,
	sumout => \vga_u0|controller|Add0~29_sumout\,
	cout => \vga_u0|controller|Add0~30\);

-- Location: FF_X25_Y49_N11
\vga_u0|controller|xCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(3));

-- Location: MLABCELL_X25_Y49_N12
\vga_u0|controller|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~13_sumout\ = SUM(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))
-- \vga_u0|controller|Add0~14\ = CARRY(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(4),
	cin => \vga_u0|controller|Add0~30\,
	sumout => \vga_u0|controller|Add0~13_sumout\,
	cout => \vga_u0|controller|Add0~14\);

-- Location: FF_X25_Y49_N13
\vga_u0|controller|xCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(4));

-- Location: MLABCELL_X25_Y49_N15
\vga_u0|controller|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~25_sumout\ = SUM(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))
-- \vga_u0|controller|Add0~26\ = CARRY(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	cin => \vga_u0|controller|Add0~14\,
	sumout => \vga_u0|controller|Add0~25_sumout\,
	cout => \vga_u0|controller|Add0~26\);

-- Location: FF_X25_Y49_N16
\vga_u0|controller|xCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(5));

-- Location: MLABCELL_X25_Y49_N18
\vga_u0|controller|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~21_sumout\ = SUM(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))
-- \vga_u0|controller|Add0~22\ = CARRY(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	cin => \vga_u0|controller|Add0~26\,
	sumout => \vga_u0|controller|Add0~21_sumout\,
	cout => \vga_u0|controller|Add0~22\);

-- Location: FF_X25_Y49_N20
\vga_u0|controller|xCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(6));

-- Location: MLABCELL_X25_Y49_N45
\vga_u0|controller|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~1_combout\ = ( !\vga_u0|controller|xCounter\(6) & ( (\vga_u0|controller|xCounter\(1) & (!\vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(1),
	datac => \vga_u0|controller|ALT_INV_xCounter\(5),
	datad => \vga_u0|controller|ALT_INV_xCounter\(0),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|Equal0~1_combout\);

-- Location: MLABCELL_X25_Y49_N21
\vga_u0|controller|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~9_sumout\ = SUM(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))
-- \vga_u0|controller|Add0~10\ = CARRY(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => \vga_u0|controller|Add0~22\,
	sumout => \vga_u0|controller|Add0~9_sumout\,
	cout => \vga_u0|controller|Add0~10\);

-- Location: FF_X25_Y49_N22
\vga_u0|controller|xCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(7));

-- Location: MLABCELL_X25_Y49_N24
\vga_u0|controller|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~5_sumout\ = SUM(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))
-- \vga_u0|controller|Add0~6\ = CARRY(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	cin => \vga_u0|controller|Add0~10\,
	sumout => \vga_u0|controller|Add0~5_sumout\,
	cout => \vga_u0|controller|Add0~6\);

-- Location: FF_X25_Y49_N26
\vga_u0|controller|xCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(8));

-- Location: MLABCELL_X25_Y49_N27
\vga_u0|controller|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~1_sumout\ = SUM(( \vga_u0|controller|xCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|Add0~6\,
	sumout => \vga_u0|controller|Add0~1_sumout\);

-- Location: FF_X25_Y49_N28
\vga_u0|controller|xCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(9));

-- Location: FF_X25_Y49_N10
\vga_u0|controller|xCounter[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[3]~DUPLICATE_q\);

-- Location: MLABCELL_X25_Y49_N36
\vga_u0|controller|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (!\vga_u0|controller|xCounter\(7) & (\vga_u0|controller|xCounter\(9) & (\vga_u0|controller|xCounter[3]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(8)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000100000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(7),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_xCounter[3]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|Equal0~0_combout\);

-- Location: MLABCELL_X25_Y49_N51
\vga_u0|controller|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~2_combout\ = ( \vga_u0|controller|xCounter\(2) & ( (\vga_u0|controller|Equal0~1_combout\ & \vga_u0|controller|Equal0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(2),
	combout => \vga_u0|controller|Equal0~2_combout\);

-- Location: FF_X31_Y49_N59
\vga_u0|controller|yCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(9));

-- Location: LABCELL_X31_Y49_N0
\vga_u0|controller|always1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~0_combout\ = ( !\vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(5) & (\vga_u0|controller|yCounter\(9) & (!\vga_u0|controller|yCounter\(6) & !\vga_u0|controller|yCounter\(8)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(6),
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|always1~0_combout\);

-- Location: FF_X25_Y49_N7
\vga_u0|controller|xCounter[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y49_N6
\vga_u0|controller|always1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~1_combout\ = ( !\vga_u0|controller|yCounter\(4) & ( (!\vga_u0|controller|yCounter\(0) & (\vga_u0|controller|yCounter\(3) & (!\vga_u0|controller|yCounter\(1) & \vga_u0|controller|yCounter\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(0),
	datab => \vga_u0|controller|ALT_INV_yCounter\(3),
	datac => \vga_u0|controller|ALT_INV_yCounter\(1),
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(4),
	combout => \vga_u0|controller|always1~1_combout\);

-- Location: LABCELL_X31_Y49_N12
\vga_u0|controller|always1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~2_combout\ = ( \vga_u0|controller|always1~1_combout\ & ( (\vga_u0|controller|always1~0_combout\ & (\vga_u0|controller|xCounter[2]~DUPLICATE_q\ & (\vga_u0|controller|Equal0~0_combout\ & \vga_u0|controller|Equal0~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_always1~0_combout\,
	datab => \vga_u0|controller|ALT_INV_xCounter[2]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	dataf => \vga_u0|controller|ALT_INV_always1~1_combout\,
	combout => \vga_u0|controller|always1~2_combout\);

-- Location: FF_X31_Y49_N31
\vga_u0|controller|yCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(0));

-- Location: LABCELL_X31_Y49_N33
\vga_u0|controller|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~33_sumout\ = SUM(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))
-- \vga_u0|controller|Add1~34\ = CARRY(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(1),
	cin => \vga_u0|controller|Add1~38\,
	sumout => \vga_u0|controller|Add1~33_sumout\,
	cout => \vga_u0|controller|Add1~34\);

-- Location: FF_X31_Y49_N35
\vga_u0|controller|yCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(1));

-- Location: LABCELL_X31_Y49_N36
\vga_u0|controller|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~29_sumout\ = SUM(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))
-- \vga_u0|controller|Add1~30\ = CARRY(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|Add1~34\,
	sumout => \vga_u0|controller|Add1~29_sumout\,
	cout => \vga_u0|controller|Add1~30\);

-- Location: FF_X31_Y49_N38
\vga_u0|controller|yCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(2));

-- Location: LABCELL_X31_Y49_N39
\vga_u0|controller|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~25_sumout\ = SUM(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))
-- \vga_u0|controller|Add1~26\ = CARRY(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|Add1~30\,
	sumout => \vga_u0|controller|Add1~25_sumout\,
	cout => \vga_u0|controller|Add1~26\);

-- Location: FF_X31_Y49_N41
\vga_u0|controller|yCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(3));

-- Location: LABCELL_X31_Y49_N42
\vga_u0|controller|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~21_sumout\ = SUM(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))
-- \vga_u0|controller|Add1~22\ = CARRY(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|Add1~26\,
	sumout => \vga_u0|controller|Add1~21_sumout\,
	cout => \vga_u0|controller|Add1~22\);

-- Location: FF_X31_Y49_N44
\vga_u0|controller|yCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(4));

-- Location: LABCELL_X31_Y49_N45
\vga_u0|controller|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~17_sumout\ = SUM(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))
-- \vga_u0|controller|Add1~18\ = CARRY(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|Add1~22\,
	sumout => \vga_u0|controller|Add1~17_sumout\,
	cout => \vga_u0|controller|Add1~18\);

-- Location: FF_X31_Y49_N47
\vga_u0|controller|yCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(5));

-- Location: LABCELL_X31_Y49_N48
\vga_u0|controller|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~13_sumout\ = SUM(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))
-- \vga_u0|controller|Add1~14\ = CARRY(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(6),
	cin => \vga_u0|controller|Add1~18\,
	sumout => \vga_u0|controller|Add1~13_sumout\,
	cout => \vga_u0|controller|Add1~14\);

-- Location: FF_X31_Y49_N50
\vga_u0|controller|yCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(6));

-- Location: LABCELL_X31_Y49_N51
\vga_u0|controller|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~9_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))
-- \vga_u0|controller|Add1~10\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|Add1~14\,
	sumout => \vga_u0|controller|Add1~9_sumout\,
	cout => \vga_u0|controller|Add1~10\);

-- Location: FF_X31_Y49_N53
\vga_u0|controller|yCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(7));

-- Location: FF_X31_Y49_N55
\vga_u0|controller|yCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(8));

-- Location: FF_X31_Y49_N52
\vga_u0|controller|yCounter[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[7]~DUPLICATE_q\);

-- Location: FF_X31_Y49_N40
\vga_u0|controller|yCounter[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[3]~DUPLICATE_q\);

-- Location: LABCELL_X29_Y49_N0
\vga_u0|controller|controller_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~9_sumout\ = SUM(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~10\ = CARRY(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~11\ = SHARE((\vga_u0|controller|yCounter\(2) & \vga_u0|controller|xCounter\(7)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|controller|controller_translator|Add1~9_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~10\,
	shareout => \vga_u0|controller|controller_translator|Add1~11\);

-- Location: LABCELL_X29_Y49_N3
\vga_u0|controller|controller_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~13_sumout\ = SUM(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~10\ ))
-- \vga_u0|controller|controller_translator|Add1~14\ = CARRY(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( \vga_u0|controller|controller_translator|Add1~10\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~15\ = SHARE((\vga_u0|controller|yCounter[3]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(8)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000011001100000000000000000011001111001100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~10\,
	sharein => \vga_u0|controller|controller_translator|Add1~11\,
	sumout => \vga_u0|controller|controller_translator|Add1~13_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~14\,
	shareout => \vga_u0|controller|controller_translator|Add1~15\);

-- Location: LABCELL_X29_Y49_N6
\vga_u0|controller|controller_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~17_sumout\ = SUM(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|xCounter\(9))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~18\ = CARRY(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|xCounter\(9))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~19\ = SHARE((!\vga_u0|controller|yCounter\(2) & (\vga_u0|controller|yCounter\(4) & \vga_u0|controller|xCounter\(9))) # (\vga_u0|controller|yCounter\(2) & ((\vga_u0|controller|xCounter\(9)) # 
-- (\vga_u0|controller|yCounter\(4)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(2),
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|controller_translator|Add1~14\,
	sharein => \vga_u0|controller|controller_translator|Add1~15\,
	sumout => \vga_u0|controller|controller_translator|Add1~17_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~18\,
	shareout => \vga_u0|controller|controller_translator|Add1~19\);

-- Location: LABCELL_X29_Y49_N9
\vga_u0|controller|controller_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~21_sumout\ = SUM(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter[3]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~18\ ))
-- \vga_u0|controller|controller_translator|Add1~22\ = CARRY(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter[3]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( \vga_u0|controller|controller_translator|Add1~18\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~23\ = SHARE((\vga_u0|controller|yCounter\(5) & \vga_u0|controller|yCounter[3]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(5),
	datac => \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~18\,
	sharein => \vga_u0|controller|controller_translator|Add1~19\,
	sumout => \vga_u0|controller|controller_translator|Add1~21_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~22\,
	shareout => \vga_u0|controller|controller_translator|Add1~23\);

-- Location: LABCELL_X29_Y49_N12
\vga_u0|controller|controller_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~25_sumout\ = SUM(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~26\ = CARRY(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~27\ = SHARE((\vga_u0|controller|yCounter\(6) & \vga_u0|controller|yCounter\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(6),
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|controller_translator|Add1~22\,
	sharein => \vga_u0|controller|controller_translator|Add1~23\,
	sumout => \vga_u0|controller|controller_translator|Add1~25_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~26\,
	shareout => \vga_u0|controller|controller_translator|Add1~27\);

-- Location: LABCELL_X29_Y49_N15
\vga_u0|controller|controller_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~29_sumout\ = SUM(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter[7]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~26\ ))
-- \vga_u0|controller|controller_translator|Add1~30\ = CARRY(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter[7]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( \vga_u0|controller|controller_translator|Add1~26\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~31\ = SHARE((\vga_u0|controller|yCounter\(5) & \vga_u0|controller|yCounter[7]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datac => \vga_u0|controller|ALT_INV_yCounter[7]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~26\,
	sharein => \vga_u0|controller|controller_translator|Add1~27\,
	sumout => \vga_u0|controller|controller_translator|Add1~29_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~30\,
	shareout => \vga_u0|controller|controller_translator|Add1~31\);

-- Location: LABCELL_X29_Y49_N18
\vga_u0|controller|controller_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~33_sumout\ = SUM(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~34\ = CARRY(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~35\ = SHARE((\vga_u0|controller|yCounter\(6) & \vga_u0|controller|yCounter\(8)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(6),
	datac => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~30\,
	sharein => \vga_u0|controller|controller_translator|Add1~31\,
	sumout => \vga_u0|controller|controller_translator|Add1~33_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~34\,
	shareout => \vga_u0|controller|controller_translator|Add1~35\);

-- Location: LABCELL_X29_Y49_N21
\vga_u0|controller|controller_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter[7]~DUPLICATE_q\ ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~38\ = CARRY(( \vga_u0|controller|yCounter[7]~DUPLICATE_q\ ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter[7]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~34\,
	sharein => \vga_u0|controller|controller_translator|Add1~35\,
	sumout => \vga_u0|controller|controller_translator|Add1~37_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~38\,
	shareout => \vga_u0|controller|controller_translator|Add1~39\);

-- Location: LABCELL_X29_Y49_N24
\vga_u0|controller|controller_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~2\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~3\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~38\,
	sharein => \vga_u0|controller|controller_translator|Add1~39\,
	sumout => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~2\,
	shareout => \vga_u0|controller|controller_translator|Add1~3\);

-- Location: LABCELL_X29_Y49_N27
\vga_u0|controller|controller_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~5_sumout\ = SUM(( GND ) + ( \vga_u0|controller|controller_translator|Add1~3\ ) + ( \vga_u0|controller|controller_translator|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|controller|controller_translator|Add1~2\,
	sharein => \vga_u0|controller|controller_translator|Add1~3\,
	sumout => \vga_u0|controller|controller_translator|Add1~5_sumout\);

-- Location: LABCELL_X31_Y49_N15
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2) = ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ & ( \vga_u0|controller|controller_translator|Add1~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2));

-- Location: LABCELL_X27_Y48_N45
\sm|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr2~0_combout\ = ( \sm|current_state[2]~DUPLICATE_q\ & ( !\sm|current_state\(3) ) ) # ( !\sm|current_state[2]~DUPLICATE_q\ & ( (!\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state\(1) & \sm|current_state\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010001000000000001000100011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state\(1),
	datad => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	combout => \sm|WideOr2~0_combout\);

-- Location: LABCELL_X33_Y48_N0
\dp|xp~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~0_combout\ = ( \dp|xp\(0) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initxp~0_combout\ & \dp|Add1~1_sumout\)) ) ) # ( !\dp|xp\(0) & ( (!\sm|initxp~0_combout\ & (\sm|WideOr1~0_combout\ & \dp|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001011001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_Add1~1_sumout\,
	dataf => \dp|ALT_INV_xp\(0),
	combout => \dp|xp~0_combout\);

-- Location: FF_X33_Y48_N2
\dp|x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~0_combout\,
	asdata => \dp|xf~0_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(0));

-- Location: LABCELL_X33_Y48_N3
\dp|xp~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~1_combout\ = ( \dp|xp\(1) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initxp~0_combout\ & \dp|Add1~5_sumout\)) ) ) # ( !\dp|xp\(1) & ( (!\sm|initxp~0_combout\ & (\sm|WideOr1~0_combout\ & \dp|Add1~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001011001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_initxp~0_combout\,
	datab => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \dp|ALT_INV_Add1~5_sumout\,
	dataf => \dp|ALT_INV_xp\(1),
	combout => \dp|xp~1_combout\);

-- Location: FF_X33_Y48_N4
\dp|x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~1_combout\,
	asdata => \dp|xf~1_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(1));

-- Location: FF_X33_Y48_N41
\dp|x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~2_combout\,
	asdata => \dp|xf~2_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(2));

-- Location: FF_X33_Y48_N14
\dp|x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~3_combout\,
	asdata => \dp|xf~3_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(3));

-- Location: FF_X33_Y48_N17
\dp|x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|xp~4_combout\,
	asdata => \dp|xf~4_combout\,
	sload => \dp|Equal4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(4));

-- Location: FF_X25_Y49_N19
\vga_u0|controller|xCounter[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[6]~DUPLICATE_q\);

-- Location: M10K_X38_Y47_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a5\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: MLABCELL_X28_Y48_N9
\vga_u0|VideoMemory|auto_generated|decode2|w_anode105w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2) = ( !\vga_u0|user_input_translator|Add1~5_sumout\ & ( (\sm|plot~0_combout\ & (!\vga_u0|user_input_translator|Add1~1_sumout\ & !\vga_u0|writeEn~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000000000010001000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_plot~0_combout\,
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	datad => \vga_u0|ALT_INV_writeEn~0_combout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2));

-- Location: LABCELL_X31_Y49_N21
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2) = ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2));

-- Location: M10K_X26_Y48_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a2\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\);

-- Location: MLABCELL_X25_Y49_N30
\vga_u0|controller|on_screen~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~0_combout\ = ( !\vga_u0|controller|xCounter\(1) & ( !\vga_u0|controller|xCounter\(5) & ( (!\vga_u0|controller|xCounter\(6) & (!\vga_u0|controller|xCounter\(2) & (!\vga_u0|controller|xCounter[3]~DUPLICATE_q\ & 
-- !\vga_u0|controller|xCounter\(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(6),
	datab => \vga_u0|controller|ALT_INV_xCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter[3]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(4),
	datae => \vga_u0|controller|ALT_INV_xCounter\(1),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(5),
	combout => \vga_u0|controller|on_screen~0_combout\);

-- Location: FF_X31_Y49_N49
\vga_u0|controller|yCounter[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[6]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y49_N3
\vga_u0|controller|LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|LessThan7~0_combout\ = ( \vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(9) & ((!\vga_u0|controller|yCounter\(5)) # ((!\vga_u0|controller|yCounter[6]~DUPLICATE_q\) # (!\vga_u0|controller|yCounter\(8))))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(7) & ( !\vga_u0|controller|yCounter\(9) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110010001100110011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|LessThan7~0_combout\);

-- Location: MLABCELL_X25_Y49_N39
\vga_u0|controller|on_screen~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~1_combout\ = ( \vga_u0|controller|xCounter\(8) & ( (!\vga_u0|controller|xCounter\(9) & \vga_u0|controller|LessThan7~0_combout\) ) ) # ( !\vga_u0|controller|xCounter\(8) & ( (\vga_u0|controller|LessThan7~0_combout\ & 
-- ((!\vga_u0|controller|xCounter\(7)) # ((!\vga_u0|controller|xCounter\(9)) # (\vga_u0|controller|on_screen~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101111000000001110111100000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(7),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_on_screen~0_combout\,
	datad => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(8),
	combout => \vga_u0|controller|on_screen~1_combout\);

-- Location: FF_X29_Y49_N28
\vga_u0|VideoMemory|auto_generated|address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~5_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1));

-- Location: FF_X29_Y49_N32
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1));

-- Location: FF_X29_Y49_N26
\vga_u0|VideoMemory|auto_generated|address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0));

-- Location: FF_X29_Y49_N38
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0));

-- Location: MLABCELL_X28_Y48_N0
\vga_u0|VideoMemory|auto_generated|decode2|w_anode126w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2) = ( !\vga_u0|user_input_translator|Add1~5_sumout\ & ( (\sm|plot~0_combout\ & (!\vga_u0|writeEn~0_combout\ & \vga_u0|user_input_translator|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_plot~0_combout\,
	datac => \vga_u0|ALT_INV_writeEn~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2));

-- Location: LABCELL_X31_Y49_N9
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2) = ( \vga_u0|controller|controller_translator|Add1~5_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2));

-- Location: M10K_X38_Y49_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a7\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\);

-- Location: LABCELL_X29_Y49_N42
\vga_u0|controller|VGA_R[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_R[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( 
-- (\vga_u0|controller|on_screen~1_combout\ & ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & (\vga_u0|controller|on_screen~1_combout\ & !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & (\vga_u0|controller|on_screen~1_combout\ & 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000001010000000000000011000011110000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\,
	datac => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\,
	combout => \vga_u0|controller|VGA_R[0]~0_combout\);

-- Location: M10K_X26_Y49_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a1\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\);

-- Location: M10K_X26_Y47_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a4\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\);

-- Location: LABCELL_X29_Y49_N30
\vga_u0|controller|VGA_G[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_G[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & ( \vga_u0|controller|on_screen~1_combout\ ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & 
-- (\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\))))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & 
-- (\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010011000000000000000000000000010100110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datad => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\,
	combout => \vga_u0|controller|VGA_G[0]~0_combout\);

-- Location: LABCELL_X29_Y49_N54
\~GND\ : cyclonev_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: M10K_X38_Y48_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a3\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\);

-- Location: M10K_X26_Y50_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y50_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a6\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\);

-- Location: LABCELL_X29_Y49_N36
\vga_u0|controller|VGA_B[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_B[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\))) ) ) ) # ( 
-- \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ & (\vga_u0|controller|on_screen~1_combout\ & 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & ( 
-- (\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & (\vga_u0|controller|on_screen~1_combout\ & !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000001010000000000000011000011110000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	datac => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\,
	combout => \vga_u0|controller|VGA_B[0]~0_combout\);

-- Location: MLABCELL_X25_Y49_N42
\vga_u0|controller|VGA_HS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (((\vga_u0|controller|xCounter\(1) & \vga_u0|controller|xCounter\(0))) # (\vga_u0|controller|xCounter[3]~DUPLICATE_q\)) # (\vga_u0|controller|xCounter\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110111111111110011011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(1),
	datab => \vga_u0|controller|ALT_INV_xCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter\(0),
	datad => \vga_u0|controller|ALT_INV_xCounter[3]~DUPLICATE_q\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|VGA_HS1~0_combout\);

-- Location: MLABCELL_X25_Y49_N54
\vga_u0|controller|VGA_HS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~1_combout\ = ( \vga_u0|controller|xCounter\(8) & ( \vga_u0|controller|xCounter\(5) ) ) # ( !\vga_u0|controller|xCounter\(8) & ( \vga_u0|controller|xCounter\(5) & ( (!\vga_u0|controller|xCounter\(9)) # 
-- ((!\vga_u0|controller|xCounter\(7)) # ((\vga_u0|controller|xCounter\(6) & \vga_u0|controller|VGA_HS1~0_combout\))) ) ) ) # ( \vga_u0|controller|xCounter\(8) & ( !\vga_u0|controller|xCounter\(5) ) ) # ( !\vga_u0|controller|xCounter\(8) & ( 
-- !\vga_u0|controller|xCounter\(5) & ( (!\vga_u0|controller|xCounter\(9)) # ((!\vga_u0|controller|xCounter\(7)) # ((!\vga_u0|controller|xCounter\(6) & !\vga_u0|controller|VGA_HS1~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111101100111111111111111111111111110011011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(6),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	datae => \vga_u0|controller|ALT_INV_xCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(5),
	combout => \vga_u0|controller|VGA_HS1~1_combout\);

-- Location: FF_X25_Y49_N55
\vga_u0|controller|VGA_HS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_HS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS1~q\);

-- Location: LABCELL_X30_Y50_N0
\vga_u0|controller|VGA_HS~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS~feeder_combout\ = ( \vga_u0|controller|VGA_HS1~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \vga_u0|controller|ALT_INV_VGA_HS1~q\,
	combout => \vga_u0|controller|VGA_HS~feeder_combout\);

-- Location: FF_X30_Y50_N1
\vga_u0|controller|VGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_HS~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS~q\);

-- Location: LABCELL_X31_Y49_N24
\vga_u0|controller|VGA_VS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~0_combout\ = ( \vga_u0|controller|yCounter\(2) & ( !\vga_u0|controller|yCounter\(4) & ( (!\vga_u0|controller|yCounter\(9) & (\vga_u0|controller|yCounter\(3) & (!\vga_u0|controller|yCounter\(1) $ 
-- (!\vga_u0|controller|yCounter\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000100100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(1),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(0),
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	datae => \vga_u0|controller|ALT_INV_yCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(4),
	combout => \vga_u0|controller|VGA_VS1~0_combout\);

-- Location: LABCELL_X30_Y50_N36
\vga_u0|controller|VGA_VS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~1_combout\ = ( \vga_u0|controller|yCounter\(5) & ( \vga_u0|controller|yCounter\(8) & ( (!\vga_u0|controller|yCounter[7]~DUPLICATE_q\) # ((!\vga_u0|controller|VGA_VS1~0_combout\) # (!\vga_u0|controller|yCounter[6]~DUPLICATE_q\)) 
-- ) ) ) # ( !\vga_u0|controller|yCounter\(5) & ( \vga_u0|controller|yCounter\(8) ) ) # ( \vga_u0|controller|yCounter\(5) & ( !\vga_u0|controller|yCounter\(8) ) ) # ( !\vga_u0|controller|yCounter\(5) & ( !\vga_u0|controller|yCounter\(8) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[7]~DUPLICATE_q\,
	datab => \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\,
	datac => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datae => \vga_u0|controller|ALT_INV_yCounter\(5),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(8),
	combout => \vga_u0|controller|VGA_VS1~1_combout\);

-- Location: FF_X30_Y50_N37
\vga_u0|controller|VGA_VS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_VS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS1~q\);

-- Location: FF_X30_Y50_N40
\vga_u0|controller|VGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_VS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS~q\);

-- Location: MLABCELL_X25_Y49_N48
\vga_u0|controller|VGA_BLANK1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_BLANK1~0_combout\ = ( \vga_u0|controller|xCounter\(7) & ( (!\vga_u0|controller|xCounter\(9) & \vga_u0|controller|LessThan7~0_combout\) ) ) # ( !\vga_u0|controller|xCounter\(7) & ( (\vga_u0|controller|LessThan7~0_combout\ & 
-- ((!\vga_u0|controller|xCounter\(8)) # (!\vga_u0|controller|xCounter\(9)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000001110000011100000111000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(7),
	combout => \vga_u0|controller|VGA_BLANK1~0_combout\);

-- Location: FF_X25_Y49_N49
\vga_u0|controller|VGA_BLANK1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_BLANK1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK1~q\);

-- Location: LABCELL_X27_Y16_N0
\vga_u0|controller|VGA_BLANK~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_BLANK~feeder_combout\ = ( \vga_u0|controller|VGA_BLANK1~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \vga_u0|controller|ALT_INV_VGA_BLANK1~q\,
	combout => \vga_u0|controller|VGA_BLANK~feeder_combout\);

-- Location: FF_X27_Y16_N1
\vga_u0|controller|VGA_BLANK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_BLANK~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK~q\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;


