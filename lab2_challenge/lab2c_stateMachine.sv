module ch_stateMachine(resetb, ydone, xdone, xpdone, ypdone, initx, inity, loadx, loady, plot, CLOCK_50, current_state, cond, colour, initxp, inityp, loadxp, loadyp);
//outputs and inputs
input resetb, ydone, xdone, xpdone, ypdone, CLOCK_50, cond;
output reg initx, inity, plot, loadx, loady, initxp, inityp, loadxp, loadyp;
output [3:0] current_state;
output reg [2:0] colour;
//regs
reg [3:0] current_state, next_state;

//next state logic
always_comb
		case (current_state)
			4'b0000: next_state <= 4'b0010;
 			4'b0001: next_state <= 4'b0010;
 			4'b0010: if (xpdone == 0) next_state <= 4'b0010;
 						else if (ypdone == 0) next_state <= 4'b0001;
							else next_state <= 4'b0011;
			4'b0011: next_state <= 4'b0100;
			4'b0100: next_state <= 4'b0110;
			4'b0101: next_state <= 4'b0110;
			4'b0110: if(cond == 1) next_state <= 4'b0111;
					else if (xdone == 0) next_state <= 4'b0110;
						else if (ydone == 0) next_state <= 4'b0101;
							else next_state <= 4'b1000;
			4'b0111: next_state <= 4'b0110;
			default: next_state <= 4'b1000;
		endcase
		
//vDFF
always_ff @(posedge(CLOCK_50) or negedge resetb) begin
	if(resetb == 0)
		current_state = 4'b0000;
	else
 		current_state <= next_state;
end
 	
//output logic
always_comb
	case (current_state)
 		4'b0000: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000011110000;
 		4'b0001: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000010110000;
 		4'b0010: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000000011000;
		4'b0011: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000000000000;
		4'b0100: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b111100000110;
		4'b0101: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b101100000110;
		4'b0110: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000100000110;
		4'b0111: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000000001110;
		4'b1000: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000000000110;
 		default: {initx,inity,loady,loadx, initxp, inityp, loadyp, loadxp, plot, colour} <= 12'b000000000000;
endcase
endmodule
