module ch_datapath(CLOCK_50, resetb, current_state,  initx, inity, loadx, loady, xdone, ydone, xpdone, ypdone, x, y, initxp, inityp, loadxp, loadyp, cond);
input resetb, initx, inity, loadx, loady, initxp, inityp, loadxp, loadyp, CLOCK_50;
input [3:0] current_state;
//outputs
output reg xdone, ydone, xpdone, ypdone, cond;
output [6:0] y;
output [7:0] x;

//regs
reg [6:0] yf, yp, x;
reg [7:0] xf, xp, y;

//constants
parameter R = 30;
parameter CX = 80;
parameter CY = 60;


always_ff @(posedge(CLOCK_50))
	begin

	//clear screen algorithm
	if (loadyp == 1) begin
 		if (inityp == 1)
 			yp = 0;
 		else
 			yp ++;
	end
	if (loadxp == 1) begin
 		if (initxp == 1)
 			xp = 0;
 		else 
 			xp ++;
	end
 	ypdone <= 0;
 	xpdone <= 0;
 	if (yp == 119)
 		ypdone <= 1;
 	if (xp == 159)
 		xpdone <= 1;
	
	//fill circle algorithm
 		if (loady == 1)
 			if (inity == 1)
 				yf = CY-R+1;
 			else
 				yf ++;
		if (loadx == 1)
 			if (initx == 1)
 				xf = CX-R+1;
 			else 
 				xf ++;
	
 		ydone <= 0;
 		xdone <= 0;
		cond <= 0;
		if (((xf-CX)*(xf-CX))+((yf-CY)*(yf-CY)) <= (R*R))
			cond <= 1;
 		if (yf == (CY+R)) begin
 			ydone <= 1;
			cond <= 0;
			end
 		if (xf == (CX+R)) begin
 			xdone <= 1;
			cond <= 0;
			end

	case(current_state)
		4'b0010: x <= xp;
		default: x <= xf;
	endcase
	
	case(current_state)
		4'b0010: y <= yp;
		default: y <= yf;
	endcase
end
			
endmodule
