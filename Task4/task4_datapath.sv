module task4_datapath(CLOCK_50, resetb, current_state, init_ox, init_oy, init_crit, load_ox, load_oy, load_crit, initx, inity, loadx, loady, xdone, ydone, x, ox, y, oy, loadcx, loadcy, loadc, initc, initcx, initcy, cnum);

	input CLOCK_50, resetb, init_ox, init_oy, init_crit, load_ox, load_oy, load_crit, initx, inity, loadx, loady, loadcx, loadcy, loadc, initc, initcx, initcy;
	input [3:0] current_state;
	
	output xdone, ydone;
	output [2:0] cnum;
	output [6:0] y, oy;
	output [7:0] x, ox;
	
	reg  signed [8:0] crit;
	reg xdone, ydone;
	reg [2:0] cnum;
	reg [6:0] yp, y, oy, cy;
	reg [7:0] xp, x, ox, cx;
	
	
	//global constants
   parameter R  = 10;

	always_ff @(posedge(CLOCK_50))
		begin
			//clear screen datapath
			if (loady == 1) begin
 				if (inity == 1)
 					yp = 0;
 				else
 					yp ++;
			end
			if (loadx == 1) begin
 				if (initx == 1)
 					xp = 0;
 				else 
 					xp ++;
			end
 			ydone <= 0;
 			xdone <= 0;
 			if (yp == 119)
 				ydone <= 1;
 			if (xp == 159)
 				xdone <= 1;
			
			//center x,y, cnum
			if (loadc == 1) begin
 				if (initc == 1)
 					cnum = 1;
 				else 
						cnum ++;
			end
			if (loadcx == 1) begin
 				if (initcx == 1)
 					cx = 48;
 				else 
 					cx = cx + 12;
			end
			if (loadcy == 1) begin
 				if (initcy == 1)
 					cy = 50;
 				else begin
 					if(cnum % 2 == 1)
						cy = 50;
					else
						cy = 60;
				end
			end
			
			//bresenham circle algorithm datapath
			if(load_oy == 1)
				if(init_oy == 1)
					oy = 0;
				else
					oy++;
					
			if(load_ox == 1) begin
				if(init_ox == 1)
					ox = R;
			end
				
			if(load_crit == 1)
				if(init_crit == 1)
					crit = (1 - R);
				else begin
					if(crit <= 0)
						crit = crit + 2*oy + 1;
					else begin
						ox--;
						crit = crit + 2*(oy - ox) + 1;
					end
				end
				
				
			//output mux for x
			case(current_state)
				4'b0101: x <= cx + ox;
				4'b0110: x <= cx + oy;
				4'b0111: x <= cx - ox;
				4'b1000: x <= cx - oy;
				4'b1001: x <= cx - ox;
				4'b1010: x <= cx - oy;
				4'b1011: x <= cx + ox;
				4'b1100: x <= cx + oy;
				4'b0010: x <= xp;
				default: x <= 0;
            endcase
				
			//output mux for y
			case(current_state)
				4'b0101: y <= cy + oy;
				4'b0110: y <= cy + ox;
				4'b0111: y <= cy + oy;
				4'b1000: y <= cy + ox;
				4'b1001: y <= cy - oy;
				4'b1010: y <= cy - ox;
				4'b1011: y <= cy - oy;
				4'b1100: y <= cy - ox;
				4'b0010: y <= yp;
				default: y <= 0;
            endcase
					
		end
endmodule

