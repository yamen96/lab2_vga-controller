-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.1 Build 190 01/19/2015 SJ Full Version"

-- DATE "09/30/2016 20:18:46"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	task4 IS
    PORT (
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	VGA_R : OUT std_logic_vector(9 DOWNTO 0);
	VGA_G : OUT std_logic_vector(9 DOWNTO 0);
	VGA_B : OUT std_logic_vector(9 DOWNTO 0);
	VGA_HS : OUT std_logic;
	VGA_VS : OUT std_logic;
	VGA_BLANK : OUT std_logic;
	VGA_SYNC : OUT std_logic;
	VGA_CLK : OUT std_logic;
	LEDR : OUT std_logic_vector(9 DOWNTO 0)
	);
END task4;

-- Design Ports Information
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[8]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[9]	=>  Location: PIN_AK8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[8]	=>  Location: PIN_E9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[9]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[8]	=>  Location: PIN_C5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[9]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_BLANK	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_SYNC	=>  Location: PIN_AH27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_W20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF task4 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_VGA_R : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_BLANK : std_logic;
SIGNAL ww_VGA_SYNC : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \vga_u0|controller|Add0~37_sumout\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \vga_u0|controller|Add0~6\ : std_logic;
SIGNAL \vga_u0|controller|Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[9]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~38\ : std_logic;
SIGNAL \vga_u0|controller|Add0~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~18\ : std_logic;
SIGNAL \vga_u0|controller|Add0~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~34\ : std_logic;
SIGNAL \vga_u0|controller|Add0~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~30\ : std_logic;
SIGNAL \vga_u0|controller|Add0~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~14\ : std_logic;
SIGNAL \vga_u0|controller|Add0~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~26\ : std_logic;
SIGNAL \vga_u0|controller|Add0~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~22\ : std_logic;
SIGNAL \vga_u0|controller|Add0~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~10\ : std_logic;
SIGNAL \vga_u0|controller|Add0~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~6\ : std_logic;
SIGNAL \vga_u0|controller|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[5]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|LessThan7~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~1_combout\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \sm|current_state[3]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|WideOr1~0_combout\ : std_logic;
SIGNAL \sm|initx~0_combout\ : std_logic;
SIGNAL \dp|xp~8_combout\ : std_logic;
SIGNAL \dp|Add1~13_sumout\ : std_logic;
SIGNAL \dp|Add1~14\ : std_logic;
SIGNAL \dp|Add1~17_sumout\ : std_logic;
SIGNAL \dp|Add1~18\ : std_logic;
SIGNAL \dp|Add1~21_sumout\ : std_logic;
SIGNAL \dp|Add1~22\ : std_logic;
SIGNAL \dp|Add1~25_sumout\ : std_logic;
SIGNAL \dp|Equal1~0_combout\ : std_logic;
SIGNAL \dp|xp~4_combout\ : std_logic;
SIGNAL \dp|Add1~26\ : std_logic;
SIGNAL \dp|Add1~29_sumout\ : std_logic;
SIGNAL \dp|Add1~30\ : std_logic;
SIGNAL \dp|Add1~9_sumout\ : std_logic;
SIGNAL \dp|Equal1~1_combout\ : std_logic;
SIGNAL \dp|xp~3_combout\ : std_logic;
SIGNAL \dp|Add1~10\ : std_logic;
SIGNAL \dp|Add1~5_sumout\ : std_logic;
SIGNAL \dp|Add1~6\ : std_logic;
SIGNAL \dp|Add1~1_sumout\ : std_logic;
SIGNAL \dp|xp~0_combout\ : std_logic;
SIGNAL \dp|xp~1_combout\ : std_logic;
SIGNAL \dp|Equal1~2_combout\ : std_logic;
SIGNAL \dp|xdone~q\ : std_logic;
SIGNAL \sm|Mux2~0_combout\ : std_logic;
SIGNAL \sm|current_state[0]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|Decoder0~0_combout\ : std_logic;
SIGNAL \dp|Add4~21_sumout\ : std_logic;
SIGNAL \dp|oy~6_combout\ : std_logic;
SIGNAL \dp|Add4~22\ : std_logic;
SIGNAL \dp|Add4~17_sumout\ : std_logic;
SIGNAL \dp|Add4~18\ : std_logic;
SIGNAL \dp|Add4~25_sumout\ : std_logic;
SIGNAL \dp|Add4~26\ : std_logic;
SIGNAL \dp|Add4~13_sumout\ : std_logic;
SIGNAL \dp|Add4~14\ : std_logic;
SIGNAL \dp|Add4~9_sumout\ : std_logic;
SIGNAL \dp|Add4~10\ : std_logic;
SIGNAL \dp|Add4~5_sumout\ : std_logic;
SIGNAL \dp|Add4~6\ : std_logic;
SIGNAL \dp|Add4~1_sumout\ : std_logic;
SIGNAL \dp|Add7~25_sumout\ : std_logic;
SIGNAL \dp|Add8~34_cout\ : std_logic;
SIGNAL \dp|Add8~6\ : std_logic;
SIGNAL \dp|Add8~10\ : std_logic;
SIGNAL \dp|Add8~14\ : std_logic;
SIGNAL \dp|Add8~17_sumout\ : std_logic;
SIGNAL \dp|crit[4]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add8~13_sumout\ : std_logic;
SIGNAL \dp|Add8~9_sumout\ : std_logic;
SIGNAL \dp|Add8~5_sumout\ : std_logic;
SIGNAL \dp|Add9~29_sumout\ : std_logic;
SIGNAL \dp|Add5~29_sumout\ : std_logic;
SIGNAL \dp|crit~7_combout\ : std_logic;
SIGNAL \dp|Add9~30\ : std_logic;
SIGNAL \dp|Add9~5_sumout\ : std_logic;
SIGNAL \dp|Add5~30\ : std_logic;
SIGNAL \dp|Add5~5_sumout\ : std_logic;
SIGNAL \dp|crit~1_combout\ : std_logic;
SIGNAL \dp|Add9~6\ : std_logic;
SIGNAL \dp|Add9~9_sumout\ : std_logic;
SIGNAL \dp|Add5~6\ : std_logic;
SIGNAL \dp|Add5~9_sumout\ : std_logic;
SIGNAL \dp|crit~2_combout\ : std_logic;
SIGNAL \dp|Add9~10\ : std_logic;
SIGNAL \dp|Add9~13_sumout\ : std_logic;
SIGNAL \dp|Add5~10\ : std_logic;
SIGNAL \dp|Add5~13_sumout\ : std_logic;
SIGNAL \dp|crit~3_combout\ : std_logic;
SIGNAL \dp|Add9~14\ : std_logic;
SIGNAL \dp|Add9~17_sumout\ : std_logic;
SIGNAL \dp|Add5~14\ : std_logic;
SIGNAL \dp|Add5~17_sumout\ : std_logic;
SIGNAL \dp|crit~4_combout\ : std_logic;
SIGNAL \dp|Add5~18\ : std_logic;
SIGNAL \dp|Add5~25_sumout\ : std_logic;
SIGNAL \dp|Add8~18\ : std_logic;
SIGNAL \dp|Add8~25_sumout\ : std_logic;
SIGNAL \dp|Add9~18\ : std_logic;
SIGNAL \dp|Add9~25_sumout\ : std_logic;
SIGNAL \dp|crit~6_combout\ : std_logic;
SIGNAL \dp|Add5~26\ : std_logic;
SIGNAL \dp|Add5~21_sumout\ : std_logic;
SIGNAL \dp|Add8~26\ : std_logic;
SIGNAL \dp|Add8~21_sumout\ : std_logic;
SIGNAL \dp|Add9~26\ : std_logic;
SIGNAL \dp|Add9~21_sumout\ : std_logic;
SIGNAL \dp|crit~5_combout\ : std_logic;
SIGNAL \dp|LessThan0~0_combout\ : std_logic;
SIGNAL \dp|Add5~22\ : std_logic;
SIGNAL \dp|Add5~34\ : std_logic;
SIGNAL \dp|Add5~1_sumout\ : std_logic;
SIGNAL \dp|Add8~30\ : std_logic;
SIGNAL \dp|Add8~1_sumout\ : std_logic;
SIGNAL \dp|Add9~22\ : std_logic;
SIGNAL \dp|Add9~34\ : std_logic;
SIGNAL \dp|Add9~1_sumout\ : std_logic;
SIGNAL \dp|crit~0_combout\ : std_logic;
SIGNAL \dp|LessThan0~1_combout\ : std_logic;
SIGNAL \dp|ox~8_combout\ : std_logic;
SIGNAL \dp|Add7~26\ : std_logic;
SIGNAL \dp|Add7~21_sumout\ : std_logic;
SIGNAL \dp|ox~1_combout\ : std_logic;
SIGNAL \dp|ox~7_combout\ : std_logic;
SIGNAL \dp|Add7~22\ : std_logic;
SIGNAL \dp|Add7~30\ : std_logic;
SIGNAL \dp|Add7~17_sumout\ : std_logic;
SIGNAL \dp|ox~6_combout\ : std_logic;
SIGNAL \dp|Add7~18\ : std_logic;
SIGNAL \dp|Add7~13_sumout\ : std_logic;
SIGNAL \dp|ox~5_combout\ : std_logic;
SIGNAL \dp|Add7~14\ : std_logic;
SIGNAL \dp|Add7~9_sumout\ : std_logic;
SIGNAL \dp|Add8~22\ : std_logic;
SIGNAL \dp|Add8~29_sumout\ : std_logic;
SIGNAL \dp|Add9~33_sumout\ : std_logic;
SIGNAL \dp|Add5~33_sumout\ : std_logic;
SIGNAL \dp|crit~8_combout\ : std_logic;
SIGNAL \dp|ox~0_combout\ : std_logic;
SIGNAL \dp|ox~4_combout\ : std_logic;
SIGNAL \dp|Add7~10\ : std_logic;
SIGNAL \dp|Add7~5_sumout\ : std_logic;
SIGNAL \dp|ox~3_combout\ : std_logic;
SIGNAL \dp|Add7~6\ : std_logic;
SIGNAL \dp|Add7~1_sumout\ : std_logic;
SIGNAL \dp|ox~2_combout\ : std_logic;
SIGNAL \sm|LessThan0~0_combout\ : std_logic;
SIGNAL \sm|Mux0~0_combout\ : std_logic;
SIGNAL \sm|WideOr0~0_combout\ : std_logic;
SIGNAL \sm|current_state[2]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|WideOr3~0_combout\ : std_logic;
SIGNAL \dp|Add7~29_sumout\ : std_logic;
SIGNAL \dp|ox~9_combout\ : std_logic;
SIGNAL \sm|LessThan0~3_combout\ : std_logic;
SIGNAL \sm|LessThan0~6_combout\ : std_logic;
SIGNAL \sm|LessThan0~1_combout\ : std_logic;
SIGNAL \sm|LessThan0~2_combout\ : std_logic;
SIGNAL \sm|LessThan0~5_combout\ : std_logic;
SIGNAL \sm|LessThan0~4_combout\ : std_logic;
SIGNAL \sm|LessThan0~7_combout\ : std_logic;
SIGNAL \dp|Add0~25_sumout\ : std_logic;
SIGNAL \sm|Decoder0~2_combout\ : std_logic;
SIGNAL \dp|Add0~26\ : std_logic;
SIGNAL \dp|Add0~21_sumout\ : std_logic;
SIGNAL \dp|Add0~22\ : std_logic;
SIGNAL \dp|Add0~17_sumout\ : std_logic;
SIGNAL \dp|Add0~18\ : std_logic;
SIGNAL \dp|Add0~13_sumout\ : std_logic;
SIGNAL \dp|Add0~14\ : std_logic;
SIGNAL \dp|Add0~9_sumout\ : std_logic;
SIGNAL \dp|Add0~10\ : std_logic;
SIGNAL \dp|Add0~5_sumout\ : std_logic;
SIGNAL \dp|yp~1_combout\ : std_logic;
SIGNAL \dp|yp~4_combout\ : std_logic;
SIGNAL \dp|yp~2_combout\ : std_logic;
SIGNAL \dp|yp~3_combout\ : std_logic;
SIGNAL \dp|Add0~6\ : std_logic;
SIGNAL \dp|Add0~1_sumout\ : std_logic;
SIGNAL \dp|yp~0_combout\ : std_logic;
SIGNAL \dp|Equal0~0_combout\ : std_logic;
SIGNAL \dp|Equal0~1_combout\ : std_logic;
SIGNAL \dp|ydone~q\ : std_logic;
SIGNAL \sm|Mux1~1_combout\ : std_logic;
SIGNAL \sm|Mux1~0_combout\ : std_logic;
SIGNAL \sm|Mux1~2_combout\ : std_logic;
SIGNAL \sm|current_state[1]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|writeEn~1_combout\ : std_logic;
SIGNAL \sm|Decoder0~1_combout\ : std_logic;
SIGNAL \sm|loadc~0_combout\ : std_logic;
SIGNAL \dp|cx[5]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add3~13_sumout\ : std_logic;
SIGNAL \dp|cx~2_combout\ : std_logic;
SIGNAL \dp|Add3~14\ : std_logic;
SIGNAL \dp|Add3~17_sumout\ : std_logic;
SIGNAL \dp|cx~5_combout\ : std_logic;
SIGNAL \dp|Add3~18\ : std_logic;
SIGNAL \dp|Add3~21_sumout\ : std_logic;
SIGNAL \dp|cx~6_combout\ : std_logic;
SIGNAL \dp|Add3~22\ : std_logic;
SIGNAL \dp|Add3~25_sumout\ : std_logic;
SIGNAL \dp|cx~7_combout\ : std_logic;
SIGNAL \dp|Add3~26\ : std_logic;
SIGNAL \dp|Add3~9_sumout\ : std_logic;
SIGNAL \dp|cx~4_combout\ : std_logic;
SIGNAL \dp|oy~1_combout\ : std_logic;
SIGNAL \dp|Add11~33_combout\ : std_logic;
SIGNAL \dp|oy~4_combout\ : std_logic;
SIGNAL \dp|Add11~40_combout\ : std_logic;
SIGNAL \dp|oy~5_combout\ : std_logic;
SIGNAL \dp|Add11~39_combout\ : std_logic;
SIGNAL \dp|oy~11_combout\ : std_logic;
SIGNAL \dp|Add11~38_combout\ : std_logic;
SIGNAL \dp|Add11~34_combout\ : std_logic;
SIGNAL \dp|oy~9_combout\ : std_logic;
SIGNAL \dp|Add11~37_combout\ : std_logic;
SIGNAL \dp|oy~10_combout\ : std_logic;
SIGNAL \dp|oy~7_combout\ : std_logic;
SIGNAL \dp|Add11~35_combout\ : std_logic;
SIGNAL \dp|Add11~36_combout\ : std_logic;
SIGNAL \dp|oy~8_combout\ : std_logic;
SIGNAL \dp|Add11~14\ : std_logic;
SIGNAL \dp|Add11~18\ : std_logic;
SIGNAL \dp|Add11~22\ : std_logic;
SIGNAL \dp|Add11~26\ : std_logic;
SIGNAL \dp|Add11~30\ : std_logic;
SIGNAL \dp|Add11~9_sumout\ : std_logic;
SIGNAL \dp|x[5]~feeder_combout\ : std_logic;
SIGNAL \dp|xp~2_combout\ : std_logic;
SIGNAL \dp|x[1]~0_combout\ : std_logic;
SIGNAL \dp|x[1]~1_combout\ : std_logic;
SIGNAL \dp|cx~3_combout\ : std_logic;
SIGNAL \dp|Add3~10\ : std_logic;
SIGNAL \dp|Add3~5_sumout\ : std_logic;
SIGNAL \dp|cx~1_combout\ : std_logic;
SIGNAL \dp|oy~0_combout\ : std_logic;
SIGNAL \dp|Add11~32_combout\ : std_logic;
SIGNAL \dp|Add11~10\ : std_logic;
SIGNAL \dp|Add11~5_sumout\ : std_logic;
SIGNAL \dp|x[6]~feeder_combout\ : std_logic;
SIGNAL \dp|Add3~6\ : std_logic;
SIGNAL \dp|Add3~1_sumout\ : std_logic;
SIGNAL \dp|cx~0_combout\ : std_logic;
SIGNAL \dp|Add11~41_combout\ : std_logic;
SIGNAL \dp|cx[7]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|Add11~42_combout\ : std_logic;
SIGNAL \dp|Add11~6\ : std_logic;
SIGNAL \dp|Add11~1_sumout\ : std_logic;
SIGNAL \dp|x[7]~feeder_combout\ : std_logic;
SIGNAL \vga_u0|writeEn~0_combout\ : std_logic;
SIGNAL \dp|oy~2_combout\ : std_logic;
SIGNAL \dp|oy~3_combout\ : std_logic;
SIGNAL \dp|Add15~21_combout\ : std_logic;
SIGNAL \dp|Add15~19_combout\ : std_logic;
SIGNAL \dp|Add15~18_combout\ : std_logic;
SIGNAL \dp|Add15~16_combout\ : std_logic;
SIGNAL \dp|Add15~17_combout\ : std_logic;
SIGNAL \dp|Add15~20_combout\ : std_logic;
SIGNAL \dp|cnum~0_combout\ : std_logic;
SIGNAL \dp|cy~0_combout\ : std_logic;
SIGNAL \dp|Add15~22_combout\ : std_logic;
SIGNAL \dp|Add15~23_combout\ : std_logic;
SIGNAL \dp|Add15~24_combout\ : std_logic;
SIGNAL \dp|Add15~37_combout\ : std_logic;
SIGNAL \dp|Add15~39_combout\ : std_logic;
SIGNAL \dp|Add15~38_combout\ : std_logic;
SIGNAL \dp|Add15~31\ : std_logic;
SIGNAL \dp|Add15~35\ : std_logic;
SIGNAL \dp|Add15~27\ : std_logic;
SIGNAL \dp|Add15~14\ : std_logic;
SIGNAL \dp|Add15~10\ : std_logic;
SIGNAL \dp|Add15~6\ : std_logic;
SIGNAL \dp|Add15~1_sumout\ : std_logic;
SIGNAL \dp|y[6]~feeder_combout\ : std_logic;
SIGNAL \dp|Add15~5_sumout\ : std_logic;
SIGNAL \dp|y[5]~feeder_combout\ : std_logic;
SIGNAL \dp|Add15~9_sumout\ : std_logic;
SIGNAL \dp|y[4]~feeder_combout\ : std_logic;
SIGNAL \dp|Add15~13_sumout\ : std_logic;
SIGNAL \dp|y[3]~feeder_combout\ : std_logic;
SIGNAL \dp|Add15~26_sumout\ : std_logic;
SIGNAL \dp|y[2]~feeder_combout\ : std_logic;
SIGNAL \dp|Add15~34_sumout\ : std_logic;
SIGNAL \dp|y[1]~feeder_combout\ : std_logic;
SIGNAL \dp|yp~6_combout\ : std_logic;
SIGNAL \dp|Add15~30_sumout\ : std_logic;
SIGNAL \dp|y[0]~feeder_combout\ : std_logic;
SIGNAL \dp|yp~5_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~6\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~7\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|LessThan3~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~2\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~3\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~5_sumout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \dp|Add11~13_sumout\ : std_logic;
SIGNAL \dp|x[0]~feeder_combout\ : std_logic;
SIGNAL \dp|Add11~17_sumout\ : std_logic;
SIGNAL \dp|x[1]~feeder_combout\ : std_logic;
SIGNAL \dp|Add11~21_sumout\ : std_logic;
SIGNAL \dp|x[2]~feeder_combout\ : std_logic;
SIGNAL \dp|xp~5_combout\ : std_logic;
SIGNAL \dp|Add11~25_sumout\ : std_logic;
SIGNAL \dp|x[3]~feeder_combout\ : std_logic;
SIGNAL \dp|xp~6_combout\ : std_logic;
SIGNAL \dp|Add11~29_sumout\ : std_logic;
SIGNAL \dp|x[4]~feeder_combout\ : std_logic;
SIGNAL \dp|xp~7_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \vga_u0|controller|VGA_R[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_G[0]~0_combout\ : std_logic;
SIGNAL \sm|WideOr4~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_B[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK~q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dp|x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \sm|current_state\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \dp|y\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|xp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|yp\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|oy\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|ox\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|crit\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dp|cx\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|cnum\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dp|cy\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \dp|ALT_INV_y\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_xp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add11~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_oy\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_yp\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add15~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_x\ : std_logic_vector(7 DOWNTO 5);
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ : std_logic;
SIGNAL \dp|ALT_INV_cx[5]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_cx[7]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_crit[4]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[1]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[2]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[3]~DUPLICATE_q\ : std_logic;
SIGNAL \sm|ALT_INV_current_state[0]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~42_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~41_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~39_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~38_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~37_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cy~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~40_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cx\ : std_logic_vector(7 DOWNTO 1);
SIGNAL \dp|ALT_INV_Add11~39_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~38_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~11_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cx~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~10_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~37_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~9_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~8_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~36_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~35_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~7_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~34_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~9_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~8_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~7_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~6_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal1~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~24_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~5_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~6_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~23_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~5_combout\ : std_logic;
SIGNAL \dp|ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cy\ : std_logic_vector(5 DOWNTO 5);
SIGNAL \dp|ALT_INV_Add15~22_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cnum\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dp|ALT_INV_Add15~21_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~20_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~19_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~18_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~17_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~16_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~33_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_cx~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add11~32_combout\ : std_logic;
SIGNAL \dp|ALT_INV_oy~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~3_combout\ : std_logic;
SIGNAL \sm|ALT_INV_loadc~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Decoder0~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_crit\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dp|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_WideOr3~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Decoder0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~3_combout\ : std_logic;
SIGNAL \sm|ALT_INV_Mux1~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ydone~q\ : std_logic;
SIGNAL \sm|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~7_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~6_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~5_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~4_combout\ : std_logic;
SIGNAL \dp|ALT_INV_ox\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \sm|ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \sm|ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xdone~q\ : std_logic;
SIGNAL \dp|ALT_INV_yp~3_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~2_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_yp~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~1_combout\ : std_logic;
SIGNAL \dp|ALT_INV_xp~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_WideOr1~0_combout\ : std_logic;
SIGNAL \sm|ALT_INV_initx~0_combout\ : std_logic;
SIGNAL \vga_u0|ALT_INV_writeEn~1_combout\ : std_logic;
SIGNAL \sm|ALT_INV_current_state\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|ALT_INV_LessThan3~0_combout\ : std_logic;
SIGNAL \vga_u0|ALT_INV_writeEn~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add8~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~33_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~33_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add9~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add5~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~34_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~30_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~29_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~25_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~21_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add15~26_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~17_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~13_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~9_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add4~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~5_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add3~1_sumout\ : std_logic;
SIGNAL \dp|ALT_INV_Add7~1_sumout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
VGA_R <= ww_VGA_R;
VGA_G <= ww_VGA_G;
VGA_B <= ww_VGA_B;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_BLANK <= ww_VGA_BLANK;
VGA_SYNC <= ww_VGA_SYNC;
VGA_CLK <= ww_VGA_CLK;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ <= (\~GND~combout\ & \~GND~combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & \dp|x\(3) & \dp|x\(2) & \dp|x\(1) & 
\dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(0);
\vga_u0|VideoMemory|auto_generated|ram_block1a8\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ <= (gnd & \sm|WideOr4~0_combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & \dp|x\(3) & \dp|x\(2) & \dp|x\(1) & 
\dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\(0) <= \sm|WideOr4~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\(0) <= \sm|WideOr4~0_combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \dp|x\(4) & 
\dp|x\(3) & \dp|x\(2) & \dp|x\(1) & \dp|x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\(6);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\
& \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\);
\dp|ALT_INV_y\(1) <= NOT \dp|y\(1);
\dp|ALT_INV_y\(0) <= NOT \dp|y\(0);
\dp|ALT_INV_Add1~29_sumout\ <= NOT \dp|Add1~29_sumout\;
\dp|ALT_INV_xp\(4) <= NOT \dp|xp\(4);
\dp|ALT_INV_Add11~29_sumout\ <= NOT \dp|Add11~29_sumout\;
\dp|ALT_INV_Add1~25_sumout\ <= NOT \dp|Add1~25_sumout\;
\dp|ALT_INV_xp\(3) <= NOT \dp|xp\(3);
\dp|ALT_INV_Add11~25_sumout\ <= NOT \dp|Add11~25_sumout\;
\dp|ALT_INV_Add1~21_sumout\ <= NOT \dp|Add1~21_sumout\;
\dp|ALT_INV_xp\(2) <= NOT \dp|xp\(2);
\dp|ALT_INV_Add11~21_sumout\ <= NOT \dp|Add11~21_sumout\;
\dp|ALT_INV_Add1~17_sumout\ <= NOT \dp|Add1~17_sumout\;
\dp|ALT_INV_xp\(1) <= NOT \dp|xp\(1);
\dp|ALT_INV_Add11~17_sumout\ <= NOT \dp|Add11~17_sumout\;
\dp|ALT_INV_Add1~13_sumout\ <= NOT \dp|Add1~13_sumout\;
\dp|ALT_INV_xp\(0) <= NOT \dp|xp\(0);
\dp|ALT_INV_Add11~13_sumout\ <= NOT \dp|Add11~13_sumout\;
\dp|ALT_INV_oy\(4) <= NOT \dp|oy\(4);
\dp|ALT_INV_oy\(5) <= NOT \dp|oy\(5);
\dp|ALT_INV_oy\(2) <= NOT \dp|oy\(2);
\dp|ALT_INV_oy\(3) <= NOT \dp|oy\(3);
\dp|ALT_INV_oy\(0) <= NOT \dp|oy\(0);
\dp|ALT_INV_oy\(1) <= NOT \dp|oy\(1);
\dp|ALT_INV_oy\(6) <= NOT \dp|oy\(6);
\dp|ALT_INV_Add0~13_sumout\ <= NOT \dp|Add0~13_sumout\;
\dp|ALT_INV_yp\(3) <= NOT \dp|yp\(3);
\dp|ALT_INV_Add15~13_sumout\ <= NOT \dp|Add15~13_sumout\;
\dp|ALT_INV_Add0~9_sumout\ <= NOT \dp|Add0~9_sumout\;
\dp|ALT_INV_yp\(4) <= NOT \dp|yp\(4);
\dp|ALT_INV_Add15~9_sumout\ <= NOT \dp|Add15~9_sumout\;
\dp|ALT_INV_Add0~5_sumout\ <= NOT \dp|Add0~5_sumout\;
\dp|ALT_INV_yp\(5) <= NOT \dp|yp\(5);
\dp|ALT_INV_Add15~5_sumout\ <= NOT \dp|Add15~5_sumout\;
\dp|ALT_INV_Add0~1_sumout\ <= NOT \dp|Add0~1_sumout\;
\dp|ALT_INV_yp\(6) <= NOT \dp|yp\(6);
\dp|ALT_INV_Add15~1_sumout\ <= NOT \dp|Add15~1_sumout\;
\dp|ALT_INV_Add1~9_sumout\ <= NOT \dp|Add1~9_sumout\;
\dp|ALT_INV_xp\(5) <= NOT \dp|xp\(5);
\dp|ALT_INV_Add11~9_sumout\ <= NOT \dp|Add11~9_sumout\;
\dp|ALT_INV_Add1~5_sumout\ <= NOT \dp|Add1~5_sumout\;
\dp|ALT_INV_xp\(6) <= NOT \dp|xp\(6);
\dp|ALT_INV_Add11~5_sumout\ <= NOT \dp|Add11~5_sumout\;
\dp|ALT_INV_Add1~1_sumout\ <= NOT \dp|Add1~1_sumout\;
\dp|ALT_INV_xp\(7) <= NOT \dp|xp\(7);
\dp|ALT_INV_Add11~1_sumout\ <= NOT \dp|Add11~1_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~5_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~1_sumout\;
\dp|ALT_INV_y\(3) <= NOT \dp|y\(3);
\dp|ALT_INV_y\(4) <= NOT \dp|y\(4);
\dp|ALT_INV_y\(5) <= NOT \dp|y\(5);
\dp|ALT_INV_y\(6) <= NOT \dp|y\(6);
\dp|ALT_INV_x\(5) <= NOT \dp|x\(5);
\dp|ALT_INV_x\(6) <= NOT \dp|x\(6);
\dp|ALT_INV_x\(7) <= NOT \dp|x\(7);
\vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|user_input_translator|Add1~5_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|user_input_translator|Add1~1_sumout\;
\vga_u0|controller|ALT_INV_yCounter\(0) <= NOT \vga_u0|controller|yCounter\(0);
\vga_u0|controller|ALT_INV_yCounter\(1) <= NOT \vga_u0|controller|yCounter\(1);
\vga_u0|controller|ALT_INV_yCounter\(2) <= NOT \vga_u0|controller|yCounter\(2);
\vga_u0|controller|ALT_INV_yCounter\(3) <= NOT \vga_u0|controller|yCounter\(3);
\vga_u0|controller|ALT_INV_yCounter\(4) <= NOT \vga_u0|controller|yCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(0) <= NOT \vga_u0|controller|xCounter\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a8\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\;
\vga_u0|controller|ALT_INV_xCounter\(2) <= NOT \vga_u0|controller|xCounter\(2);
\vga_u0|controller|ALT_INV_xCounter\(3) <= NOT \vga_u0|controller|xCounter\(3);
\vga_u0|controller|ALT_INV_xCounter\(5) <= NOT \vga_u0|controller|xCounter\(5);
\vga_u0|controller|ALT_INV_xCounter\(6) <= NOT \vga_u0|controller|xCounter\(6);
\vga_u0|controller|ALT_INV_xCounter\(1) <= NOT \vga_u0|controller|xCounter\(1);
\vga_u0|controller|ALT_INV_xCounter\(4) <= NOT \vga_u0|controller|xCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(7) <= NOT \vga_u0|controller|xCounter\(7);
\vga_u0|controller|ALT_INV_xCounter\(8) <= NOT \vga_u0|controller|xCounter\(8);
\vga_u0|controller|ALT_INV_xCounter\(9) <= NOT \vga_u0|controller|xCounter\(9);
\vga_u0|controller|ALT_INV_yCounter\(5) <= NOT \vga_u0|controller|yCounter\(5);
\vga_u0|controller|ALT_INV_yCounter\(6) <= NOT \vga_u0|controller|yCounter\(6);
\vga_u0|controller|ALT_INV_yCounter\(7) <= NOT \vga_u0|controller|yCounter\(7);
\vga_u0|controller|ALT_INV_yCounter\(8) <= NOT \vga_u0|controller|yCounter\(8);
\vga_u0|controller|ALT_INV_yCounter\(9) <= NOT \vga_u0|controller|yCounter\(9);
\dp|ALT_INV_cx[5]~DUPLICATE_q\ <= NOT \dp|cx[5]~DUPLICATE_q\;
\dp|ALT_INV_cx[7]~DUPLICATE_q\ <= NOT \dp|cx[7]~DUPLICATE_q\;
\dp|ALT_INV_crit[4]~DUPLICATE_q\ <= NOT \dp|crit[4]~DUPLICATE_q\;
\sm|ALT_INV_current_state[1]~DUPLICATE_q\ <= NOT \sm|current_state[1]~DUPLICATE_q\;
\sm|ALT_INV_current_state[2]~DUPLICATE_q\ <= NOT \sm|current_state[2]~DUPLICATE_q\;
\sm|ALT_INV_current_state[3]~DUPLICATE_q\ <= NOT \sm|current_state[3]~DUPLICATE_q\;
\sm|ALT_INV_current_state[0]~DUPLICATE_q\ <= NOT \sm|current_state[0]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[8]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[9]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[5]~DUPLICATE_q\;
\dp|ALT_INV_Add11~42_combout\ <= NOT \dp|Add11~42_combout\;
\dp|ALT_INV_Add11~41_combout\ <= NOT \dp|Add11~41_combout\;
\dp|ALT_INV_Add15~39_combout\ <= NOT \dp|Add15~39_combout\;
\dp|ALT_INV_Add15~38_combout\ <= NOT \dp|Add15~38_combout\;
\dp|ALT_INV_Add15~37_combout\ <= NOT \dp|Add15~37_combout\;
\dp|ALT_INV_cy~0_combout\ <= NOT \dp|cy~0_combout\;
\dp|ALT_INV_Add11~40_combout\ <= NOT \dp|Add11~40_combout\;
\dp|ALT_INV_cx\(4) <= NOT \dp|cx\(4);
\dp|ALT_INV_Add11~39_combout\ <= NOT \dp|Add11~39_combout\;
\dp|ALT_INV_cx\(3) <= NOT \dp|cx\(3);
\dp|ALT_INV_Add11~38_combout\ <= NOT \dp|Add11~38_combout\;
\dp|ALT_INV_oy~11_combout\ <= NOT \dp|oy~11_combout\;
\dp|ALT_INV_cx\(2) <= NOT \dp|cx\(2);
\dp|ALT_INV_cx~2_combout\ <= NOT \dp|cx~2_combout\;
\dp|ALT_INV_cx\(1) <= NOT \dp|cx\(1);
\dp|ALT_INV_oy~10_combout\ <= NOT \dp|oy~10_combout\;
\dp|ALT_INV_Add11~37_combout\ <= NOT \dp|Add11~37_combout\;
\dp|ALT_INV_oy~9_combout\ <= NOT \dp|oy~9_combout\;
\dp|ALT_INV_oy~8_combout\ <= NOT \dp|oy~8_combout\;
\dp|ALT_INV_Add11~36_combout\ <= NOT \dp|Add11~36_combout\;
\dp|ALT_INV_Add11~35_combout\ <= NOT \dp|Add11~35_combout\;
\dp|ALT_INV_oy~7_combout\ <= NOT \dp|oy~7_combout\;
\dp|ALT_INV_Add11~34_combout\ <= NOT \dp|Add11~34_combout\;
\dp|ALT_INV_Equal0~0_combout\ <= NOT \dp|Equal0~0_combout\;
\dp|ALT_INV_yp~4_combout\ <= NOT \dp|yp~4_combout\;
\dp|ALT_INV_ox~9_combout\ <= NOT \dp|ox~9_combout\;
\dp|ALT_INV_ox~8_combout\ <= NOT \dp|ox~8_combout\;
\dp|ALT_INV_ox~7_combout\ <= NOT \dp|ox~7_combout\;
\dp|ALT_INV_oy~6_combout\ <= NOT \dp|oy~6_combout\;
\dp|ALT_INV_Equal1~1_combout\ <= NOT \dp|Equal1~1_combout\;
\dp|ALT_INV_Equal1~0_combout\ <= NOT \dp|Equal1~0_combout\;
\dp|ALT_INV_Add15~24_combout\ <= NOT \dp|Add15~24_combout\;
\dp|ALT_INV_oy~5_combout\ <= NOT \dp|oy~5_combout\;
\dp|ALT_INV_ox~6_combout\ <= NOT \dp|ox~6_combout\;
\dp|ALT_INV_Add15~23_combout\ <= NOT \dp|Add15~23_combout\;
\dp|ALT_INV_oy~4_combout\ <= NOT \dp|oy~4_combout\;
\dp|ALT_INV_ox~5_combout\ <= NOT \dp|ox~5_combout\;
\dp|ALT_INV_LessThan0~1_combout\ <= NOT \dp|LessThan0~1_combout\;
\dp|ALT_INV_cy\(5) <= NOT \dp|cy\(5);
\dp|ALT_INV_Add15~22_combout\ <= NOT \dp|Add15~22_combout\;
\dp|ALT_INV_cnum\(0) <= NOT \dp|cnum\(0);
\dp|ALT_INV_Add15~21_combout\ <= NOT \dp|Add15~21_combout\;
\dp|ALT_INV_Add15~20_combout\ <= NOT \dp|Add15~20_combout\;
\dp|ALT_INV_Add15~19_combout\ <= NOT \dp|Add15~19_combout\;
\dp|ALT_INV_oy~3_combout\ <= NOT \dp|oy~3_combout\;
\dp|ALT_INV_Add15~18_combout\ <= NOT \dp|Add15~18_combout\;
\dp|ALT_INV_Add15~17_combout\ <= NOT \dp|Add15~17_combout\;
\dp|ALT_INV_Add15~16_combout\ <= NOT \dp|Add15~16_combout\;
\dp|ALT_INV_oy~2_combout\ <= NOT \dp|oy~2_combout\;
\dp|ALT_INV_Add11~33_combout\ <= NOT \dp|Add11~33_combout\;
\dp|ALT_INV_oy~1_combout\ <= NOT \dp|oy~1_combout\;
\dp|ALT_INV_ox~4_combout\ <= NOT \dp|ox~4_combout\;
\dp|ALT_INV_cx\(5) <= NOT \dp|cx\(5);
\dp|ALT_INV_cx~1_combout\ <= NOT \dp|cx~1_combout\;
\dp|ALT_INV_cx\(6) <= NOT \dp|cx\(6);
\dp|ALT_INV_Add11~32_combout\ <= NOT \dp|Add11~32_combout\;
\dp|ALT_INV_oy~0_combout\ <= NOT \dp|oy~0_combout\;
\dp|ALT_INV_ox~3_combout\ <= NOT \dp|ox~3_combout\;
\dp|ALT_INV_cx\(7) <= NOT \dp|cx\(7);
\sm|ALT_INV_loadc~0_combout\ <= NOT \sm|loadc~0_combout\;
\sm|ALT_INV_Decoder0~1_combout\ <= NOT \sm|Decoder0~1_combout\;
\dp|ALT_INV_ox~2_combout\ <= NOT \dp|ox~2_combout\;
\dp|ALT_INV_ox~1_combout\ <= NOT \dp|ox~1_combout\;
\dp|ALT_INV_ox~0_combout\ <= NOT \dp|ox~0_combout\;
\dp|ALT_INV_crit\(7) <= NOT \dp|crit\(7);
\dp|ALT_INV_crit\(0) <= NOT \dp|crit\(0);
\dp|ALT_INV_LessThan0~0_combout\ <= NOT \dp|LessThan0~0_combout\;
\dp|ALT_INV_crit\(5) <= NOT \dp|crit\(5);
\dp|ALT_INV_crit\(6) <= NOT \dp|crit\(6);
\dp|ALT_INV_crit\(4) <= NOT \dp|crit\(4);
\dp|ALT_INV_crit\(3) <= NOT \dp|crit\(3);
\dp|ALT_INV_crit\(2) <= NOT \dp|crit\(2);
\dp|ALT_INV_crit\(1) <= NOT \dp|crit\(1);
\dp|ALT_INV_crit\(8) <= NOT \dp|crit\(8);
\sm|ALT_INV_WideOr3~0_combout\ <= NOT \sm|WideOr3~0_combout\;
\sm|ALT_INV_Decoder0~0_combout\ <= NOT \sm|Decoder0~0_combout\;
\vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ <= NOT \vga_u0|controller|VGA_VS1~0_combout\;
\vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ <= NOT \vga_u0|controller|VGA_HS1~0_combout\;
\dp|ALT_INV_xp~4_combout\ <= NOT \dp|xp~4_combout\;
\dp|ALT_INV_xp~3_combout\ <= NOT \dp|xp~3_combout\;
\sm|ALT_INV_Mux1~1_combout\ <= NOT \sm|Mux1~1_combout\;
\dp|ALT_INV_ydone~q\ <= NOT \dp|ydone~q\;
\sm|ALT_INV_Mux1~0_combout\ <= NOT \sm|Mux1~0_combout\;
\sm|ALT_INV_LessThan0~7_combout\ <= NOT \sm|LessThan0~7_combout\;
\sm|ALT_INV_LessThan0~6_combout\ <= NOT \sm|LessThan0~6_combout\;
\sm|ALT_INV_LessThan0~5_combout\ <= NOT \sm|LessThan0~5_combout\;
\sm|ALT_INV_LessThan0~4_combout\ <= NOT \sm|LessThan0~4_combout\;
\dp|ALT_INV_ox\(4) <= NOT \dp|ox\(4);
\dp|ALT_INV_ox\(5) <= NOT \dp|ox\(5);
\sm|ALT_INV_LessThan0~3_combout\ <= NOT \sm|LessThan0~3_combout\;
\sm|ALT_INV_LessThan0~2_combout\ <= NOT \sm|LessThan0~2_combout\;
\dp|ALT_INV_ox\(2) <= NOT \dp|ox\(2);
\dp|ALT_INV_ox\(3) <= NOT \dp|ox\(3);
\sm|ALT_INV_LessThan0~1_combout\ <= NOT \sm|LessThan0~1_combout\;
\dp|ALT_INV_ox\(0) <= NOT \dp|ox\(0);
\dp|ALT_INV_ox\(1) <= NOT \dp|ox\(1);
\sm|ALT_INV_LessThan0~0_combout\ <= NOT \sm|LessThan0~0_combout\;
\dp|ALT_INV_ox\(7) <= NOT \dp|ox\(7);
\dp|ALT_INV_ox\(6) <= NOT \dp|ox\(6);
\dp|ALT_INV_xdone~q\ <= NOT \dp|xdone~q\;
\dp|ALT_INV_yp~3_combout\ <= NOT \dp|yp~3_combout\;
\dp|ALT_INV_yp~2_combout\ <= NOT \dp|yp~2_combout\;
\dp|ALT_INV_yp~1_combout\ <= NOT \dp|yp~1_combout\;
\dp|ALT_INV_yp~0_combout\ <= NOT \dp|yp~0_combout\;
\dp|ALT_INV_xp~1_combout\ <= NOT \dp|xp~1_combout\;
\dp|ALT_INV_xp~0_combout\ <= NOT \dp|xp~0_combout\;
\sm|ALT_INV_WideOr1~0_combout\ <= NOT \sm|WideOr1~0_combout\;
\sm|ALT_INV_initx~0_combout\ <= NOT \sm|initx~0_combout\;
\vga_u0|ALT_INV_writeEn~1_combout\ <= NOT \vga_u0|writeEn~1_combout\;
\sm|ALT_INV_current_state\(1) <= NOT \sm|current_state\(1);
\sm|ALT_INV_current_state\(2) <= NOT \sm|current_state\(2);
\sm|ALT_INV_current_state\(3) <= NOT \sm|current_state\(3);
\sm|ALT_INV_current_state\(0) <= NOT \sm|current_state\(0);
\vga_u0|ALT_INV_LessThan3~0_combout\ <= NOT \vga_u0|LessThan3~0_combout\;
\vga_u0|ALT_INV_writeEn~0_combout\ <= NOT \vga_u0|writeEn~0_combout\;
\vga_u0|controller|ALT_INV_always1~1_combout\ <= NOT \vga_u0|controller|always1~1_combout\;
\vga_u0|controller|ALT_INV_always1~0_combout\ <= NOT \vga_u0|controller|always1~0_combout\;
\vga_u0|controller|ALT_INV_Equal0~1_combout\ <= NOT \vga_u0|controller|Equal0~1_combout\;
\vga_u0|controller|ALT_INV_Equal0~0_combout\ <= NOT \vga_u0|controller|Equal0~0_combout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1);
\vga_u0|controller|ALT_INV_on_screen~1_combout\ <= NOT \vga_u0|controller|on_screen~1_combout\;
\vga_u0|controller|ALT_INV_on_screen~0_combout\ <= NOT \vga_u0|controller|on_screen~0_combout\;
\vga_u0|controller|ALT_INV_LessThan7~0_combout\ <= NOT \vga_u0|controller|LessThan7~0_combout\;
\dp|ALT_INV_Add8~29_sumout\ <= NOT \dp|Add8~29_sumout\;
\dp|ALT_INV_Add8~25_sumout\ <= NOT \dp|Add8~25_sumout\;
\dp|ALT_INV_Add8~21_sumout\ <= NOT \dp|Add8~21_sumout\;
\dp|ALT_INV_Add8~17_sumout\ <= NOT \dp|Add8~17_sumout\;
\dp|ALT_INV_Add8~13_sumout\ <= NOT \dp|Add8~13_sumout\;
\dp|ALT_INV_Add8~9_sumout\ <= NOT \dp|Add8~9_sumout\;
\dp|ALT_INV_Add8~5_sumout\ <= NOT \dp|Add8~5_sumout\;
\dp|ALT_INV_Add8~1_sumout\ <= NOT \dp|Add8~1_sumout\;
\dp|ALT_INV_Add9~33_sumout\ <= NOT \dp|Add9~33_sumout\;
\dp|ALT_INV_Add5~33_sumout\ <= NOT \dp|Add5~33_sumout\;
\dp|ALT_INV_Add9~29_sumout\ <= NOT \dp|Add9~29_sumout\;
\dp|ALT_INV_Add5~29_sumout\ <= NOT \dp|Add5~29_sumout\;
\dp|ALT_INV_Add9~25_sumout\ <= NOT \dp|Add9~25_sumout\;
\dp|ALT_INV_Add5~25_sumout\ <= NOT \dp|Add5~25_sumout\;
\dp|ALT_INV_Add9~21_sumout\ <= NOT \dp|Add9~21_sumout\;
\dp|ALT_INV_Add5~21_sumout\ <= NOT \dp|Add5~21_sumout\;
\dp|ALT_INV_Add9~17_sumout\ <= NOT \dp|Add9~17_sumout\;
\dp|ALT_INV_Add5~17_sumout\ <= NOT \dp|Add5~17_sumout\;
\dp|ALT_INV_Add9~13_sumout\ <= NOT \dp|Add9~13_sumout\;
\dp|ALT_INV_Add5~13_sumout\ <= NOT \dp|Add5~13_sumout\;
\dp|ALT_INV_Add9~9_sumout\ <= NOT \dp|Add9~9_sumout\;
\dp|ALT_INV_Add5~9_sumout\ <= NOT \dp|Add5~9_sumout\;
\dp|ALT_INV_Add9~5_sumout\ <= NOT \dp|Add9~5_sumout\;
\dp|ALT_INV_Add5~5_sumout\ <= NOT \dp|Add5~5_sumout\;
\dp|ALT_INV_Add9~1_sumout\ <= NOT \dp|Add9~1_sumout\;
\dp|ALT_INV_Add5~1_sumout\ <= NOT \dp|Add5~1_sumout\;
\dp|ALT_INV_Add15~34_sumout\ <= NOT \dp|Add15~34_sumout\;
\dp|ALT_INV_Add15~30_sumout\ <= NOT \dp|Add15~30_sumout\;
\dp|ALT_INV_Add3~25_sumout\ <= NOT \dp|Add3~25_sumout\;
\dp|ALT_INV_Add3~21_sumout\ <= NOT \dp|Add3~21_sumout\;
\dp|ALT_INV_Add3~17_sumout\ <= NOT \dp|Add3~17_sumout\;
\dp|ALT_INV_Add3~13_sumout\ <= NOT \dp|Add3~13_sumout\;
\dp|ALT_INV_Add0~25_sumout\ <= NOT \dp|Add0~25_sumout\;
\dp|ALT_INV_Add0~21_sumout\ <= NOT \dp|Add0~21_sumout\;
\dp|ALT_INV_yp\(0) <= NOT \dp|yp\(0);
\dp|ALT_INV_yp\(1) <= NOT \dp|yp\(1);
\dp|ALT_INV_yp\(2) <= NOT \dp|yp\(2);
\dp|ALT_INV_Add4~25_sumout\ <= NOT \dp|Add4~25_sumout\;
\dp|ALT_INV_Add7~29_sumout\ <= NOT \dp|Add7~29_sumout\;
\dp|ALT_INV_Add4~21_sumout\ <= NOT \dp|Add4~21_sumout\;
\dp|ALT_INV_Add4~17_sumout\ <= NOT \dp|Add4~17_sumout\;
\dp|ALT_INV_Add7~25_sumout\ <= NOT \dp|Add7~25_sumout\;
\dp|ALT_INV_Add7~21_sumout\ <= NOT \dp|Add7~21_sumout\;
\dp|ALT_INV_Add0~17_sumout\ <= NOT \dp|Add0~17_sumout\;
\dp|ALT_INV_Add15~26_sumout\ <= NOT \dp|Add15~26_sumout\;
\dp|ALT_INV_Add4~13_sumout\ <= NOT \dp|Add4~13_sumout\;
\dp|ALT_INV_Add7~17_sumout\ <= NOT \dp|Add7~17_sumout\;
\dp|ALT_INV_Add4~9_sumout\ <= NOT \dp|Add4~9_sumout\;
\dp|ALT_INV_Add7~13_sumout\ <= NOT \dp|Add7~13_sumout\;
\dp|ALT_INV_Add3~9_sumout\ <= NOT \dp|Add3~9_sumout\;
\dp|ALT_INV_Add4~5_sumout\ <= NOT \dp|Add4~5_sumout\;
\dp|ALT_INV_Add7~9_sumout\ <= NOT \dp|Add7~9_sumout\;
\dp|ALT_INV_Add3~5_sumout\ <= NOT \dp|Add3~5_sumout\;
\dp|ALT_INV_Add4~1_sumout\ <= NOT \dp|Add4~1_sumout\;
\dp|ALT_INV_Add7~5_sumout\ <= NOT \dp|Add7~5_sumout\;
\dp|ALT_INV_Add3~1_sumout\ <= NOT \dp|Add3~1_sumout\;
\dp|ALT_INV_Add7~1_sumout\ <= NOT \dp|Add7~1_sumout\;
\dp|ALT_INV_y\(2) <= NOT \dp|y\(2);

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X28_Y0_N2
\VGA_R[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(8));

-- Location: IOOBUF_X28_Y0_N53
\VGA_R[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(9));

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X30_Y81_N2
\VGA_G[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(8));

-- Location: IOOBUF_X30_Y81_N53
\VGA_G[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(9));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X22_Y81_N53
\VGA_B[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(8));

-- Location: IOOBUF_X30_Y81_N36
\VGA_B[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(9));

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X34_Y81_N59
\VGA_BLANK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_BLANK~q\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK);

-- Location: IOOBUF_X84_Y0_N53
\VGA_SYNC~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_VGA_SYNC);

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOOBUF_X52_Y0_N2
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N19
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X60_Y0_N2
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X80_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X60_Y0_N19
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X80_Y0_N19
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X84_Y0_N2
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X89_Y6_N5
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X89_Y8_N5
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X89_Y6_N22
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "gclk_far",
	nreset_invert => "true",
	output_clock_frequency => "300.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 4000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 10,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "fb_1",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 6,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 6,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 0,
	pll_m_cnt_prst => 1,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 1,
	pll_n_cnt_lo_div => 1,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "false",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	ecnc1test => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	tclk => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 6,
	dprio0_cnt_lo_div => 6,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "25.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	up0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	vco0ph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0));

-- Location: CLKCTRL_G6
\vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0),
	outclk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\);

-- Location: LABCELL_X33_Y80_N0
\vga_u0|controller|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~37_sumout\ = SUM(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add0~38\ = CARRY(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add0~37_sumout\,
	cout => \vga_u0|controller|Add0~38\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: FF_X33_Y80_N28
\vga_u0|controller|xCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(9));

-- Location: LABCELL_X33_Y80_N24
\vga_u0|controller|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~5_sumout\ = SUM(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))
-- \vga_u0|controller|Add0~6\ = CARRY(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	cin => \vga_u0|controller|Add0~10\,
	sumout => \vga_u0|controller|Add0~5_sumout\,
	cout => \vga_u0|controller|Add0~6\);

-- Location: LABCELL_X33_Y80_N27
\vga_u0|controller|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~1_sumout\ = SUM(( \vga_u0|controller|xCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|Add0~6\,
	sumout => \vga_u0|controller|Add0~1_sumout\);

-- Location: FF_X33_Y80_N29
\vga_u0|controller|xCounter[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[9]~DUPLICATE_q\);

-- Location: LABCELL_X33_Y80_N36
\vga_u0|controller|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (\vga_u0|controller|xCounter\(8) & (!\vga_u0|controller|xCounter\(7) & (\vga_u0|controller|xCounter[9]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001000000000000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(7),
	datac => \vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|Equal0~0_combout\);

-- Location: LABCELL_X33_Y80_N51
\vga_u0|controller|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~1_combout\ = ( !\vga_u0|controller|xCounter\(6) & ( (\vga_u0|controller|xCounter\(0) & (!\vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(0),
	datac => \vga_u0|controller|ALT_INV_xCounter\(5),
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|Equal0~1_combout\);

-- Location: LABCELL_X31_Y80_N15
\vga_u0|controller|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~2_combout\ = ( \vga_u0|controller|xCounter\(2) & ( (\vga_u0|controller|Equal0~0_combout\ & \vga_u0|controller|Equal0~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(2),
	combout => \vga_u0|controller|Equal0~2_combout\);

-- Location: FF_X33_Y80_N2
\vga_u0|controller|xCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(0));

-- Location: LABCELL_X33_Y80_N3
\vga_u0|controller|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~17_sumout\ = SUM(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))
-- \vga_u0|controller|Add0~18\ = CARRY(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	cin => \vga_u0|controller|Add0~38\,
	sumout => \vga_u0|controller|Add0~17_sumout\,
	cout => \vga_u0|controller|Add0~18\);

-- Location: FF_X33_Y80_N5
\vga_u0|controller|xCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(1));

-- Location: LABCELL_X33_Y80_N6
\vga_u0|controller|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~33_sumout\ = SUM(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))
-- \vga_u0|controller|Add0~34\ = CARRY(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	cin => \vga_u0|controller|Add0~18\,
	sumout => \vga_u0|controller|Add0~33_sumout\,
	cout => \vga_u0|controller|Add0~34\);

-- Location: FF_X33_Y80_N7
\vga_u0|controller|xCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(2));

-- Location: LABCELL_X33_Y80_N9
\vga_u0|controller|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~29_sumout\ = SUM(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))
-- \vga_u0|controller|Add0~30\ = CARRY(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	cin => \vga_u0|controller|Add0~34\,
	sumout => \vga_u0|controller|Add0~29_sumout\,
	cout => \vga_u0|controller|Add0~30\);

-- Location: FF_X33_Y80_N11
\vga_u0|controller|xCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(3));

-- Location: LABCELL_X33_Y80_N12
\vga_u0|controller|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~13_sumout\ = SUM(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))
-- \vga_u0|controller|Add0~14\ = CARRY(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(4),
	cin => \vga_u0|controller|Add0~30\,
	sumout => \vga_u0|controller|Add0~13_sumout\,
	cout => \vga_u0|controller|Add0~14\);

-- Location: FF_X33_Y80_N13
\vga_u0|controller|xCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(4));

-- Location: LABCELL_X33_Y80_N15
\vga_u0|controller|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~25_sumout\ = SUM(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))
-- \vga_u0|controller|Add0~26\ = CARRY(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	cin => \vga_u0|controller|Add0~14\,
	sumout => \vga_u0|controller|Add0~25_sumout\,
	cout => \vga_u0|controller|Add0~26\);

-- Location: FF_X33_Y80_N17
\vga_u0|controller|xCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(5));

-- Location: LABCELL_X33_Y80_N18
\vga_u0|controller|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~21_sumout\ = SUM(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))
-- \vga_u0|controller|Add0~22\ = CARRY(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	cin => \vga_u0|controller|Add0~26\,
	sumout => \vga_u0|controller|Add0~21_sumout\,
	cout => \vga_u0|controller|Add0~22\);

-- Location: FF_X33_Y80_N19
\vga_u0|controller|xCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(6));

-- Location: LABCELL_X33_Y80_N21
\vga_u0|controller|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~9_sumout\ = SUM(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))
-- \vga_u0|controller|Add0~10\ = CARRY(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => \vga_u0|controller|Add0~22\,
	sumout => \vga_u0|controller|Add0~9_sumout\,
	cout => \vga_u0|controller|Add0~10\);

-- Location: FF_X33_Y80_N22
\vga_u0|controller|xCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(7));

-- Location: FF_X33_Y80_N26
\vga_u0|controller|xCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(8));

-- Location: LABCELL_X33_Y80_N30
\vga_u0|controller|on_screen~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~0_combout\ = ( !\vga_u0|controller|xCounter\(1) & ( !\vga_u0|controller|xCounter\(4) & ( (!\vga_u0|controller|xCounter\(2) & (!\vga_u0|controller|xCounter\(5) & (!\vga_u0|controller|xCounter\(6) & 
-- !\vga_u0|controller|xCounter\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(2),
	datab => \vga_u0|controller|ALT_INV_xCounter\(5),
	datac => \vga_u0|controller|ALT_INV_xCounter\(6),
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	datae => \vga_u0|controller|ALT_INV_xCounter\(1),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|on_screen~0_combout\);

-- Location: LABCELL_X31_Y80_N30
\vga_u0|controller|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add1~37_sumout\,
	cout => \vga_u0|controller|Add1~38\);

-- Location: LABCELL_X31_Y80_N12
\vga_u0|controller|always1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~1_combout\ = ( \vga_u0|controller|yCounter\(3) & ( (!\vga_u0|controller|yCounter\(0) & (!\vga_u0|controller|yCounter\(4) & (!\vga_u0|controller|yCounter\(1) & \vga_u0|controller|yCounter\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(0),
	datab => \vga_u0|controller|ALT_INV_yCounter\(4),
	datac => \vga_u0|controller|ALT_INV_yCounter\(1),
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(3),
	combout => \vga_u0|controller|always1~1_combout\);

-- Location: LABCELL_X31_Y80_N48
\vga_u0|controller|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~13_sumout\ = SUM(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))
-- \vga_u0|controller|Add1~14\ = CARRY(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(6),
	cin => \vga_u0|controller|Add1~18\,
	sumout => \vga_u0|controller|Add1~13_sumout\,
	cout => \vga_u0|controller|Add1~14\);

-- Location: LABCELL_X31_Y80_N51
\vga_u0|controller|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~9_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))
-- \vga_u0|controller|Add1~10\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|Add1~14\,
	sumout => \vga_u0|controller|Add1~9_sumout\,
	cout => \vga_u0|controller|Add1~10\);

-- Location: FF_X31_Y80_N52
\vga_u0|controller|yCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~9_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(7));

-- Location: LABCELL_X31_Y80_N54
\vga_u0|controller|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~5_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))
-- \vga_u0|controller|Add1~6\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|Add1~10\,
	sumout => \vga_u0|controller|Add1~5_sumout\,
	cout => \vga_u0|controller|Add1~6\);

-- Location: FF_X31_Y80_N56
\vga_u0|controller|yCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(8));

-- Location: LABCELL_X31_Y80_N57
\vga_u0|controller|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	cin => \vga_u0|controller|Add1~6\,
	sumout => \vga_u0|controller|Add1~1_sumout\);

-- Location: FF_X31_Y80_N59
\vga_u0|controller|yCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~1_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(9));

-- Location: FF_X31_Y80_N47
\vga_u0|controller|yCounter[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[5]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y80_N0
\vga_u0|controller|always1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~0_combout\ = ( !\vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(6) & (\vga_u0|controller|yCounter\(9) & (!\vga_u0|controller|yCounter[5]~DUPLICATE_q\ & !\vga_u0|controller|yCounter\(8)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|always1~0_combout\);

-- Location: LABCELL_X31_Y80_N18
\vga_u0|controller|always1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~2_combout\ = ( \vga_u0|controller|xCounter\(2) & ( (\vga_u0|controller|Equal0~1_combout\ & (\vga_u0|controller|always1~1_combout\ & (\vga_u0|controller|always1~0_combout\ & \vga_u0|controller|Equal0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	datab => \vga_u0|controller|ALT_INV_always1~1_combout\,
	datac => \vga_u0|controller|ALT_INV_always1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(2),
	combout => \vga_u0|controller|always1~2_combout\);

-- Location: FF_X31_Y80_N31
\vga_u0|controller|yCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~37_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(0));

-- Location: LABCELL_X31_Y80_N33
\vga_u0|controller|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~33_sumout\ = SUM(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))
-- \vga_u0|controller|Add1~34\ = CARRY(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(1),
	cin => \vga_u0|controller|Add1~38\,
	sumout => \vga_u0|controller|Add1~33_sumout\,
	cout => \vga_u0|controller|Add1~34\);

-- Location: FF_X31_Y80_N35
\vga_u0|controller|yCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~33_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(1));

-- Location: LABCELL_X31_Y80_N36
\vga_u0|controller|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~29_sumout\ = SUM(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))
-- \vga_u0|controller|Add1~30\ = CARRY(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|Add1~34\,
	sumout => \vga_u0|controller|Add1~29_sumout\,
	cout => \vga_u0|controller|Add1~30\);

-- Location: FF_X31_Y80_N38
\vga_u0|controller|yCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(2));

-- Location: LABCELL_X31_Y80_N39
\vga_u0|controller|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~25_sumout\ = SUM(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))
-- \vga_u0|controller|Add1~26\ = CARRY(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|Add1~30\,
	sumout => \vga_u0|controller|Add1~25_sumout\,
	cout => \vga_u0|controller|Add1~26\);

-- Location: FF_X31_Y80_N41
\vga_u0|controller|yCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(3));

-- Location: LABCELL_X31_Y80_N42
\vga_u0|controller|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~21_sumout\ = SUM(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))
-- \vga_u0|controller|Add1~22\ = CARRY(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|Add1~26\,
	sumout => \vga_u0|controller|Add1~21_sumout\,
	cout => \vga_u0|controller|Add1~22\);

-- Location: FF_X31_Y80_N44
\vga_u0|controller|yCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~21_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(4));

-- Location: LABCELL_X31_Y80_N45
\vga_u0|controller|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~17_sumout\ = SUM(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))
-- \vga_u0|controller|Add1~18\ = CARRY(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|Add1~22\,
	sumout => \vga_u0|controller|Add1~17_sumout\,
	cout => \vga_u0|controller|Add1~18\);

-- Location: FF_X31_Y80_N46
\vga_u0|controller|yCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(5));

-- Location: FF_X31_Y80_N50
\vga_u0|controller|yCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(6));

-- Location: LABCELL_X31_Y80_N3
\vga_u0|controller|LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|LessThan7~0_combout\ = ( \vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(9) & ((!\vga_u0|controller|yCounter\(6)) # ((!\vga_u0|controller|yCounter[5]~DUPLICATE_q\) # (!\vga_u0|controller|yCounter\(8))))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(7) & ( !\vga_u0|controller|yCounter\(9) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110010001100110011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|LessThan7~0_combout\);

-- Location: LABCELL_X33_Y80_N39
\vga_u0|controller|on_screen~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~1_combout\ = ( \vga_u0|controller|xCounter[9]~DUPLICATE_q\ & ( (!\vga_u0|controller|xCounter\(8) & (\vga_u0|controller|LessThan7~0_combout\ & ((!\vga_u0|controller|xCounter\(7)) # (\vga_u0|controller|on_screen~0_combout\)))) ) 
-- ) # ( !\vga_u0|controller|xCounter[9]~DUPLICATE_q\ & ( \vga_u0|controller|LessThan7~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000100010100000000010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(7),
	datac => \vga_u0|controller|ALT_INV_on_screen~0_combout\,
	datad => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\,
	combout => \vga_u0|controller|on_screen~1_combout\);

-- Location: CLKCTRL_G5
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: FF_X31_Y78_N7
\sm|current_state[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux0~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[3]~DUPLICATE_q\);

-- Location: FF_X31_Y78_N20
\sm|current_state[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux1~2_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(1));

-- Location: FF_X31_Y78_N32
\sm|current_state[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux2~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(0));

-- Location: LABCELL_X31_Y78_N3
\sm|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr1~0_combout\ = ( \sm|current_state\(0) & ( (!\sm|current_state\(1) & (!\sm|current_state\(3) & !\sm|current_state[2]~DUPLICATE_q\)) ) ) # ( !\sm|current_state\(0) & ( (!\sm|current_state\(3) & !\sm|current_state[2]~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(3),
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \sm|WideOr1~0_combout\);

-- Location: FF_X31_Y78_N44
\sm|current_state[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|WideOr0~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(2));

-- Location: LABCELL_X31_Y78_N45
\sm|initx~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|initx~0_combout\ = ( !\sm|current_state\(3) & ( (!\sm|current_state\(1) & !\sm|current_state\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101000001010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state\(3),
	combout => \sm|initx~0_combout\);

-- Location: LABCELL_X29_Y78_N36
\dp|xp~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~8_combout\ = (\sm|WideOr1~0_combout\ & \sm|initx~0_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datac => \sm|ALT_INV_initx~0_combout\,
	combout => \dp|xp~8_combout\);

-- Location: FF_X29_Y77_N11
\dp|xp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~25_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(3));

-- Location: LABCELL_X29_Y77_N0
\dp|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~13_sumout\ = SUM(( \dp|xp\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add1~14\ = CARRY(( \dp|xp\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(0),
	cin => GND,
	sumout => \dp|Add1~13_sumout\,
	cout => \dp|Add1~14\);

-- Location: FF_X29_Y77_N2
\dp|xp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~13_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(0));

-- Location: LABCELL_X29_Y77_N3
\dp|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~17_sumout\ = SUM(( \dp|xp\(1) ) + ( GND ) + ( \dp|Add1~14\ ))
-- \dp|Add1~18\ = CARRY(( \dp|xp\(1) ) + ( GND ) + ( \dp|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(1),
	cin => \dp|Add1~14\,
	sumout => \dp|Add1~17_sumout\,
	cout => \dp|Add1~18\);

-- Location: FF_X29_Y77_N4
\dp|xp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~17_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(1));

-- Location: LABCELL_X29_Y77_N6
\dp|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~21_sumout\ = SUM(( \dp|xp\(2) ) + ( GND ) + ( \dp|Add1~18\ ))
-- \dp|Add1~22\ = CARRY(( \dp|xp\(2) ) + ( GND ) + ( \dp|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(2),
	cin => \dp|Add1~18\,
	sumout => \dp|Add1~21_sumout\,
	cout => \dp|Add1~22\);

-- Location: FF_X29_Y77_N8
\dp|xp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~21_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(2));

-- Location: LABCELL_X29_Y77_N9
\dp|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~25_sumout\ = SUM(( \dp|xp\(3) ) + ( GND ) + ( \dp|Add1~22\ ))
-- \dp|Add1~26\ = CARRY(( \dp|xp\(3) ) + ( GND ) + ( \dp|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(3),
	cin => \dp|Add1~22\,
	sumout => \dp|Add1~25_sumout\,
	cout => \dp|Add1~26\);

-- Location: LABCELL_X29_Y77_N48
\dp|Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~0_combout\ = ( \dp|Add1~21_sumout\ & ( \dp|xp\(2) & ( (!\sm|WideOr1~0_combout\ & (((\dp|xp\(3))))) # (\sm|WideOr1~0_combout\ & (\dp|Add1~25_sumout\ & (!\sm|initx~0_combout\))) ) ) ) # ( !\dp|Add1~21_sumout\ & ( \dp|xp\(2) & ( 
-- (!\sm|WideOr1~0_combout\ & \dp|xp\(3)) ) ) ) # ( \dp|Add1~21_sumout\ & ( !\dp|xp\(2) & ( (\dp|Add1~25_sumout\ & (!\sm|initx~0_combout\ & \sm|WideOr1~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001000000010000000000111100000000010011110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add1~25_sumout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \sm|ALT_INV_WideOr1~0_combout\,
	datad => \dp|ALT_INV_xp\(3),
	datae => \dp|ALT_INV_Add1~21_sumout\,
	dataf => \dp|ALT_INV_xp\(2),
	combout => \dp|Equal1~0_combout\);

-- Location: LABCELL_X29_Y77_N24
\dp|xp~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~4_combout\ = ( \sm|WideOr1~0_combout\ & ( (!\sm|initx~0_combout\ & \dp|Add1~17_sumout\) ) ) # ( !\sm|WideOr1~0_combout\ & ( \dp|xp\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xp\(1),
	datad => \dp|ALT_INV_Add1~17_sumout\,
	dataf => \sm|ALT_INV_WideOr1~0_combout\,
	combout => \dp|xp~4_combout\);

-- Location: FF_X29_Y77_N14
\dp|xp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~29_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(4));

-- Location: LABCELL_X29_Y77_N12
\dp|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~29_sumout\ = SUM(( \dp|xp\(4) ) + ( GND ) + ( \dp|Add1~26\ ))
-- \dp|Add1~30\ = CARRY(( \dp|xp\(4) ) + ( GND ) + ( \dp|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(4),
	cin => \dp|Add1~26\,
	sumout => \dp|Add1~29_sumout\,
	cout => \dp|Add1~30\);

-- Location: LABCELL_X29_Y77_N15
\dp|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~9_sumout\ = SUM(( \dp|xp\(5) ) + ( GND ) + ( \dp|Add1~30\ ))
-- \dp|Add1~10\ = CARRY(( \dp|xp\(5) ) + ( GND ) + ( \dp|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(5),
	cin => \dp|Add1~30\,
	sumout => \dp|Add1~9_sumout\,
	cout => \dp|Add1~10\);

-- Location: FF_X29_Y77_N17
\dp|xp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~9_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(5));

-- Location: LABCELL_X29_Y77_N54
\dp|Equal1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~1_combout\ = ( \dp|Add1~9_sumout\ & ( \sm|initx~0_combout\ & ( (!\dp|xp\(5) & (!\sm|WideOr1~0_combout\ & \dp|xp\(4))) ) ) ) # ( !\dp|Add1~9_sumout\ & ( \sm|initx~0_combout\ & ( (!\dp|xp\(5) & (!\sm|WideOr1~0_combout\ & \dp|xp\(4))) ) ) ) # ( 
-- \dp|Add1~9_sumout\ & ( !\sm|initx~0_combout\ & ( (!\dp|xp\(5) & (!\sm|WideOr1~0_combout\ & \dp|xp\(4))) ) ) ) # ( !\dp|Add1~9_sumout\ & ( !\sm|initx~0_combout\ & ( (!\sm|WideOr1~0_combout\ & (((!\dp|xp\(5) & \dp|xp\(4))))) # (\sm|WideOr1~0_combout\ & 
-- (\dp|Add1~29_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111000101000000001100000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add1~29_sumout\,
	datab => \dp|ALT_INV_xp\(5),
	datac => \sm|ALT_INV_WideOr1~0_combout\,
	datad => \dp|ALT_INV_xp\(4),
	datae => \dp|ALT_INV_Add1~9_sumout\,
	dataf => \sm|ALT_INV_initx~0_combout\,
	combout => \dp|Equal1~1_combout\);

-- Location: LABCELL_X29_Y77_N33
\dp|xp~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~3_combout\ = (!\sm|WideOr1~0_combout\ & (((\dp|xp\(0))))) # (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & (\dp|Add1~13_sumout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010010101110000001001010111000000100101011100000010010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add1~13_sumout\,
	datad => \dp|ALT_INV_xp\(0),
	combout => \dp|xp~3_combout\);

-- Location: FF_X29_Y77_N22
\dp|xp[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~1_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(7));

-- Location: LABCELL_X29_Y77_N18
\dp|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~5_sumout\ = SUM(( \dp|xp\(6) ) + ( GND ) + ( \dp|Add1~10\ ))
-- \dp|Add1~6\ = CARRY(( \dp|xp\(6) ) + ( GND ) + ( \dp|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(6),
	cin => \dp|Add1~10\,
	sumout => \dp|Add1~5_sumout\,
	cout => \dp|Add1~6\);

-- Location: FF_X29_Y77_N19
\dp|xp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add1~5_sumout\,
	sclr => \dp|xp~8_combout\,
	ena => \sm|WideOr1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xp\(6));

-- Location: LABCELL_X29_Y77_N21
\dp|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add1~1_sumout\ = SUM(( \dp|xp\(7) ) + ( GND ) + ( \dp|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_xp\(7),
	cin => \dp|Add1~6\,
	sumout => \dp|Add1~1_sumout\);

-- Location: LABCELL_X29_Y77_N27
\dp|xp~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~0_combout\ = ( \dp|xp\(7) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initx~0_combout\ & \dp|Add1~1_sumout\)) ) ) # ( !\dp|xp\(7) & ( (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & \dp|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001000100000000000100010010101010111011101010101011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datad => \dp|ALT_INV_Add1~1_sumout\,
	dataf => \dp|ALT_INV_xp\(7),
	combout => \dp|xp~0_combout\);

-- Location: LABCELL_X29_Y77_N30
\dp|xp~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~1_combout\ = ( \dp|xp\(6) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initx~0_combout\ & \dp|Add1~5_sumout\)) ) ) # ( !\dp|xp\(6) & ( (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & \dp|Add1~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010101110101011101010111010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add1~5_sumout\,
	dataf => \dp|ALT_INV_xp\(6),
	combout => \dp|xp~1_combout\);

-- Location: LABCELL_X30_Y77_N12
\dp|Equal1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal1~2_combout\ = ( \dp|xp~0_combout\ & ( !\dp|xp~1_combout\ & ( (\dp|Equal1~0_combout\ & (\dp|xp~4_combout\ & (\dp|Equal1~1_combout\ & \dp|xp~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Equal1~0_combout\,
	datab => \dp|ALT_INV_xp~4_combout\,
	datac => \dp|ALT_INV_Equal1~1_combout\,
	datad => \dp|ALT_INV_xp~3_combout\,
	datae => \dp|ALT_INV_xp~0_combout\,
	dataf => \dp|ALT_INV_xp~1_combout\,
	combout => \dp|Equal1~2_combout\);

-- Location: FF_X30_Y77_N13
\dp|xdone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Equal1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|xdone~q\);

-- Location: LABCELL_X31_Y78_N30
\sm|Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux2~0_combout\ = ( !\sm|current_state[2]~DUPLICATE_q\ & ( ((!\sm|current_state[0]~DUPLICATE_q\ & (((\sm|current_state[1]~DUPLICATE_q\ & \dp|xdone~q\)) # (\sm|current_state\(3))))) ) ) # ( \sm|current_state[2]~DUPLICATE_q\ & ( 
-- (!\sm|current_state[1]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # ((!\sm|LessThan0~7_combout\ & (!\sm|LessThan0~0_combout\ & \sm|current_state\(3)))))) # (\sm|current_state[1]~DUPLICATE_q\ & (((!\sm|current_state\(3) $ 
-- (\sm|current_state[0]~DUPLICATE_q\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000001111111111111111111100110000000000000000000000000010110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_LessThan0~7_combout\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_LessThan0~0_combout\,
	datad => \sm|ALT_INV_current_state\(3),
	datae => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datag => \dp|ALT_INV_xdone~q\,
	combout => \sm|Mux2~0_combout\);

-- Location: FF_X31_Y78_N31
\sm|current_state[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux2~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[0]~DUPLICATE_q\);

-- Location: MLABCELL_X21_Y78_N57
\sm|Decoder0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Decoder0~0_combout\ = ( !\sm|current_state[0]~DUPLICATE_q\ & ( (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|Decoder0~0_combout\);

-- Location: LABCELL_X29_Y79_N30
\dp|Add4~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~21_sumout\ = SUM(( \dp|oy\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add4~22\ = CARRY(( \dp|oy\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(0),
	cin => GND,
	sumout => \dp|Add4~21_sumout\,
	cout => \dp|Add4~22\);

-- Location: LABCELL_X27_Y80_N33
\dp|oy~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~6_combout\ = ( \sm|Decoder0~0_combout\ & ( \sm|WideOr3~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	dataf => \sm|ALT_INV_Decoder0~0_combout\,
	combout => \dp|oy~6_combout\);

-- Location: FF_X29_Y79_N32
\dp|oy[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~21_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(0));

-- Location: LABCELL_X29_Y79_N33
\dp|Add4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~17_sumout\ = SUM(( \dp|oy\(1) ) + ( GND ) + ( \dp|Add4~22\ ))
-- \dp|Add4~18\ = CARRY(( \dp|oy\(1) ) + ( GND ) + ( \dp|Add4~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(1),
	cin => \dp|Add4~22\,
	sumout => \dp|Add4~17_sumout\,
	cout => \dp|Add4~18\);

-- Location: FF_X29_Y79_N35
\dp|oy[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~17_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(1));

-- Location: LABCELL_X29_Y79_N36
\dp|Add4~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~25_sumout\ = SUM(( \dp|oy\(2) ) + ( GND ) + ( \dp|Add4~18\ ))
-- \dp|Add4~26\ = CARRY(( \dp|oy\(2) ) + ( GND ) + ( \dp|Add4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(2),
	cin => \dp|Add4~18\,
	sumout => \dp|Add4~25_sumout\,
	cout => \dp|Add4~26\);

-- Location: FF_X29_Y79_N38
\dp|oy[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~25_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(2));

-- Location: LABCELL_X29_Y79_N39
\dp|Add4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~13_sumout\ = SUM(( \dp|oy\(3) ) + ( GND ) + ( \dp|Add4~26\ ))
-- \dp|Add4~14\ = CARRY(( \dp|oy\(3) ) + ( GND ) + ( \dp|Add4~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(3),
	cin => \dp|Add4~26\,
	sumout => \dp|Add4~13_sumout\,
	cout => \dp|Add4~14\);

-- Location: FF_X29_Y79_N41
\dp|oy[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~13_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(3));

-- Location: LABCELL_X29_Y79_N42
\dp|Add4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~9_sumout\ = SUM(( \dp|oy\(4) ) + ( GND ) + ( \dp|Add4~14\ ))
-- \dp|Add4~10\ = CARRY(( \dp|oy\(4) ) + ( GND ) + ( \dp|Add4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(4),
	cin => \dp|Add4~14\,
	sumout => \dp|Add4~9_sumout\,
	cout => \dp|Add4~10\);

-- Location: FF_X29_Y79_N44
\dp|oy[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~9_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(4));

-- Location: LABCELL_X29_Y79_N45
\dp|Add4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~5_sumout\ = SUM(( \dp|oy\(5) ) + ( GND ) + ( \dp|Add4~10\ ))
-- \dp|Add4~6\ = CARRY(( \dp|oy\(5) ) + ( GND ) + ( \dp|Add4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(5),
	cin => \dp|Add4~10\,
	sumout => \dp|Add4~5_sumout\,
	cout => \dp|Add4~6\);

-- Location: FF_X29_Y79_N47
\dp|oy[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~5_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(5));

-- Location: LABCELL_X29_Y79_N48
\dp|Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add4~1_sumout\ = SUM(( \dp|oy\(6) ) + ( GND ) + ( \dp|Add4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_oy\(6),
	cin => \dp|Add4~6\,
	sumout => \dp|Add4~1_sumout\);

-- Location: FF_X29_Y79_N50
\dp|oy[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add4~1_sumout\,
	sclr => \dp|oy~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|oy\(6));

-- Location: MLABCELL_X21_Y78_N30
\dp|Add7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~25_sumout\ = SUM(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(0)) ) + ( VCC ) + ( !VCC ))
-- \dp|Add7~26\ = CARRY(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(0)) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_ox\(0),
	cin => GND,
	sumout => \dp|Add7~25_sumout\,
	cout => \dp|Add7~26\);

-- Location: LABCELL_X29_Y79_N0
\dp|Add8~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~34_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \dp|Add8~34_cout\);

-- Location: LABCELL_X29_Y79_N3
\dp|Add8~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~5_sumout\ = SUM(( !\dp|Add7~25_sumout\ ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(0))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~21_sumout\)))) ) + ( \dp|Add8~34_cout\ ))
-- \dp|Add8~6\ = CARRY(( !\dp|Add7~25_sumout\ ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(0))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~21_sumout\)))) ) + ( \dp|Add8~34_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101011011000100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(0),
	datad => \dp|ALT_INV_Add7~25_sumout\,
	dataf => \dp|ALT_INV_Add4~21_sumout\,
	cin => \dp|Add8~34_cout\,
	sumout => \dp|Add8~5_sumout\,
	cout => \dp|Add8~6\);

-- Location: LABCELL_X29_Y79_N6
\dp|Add8~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~9_sumout\ = SUM(( !\dp|Add7~21_sumout\ ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(1))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~17_sumout\)))) ) + ( \dp|Add8~6\ ))
-- \dp|Add8~10\ = CARRY(( !\dp|Add7~21_sumout\ ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(1))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~17_sumout\)))) ) + ( \dp|Add8~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111101011011000100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(1),
	datad => \dp|ALT_INV_Add7~21_sumout\,
	dataf => \dp|ALT_INV_Add4~17_sumout\,
	cin => \dp|Add8~6\,
	sumout => \dp|Add8~9_sumout\,
	cout => \dp|Add8~10\);

-- Location: LABCELL_X29_Y79_N9
\dp|Add8~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~13_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(2))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~25_sumout\)))) ) + ( !\dp|Add7~29_sumout\ ) + ( \dp|Add8~10\ ))
-- \dp|Add8~14\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(2))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~25_sumout\)))) ) + ( !\dp|Add7~29_sumout\ ) + ( \dp|Add8~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(2),
	datad => \dp|ALT_INV_Add4~25_sumout\,
	dataf => \dp|ALT_INV_Add7~29_sumout\,
	cin => \dp|Add8~10\,
	sumout => \dp|Add8~13_sumout\,
	cout => \dp|Add8~14\);

-- Location: LABCELL_X29_Y79_N12
\dp|Add8~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~17_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(3))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~13_sumout\)))) ) + ( !\dp|Add7~17_sumout\ ) + ( \dp|Add8~14\ ))
-- \dp|Add8~18\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(3))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~13_sumout\)))) ) + ( !\dp|Add7~17_sumout\ ) + ( \dp|Add8~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(3),
	datad => \dp|ALT_INV_Add4~13_sumout\,
	dataf => \dp|ALT_INV_Add7~17_sumout\,
	cin => \dp|Add8~14\,
	sumout => \dp|Add8~17_sumout\,
	cout => \dp|Add8~18\);

-- Location: FF_X28_Y79_N11
\dp|crit[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~4_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit[4]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y79_N0
\dp|Add9~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~29_sumout\ = SUM(( \dp|crit\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add9~30\ = CARRY(( \dp|crit\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_crit\(0),
	cin => GND,
	sumout => \dp|Add9~29_sumout\,
	cout => \dp|Add9~30\);

-- Location: MLABCELL_X28_Y79_N30
\dp|Add5~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~29_sumout\ = SUM(( \dp|crit\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add5~30\ = CARRY(( \dp|crit\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_crit\(0),
	cin => GND,
	sumout => \dp|Add5~29_sumout\,
	cout => \dp|Add5~30\);

-- Location: LABCELL_X27_Y79_N36
\dp|crit~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~7_combout\ = ( \dp|LessThan0~1_combout\ & ( (!\sm|Decoder0~0_combout\ & \dp|Add9~29_sumout\) ) ) # ( !\dp|LessThan0~1_combout\ & ( (!\sm|Decoder0~0_combout\ & \dp|Add5~29_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add9~29_sumout\,
	datad => \dp|ALT_INV_Add5~29_sumout\,
	dataf => \dp|ALT_INV_LessThan0~1_combout\,
	combout => \dp|crit~7_combout\);

-- Location: FF_X27_Y79_N38
\dp|crit[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~7_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(0));

-- Location: LABCELL_X27_Y79_N3
\dp|Add9~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~5_sumout\ = SUM(( \dp|Add8~5_sumout\ ) + ( \dp|crit\(1) ) + ( \dp|Add9~30\ ))
-- \dp|Add9~6\ = CARRY(( \dp|Add8~5_sumout\ ) + ( \dp|crit\(1) ) + ( \dp|Add9~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(1),
	datac => \dp|ALT_INV_Add8~5_sumout\,
	cin => \dp|Add9~30\,
	sumout => \dp|Add9~5_sumout\,
	cout => \dp|Add9~6\);

-- Location: MLABCELL_X28_Y79_N33
\dp|Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~5_sumout\ = SUM(( \dp|crit\(1) ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(0))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~21_sumout\)))) ) + ( \dp|Add5~30\ ))
-- \dp|Add5~6\ = CARRY(( \dp|crit\(1) ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(0))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~21_sumout\)))) ) + ( \dp|Add5~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100111101000100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(0),
	datad => \dp|ALT_INV_crit\(1),
	dataf => \dp|ALT_INV_Add4~21_sumout\,
	cin => \dp|Add5~30\,
	sumout => \dp|Add5~5_sumout\,
	cout => \dp|Add5~6\);

-- Location: MLABCELL_X28_Y79_N3
\dp|crit~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~1_combout\ = ( \dp|Add5~5_sumout\ & ( (!\sm|Decoder0~0_combout\ & ((!\dp|LessThan0~1_combout\) # (\dp|Add9~5_sumout\))) ) ) # ( !\dp|Add5~5_sumout\ & ( (\dp|LessThan0~1_combout\ & (!\sm|Decoder0~0_combout\ & \dp|Add9~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010001100100011001000110010001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_LessThan0~1_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add9~5_sumout\,
	dataf => \dp|ALT_INV_Add5~5_sumout\,
	combout => \dp|crit~1_combout\);

-- Location: FF_X28_Y79_N5
\dp|crit[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~1_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(1));

-- Location: LABCELL_X27_Y79_N6
\dp|Add9~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~9_sumout\ = SUM(( \dp|Add8~9_sumout\ ) + ( \dp|crit\(2) ) + ( \dp|Add9~6\ ))
-- \dp|Add9~10\ = CARRY(( \dp|Add8~9_sumout\ ) + ( \dp|crit\(2) ) + ( \dp|Add9~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_crit\(2),
	datad => \dp|ALT_INV_Add8~9_sumout\,
	cin => \dp|Add9~6\,
	sumout => \dp|Add9~9_sumout\,
	cout => \dp|Add9~10\);

-- Location: MLABCELL_X28_Y79_N36
\dp|Add5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~9_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(1))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~17_sumout\)))) ) + ( \dp|crit\(2) ) + ( \dp|Add5~6\ ))
-- \dp|Add5~10\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(1))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~17_sumout\)))) ) + ( \dp|crit\(2) ) + ( \dp|Add5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(1),
	datad => \dp|ALT_INV_Add4~17_sumout\,
	dataf => \dp|ALT_INV_crit\(2),
	cin => \dp|Add5~6\,
	sumout => \dp|Add5~9_sumout\,
	cout => \dp|Add5~10\);

-- Location: MLABCELL_X28_Y79_N6
\dp|crit~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~2_combout\ = (!\sm|Decoder0~0_combout\ & ((!\dp|LessThan0~1_combout\ & ((\dp|Add5~9_sumout\))) # (\dp|LessThan0~1_combout\ & (\dp|Add9~9_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010010001100000001001000110000000100100011000000010010001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_LessThan0~1_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add9~9_sumout\,
	datad => \dp|ALT_INV_Add5~9_sumout\,
	combout => \dp|crit~2_combout\);

-- Location: FF_X28_Y79_N8
\dp|crit[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~2_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(2));

-- Location: LABCELL_X27_Y79_N9
\dp|Add9~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~13_sumout\ = SUM(( \dp|Add8~13_sumout\ ) + ( \dp|crit\(3) ) + ( \dp|Add9~10\ ))
-- \dp|Add9~14\ = CARRY(( \dp|Add8~13_sumout\ ) + ( \dp|crit\(3) ) + ( \dp|Add9~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(3),
	datac => \dp|ALT_INV_Add8~13_sumout\,
	cin => \dp|Add9~10\,
	sumout => \dp|Add9~13_sumout\,
	cout => \dp|Add9~14\);

-- Location: MLABCELL_X28_Y79_N39
\dp|Add5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~13_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(2))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~25_sumout\)))) ) + ( \dp|crit\(3) ) + ( \dp|Add5~10\ ))
-- \dp|Add5~14\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(2))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~25_sumout\)))) ) + ( \dp|crit\(3) ) + ( \dp|Add5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(2),
	datad => \dp|ALT_INV_Add4~25_sumout\,
	dataf => \dp|ALT_INV_crit\(3),
	cin => \dp|Add5~10\,
	sumout => \dp|Add5~13_sumout\,
	cout => \dp|Add5~14\);

-- Location: MLABCELL_X28_Y79_N0
\dp|crit~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~3_combout\ = ( \dp|Add5~13_sumout\ & ( (!\dp|LessThan0~1_combout\) # ((\dp|Add9~13_sumout\) # (\sm|Decoder0~0_combout\)) ) ) # ( !\dp|Add5~13_sumout\ & ( ((\dp|LessThan0~1_combout\ & \dp|Add9~13_sumout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001101110111001100110111011110111011111111111011101111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_LessThan0~1_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_Add9~13_sumout\,
	dataf => \dp|ALT_INV_Add5~13_sumout\,
	combout => \dp|crit~3_combout\);

-- Location: FF_X28_Y79_N2
\dp|crit[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~3_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(3));

-- Location: LABCELL_X27_Y79_N12
\dp|Add9~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~17_sumout\ = SUM(( \dp|Add8~17_sumout\ ) + ( \dp|crit[4]~DUPLICATE_q\ ) + ( \dp|Add9~14\ ))
-- \dp|Add9~18\ = CARRY(( \dp|Add8~17_sumout\ ) + ( \dp|crit[4]~DUPLICATE_q\ ) + ( \dp|Add9~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add8~17_sumout\,
	datac => \dp|ALT_INV_crit[4]~DUPLICATE_q\,
	cin => \dp|Add9~14\,
	sumout => \dp|Add9~17_sumout\,
	cout => \dp|Add9~18\);

-- Location: MLABCELL_X28_Y79_N42
\dp|Add5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~17_sumout\ = SUM(( \dp|crit\(4) ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(3))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~13_sumout\)))) ) + ( \dp|Add5~14\ ))
-- \dp|Add5~18\ = CARRY(( \dp|crit\(4) ) + ( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(3))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~13_sumout\)))) ) + ( \dp|Add5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100111101000100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(3),
	datad => \dp|ALT_INV_crit\(4),
	dataf => \dp|ALT_INV_Add4~13_sumout\,
	cin => \dp|Add5~14\,
	sumout => \dp|Add5~17_sumout\,
	cout => \dp|Add5~18\);

-- Location: MLABCELL_X28_Y79_N9
\dp|crit~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~4_combout\ = ( \dp|Add5~17_sumout\ & ( (!\sm|Decoder0~0_combout\ & ((!\dp|LessThan0~1_combout\) # (\dp|Add9~17_sumout\))) ) ) # ( !\dp|Add5~17_sumout\ & ( (\dp|LessThan0~1_combout\ & (!\sm|Decoder0~0_combout\ & \dp|Add9~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010001100100011001000110010001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_LessThan0~1_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add9~17_sumout\,
	dataf => \dp|ALT_INV_Add5~17_sumout\,
	combout => \dp|crit~4_combout\);

-- Location: FF_X28_Y79_N10
\dp|crit[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~4_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(4));

-- Location: MLABCELL_X28_Y79_N45
\dp|Add5~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~25_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(4))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~9_sumout\)))) ) + ( \dp|crit\(5) ) + ( \dp|Add5~18\ ))
-- \dp|Add5~26\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(4))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~9_sumout\)))) ) + ( \dp|crit\(5) ) + ( \dp|Add5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(4),
	datad => \dp|ALT_INV_Add4~9_sumout\,
	dataf => \dp|ALT_INV_crit\(5),
	cin => \dp|Add5~18\,
	sumout => \dp|Add5~25_sumout\,
	cout => \dp|Add5~26\);

-- Location: LABCELL_X29_Y79_N15
\dp|Add8~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~25_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(4))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~9_sumout\)))) ) + ( !\dp|Add7~13_sumout\ ) + ( \dp|Add8~18\ ))
-- \dp|Add8~26\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(4))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~9_sumout\)))) ) + ( !\dp|Add7~13_sumout\ ) + ( \dp|Add8~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(4),
	datad => \dp|ALT_INV_Add4~9_sumout\,
	dataf => \dp|ALT_INV_Add7~13_sumout\,
	cin => \dp|Add8~18\,
	sumout => \dp|Add8~25_sumout\,
	cout => \dp|Add8~26\);

-- Location: LABCELL_X27_Y79_N15
\dp|Add9~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~25_sumout\ = SUM(( \dp|Add8~25_sumout\ ) + ( \dp|crit\(5) ) + ( \dp|Add9~18\ ))
-- \dp|Add9~26\ = CARRY(( \dp|Add8~25_sumout\ ) + ( \dp|crit\(5) ) + ( \dp|Add9~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_crit\(5),
	datad => \dp|ALT_INV_Add8~25_sumout\,
	cin => \dp|Add9~18\,
	sumout => \dp|Add9~25_sumout\,
	cout => \dp|Add9~26\);

-- Location: LABCELL_X27_Y79_N45
\dp|crit~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~6_combout\ = ( \dp|Add9~25_sumout\ & ( ((\sm|Decoder0~0_combout\) # (\dp|LessThan0~1_combout\)) # (\dp|Add5~25_sumout\) ) ) # ( !\dp|Add9~25_sumout\ & ( ((\dp|Add5~25_sumout\ & !\dp|LessThan0~1_combout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011111111010100001111111101011111111111110101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add5~25_sumout\,
	datac => \dp|ALT_INV_LessThan0~1_combout\,
	datad => \sm|ALT_INV_Decoder0~0_combout\,
	dataf => \dp|ALT_INV_Add9~25_sumout\,
	combout => \dp|crit~6_combout\);

-- Location: FF_X27_Y79_N46
\dp|crit[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~6_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(5));

-- Location: MLABCELL_X28_Y79_N48
\dp|Add5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~21_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(5))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~5_sumout\)))) ) + ( \dp|crit\(6) ) + ( \dp|Add5~26\ ))
-- \dp|Add5~22\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(5))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~5_sumout\)))) ) + ( \dp|crit\(6) ) + ( \dp|Add5~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(5),
	datad => \dp|ALT_INV_Add4~5_sumout\,
	dataf => \dp|ALT_INV_crit\(6),
	cin => \dp|Add5~26\,
	sumout => \dp|Add5~21_sumout\,
	cout => \dp|Add5~22\);

-- Location: LABCELL_X29_Y79_N18
\dp|Add8~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~21_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(5))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~5_sumout\)))) ) + ( !\dp|Add7~9_sumout\ ) + ( \dp|Add8~26\ ))
-- \dp|Add8~22\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(5))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~5_sumout\)))) ) + ( !\dp|Add7~9_sumout\ ) + ( \dp|Add8~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(5),
	datad => \dp|ALT_INV_Add4~5_sumout\,
	dataf => \dp|ALT_INV_Add7~9_sumout\,
	cin => \dp|Add8~26\,
	sumout => \dp|Add8~21_sumout\,
	cout => \dp|Add8~22\);

-- Location: LABCELL_X27_Y79_N18
\dp|Add9~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~21_sumout\ = SUM(( \dp|Add8~21_sumout\ ) + ( \dp|crit\(6) ) + ( \dp|Add9~26\ ))
-- \dp|Add9~22\ = CARRY(( \dp|Add8~21_sumout\ ) + ( \dp|crit\(6) ) + ( \dp|Add9~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_crit\(6),
	datac => \dp|ALT_INV_Add8~21_sumout\,
	cin => \dp|Add9~26\,
	sumout => \dp|Add9~21_sumout\,
	cout => \dp|Add9~22\);

-- Location: LABCELL_X27_Y79_N39
\dp|crit~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~5_combout\ = ( \dp|Add9~21_sumout\ & ( ((\dp|Add5~21_sumout\) # (\dp|LessThan0~1_combout\)) # (\sm|Decoder0~0_combout\) ) ) # ( !\dp|Add9~21_sumout\ & ( ((!\dp|LessThan0~1_combout\ & \dp|Add5~21_sumout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111110011001100111111001100111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_LessThan0~1_combout\,
	datad => \dp|ALT_INV_Add5~21_sumout\,
	dataf => \dp|ALT_INV_Add9~21_sumout\,
	combout => \dp|crit~5_combout\);

-- Location: FF_X27_Y79_N40
\dp|crit[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~5_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(6));

-- Location: MLABCELL_X28_Y79_N12
\dp|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|LessThan0~0_combout\ = ( !\dp|crit\(1) & ( !\dp|crit\(5) & ( (!\dp|crit\(6) & (!\dp|crit\(2) & (!\dp|crit\(3) & !\dp|crit[4]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(6),
	datab => \dp|ALT_INV_crit\(2),
	datac => \dp|ALT_INV_crit\(3),
	datad => \dp|ALT_INV_crit[4]~DUPLICATE_q\,
	datae => \dp|ALT_INV_crit\(1),
	dataf => \dp|ALT_INV_crit\(5),
	combout => \dp|LessThan0~0_combout\);

-- Location: MLABCELL_X28_Y79_N51
\dp|Add5~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~33_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(6))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~1_sumout\)))) ) + ( \dp|crit\(7) ) + ( \dp|Add5~22\ ))
-- \dp|Add5~34\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(6))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~1_sumout\)))) ) + ( \dp|crit\(7) ) + ( \dp|Add5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(6),
	datad => \dp|ALT_INV_Add4~1_sumout\,
	dataf => \dp|ALT_INV_crit\(7),
	cin => \dp|Add5~22\,
	sumout => \dp|Add5~33_sumout\,
	cout => \dp|Add5~34\);

-- Location: MLABCELL_X28_Y79_N54
\dp|Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add5~1_sumout\ = SUM(( \dp|crit\(8) ) + ( GND ) + ( \dp|Add5~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_crit\(8),
	cin => \dp|Add5~34\,
	sumout => \dp|Add5~1_sumout\);

-- Location: LABCELL_X29_Y79_N21
\dp|Add8~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~29_sumout\ = SUM(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(6))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~1_sumout\)))) ) + ( !\dp|Add7~5_sumout\ ) + ( \dp|Add8~22\ ))
-- \dp|Add8~30\ = CARRY(( (!\sm|WideOr3~0_combout\ & (((\dp|oy\(6))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~1_sumout\)))) ) + ( !\dp|Add7~5_sumout\ ) + ( \dp|Add8~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000000000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_oy\(6),
	datad => \dp|ALT_INV_Add4~1_sumout\,
	dataf => \dp|ALT_INV_Add7~5_sumout\,
	cin => \dp|Add8~22\,
	sumout => \dp|Add8~29_sumout\,
	cout => \dp|Add8~30\);

-- Location: LABCELL_X29_Y79_N24
\dp|Add8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add8~1_sumout\ = SUM(( GND ) + ( !\dp|Add7~1_sumout\ ) + ( \dp|Add8~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add7~1_sumout\,
	cin => \dp|Add8~30\,
	sumout => \dp|Add8~1_sumout\);

-- Location: LABCELL_X27_Y79_N21
\dp|Add9~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~33_sumout\ = SUM(( \dp|Add8~29_sumout\ ) + ( \dp|crit\(7) ) + ( \dp|Add9~22\ ))
-- \dp|Add9~34\ = CARRY(( \dp|Add8~29_sumout\ ) + ( \dp|crit\(7) ) + ( \dp|Add9~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(7),
	datac => \dp|ALT_INV_Add8~29_sumout\,
	cin => \dp|Add9~22\,
	sumout => \dp|Add9~33_sumout\,
	cout => \dp|Add9~34\);

-- Location: LABCELL_X27_Y79_N24
\dp|Add9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add9~1_sumout\ = SUM(( \dp|Add8~1_sumout\ ) + ( \dp|crit\(8) ) + ( \dp|Add9~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_crit\(8),
	datad => \dp|ALT_INV_Add8~1_sumout\,
	cin => \dp|Add9~34\,
	sumout => \dp|Add9~1_sumout\);

-- Location: LABCELL_X27_Y79_N30
\dp|crit~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~0_combout\ = ( \dp|Add9~1_sumout\ & ( ((\dp|Add5~1_sumout\) # (\sm|Decoder0~0_combout\)) # (\dp|LessThan0~1_combout\) ) ) # ( !\dp|Add9~1_sumout\ & ( ((!\dp|LessThan0~1_combout\ & \dp|Add5~1_sumout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111001111000011111100111100111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_LessThan0~1_combout\,
	datac => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_Add5~1_sumout\,
	dataf => \dp|ALT_INV_Add9~1_sumout\,
	combout => \dp|crit~0_combout\);

-- Location: FF_X27_Y79_N31
\dp|crit[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~0_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(8));

-- Location: LABCELL_X27_Y79_N42
\dp|LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|LessThan0~1_combout\ = ( !\dp|crit\(8) & ( (!\dp|LessThan0~0_combout\) # ((\dp|crit\(0)) # (\dp|crit\(7))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111111111110011111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_LessThan0~0_combout\,
	datac => \dp|ALT_INV_crit\(7),
	datad => \dp|ALT_INV_crit\(0),
	dataf => \dp|ALT_INV_crit\(8),
	combout => \dp|LessThan0~1_combout\);

-- Location: MLABCELL_X21_Y78_N0
\dp|ox~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~8_combout\ = ( \dp|Add7~25_sumout\ & ( ((\sm|WideOr3~0_combout\ & ((\dp|LessThan0~1_combout\) # (\sm|Decoder0~0_combout\)))) # (\dp|ox\(0)) ) ) # ( !\dp|Add7~25_sumout\ & ( (!\sm|WideOr3~0_combout\ & (((\dp|ox\(0))))) # (\sm|WideOr3~0_combout\ & 
-- (((!\dp|LessThan0~1_combout\ & \dp|ox\(0))) # (\sm|Decoder0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000111111011000100011111101100010101111111110001010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_LessThan0~1_combout\,
	datad => \dp|ALT_INV_ox\(0),
	dataf => \dp|ALT_INV_Add7~25_sumout\,
	combout => \dp|ox~8_combout\);

-- Location: FF_X21_Y78_N2
\dp|ox[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(0));

-- Location: MLABCELL_X21_Y78_N33
\dp|Add7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~21_sumout\ = SUM(( (\dp|ox\(1) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~26\ ))
-- \dp|Add7~22\ = CARRY(( (\dp|ox\(1) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111000001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_ox\(1),
	cin => \dp|Add7~26\,
	sumout => \dp|Add7~21_sumout\,
	cout => \dp|Add7~22\);

-- Location: LABCELL_X27_Y79_N54
\dp|ox~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~1_combout\ = ( \sm|WideOr3~0_combout\ & ( \dp|crit\(8) & ( \sm|Decoder0~0_combout\ ) ) ) # ( \sm|WideOr3~0_combout\ & ( !\dp|crit\(8) & ( (((!\dp|LessThan0~0_combout\) # (\dp|crit\(0))) # (\sm|Decoder0~0_combout\)) # (\dp|crit\(7)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(7),
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_crit\(0),
	datad => \dp|ALT_INV_LessThan0~0_combout\,
	datae => \sm|ALT_INV_WideOr3~0_combout\,
	dataf => \dp|ALT_INV_crit\(8),
	combout => \dp|ox~1_combout\);

-- Location: LABCELL_X29_Y80_N21
\dp|ox~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~7_combout\ = ( \dp|ox~1_combout\ & ( (\dp|ox~0_combout\ & \dp|Add7~21_sumout\) ) ) # ( !\dp|ox~1_combout\ & ( ((\dp|ox~0_combout\ & \dp|Add7~21_sumout\)) # (\dp|ox\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111111111000000111111111100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_ox~0_combout\,
	datac => \dp|ALT_INV_Add7~21_sumout\,
	datad => \dp|ALT_INV_ox\(1),
	dataf => \dp|ALT_INV_ox~1_combout\,
	combout => \dp|ox~7_combout\);

-- Location: FF_X29_Y80_N23
\dp|ox[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(1));

-- Location: MLABCELL_X21_Y78_N36
\dp|Add7~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~29_sumout\ = SUM(( VCC ) + ( (\dp|ox\(2) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( \dp|Add7~22\ ))
-- \dp|Add7~30\ = CARRY(( VCC ) + ( (\dp|ox\(2) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( \dp|Add7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110001000100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	dataf => \dp|ALT_INV_ox\(2),
	cin => \dp|Add7~22\,
	sumout => \dp|Add7~29_sumout\,
	cout => \dp|Add7~30\);

-- Location: MLABCELL_X21_Y78_N39
\dp|Add7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~17_sumout\ = SUM(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(3)) ) + ( VCC ) + ( \dp|Add7~30\ ))
-- \dp|Add7~18\ = CARRY(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(3)) ) + ( VCC ) + ( \dp|Add7~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_ox\(3),
	cin => \dp|Add7~30\,
	sumout => \dp|Add7~17_sumout\,
	cout => \dp|Add7~18\);

-- Location: MLABCELL_X21_Y78_N3
\dp|ox~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~6_combout\ = ( \dp|LessThan0~1_combout\ & ( (!\sm|WideOr3~0_combout\ & (((\dp|ox\(3))))) # (\sm|WideOr3~0_combout\ & (((\dp|Add7~17_sumout\)) # (\sm|Decoder0~0_combout\))) ) ) # ( !\dp|LessThan0~1_combout\ & ( ((\sm|WideOr3~0_combout\ & 
-- \sm|Decoder0~0_combout\)) # (\dp|ox\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000111111111000100011111111100010101101111110001010110111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add7~17_sumout\,
	datad => \dp|ALT_INV_ox\(3),
	dataf => \dp|ALT_INV_LessThan0~1_combout\,
	combout => \dp|ox~6_combout\);

-- Location: FF_X21_Y78_N5
\dp|ox[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(3));

-- Location: MLABCELL_X21_Y78_N42
\dp|Add7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~13_sumout\ = SUM(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(4)) ) + ( VCC ) + ( \dp|Add7~18\ ))
-- \dp|Add7~14\ = CARRY(( ((\sm|WideOr3~0_combout\ & \sm|Decoder0~0_combout\)) # (\dp|ox\(4)) ) + ( VCC ) + ( \dp|Add7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000001111100011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_ox\(4),
	cin => \dp|Add7~18\,
	sumout => \dp|Add7~13_sumout\,
	cout => \dp|Add7~14\);

-- Location: MLABCELL_X21_Y78_N24
\dp|ox~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~5_combout\ = ( \dp|LessThan0~1_combout\ & ( (!\sm|WideOr3~0_combout\ & (\dp|ox\(4))) # (\sm|WideOr3~0_combout\ & (((\dp|Add7~13_sumout\) # (\sm|Decoder0~0_combout\)))) ) ) # ( !\dp|LessThan0~1_combout\ & ( ((\sm|Decoder0~0_combout\ & 
-- \sm|WideOr3~0_combout\)) # (\dp|ox\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011101010011010111110101001101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ox\(4),
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_Add7~13_sumout\,
	dataf => \dp|ALT_INV_LessThan0~1_combout\,
	combout => \dp|ox~5_combout\);

-- Location: FF_X21_Y78_N26
\dp|ox[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(4));

-- Location: MLABCELL_X21_Y78_N45
\dp|Add7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~9_sumout\ = SUM(( (\dp|ox\(5) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~14\ ))
-- \dp|Add7~10\ = CARRY(( (\dp|ox\(5) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111000001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_ox\(5),
	cin => \dp|Add7~14\,
	sumout => \dp|Add7~9_sumout\,
	cout => \dp|Add7~10\);

-- Location: LABCELL_X27_Y79_N33
\dp|crit~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|crit~8_combout\ = ( \dp|Add5~33_sumout\ & ( ((!\dp|LessThan0~1_combout\) # (\sm|Decoder0~0_combout\)) # (\dp|Add9~33_sumout\) ) ) # ( !\dp|Add5~33_sumout\ & ( ((\dp|Add9~33_sumout\ & \dp|LessThan0~1_combout\)) # (\sm|Decoder0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111111111000001011111111111110101111111111111010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add9~33_sumout\,
	datac => \dp|ALT_INV_LessThan0~1_combout\,
	datad => \sm|ALT_INV_Decoder0~0_combout\,
	dataf => \dp|ALT_INV_Add5~33_sumout\,
	combout => \dp|crit~8_combout\);

-- Location: FF_X27_Y79_N35
\dp|crit[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|crit~8_combout\,
	ena => \sm|WideOr3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|crit\(7));

-- Location: LABCELL_X27_Y79_N48
\dp|ox~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~0_combout\ = ( \sm|WideOr3~0_combout\ & ( !\dp|crit\(8) & ( (!\sm|Decoder0~0_combout\ & (((!\dp|LessThan0~0_combout\) # (\dp|crit\(0))) # (\dp|crit\(7)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011000100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_crit\(7),
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_crit\(0),
	datad => \dp|ALT_INV_LessThan0~0_combout\,
	datae => \sm|ALT_INV_WideOr3~0_combout\,
	dataf => \dp|ALT_INV_crit\(8),
	combout => \dp|ox~0_combout\);

-- Location: LABCELL_X29_Y80_N18
\dp|ox~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~4_combout\ = ( \dp|Add7~9_sumout\ & ( ((!\dp|ox~1_combout\ & \dp|ox\(5))) # (\dp|ox~0_combout\) ) ) # ( !\dp|Add7~9_sumout\ & ( (!\dp|ox~1_combout\ & \dp|ox\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000110011111100110011001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_ox~0_combout\,
	datac => \dp|ALT_INV_ox~1_combout\,
	datad => \dp|ALT_INV_ox\(5),
	dataf => \dp|ALT_INV_Add7~9_sumout\,
	combout => \dp|ox~4_combout\);

-- Location: FF_X29_Y80_N20
\dp|ox[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(5));

-- Location: MLABCELL_X21_Y78_N48
\dp|Add7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~5_sumout\ = SUM(( (\dp|ox\(6) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~10\ ))
-- \dp|Add7~6\ = CARRY(( (\dp|ox\(6) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( VCC ) + ( \dp|Add7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_ox\(6),
	cin => \dp|Add7~10\,
	sumout => \dp|Add7~5_sumout\,
	cout => \dp|Add7~6\);

-- Location: LABCELL_X29_Y80_N33
\dp|ox~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~3_combout\ = ( \dp|ox~1_combout\ & ( (\dp|Add7~5_sumout\ & \dp|ox~0_combout\) ) ) # ( !\dp|ox~1_combout\ & ( ((\dp|Add7~5_sumout\ & \dp|ox~0_combout\)) # (\dp|ox\(6)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111111111000000111111111100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add7~5_sumout\,
	datac => \dp|ALT_INV_ox~0_combout\,
	datad => \dp|ALT_INV_ox\(6),
	dataf => \dp|ALT_INV_ox~1_combout\,
	combout => \dp|ox~3_combout\);

-- Location: FF_X29_Y80_N35
\dp|ox[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(6));

-- Location: MLABCELL_X21_Y78_N51
\dp|Add7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add7~1_sumout\ = SUM(( VCC ) + ( (\dp|ox\(7) & ((!\sm|WideOr3~0_combout\) # (!\sm|Decoder0~0_combout\))) ) + ( \dp|Add7~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110001000100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	dataf => \dp|ALT_INV_ox\(7),
	cin => \dp|Add7~6\,
	sumout => \dp|Add7~1_sumout\);

-- Location: LABCELL_X29_Y80_N9
\dp|ox~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~2_combout\ = ( \dp|ox~1_combout\ & ( (\dp|Add7~1_sumout\ & \dp|ox~0_combout\) ) ) # ( !\dp|ox~1_combout\ & ( ((\dp|Add7~1_sumout\ & \dp|ox~0_combout\)) # (\dp|ox\(7)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111111111000000111111111100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add7~1_sumout\,
	datac => \dp|ALT_INV_ox~0_combout\,
	datad => \dp|ALT_INV_ox\(7),
	dataf => \dp|ALT_INV_ox~1_combout\,
	combout => \dp|ox~2_combout\);

-- Location: FF_X29_Y80_N11
\dp|ox[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(7));

-- Location: LABCELL_X29_Y80_N15
\sm|LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~0_combout\ = ( !\dp|ox\(6) & ( (!\dp|ox\(7) & \dp|oy\(6)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_ox\(7),
	datad => \dp|ALT_INV_oy\(6),
	dataf => \dp|ALT_INV_ox\(6),
	combout => \sm|LessThan0~0_combout\);

-- Location: LABCELL_X31_Y78_N6
\sm|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux0~0_combout\ = ( \sm|current_state\(3) & ( \sm|current_state[1]~DUPLICATE_q\ & ( (!\sm|current_state\(2)) # (\sm|current_state\(0)) ) ) ) # ( !\sm|current_state\(3) & ( \sm|current_state[1]~DUPLICATE_q\ & ( (\sm|current_state\(0) & 
-- \sm|current_state\(2)) ) ) ) # ( \sm|current_state\(3) & ( !\sm|current_state[1]~DUPLICATE_q\ & ( ((!\sm|current_state\(0)) # ((!\sm|current_state\(2)) # (\sm|LessThan0~7_combout\))) # (\sm|LessThan0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111101111100000000001100111111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_LessThan0~0_combout\,
	datab => \sm|ALT_INV_current_state\(0),
	datac => \sm|ALT_INV_LessThan0~7_combout\,
	datad => \sm|ALT_INV_current_state\(2),
	datae => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	combout => \sm|Mux0~0_combout\);

-- Location: FF_X31_Y78_N8
\sm|current_state[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux0~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state\(3));

-- Location: LABCELL_X31_Y78_N42
\sm|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr0~0_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state\(1) & ((\sm|current_state\(2)))) # (\sm|current_state\(1) & ((!\sm|current_state\(2)) # (\sm|current_state\(3)))) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( 
-- \sm|current_state\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100110000111111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state\(3),
	datac => \sm|ALT_INV_current_state\(1),
	datad => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|WideOr0~0_combout\);

-- Location: FF_X31_Y78_N43
\sm|current_state[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|WideOr0~0_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[2]~DUPLICATE_q\);

-- Location: MLABCELL_X21_Y78_N54
\sm|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr3~0_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( (\sm|current_state[2]~DUPLICATE_q\ & (\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\)) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( 
-- (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ $ (\sm|current_state[1]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000101000001010000010100000100010000000100000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|WideOr3~0_combout\);

-- Location: MLABCELL_X21_Y78_N15
\dp|ox~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|ox~9_combout\ = ( \dp|ox~0_combout\ & ( ((!\dp|ox~1_combout\ & \dp|ox\(2))) # (\dp|Add7~29_sumout\) ) ) # ( !\dp|ox~0_combout\ & ( (!\dp|ox~1_combout\ & \dp|ox\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000001010101111101010101010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add7~29_sumout\,
	datac => \dp|ALT_INV_ox~1_combout\,
	datad => \dp|ALT_INV_ox\(2),
	dataf => \dp|ALT_INV_ox~0_combout\,
	combout => \dp|ox~9_combout\);

-- Location: FF_X21_Y78_N17
\dp|ox[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|ox~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ox\(2));

-- Location: MLABCELL_X21_Y78_N18
\sm|LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~3_combout\ = ( \dp|oy\(3) & ( (!\dp|ox\(3)) # ((!\dp|ox\(2) & \dp|oy\(2))) ) ) # ( !\dp|oy\(3) & ( (!\dp|ox\(2) & (\dp|oy\(2) & !\dp|ox\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000011111111000011001111111100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_ox\(2),
	datac => \dp|ALT_INV_oy\(2),
	datad => \dp|ALT_INV_ox\(3),
	dataf => \dp|ALT_INV_oy\(3),
	combout => \sm|LessThan0~3_combout\);

-- Location: MLABCELL_X21_Y78_N12
\sm|LessThan0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~6_combout\ = ( !\dp|ox\(7) & ( !\dp|oy\(6) $ (\dp|ox\(6)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110000110011110011000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_oy\(6),
	datad => \dp|ALT_INV_ox\(6),
	dataf => \dp|ALT_INV_ox\(7),
	combout => \sm|LessThan0~6_combout\);

-- Location: MLABCELL_X21_Y78_N6
\sm|LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~1_combout\ = ( \dp|ox\(1) & ( (\dp|oy\(1) & (\dp|oy\(0) & !\dp|ox\(0))) ) ) # ( !\dp|ox\(1) & ( ((\dp|oy\(0) & !\dp|ox\(0))) # (\dp|oy\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100110011001111110011001100000011000000000000001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_oy\(1),
	datac => \dp|ALT_INV_oy\(0),
	datad => \dp|ALT_INV_ox\(0),
	dataf => \dp|ALT_INV_ox\(1),
	combout => \sm|LessThan0~1_combout\);

-- Location: MLABCELL_X21_Y78_N9
\sm|LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~2_combout\ = ( \dp|oy\(3) & ( (\dp|ox\(3) & (!\dp|oy\(2) $ (\dp|ox\(2)))) ) ) # ( !\dp|oy\(3) & ( (!\dp|ox\(3) & (!\dp|oy\(2) $ (\dp|ox\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010010100000000101001010000000000000000101001010000000010100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_oy\(2),
	datac => \dp|ALT_INV_ox\(2),
	datad => \dp|ALT_INV_ox\(3),
	dataf => \dp|ALT_INV_oy\(3),
	combout => \sm|LessThan0~2_combout\);

-- Location: MLABCELL_X21_Y78_N21
\sm|LessThan0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~5_combout\ = ( \dp|ox\(5) & ( (!\dp|ox\(4) & (\dp|oy\(5) & \dp|oy\(4))) ) ) # ( !\dp|ox\(5) & ( ((!\dp|ox\(4) & \dp|oy\(4))) # (\dp|oy\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111110101111000011111010111100000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ox\(4),
	datac => \dp|ALT_INV_oy\(5),
	datad => \dp|ALT_INV_oy\(4),
	dataf => \dp|ALT_INV_ox\(5),
	combout => \sm|LessThan0~5_combout\);

-- Location: MLABCELL_X21_Y78_N27
\sm|LessThan0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~4_combout\ = ( \dp|ox\(5) & ( (\dp|oy\(5) & (!\dp|ox\(4) $ (\dp|oy\(4)))) ) ) # ( !\dp|ox\(5) & ( (!\dp|oy\(5) & (!\dp|ox\(4) $ (\dp|oy\(4)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000001010000101000000101000000001010000001010000101000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ox\(4),
	datac => \dp|ALT_INV_oy\(5),
	datad => \dp|ALT_INV_oy\(4),
	dataf => \dp|ALT_INV_ox\(5),
	combout => \sm|LessThan0~4_combout\);

-- Location: LABCELL_X29_Y78_N30
\sm|LessThan0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|LessThan0~7_combout\ = ( \sm|LessThan0~5_combout\ & ( \sm|LessThan0~4_combout\ & ( \sm|LessThan0~6_combout\ ) ) ) # ( !\sm|LessThan0~5_combout\ & ( \sm|LessThan0~4_combout\ & ( (\sm|LessThan0~6_combout\ & (((\sm|LessThan0~1_combout\ & 
-- \sm|LessThan0~2_combout\)) # (\sm|LessThan0~3_combout\))) ) ) ) # ( \sm|LessThan0~5_combout\ & ( !\sm|LessThan0~4_combout\ & ( \sm|LessThan0~6_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100010001000100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_LessThan0~3_combout\,
	datab => \sm|ALT_INV_LessThan0~6_combout\,
	datac => \sm|ALT_INV_LessThan0~1_combout\,
	datad => \sm|ALT_INV_LessThan0~2_combout\,
	datae => \sm|ALT_INV_LessThan0~5_combout\,
	dataf => \sm|ALT_INV_LessThan0~4_combout\,
	combout => \sm|LessThan0~7_combout\);

-- Location: LABCELL_X31_Y79_N0
\dp|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~25_sumout\ = SUM(( \dp|yp\(0) ) + ( VCC ) + ( !VCC ))
-- \dp|Add0~26\ = CARRY(( \dp|yp\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(0),
	cin => GND,
	sumout => \dp|Add0~25_sumout\,
	cout => \dp|Add0~26\);

-- Location: LABCELL_X31_Y79_N45
\sm|Decoder0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Decoder0~2_combout\ = ( !\sm|current_state[1]~DUPLICATE_q\ & ( (!\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	combout => \sm|Decoder0~2_combout\);

-- Location: FF_X31_Y79_N2
\dp|yp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~25_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(0));

-- Location: LABCELL_X31_Y79_N3
\dp|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~21_sumout\ = SUM(( \dp|yp\(1) ) + ( GND ) + ( \dp|Add0~26\ ))
-- \dp|Add0~22\ = CARRY(( \dp|yp\(1) ) + ( GND ) + ( \dp|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(1),
	cin => \dp|Add0~26\,
	sumout => \dp|Add0~21_sumout\,
	cout => \dp|Add0~22\);

-- Location: FF_X31_Y79_N5
\dp|yp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~21_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(1));

-- Location: LABCELL_X31_Y79_N6
\dp|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~17_sumout\ = SUM(( \dp|yp\(2) ) + ( GND ) + ( \dp|Add0~22\ ))
-- \dp|Add0~18\ = CARRY(( \dp|yp\(2) ) + ( GND ) + ( \dp|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(2),
	cin => \dp|Add0~22\,
	sumout => \dp|Add0~17_sumout\,
	cout => \dp|Add0~18\);

-- Location: FF_X31_Y79_N8
\dp|yp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~17_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(2));

-- Location: LABCELL_X31_Y79_N9
\dp|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~13_sumout\ = SUM(( \dp|yp\(3) ) + ( GND ) + ( \dp|Add0~18\ ))
-- \dp|Add0~14\ = CARRY(( \dp|yp\(3) ) + ( GND ) + ( \dp|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(3),
	cin => \dp|Add0~18\,
	sumout => \dp|Add0~13_sumout\,
	cout => \dp|Add0~14\);

-- Location: FF_X31_Y79_N10
\dp|yp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~13_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(3));

-- Location: LABCELL_X31_Y79_N12
\dp|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~9_sumout\ = SUM(( \dp|yp\(4) ) + ( GND ) + ( \dp|Add0~14\ ))
-- \dp|Add0~10\ = CARRY(( \dp|yp\(4) ) + ( GND ) + ( \dp|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(4),
	cin => \dp|Add0~14\,
	sumout => \dp|Add0~9_sumout\,
	cout => \dp|Add0~10\);

-- Location: FF_X31_Y79_N13
\dp|yp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~9_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(4));

-- Location: LABCELL_X31_Y79_N15
\dp|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~5_sumout\ = SUM(( \dp|yp\(5) ) + ( GND ) + ( \dp|Add0~10\ ))
-- \dp|Add0~6\ = CARRY(( \dp|yp\(5) ) + ( GND ) + ( \dp|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(5),
	cin => \dp|Add0~10\,
	sumout => \dp|Add0~5_sumout\,
	cout => \dp|Add0~6\);

-- Location: FF_X31_Y79_N17
\dp|yp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~5_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(5));

-- Location: LABCELL_X31_Y79_N33
\dp|yp~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~1_combout\ = ( \dp|yp\(5) & ( \dp|Add0~5_sumout\ & ( (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(5) & ( 
-- \dp|Add0~5_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(5) & ( !\dp|Add0~5_sumout\ & ( 
-- ((\sm|current_state[3]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011111110111111100000000100000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(5),
	dataf => \dp|ALT_INV_Add0~5_sumout\,
	combout => \dp|yp~1_combout\);

-- Location: LABCELL_X31_Y79_N42
\dp|yp~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~4_combout\ = ( \dp|yp\(2) & ( (!\sm|initx~0_combout\) # ((\sm|current_state[0]~DUPLICATE_q\ & \dp|Add0~17_sumout\)) ) ) # ( !\dp|yp\(2) & ( (\sm|current_state[0]~DUPLICATE_q\ & (\dp|Add0~17_sumout\ & \sm|initx~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001000000010000000111110001111100011111000111110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \dp|ALT_INV_Add0~17_sumout\,
	datac => \sm|ALT_INV_initx~0_combout\,
	dataf => \dp|ALT_INV_yp\(2),
	combout => \dp|yp~4_combout\);

-- Location: LABCELL_X31_Y79_N54
\dp|yp~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~2_combout\ = ( \dp|yp\(4) & ( \dp|Add0~9_sumout\ & ( (((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(4) & ( 
-- \dp|Add0~9_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(4) & ( !\dp|Add0~9_sumout\ & ( 
-- ((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011101111111111100001000000000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(4),
	dataf => \dp|ALT_INV_Add0~9_sumout\,
	combout => \dp|yp~2_combout\);

-- Location: LABCELL_X31_Y79_N30
\dp|yp~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~3_combout\ = ( \dp|yp\(3) & ( \dp|Add0~13_sumout\ & ( (((\sm|current_state[3]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(3) & ( 
-- \dp|Add0~13_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & (\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[3]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(3) & ( !\dp|Add0~13_sumout\ & ( 
-- ((\sm|current_state[3]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011101111111111100001000000000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(3),
	dataf => \dp|ALT_INV_Add0~13_sumout\,
	combout => \dp|yp~3_combout\);

-- Location: LABCELL_X31_Y79_N18
\dp|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add0~1_sumout\ = SUM(( \dp|yp\(6) ) + ( GND ) + ( \dp|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_yp\(6),
	cin => \dp|Add0~6\,
	sumout => \dp|Add0~1_sumout\);

-- Location: FF_X31_Y79_N20
\dp|yp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Add0~1_sumout\,
	sclr => \sm|Decoder0~2_combout\,
	ena => \sm|initx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|yp\(6));

-- Location: LABCELL_X31_Y79_N57
\dp|yp~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~0_combout\ = ( \dp|yp\(6) & ( \dp|Add0~1_sumout\ & ( (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(6) & ( 
-- \dp|Add0~1_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(6) & ( !\dp|Add0~1_sumout\ & ( 
-- ((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011111110111111100000000100000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(6),
	dataf => \dp|ALT_INV_Add0~1_sumout\,
	combout => \dp|yp~0_combout\);

-- Location: LABCELL_X31_Y79_N48
\dp|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal0~0_combout\ = ( \dp|yp\(1) & ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|initx~0_combout\ & (\dp|yp\(0))) # (\sm|initx~0_combout\ & (((\dp|Add0~21_sumout\ & \dp|Add0~25_sumout\)))) ) ) ) # ( !\dp|yp\(1) & ( \sm|current_state[0]~DUPLICATE_q\ & 
-- ( (\dp|Add0~21_sumout\ & (\sm|initx~0_combout\ & \dp|Add0~25_sumout\)) ) ) ) # ( \dp|yp\(1) & ( !\sm|current_state[0]~DUPLICATE_q\ & ( (\dp|yp\(0) & !\sm|initx~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010100000101000000000000000000110101000001010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yp\(0),
	datab => \dp|ALT_INV_Add0~21_sumout\,
	datac => \sm|ALT_INV_initx~0_combout\,
	datad => \dp|ALT_INV_Add0~25_sumout\,
	datae => \dp|ALT_INV_yp\(1),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \dp|Equal0~0_combout\);

-- Location: LABCELL_X31_Y79_N36
\dp|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Equal0~1_combout\ = ( \dp|yp~0_combout\ & ( \dp|Equal0~0_combout\ & ( (\dp|yp~1_combout\ & (\dp|yp~4_combout\ & (\dp|yp~2_combout\ & !\dp|yp~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_yp~1_combout\,
	datab => \dp|ALT_INV_yp~4_combout\,
	datac => \dp|ALT_INV_yp~2_combout\,
	datad => \dp|ALT_INV_yp~3_combout\,
	datae => \dp|ALT_INV_yp~0_combout\,
	dataf => \dp|ALT_INV_Equal0~0_combout\,
	combout => \dp|Equal0~1_combout\);

-- Location: FF_X31_Y79_N37
\dp|ydone\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|ydone~q\);

-- Location: LABCELL_X31_Y78_N12
\sm|Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux1~1_combout\ = ( \dp|xdone~q\ & ( \dp|ydone~q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\sm|current_state\(3)) # (\sm|current_state[1]~DUPLICATE_q\))) # (\sm|current_state[0]~DUPLICATE_q\ & 
-- (!\sm|current_state[1]~DUPLICATE_q\)))) # (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state\(3) & (!\sm|current_state[0]~DUPLICATE_q\ $ (!\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( !\dp|xdone~q\ & ( \dp|ydone~q\ & ( 
-- (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\sm|current_state\(3)) # (\sm|current_state[1]~DUPLICATE_q\))) # (\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\)))) # 
-- (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state\(3) & (!\sm|current_state[0]~DUPLICATE_q\ $ (!\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( \dp|xdone~q\ & ( !\dp|ydone~q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & 
-- (!\sm|current_state[1]~DUPLICATE_q\ $ (((!\sm|current_state[0]~DUPLICATE_q\ & \sm|current_state\(3)))))) # (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state\(3) & (!\sm|current_state[0]~DUPLICATE_q\ $ (!\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # 
-- ( !\dp|xdone~q\ & ( !\dp|ydone~q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\sm|current_state\(3)) # (\sm|current_state[1]~DUPLICATE_q\))) # (\sm|current_state[0]~DUPLICATE_q\ & 
-- (!\sm|current_state[1]~DUPLICATE_q\)))) # (\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state\(3) & (!\sm|current_state[0]~DUPLICATE_q\ $ (!\sm|current_state[1]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110011001100000110001100110000011100110011000001110011001100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state\(3),
	datae => \dp|ALT_INV_xdone~q\,
	dataf => \dp|ALT_INV_ydone~q\,
	combout => \sm|Mux1~1_combout\);

-- Location: LABCELL_X31_Y78_N36
\sm|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux1~0_combout\ = ( \sm|current_state\(3) & ( \sm|current_state\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state\(3),
	combout => \sm|Mux1~0_combout\);

-- Location: LABCELL_X31_Y78_N18
\sm|Mux1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Mux1~2_combout\ = ( \sm|current_state\(1) & ( \sm|current_state[0]~DUPLICATE_q\ & ( (\sm|Mux1~0_combout\) # (\sm|Mux1~1_combout\) ) ) ) # ( !\sm|current_state\(1) & ( \sm|current_state[0]~DUPLICATE_q\ & ( ((\sm|Mux1~0_combout\ & 
-- ((\sm|LessThan0~0_combout\) # (\sm|LessThan0~7_combout\)))) # (\sm|Mux1~1_combout\) ) ) ) # ( \sm|current_state\(1) & ( !\sm|current_state[0]~DUPLICATE_q\ & ( \sm|Mux1~1_combout\ ) ) ) # ( !\sm|current_state\(1) & ( !\sm|current_state[0]~DUPLICATE_q\ & ( 
-- \sm|Mux1~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110111001111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_LessThan0~7_combout\,
	datab => \sm|ALT_INV_Mux1~1_combout\,
	datac => \sm|ALT_INV_Mux1~0_combout\,
	datad => \sm|ALT_INV_LessThan0~0_combout\,
	datae => \sm|ALT_INV_current_state\(1),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|Mux1~2_combout\);

-- Location: FF_X31_Y78_N19
\sm|current_state[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \sm|Mux1~2_combout\,
	clrn => \KEY[3]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sm|current_state[1]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y79_N21
\vga_u0|writeEn~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~1_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & ((!\sm|current_state[2]~DUPLICATE_q\))) # (\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[1]~DUPLICATE_q\ & 
-- \sm|current_state[2]~DUPLICATE_q\)) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[1]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000011110000000000111111000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \vga_u0|writeEn~1_combout\);

-- Location: LABCELL_X31_Y78_N51
\sm|Decoder0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|Decoder0~1_combout\ = ( \sm|current_state\(0) & ( (\sm|current_state\(1) & (!\sm|current_state\(2) & !\sm|current_state[3]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(2),
	datad => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \sm|Decoder0~1_combout\);

-- Location: MLABCELL_X28_Y78_N57
\sm|loadc~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|loadc~0_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( (\sm|current_state[1]~DUPLICATE_q\ & (!\sm|current_state[2]~DUPLICATE_q\ & !\sm|current_state\(3))) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( (\sm|current_state[1]~DUPLICATE_q\ & 
-- (\sm|current_state[2]~DUPLICATE_q\ & \sm|current_state\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010101010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state\(3),
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \sm|loadc~0_combout\);

-- Location: FF_X30_Y78_N29
\dp|cx[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx[5]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y78_N30
\dp|Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~13_sumout\ = SUM(( \dp|cx\(1) ) + ( VCC ) + ( !VCC ))
-- \dp|Add3~14\ = CARRY(( \dp|cx\(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_cx\(1),
	cin => GND,
	sumout => \dp|Add3~13_sumout\,
	cout => \dp|Add3~14\);

-- Location: LABCELL_X29_Y78_N12
\dp|cx~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~2_combout\ = (!\sm|loadc~0_combout\ & (((\dp|cx\(1))))) # (\sm|loadc~0_combout\ & (!\sm|Decoder0~1_combout\ & (\dp|Add3~13_sumout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001011001110000000101100111000000010110011100000001011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_Add3~13_sumout\,
	datad => \dp|ALT_INV_cx\(1),
	combout => \dp|cx~2_combout\);

-- Location: FF_X29_Y78_N13
\dp|cx[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(1));

-- Location: LABCELL_X30_Y78_N33
\dp|Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~17_sumout\ = SUM(( \dp|cx\(2) ) + ( GND ) + ( \dp|Add3~14\ ))
-- \dp|Add3~18\ = CARRY(( \dp|cx\(2) ) + ( GND ) + ( \dp|Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_cx\(2),
	cin => \dp|Add3~14\,
	sumout => \dp|Add3~17_sumout\,
	cout => \dp|Add3~18\);

-- Location: LABCELL_X30_Y78_N54
\dp|cx~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~5_combout\ = ( \sm|Decoder0~1_combout\ & ( (\dp|cx\(2)) # (\sm|loadc~0_combout\) ) ) # ( !\sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & ((\dp|cx\(2)))) # (\sm|loadc~0_combout\ & (\dp|Add3~17_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_Add3~17_sumout\,
	datad => \dp|ALT_INV_cx\(2),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cx~5_combout\);

-- Location: FF_X30_Y78_N56
\dp|cx[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(2));

-- Location: LABCELL_X30_Y78_N36
\dp|Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~21_sumout\ = SUM(( \dp|cx\(3) ) + ( VCC ) + ( \dp|Add3~18\ ))
-- \dp|Add3~22\ = CARRY(( \dp|cx\(3) ) + ( VCC ) + ( \dp|Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_cx\(3),
	cin => \dp|Add3~18\,
	sumout => \dp|Add3~21_sumout\,
	cout => \dp|Add3~22\);

-- Location: LABCELL_X30_Y78_N57
\dp|cx~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~6_combout\ = ( \sm|Decoder0~1_combout\ & ( (\dp|cx\(3)) # (\sm|loadc~0_combout\) ) ) # ( !\sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & ((\dp|cx\(3)))) # (\sm|loadc~0_combout\ & (\dp|Add3~21_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000111011101000100011101110100110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add3~21_sumout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datad => \dp|ALT_INV_cx\(3),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cx~6_combout\);

-- Location: FF_X30_Y78_N59
\dp|cx[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(3));

-- Location: LABCELL_X30_Y78_N39
\dp|Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~25_sumout\ = SUM(( \dp|cx\(4) ) + ( VCC ) + ( \dp|Add3~22\ ))
-- \dp|Add3~26\ = CARRY(( \dp|cx\(4) ) + ( VCC ) + ( \dp|Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_cx\(4),
	cin => \dp|Add3~22\,
	sumout => \dp|Add3~25_sumout\,
	cout => \dp|Add3~26\);

-- Location: LABCELL_X30_Y78_N24
\dp|cx~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~7_combout\ = ( \sm|Decoder0~1_combout\ & ( (\dp|cx\(4)) # (\sm|loadc~0_combout\) ) ) # ( !\sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & ((\dp|cx\(4)))) # (\sm|loadc~0_combout\ & (\dp|Add3~25_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_Add3~25_sumout\,
	datad => \dp|ALT_INV_cx\(4),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cx~7_combout\);

-- Location: FF_X30_Y78_N26
\dp|cx[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(4));

-- Location: LABCELL_X30_Y78_N42
\dp|Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~9_sumout\ = SUM(( \dp|cx[5]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~26\ ))
-- \dp|Add3~10\ = CARRY(( \dp|cx[5]~DUPLICATE_q\ ) + ( GND ) + ( \dp|Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_cx[5]~DUPLICATE_q\,
	cin => \dp|Add3~26\,
	sumout => \dp|Add3~9_sumout\,
	cout => \dp|Add3~10\);

-- Location: LABCELL_X30_Y78_N27
\dp|cx~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~4_combout\ = ( \sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & \dp|cx\(5)) ) ) # ( !\sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & ((\dp|cx\(5)))) # (\sm|loadc~0_combout\ & (\dp|Add3~9_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111000000111100111100000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_Add3~9_sumout\,
	datad => \dp|ALT_INV_cx\(5),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cx~4_combout\);

-- Location: FF_X30_Y78_N28
\dp|cx[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(5));

-- Location: MLABCELL_X28_Y79_N24
\dp|oy~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~1_combout\ = ( \dp|Add4~5_sumout\ & ( (!\sm|WideOr3~0_combout\ & ((\dp|oy\(5)))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\)) ) ) # ( !\dp|Add4~5_sumout\ & ( (!\sm|WideOr3~0_combout\ & \dp|oy\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000101110001011100010111000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(5),
	dataf => \dp|ALT_INV_Add4~5_sumout\,
	combout => \dp|oy~1_combout\);

-- Location: LABCELL_X29_Y80_N0
\dp|Add11~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~33_combout\ = ( \dp|oy~1_combout\ & ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ $ (((!\dp|ox~4_combout\))))) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- ((!\sm|current_state[1]~DUPLICATE_q\ $ (\dp|ox~4_combout\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( !\dp|oy~1_combout\ & ( \sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ $ 
-- (((!\dp|ox~4_combout\))))) # (\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ $ (\dp|ox~4_combout\)))) ) ) ) # ( \dp|oy~1_combout\ & ( !\sm|current_state[0]~DUPLICATE_q\ & ( 
-- (!\sm|current_state[3]~DUPLICATE_q\) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|oy~1_combout\ & ( !\sm|current_state[0]~DUPLICATE_q\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & \sm|current_state[3]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100111111110011001101011010100001000101101010110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \dp|ALT_INV_ox~4_combout\,
	datad => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datae => \dp|ALT_INV_oy~1_combout\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \dp|Add11~33_combout\);

-- Location: LABCELL_X29_Y79_N57
\dp|oy~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~4_combout\ = ( \dp|oy\(4) & ( (!\sm|WideOr3~0_combout\) # ((!\sm|Decoder0~0_combout\ & \dp|Add4~9_sumout\)) ) ) # ( !\dp|oy\(4) & ( (!\sm|Decoder0~0_combout\ & (\dp|Add4~9_sumout\ & \sm|WideOr3~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110011111111000011001111111100001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \dp|ALT_INV_Add4~9_sumout\,
	datad => \sm|ALT_INV_WideOr3~0_combout\,
	dataf => \dp|ALT_INV_oy\(4),
	combout => \dp|oy~4_combout\);

-- Location: MLABCELL_X28_Y78_N51
\dp|Add11~40\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~40_combout\ = ( \dp|ox~5_combout\ & ( \dp|oy~4_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[1]~DUPLICATE_q\) # (!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\ & 
-- \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~5_combout\ & ( \dp|oy~4_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)))) # 
-- (\sm|current_state\(3) & (((!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( \dp|ox~5_combout\ & ( !\dp|oy~4_combout\ & ( (!\sm|current_state\(3) & 
-- (((!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~5_combout\ & ( 
-- !\dp|oy~4_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (!\sm|current_state[0]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001001010010001001010010010111011010110111011101110110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~5_combout\,
	dataf => \dp|ALT_INV_oy~4_combout\,
	combout => \dp|Add11~40_combout\);

-- Location: LABCELL_X29_Y79_N54
\dp|oy~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~5_combout\ = ( \dp|oy\(3) & ( (!\sm|WideOr3~0_combout\) # ((!\sm|Decoder0~0_combout\ & \dp|Add4~13_sumout\)) ) ) # ( !\dp|oy\(3) & ( (!\sm|Decoder0~0_combout\ & (\sm|WideOr3~0_combout\ & \dp|Add4~13_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110011110000111111001111000011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_Decoder0~0_combout\,
	datac => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_Add4~13_sumout\,
	dataf => \dp|ALT_INV_oy\(3),
	combout => \dp|oy~5_combout\);

-- Location: LABCELL_X29_Y80_N27
\dp|Add11~39\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~39_combout\ = ( \dp|ox~6_combout\ & ( \dp|oy~5_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # ((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- (((\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~6_combout\ & ( \dp|oy~5_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (((!\sm|current_state[0]~DUPLICATE_q\)) # 
-- (\sm|current_state[1]~DUPLICATE_q\))) # (\sm|current_state[3]~DUPLICATE_q\ & (((!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( \dp|ox~6_combout\ & ( !\dp|oy~5_combout\ & ( 
-- (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & ((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # 
-- (\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~6_combout\ & ( !\dp|oy~5_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[1]~DUPLICATE_q\ & ((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # (!\sm|current_state[0]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111001000000010101100010011110101001110111111101000110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~6_combout\,
	dataf => \dp|ALT_INV_oy~5_combout\,
	combout => \dp|Add11~39_combout\);

-- Location: MLABCELL_X28_Y79_N27
\dp|oy~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~11_combout\ = (!\sm|WideOr3~0_combout\ & (((\dp|oy\(2))))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\ & ((\dp|Add4~25_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000101110000011000010111000001100001011100000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_oy\(2),
	datad => \dp|ALT_INV_Add4~25_sumout\,
	combout => \dp|oy~11_combout\);

-- Location: MLABCELL_X28_Y78_N48
\dp|Add11~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~38_combout\ = ( \dp|ox~9_combout\ & ( \dp|oy~11_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\) # (!\sm|current_state[1]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\ & 
-- \sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~9_combout\ & ( \dp|oy~11_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)))) # 
-- (\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( \dp|ox~9_combout\ & ( !\dp|oy~11_combout\ & ( (!\sm|current_state\(3) & 
-- (((\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~9_combout\ & ( 
-- !\dp|oy~11_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\ & \sm|current_state[1]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # 
-- (!\sm|current_state[1]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001001010010010100100010010110101101110111011101110110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~9_combout\,
	dataf => \dp|ALT_INV_oy~11_combout\,
	combout => \dp|Add11~38_combout\);

-- Location: LABCELL_X31_Y78_N54
\dp|Add11~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~34_combout\ = ( \sm|current_state\(3) & ( \sm|current_state\(2) ) ) # ( !\sm|current_state\(3) & ( !\sm|current_state[0]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state\(3),
	combout => \dp|Add11~34_combout\);

-- Location: LABCELL_X27_Y80_N30
\dp|oy~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~9_combout\ = ( \dp|Add4~17_sumout\ & ( (!\sm|WideOr3~0_combout\ & ((\dp|oy\(1)))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\)) ) ) # ( !\dp|Add4~17_sumout\ & ( (!\sm|WideOr3~0_combout\ & \dp|oy\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101001010000111110100101000011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \sm|ALT_INV_Decoder0~0_combout\,
	datad => \dp|ALT_INV_oy\(1),
	dataf => \dp|ALT_INV_Add4~17_sumout\,
	combout => \dp|oy~9_combout\);

-- Location: MLABCELL_X28_Y78_N42
\dp|Add11~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~37_combout\ = ( \dp|ox~7_combout\ & ( (!\sm|current_state\(3) & (!\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state\(3) & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\dp|oy~9_combout\))) # (\sm|current_state[0]~DUPLICATE_q\ & 
-- (\sm|current_state[1]~DUPLICATE_q\)))) ) ) # ( !\dp|ox~7_combout\ & ( (!\sm|current_state\(3) & (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state\(3) & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\dp|oy~9_combout\))) # 
-- (\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111001001100110011100100110011011011000100110011101100010011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \dp|ALT_INV_oy~9_combout\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_ox~7_combout\,
	combout => \dp|Add11~37_combout\);

-- Location: LABCELL_X27_Y80_N39
\dp|oy~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~10_combout\ = ( \dp|Add4~17_sumout\ & ( (\dp|oy\(1)) # (\sm|WideOr3~0_combout\) ) ) # ( !\dp|Add4~17_sumout\ & ( (!\sm|WideOr3~0_combout\ & \dp|oy\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101001010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_oy\(1),
	dataf => \dp|ALT_INV_Add4~17_sumout\,
	combout => \dp|oy~10_combout\);

-- Location: MLABCELL_X28_Y79_N18
\dp|oy~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~7_combout\ = ( \dp|Add4~21_sumout\ & ( (!\sm|WideOr3~0_combout\ & ((\dp|oy\(0)))) # (\sm|WideOr3~0_combout\ & (!\sm|Decoder0~0_combout\)) ) ) # ( !\dp|Add4~21_sumout\ & ( (!\sm|WideOr3~0_combout\ & \dp|oy\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000100010111011100010001011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_oy\(0),
	dataf => \dp|ALT_INV_Add4~21_sumout\,
	combout => \dp|oy~7_combout\);

-- Location: MLABCELL_X28_Y78_N45
\dp|Add11~35\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~35_combout\ = ( \dp|ox~8_combout\ & ( (!\sm|current_state\(3) & (!\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state\(3) & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\dp|oy~7_combout\))) # (\sm|current_state[0]~DUPLICATE_q\ & 
-- (\sm|current_state[1]~DUPLICATE_q\)))) ) ) # ( !\dp|ox~8_combout\ & ( (!\sm|current_state\(3) & (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state\(3) & ((!\sm|current_state[0]~DUPLICATE_q\ & ((!\dp|oy~7_combout\))) # 
-- (\sm|current_state[0]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111001001100110011100100110011011011000100110011101100010011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datac => \dp|ALT_INV_oy~7_combout\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_ox~8_combout\,
	combout => \dp|Add11~35_combout\);

-- Location: LABCELL_X31_Y78_N57
\dp|Add11~36\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~36_combout\ = !\sm|current_state\(2) $ (((!\sm|current_state[0]~DUPLICATE_q\) # (!\sm|current_state[1]~DUPLICATE_q\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011000110110001101100011011000110110001101100011011000110110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state\(2),
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	combout => \dp|Add11~36_combout\);

-- Location: LABCELL_X27_Y80_N36
\dp|oy~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~8_combout\ = ( \dp|Add4~21_sumout\ & ( (\dp|oy\(0)) # (\sm|WideOr3~0_combout\) ) ) # ( !\dp|Add4~21_sumout\ & ( (!\sm|WideOr3~0_combout\ & \dp|oy\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101001010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_oy\(0),
	dataf => \dp|ALT_INV_Add4~21_sumout\,
	combout => \dp|oy~8_combout\);

-- Location: LABCELL_X30_Y78_N0
\dp|Add11~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~13_sumout\ = SUM(( !\dp|Add11~36_combout\ ) + ( (!\dp|Add11~34_combout\ & (((\dp|Add11~35_combout\)))) # (\dp|Add11~34_combout\ & (!\dp|oy~6_combout\ & ((\dp|oy~8_combout\)))) ) + ( !VCC ))
-- \dp|Add11~14\ = CARRY(( !\dp|Add11~36_combout\ ) + ( (!\dp|Add11~34_combout\ & (((\dp|Add11~35_combout\)))) # (\dp|Add11~34_combout\ & (!\dp|oy~6_combout\ & ((\dp|oy~8_combout\)))) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100111101000100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_oy~6_combout\,
	datab => \dp|ALT_INV_Add11~34_combout\,
	datac => \dp|ALT_INV_Add11~35_combout\,
	datad => \dp|ALT_INV_Add11~36_combout\,
	dataf => \dp|ALT_INV_oy~8_combout\,
	cin => GND,
	sumout => \dp|Add11~13_sumout\,
	cout => \dp|Add11~14\);

-- Location: LABCELL_X30_Y78_N3
\dp|Add11~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~17_sumout\ = SUM(( (!\dp|Add11~34_combout\ & (((\dp|Add11~37_combout\)))) # (\dp|Add11~34_combout\ & (!\dp|oy~6_combout\ & ((\dp|oy~10_combout\)))) ) + ( \dp|cx~2_combout\ ) + ( \dp|Add11~14\ ))
-- \dp|Add11~18\ = CARRY(( (!\dp|Add11~34_combout\ & (((\dp|Add11~37_combout\)))) # (\dp|Add11~34_combout\ & (!\dp|oy~6_combout\ & ((\dp|oy~10_combout\)))) ) + ( \dp|cx~2_combout\ ) + ( \dp|Add11~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000110000101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_oy~6_combout\,
	datab => \dp|ALT_INV_Add11~34_combout\,
	datac => \dp|ALT_INV_Add11~37_combout\,
	datad => \dp|ALT_INV_oy~10_combout\,
	dataf => \dp|ALT_INV_cx~2_combout\,
	cin => \dp|Add11~14\,
	sumout => \dp|Add11~17_sumout\,
	cout => \dp|Add11~18\);

-- Location: LABCELL_X30_Y78_N6
\dp|Add11~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~21_sumout\ = SUM(( \dp|Add11~38_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(2))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~17_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~18\ ))
-- \dp|Add11~22\ = CARRY(( \dp|Add11~38_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(2))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~17_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111000101100000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_cx\(2),
	datad => \dp|ALT_INV_Add11~38_combout\,
	dataf => \dp|ALT_INV_Add3~17_sumout\,
	cin => \dp|Add11~18\,
	sumout => \dp|Add11~21_sumout\,
	cout => \dp|Add11~22\);

-- Location: LABCELL_X30_Y78_N9
\dp|Add11~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~25_sumout\ = SUM(( (!\sm|loadc~0_combout\ & (((\dp|cx\(3))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~21_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~39_combout\ ) + ( \dp|Add11~22\ ))
-- \dp|Add11~26\ = CARRY(( (!\sm|loadc~0_combout\ & (((\dp|cx\(3))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~21_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~39_combout\ ) + ( \dp|Add11~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000001110100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_cx\(3),
	datad => \dp|ALT_INV_Add3~21_sumout\,
	dataf => \dp|ALT_INV_Add11~39_combout\,
	cin => \dp|Add11~22\,
	sumout => \dp|Add11~25_sumout\,
	cout => \dp|Add11~26\);

-- Location: LABCELL_X30_Y78_N12
\dp|Add11~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~29_sumout\ = SUM(( \dp|Add11~40_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(4))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~25_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~26\ ))
-- \dp|Add11~30\ = CARRY(( \dp|Add11~40_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(4))))) # (\sm|loadc~0_combout\ & (((\dp|Add3~25_sumout\)) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111000101100000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_cx\(4),
	datad => \dp|ALT_INV_Add11~40_combout\,
	dataf => \dp|ALT_INV_Add3~25_sumout\,
	cin => \dp|Add11~26\,
	sumout => \dp|Add11~29_sumout\,
	cout => \dp|Add11~30\);

-- Location: LABCELL_X30_Y78_N15
\dp|Add11~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~9_sumout\ = SUM(( \dp|Add11~33_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(5))))) # (\sm|loadc~0_combout\ & (!\sm|Decoder0~1_combout\ & ((\dp|Add3~9_sumout\)))) ) + ( \dp|Add11~30\ ))
-- \dp|Add11~10\ = CARRY(( \dp|Add11~33_combout\ ) + ( (!\sm|loadc~0_combout\ & (((\dp|cx\(5))))) # (\sm|loadc~0_combout\ & (!\sm|Decoder0~1_combout\ & ((\dp|Add3~9_sumout\)))) ) + ( \dp|Add11~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100111101000100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_cx\(5),
	datad => \dp|ALT_INV_Add11~33_combout\,
	dataf => \dp|ALT_INV_Add3~9_sumout\,
	cin => \dp|Add11~30\,
	sumout => \dp|Add11~9_sumout\,
	cout => \dp|Add11~10\);

-- Location: LABCELL_X30_Y77_N39
\dp|x[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[5]~feeder_combout\ = \dp|Add11~9_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Add11~9_sumout\,
	combout => \dp|x[5]~feeder_combout\);

-- Location: LABCELL_X29_Y77_N39
\dp|xp~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~2_combout\ = ( \dp|Add1~9_sumout\ & ( (!\sm|WideOr1~0_combout\ & ((\dp|xp\(5)))) # (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\)) ) ) # ( !\dp|Add1~9_sumout\ & ( (!\sm|WideOr1~0_combout\ & \dp|xp\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101001001110010011100100111001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xp\(5),
	dataf => \dp|ALT_INV_Add1~9_sumout\,
	combout => \dp|xp~2_combout\);

-- Location: LABCELL_X31_Y78_N27
\dp|x[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[1]~0_combout\ = ( \sm|current_state\(0) & ( !\sm|current_state\(3) $ (\sm|current_state[2]~DUPLICATE_q\) ) ) # ( !\sm|current_state\(0) & ( (!\sm|current_state\(1) & (!\sm|current_state\(3))) # (\sm|current_state\(1) & (\sm|current_state\(3) & 
-- \sm|current_state[2]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100101101000001010010111110000000011111111000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datac => \sm|ALT_INV_current_state\(3),
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \dp|x[1]~0_combout\);

-- Location: LABCELL_X31_Y78_N0
\dp|x[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[1]~1_combout\ = ( \sm|current_state\(0) & ( !\sm|current_state\(3) $ (\sm|current_state[2]~DUPLICATE_q\) ) ) # ( !\sm|current_state\(0) & ( !\sm|current_state\(3) $ (((\sm|current_state\(1) & \sm|current_state[2]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100100111001001110010011100100111000011110000111100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datab => \sm|ALT_INV_current_state\(3),
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \dp|x[1]~1_combout\);

-- Location: FF_X30_Y77_N40
\dp|x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[5]~feeder_combout\,
	asdata => \dp|xp~2_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(5));

-- Location: LABCELL_X29_Y78_N15
\dp|cx~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~3_combout\ = ( \dp|cx~1_combout\ & ( (!\sm|Decoder0~1_combout\) # (!\sm|loadc~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011101110111011101110111011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~1_combout\,
	datab => \sm|ALT_INV_loadc~0_combout\,
	dataf => \dp|ALT_INV_cx~1_combout\,
	combout => \dp|cx~3_combout\);

-- Location: FF_X29_Y78_N16
\dp|cx[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(6));

-- Location: LABCELL_X30_Y78_N45
\dp|Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~5_sumout\ = SUM(( \dp|cx\(6) ) + ( GND ) + ( \dp|Add3~10\ ))
-- \dp|Add3~6\ = CARRY(( \dp|cx\(6) ) + ( GND ) + ( \dp|Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_cx\(6),
	cin => \dp|Add3~10\,
	sumout => \dp|Add3~5_sumout\,
	cout => \dp|Add3~6\);

-- Location: LABCELL_X29_Y78_N18
\dp|cx~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~1_combout\ = ( \dp|cx\(6) & ( (!\sm|loadc~0_combout\) # (\dp|Add3~5_sumout\) ) ) # ( !\dp|cx\(6) & ( (\sm|loadc~0_combout\ & \dp|Add3~5_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001111001111110011111100111111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \dp|ALT_INV_Add3~5_sumout\,
	dataf => \dp|ALT_INV_cx\(6),
	combout => \dp|cx~1_combout\);

-- Location: MLABCELL_X28_Y79_N21
\dp|oy~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~0_combout\ = ( \dp|oy\(6) & ( (!\sm|WideOr3~0_combout\) # ((!\sm|Decoder0~0_combout\ & \dp|Add4~1_sumout\)) ) ) # ( !\dp|oy\(6) & ( (!\sm|Decoder0~0_combout\ & (\sm|WideOr3~0_combout\ & \dp|Add4~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000010000000100000001011001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_Decoder0~0_combout\,
	datab => \sm|ALT_INV_WideOr3~0_combout\,
	datac => \dp|ALT_INV_Add4~1_sumout\,
	dataf => \dp|ALT_INV_oy\(6),
	combout => \dp|oy~0_combout\);

-- Location: LABCELL_X29_Y80_N24
\dp|Add11~32\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~32_combout\ = ( \dp|ox~3_combout\ & ( \dp|oy~0_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # ((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- (((\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~3_combout\ & ( \dp|oy~0_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (((!\sm|current_state[0]~DUPLICATE_q\)) # 
-- (\sm|current_state[1]~DUPLICATE_q\))) # (\sm|current_state[3]~DUPLICATE_q\ & (((!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( \dp|ox~3_combout\ & ( !\dp|oy~0_combout\ & ( 
-- (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & ((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[0]~DUPLICATE_q\) # 
-- (\sm|current_state[1]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~3_combout\ & ( !\dp|oy~0_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[1]~DUPLICATE_q\ & ((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # (!\sm|current_state[0]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110001011000000011001010010011110011010110111111001110100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~3_combout\,
	dataf => \dp|ALT_INV_oy~0_combout\,
	combout => \dp|Add11~32_combout\);

-- Location: LABCELL_X30_Y78_N18
\dp|Add11~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~5_sumout\ = SUM(( (\dp|cx~1_combout\ & ((!\sm|loadc~0_combout\) # (!\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~32_combout\ ) + ( \dp|Add11~10\ ))
-- \dp|Add11~6\ = CARRY(( (\dp|cx~1_combout\ & ((!\sm|loadc~0_combout\) # (!\sm|Decoder0~1_combout\))) ) + ( \dp|Add11~32_combout\ ) + ( \dp|Add11~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \sm|ALT_INV_Decoder0~1_combout\,
	datad => \dp|ALT_INV_cx~1_combout\,
	dataf => \dp|ALT_INV_Add11~32_combout\,
	cin => \dp|Add11~10\,
	sumout => \dp|Add11~5_sumout\,
	cout => \dp|Add11~6\);

-- Location: LABCELL_X30_Y77_N0
\dp|x[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[6]~feeder_combout\ = \dp|Add11~5_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_Add11~5_sumout\,
	combout => \dp|x[6]~feeder_combout\);

-- Location: FF_X30_Y77_N1
\dp|x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[6]~feeder_combout\,
	asdata => \dp|xp~1_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(6));

-- Location: LABCELL_X30_Y78_N48
\dp|Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add3~1_sumout\ = SUM(( \dp|cx\(7) ) + ( GND ) + ( \dp|Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_cx\(7),
	cin => \dp|Add3~6\,
	sumout => \dp|Add3~1_sumout\);

-- Location: LABCELL_X29_Y78_N39
\dp|cx~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cx~0_combout\ = ( \sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & \dp|cx\(7)) ) ) # ( !\sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & ((\dp|cx\(7)))) # (\sm|loadc~0_combout\ & (\dp|Add3~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111110011000000111111001100000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add3~1_sumout\,
	datac => \sm|ALT_INV_loadc~0_combout\,
	datad => \dp|ALT_INV_cx\(7),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cx~0_combout\);

-- Location: FF_X29_Y78_N40
\dp|cx[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx\(7));

-- Location: LABCELL_X31_Y78_N48
\dp|Add11~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~41_combout\ = ( \sm|current_state\(0) & ( (\dp|cx\(7) & ((!\sm|current_state\(1)) # ((\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(3))))) ) ) # ( !\sm|current_state\(0) & ( \sm|current_state\(3) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100001011000011110000101100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datab => \sm|ALT_INV_current_state\(3),
	datac => \dp|ALT_INV_cx\(7),
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \dp|Add11~41_combout\);

-- Location: FF_X29_Y78_N41
\dp|cx[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cx~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cx[7]~DUPLICATE_q\);

-- Location: LABCELL_X29_Y78_N24
\dp|Add11~42\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~42_combout\ = ( \sm|current_state[0]~DUPLICATE_q\ & ( \dp|ox~2_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[2]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\ & 
-- (\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\)) ) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( \dp|ox~2_combout\ & ( (\dp|cx[7]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ $ (((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (!\sm|current_state[2]~DUPLICATE_q\))))) ) ) ) # ( \sm|current_state[0]~DUPLICATE_q\ & ( !\dp|ox~2_combout\ & ( (!\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[2]~DUPLICATE_q\)) # 
-- (\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[2]~DUPLICATE_q\)) ) ) ) # ( !\sm|current_state[0]~DUPLICATE_q\ & ( !\dp|ox~2_combout\ & ( (\dp|cx[7]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ $ 
-- (((!\sm|current_state[1]~DUPLICATE_q\) # (!\sm|current_state[2]~DUPLICATE_q\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010010010100000000101000010001000100100000010110100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datab => \dp|ALT_INV_cx[7]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datae => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_ox~2_combout\,
	combout => \dp|Add11~42_combout\);

-- Location: LABCELL_X30_Y78_N21
\dp|Add11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add11~1_sumout\ = SUM(( (\dp|Add11~41_combout\ & (!\sm|current_state\(3) $ (!\sm|current_state[2]~DUPLICATE_q\))) ) + ( \dp|Add11~42_combout\ ) + ( \dp|Add11~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datad => \dp|ALT_INV_Add11~41_combout\,
	dataf => \dp|ALT_INV_Add11~42_combout\,
	cin => \dp|Add11~6\,
	sumout => \dp|Add11~1_sumout\);

-- Location: LABCELL_X30_Y77_N3
\dp|x[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[7]~feeder_combout\ = \dp|Add11~1_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_Add11~1_sumout\,
	combout => \dp|x[7]~feeder_combout\);

-- Location: FF_X30_Y77_N5
\dp|x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[7]~feeder_combout\,
	asdata => \dp|xp~0_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(7));

-- Location: LABCELL_X30_Y79_N15
\vga_u0|writeEn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~0_combout\ = ( \dp|x\(7) & ( (\dp|x\(6)) # (\dp|x\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_x\(5),
	datad => \dp|ALT_INV_x\(6),
	dataf => \dp|ALT_INV_x\(7),
	combout => \vga_u0|writeEn~0_combout\);

-- Location: LABCELL_X29_Y80_N36
\dp|oy~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~2_combout\ = ( !\sm|Decoder0~0_combout\ & ( \sm|WideOr3~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	dataf => \sm|ALT_INV_Decoder0~0_combout\,
	combout => \dp|oy~2_combout\);

-- Location: LABCELL_X29_Y80_N39
\dp|oy~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|oy~3_combout\ = (!\sm|WideOr3~0_combout\ & \dp|oy\(6))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr3~0_combout\,
	datad => \dp|ALT_INV_oy\(6),
	combout => \dp|oy~3_combout\);

-- Location: LABCELL_X29_Y80_N12
\dp|Add15~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~21_combout\ = ( \dp|oy~3_combout\ & ( (\dp|ox~3_combout\) # (\sm|current_state[0]~DUPLICATE_q\) ) ) # ( !\dp|oy~3_combout\ & ( (!\sm|current_state[0]~DUPLICATE_q\ & (\dp|ox~3_combout\)) # (\sm|current_state[0]~DUPLICATE_q\ & 
-- (((\dp|Add4~1_sumout\ & \dp|oy~2_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100111001000100010011101110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datab => \dp|ALT_INV_ox~3_combout\,
	datac => \dp|ALT_INV_Add4~1_sumout\,
	datad => \dp|ALT_INV_oy~2_combout\,
	dataf => \dp|ALT_INV_oy~3_combout\,
	combout => \dp|Add15~21_combout\);

-- Location: LABCELL_X31_Y78_N39
\dp|Add15~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~19_combout\ = ( \sm|current_state\(0) & ( !\sm|current_state\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state\(2),
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \dp|Add15~19_combout\);

-- Location: LABCELL_X29_Y80_N30
\dp|Add15~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~18_combout\ = ( \dp|ox~1_combout\ & ( (\sm|current_state[2]~DUPLICATE_q\ & ((!\dp|ox~0_combout\) # (!\dp|Add7~5_sumout\))) ) ) # ( !\dp|ox~1_combout\ & ( (!\dp|ox\(6) & (\sm|current_state[2]~DUPLICATE_q\ & ((!\dp|ox~0_combout\) # 
-- (!\dp|Add7~5_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011100000000000001110000000000000111011100000000011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ox~0_combout\,
	datab => \dp|ALT_INV_Add7~5_sumout\,
	datac => \dp|ALT_INV_ox\(6),
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_ox~1_combout\,
	combout => \dp|Add15~18_combout\);

-- Location: LABCELL_X29_Y80_N6
\dp|Add15~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~16_combout\ = ( !\sm|current_state[0]~DUPLICATE_q\ & ( !\sm|current_state[2]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	combout => \dp|Add15~16_combout\);

-- Location: LABCELL_X29_Y80_N42
\dp|Add15~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~17_combout\ = ( \sm|current_state[1]~DUPLICATE_q\ & ( \dp|ox\(6) & ( (\dp|Add15~16_combout\ & (\dp|ox~1_combout\ & ((!\dp|ox~0_combout\) # (!\dp|Add7~5_sumout\)))) ) ) ) # ( !\sm|current_state[1]~DUPLICATE_q\ & ( \dp|ox\(6) & ( 
-- (\dp|Add15~16_combout\ & ((!\dp|ox~1_combout\) # ((\dp|ox~0_combout\ & \dp|Add7~5_sumout\)))) ) ) ) # ( \sm|current_state[1]~DUPLICATE_q\ & ( !\dp|ox\(6) & ( (\dp|Add15~16_combout\ & ((!\dp|ox~0_combout\) # (!\dp|Add7~5_sumout\))) ) ) ) # ( 
-- !\sm|current_state[1]~DUPLICATE_q\ & ( !\dp|ox\(6) & ( (\dp|ox~0_combout\ & (\dp|Add15~16_combout\ & \dp|Add7~5_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010001001100110010001000110000001100010000001100000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_ox~0_combout\,
	datab => \dp|ALT_INV_Add15~16_combout\,
	datac => \dp|ALT_INV_ox~1_combout\,
	datad => \dp|ALT_INV_Add7~5_sumout\,
	datae => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_ox\(6),
	combout => \dp|Add15~17_combout\);

-- Location: LABCELL_X29_Y80_N48
\dp|Add15~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~20_combout\ = ( \dp|Add4~1_sumout\ & ( \dp|Add15~17_combout\ ) ) # ( !\dp|Add4~1_sumout\ & ( \dp|Add15~17_combout\ ) ) # ( \dp|Add4~1_sumout\ & ( !\dp|Add15~17_combout\ & ( ((\dp|Add15~19_combout\ & (!\dp|oy~3_combout\ & !\dp|oy~2_combout\))) # 
-- (\dp|Add15~18_combout\) ) ) ) # ( !\dp|Add4~1_sumout\ & ( !\dp|Add15~17_combout\ & ( ((\dp|Add15~19_combout\ & !\dp|oy~3_combout\)) # (\dp|Add15~18_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010011111111010000001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add15~19_combout\,
	datab => \dp|ALT_INV_oy~3_combout\,
	datac => \dp|ALT_INV_oy~2_combout\,
	datad => \dp|ALT_INV_Add15~18_combout\,
	datae => \dp|ALT_INV_Add4~1_sumout\,
	dataf => \dp|ALT_INV_Add15~17_combout\,
	combout => \dp|Add15~20_combout\);

-- Location: LABCELL_X29_Y78_N21
\dp|cnum~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cnum~0_combout\ = ( \sm|Decoder0~1_combout\ & ( (!\sm|loadc~0_combout\ & \dp|cnum\(0)) ) ) # ( !\sm|Decoder0~1_combout\ & ( !\sm|loadc~0_combout\ $ (!\dp|cnum\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111001100001100111100110000000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_loadc~0_combout\,
	datad => \dp|ALT_INV_cnum\(0),
	dataf => \sm|ALT_INV_Decoder0~1_combout\,
	combout => \dp|cnum~0_combout\);

-- Location: FF_X29_Y78_N22
\dp|cnum[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cnum~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cnum\(0));

-- Location: MLABCELL_X28_Y78_N54
\dp|cy~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|cy~0_combout\ = ( \dp|cnum\(0) & ( !\sm|Decoder0~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_Decoder0~1_combout\,
	dataf => \dp|ALT_INV_cnum\(0),
	combout => \dp|cy~0_combout\);

-- Location: FF_X28_Y78_N56
\dp|cy[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|cy~0_combout\,
	ena => \sm|loadc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|cy\(5));

-- Location: MLABCELL_X28_Y78_N33
\dp|Add15~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~22_combout\ = ( \dp|ox~4_combout\ & ( \dp|oy~1_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~4_combout\ & ( 
-- \dp|oy~1_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~4_combout\ & ( !\dp|oy~1_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~4_combout\ & ( !\dp|oy~1_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111010100100010000010101101110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~4_combout\,
	dataf => \dp|ALT_INV_oy~1_combout\,
	combout => \dp|Add15~22_combout\);

-- Location: MLABCELL_X28_Y78_N39
\dp|Add15~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~23_combout\ = ( \dp|ox~5_combout\ & ( \dp|oy~4_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~5_combout\ & ( 
-- \dp|oy~4_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~5_combout\ & ( !\dp|oy~4_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~5_combout\ & ( !\dp|oy~4_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111010100100010000010101101110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~5_combout\,
	dataf => \dp|ALT_INV_oy~4_combout\,
	combout => \dp|Add15~23_combout\);

-- Location: MLABCELL_X28_Y78_N36
\dp|Add15~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~24_combout\ = ( \dp|ox~6_combout\ & ( \dp|oy~5_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~6_combout\ & ( 
-- \dp|oy~5_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\ & \sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~6_combout\ & ( !\dp|oy~5_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~6_combout\ & ( !\dp|oy~5_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111001001010010000011011010110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~6_combout\,
	dataf => \dp|ALT_INV_oy~5_combout\,
	combout => \dp|Add15~24_combout\);

-- Location: MLABCELL_X28_Y78_N30
\dp|Add15~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~37_combout\ = ( \dp|ox~9_combout\ & ( \dp|oy~11_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~9_combout\ & ( 
-- \dp|oy~11_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\ & \sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~9_combout\ & ( !\dp|oy~11_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~9_combout\ & ( !\dp|oy~11_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111001001010010000011011010110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~9_combout\,
	dataf => \dp|ALT_INV_oy~11_combout\,
	combout => \dp|Add15~37_combout\);

-- Location: MLABCELL_X28_Y78_N27
\dp|Add15~39\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~39_combout\ = ( \dp|ox~7_combout\ & ( \dp|oy~9_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~7_combout\ & ( 
-- \dp|oy~9_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\ & !\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~7_combout\ & ( !\dp|oy~9_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~7_combout\ & ( !\dp|oy~9_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111010100100010000010101101110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~7_combout\,
	dataf => \dp|ALT_INV_oy~9_combout\,
	combout => \dp|Add15~39_combout\);

-- Location: MLABCELL_X28_Y78_N24
\dp|Add15~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~38_combout\ = ( \dp|ox~8_combout\ & ( \dp|oy~7_combout\ & ( (!\sm|current_state\(3)) # ((!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) ) ) # ( !\dp|ox~8_combout\ & ( 
-- \dp|oy~7_combout\ & ( (!\sm|current_state\(3) & (((\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\ & \sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) ) # ( 
-- \dp|ox~8_combout\ & ( !\dp|oy~7_combout\ & ( (!\sm|current_state\(3) & (((!\sm|current_state[0]~DUPLICATE_q\)))) # (\sm|current_state\(3) & (!\sm|current_state[2]~DUPLICATE_q\ & ((!\sm|current_state[1]~DUPLICATE_q\) # 
-- (\sm|current_state[0]~DUPLICATE_q\)))) ) ) ) # ( !\dp|ox~8_combout\ & ( !\dp|oy~7_combout\ & ( (\sm|current_state\(3) & (((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101010101111001001010010000011011010110111110101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_ox~8_combout\,
	dataf => \dp|ALT_INV_oy~7_combout\,
	combout => \dp|Add15~38_combout\);

-- Location: MLABCELL_X28_Y78_N0
\dp|Add15~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~30_sumout\ = SUM(( !\sm|current_state[2]~DUPLICATE_q\ $ (((!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) + ( \dp|Add15~38_combout\ ) + ( !VCC ))
-- \dp|Add15~31\ = CARRY(( !\sm|current_state[2]~DUPLICATE_q\ $ (((!\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) + ( \dp|Add15~38_combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000011110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	dataf => \dp|ALT_INV_Add15~38_combout\,
	cin => GND,
	sumout => \dp|Add15~30_sumout\,
	cout => \dp|Add15~31\);

-- Location: MLABCELL_X28_Y78_N3
\dp|Add15~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~34_sumout\ = SUM(( \dp|Add15~39_combout\ ) + ( (!\sm|loadc~0_combout\ & ((!\dp|cy\(5)))) # (\sm|loadc~0_combout\ & (!\dp|cy~0_combout\)) ) + ( \dp|Add15~31\ ))
-- \dp|Add15~35\ = CARRY(( \dp|Add15~39_combout\ ) + ( (!\sm|loadc~0_combout\ & ((!\dp|cy\(5)))) # (\sm|loadc~0_combout\ & (!\dp|cy~0_combout\)) ) + ( \dp|Add15~31\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001011111010100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_cy~0_combout\,
	datac => \sm|ALT_INV_loadc~0_combout\,
	datad => \dp|ALT_INV_Add15~39_combout\,
	dataf => \dp|ALT_INV_cy\(5),
	cin => \dp|Add15~31\,
	sumout => \dp|Add15~34_sumout\,
	cout => \dp|Add15~35\);

-- Location: MLABCELL_X28_Y78_N6
\dp|Add15~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~26_sumout\ = SUM(( (!\sm|loadc~0_combout\ & (((!\dp|cy\(5))))) # (\sm|loadc~0_combout\ & (((!\dp|cnum\(0))) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add15~37_combout\ ) + ( \dp|Add15~35\ ))
-- \dp|Add15~27\ = CARRY(( (!\sm|loadc~0_combout\ & (((!\dp|cy\(5))))) # (\sm|loadc~0_combout\ & (((!\dp|cnum\(0))) # (\sm|Decoder0~1_combout\))) ) + ( \dp|Add15~37_combout\ ) + ( \dp|Add15~35\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000001111101101010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_loadc~0_combout\,
	datab => \sm|ALT_INV_Decoder0~1_combout\,
	datac => \dp|ALT_INV_cnum\(0),
	datad => \dp|ALT_INV_cy\(5),
	dataf => \dp|ALT_INV_Add15~37_combout\,
	cin => \dp|Add15~35\,
	sumout => \dp|Add15~26_sumout\,
	cout => \dp|Add15~27\);

-- Location: MLABCELL_X28_Y78_N9
\dp|Add15~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~13_sumout\ = SUM(( \dp|Add15~24_combout\ ) + ( VCC ) + ( \dp|Add15~27\ ))
-- \dp|Add15~14\ = CARRY(( \dp|Add15~24_combout\ ) + ( VCC ) + ( \dp|Add15~27\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \dp|ALT_INV_Add15~24_combout\,
	cin => \dp|Add15~27\,
	sumout => \dp|Add15~13_sumout\,
	cout => \dp|Add15~14\);

-- Location: MLABCELL_X28_Y78_N12
\dp|Add15~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~9_sumout\ = SUM(( VCC ) + ( \dp|Add15~23_combout\ ) + ( \dp|Add15~14\ ))
-- \dp|Add15~10\ = CARRY(( VCC ) + ( \dp|Add15~23_combout\ ) + ( \dp|Add15~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~23_combout\,
	cin => \dp|Add15~14\,
	sumout => \dp|Add15~9_sumout\,
	cout => \dp|Add15~10\);

-- Location: MLABCELL_X28_Y78_N15
\dp|Add15~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~5_sumout\ = SUM(( (!\sm|loadc~0_combout\ & (((\dp|cy\(5))))) # (\sm|loadc~0_combout\ & (\dp|cnum\(0) & (!\sm|Decoder0~1_combout\))) ) + ( \dp|Add15~22_combout\ ) + ( \dp|Add15~10\ ))
-- \dp|Add15~6\ = CARRY(( (!\sm|loadc~0_combout\ & (((\dp|cy\(5))))) # (\sm|loadc~0_combout\ & (\dp|cnum\(0) & (!\sm|Decoder0~1_combout\))) ) + ( \dp|Add15~22_combout\ ) + ( \dp|Add15~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000001000011011100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_cnum\(0),
	datab => \sm|ALT_INV_loadc~0_combout\,
	datac => \sm|ALT_INV_Decoder0~1_combout\,
	datad => \dp|ALT_INV_cy\(5),
	dataf => \dp|ALT_INV_Add15~22_combout\,
	cin => \dp|Add15~10\,
	sumout => \dp|Add15~5_sumout\,
	cout => \dp|Add15~6\);

-- Location: MLABCELL_X28_Y78_N18
\dp|Add15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|Add15~1_sumout\ = SUM(( (!\sm|current_state\(3) & (\dp|Add15~21_combout\)) # (\sm|current_state\(3) & ((\dp|Add15~20_combout\))) ) + ( GND ) + ( \dp|Add15~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(3),
	datab => \dp|ALT_INV_Add15~21_combout\,
	datac => \dp|ALT_INV_Add15~20_combout\,
	cin => \dp|Add15~6\,
	sumout => \dp|Add15~1_sumout\);

-- Location: LABCELL_X30_Y79_N0
\dp|y[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[6]~feeder_combout\ = ( \dp|Add15~1_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~1_sumout\,
	combout => \dp|y[6]~feeder_combout\);

-- Location: FF_X30_Y79_N2
\dp|y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[6]~feeder_combout\,
	asdata => \dp|yp~0_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(6));

-- Location: LABCELL_X30_Y79_N9
\dp|y[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[5]~feeder_combout\ = ( \dp|Add15~5_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~5_sumout\,
	combout => \dp|y[5]~feeder_combout\);

-- Location: FF_X30_Y79_N11
\dp|y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[5]~feeder_combout\,
	asdata => \dp|yp~1_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(5));

-- Location: LABCELL_X30_Y79_N6
\dp|y[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[4]~feeder_combout\ = ( \dp|Add15~9_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~9_sumout\,
	combout => \dp|y[4]~feeder_combout\);

-- Location: FF_X30_Y79_N8
\dp|y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[4]~feeder_combout\,
	asdata => \dp|yp~2_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(4));

-- Location: LABCELL_X30_Y79_N3
\dp|y[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[3]~feeder_combout\ = ( \dp|Add15~13_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~13_sumout\,
	combout => \dp|y[3]~feeder_combout\);

-- Location: FF_X30_Y79_N5
\dp|y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[3]~feeder_combout\,
	asdata => \dp|yp~3_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(3));

-- Location: LABCELL_X30_Y77_N6
\dp|y[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[2]~feeder_combout\ = ( \dp|Add15~26_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add15~26_sumout\,
	combout => \dp|y[2]~feeder_combout\);

-- Location: FF_X30_Y77_N7
\dp|y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[2]~feeder_combout\,
	asdata => \dp|yp~4_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(2));

-- Location: LABCELL_X30_Y77_N9
\dp|y[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[1]~feeder_combout\ = \dp|Add15~34_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add15~34_sumout\,
	combout => \dp|y[1]~feeder_combout\);

-- Location: LABCELL_X31_Y79_N24
\dp|yp~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~6_combout\ = ( \dp|yp\(1) & ( \dp|Add0~21_sumout\ & ( (((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[0]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(1) & ( 
-- \dp|Add0~21_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & (\sm|current_state[0]~DUPLICATE_q\ & !\sm|current_state[1]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(1) & ( !\dp|Add0~21_sumout\ & ( 
-- ((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011101111111111100001000000000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(1),
	dataf => \dp|ALT_INV_Add0~21_sumout\,
	combout => \dp|yp~6_combout\);

-- Location: FF_X30_Y77_N11
\dp|y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[1]~feeder_combout\,
	asdata => \dp|yp~6_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(1));

-- Location: LABCELL_X30_Y77_N45
\dp|y[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|y[0]~feeder_combout\ = \dp|Add15~30_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_Add15~30_sumout\,
	combout => \dp|y[0]~feeder_combout\);

-- Location: LABCELL_X31_Y79_N27
\dp|yp~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|yp~5_combout\ = ( \dp|yp\(0) & ( \dp|Add0~25_sumout\ & ( (((\sm|current_state[0]~DUPLICATE_q\) # (\sm|current_state[1]~DUPLICATE_q\)) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) ) # ( !\dp|yp\(0) & ( 
-- \dp|Add0~25_sumout\ & ( (!\sm|current_state[2]~DUPLICATE_q\ & (!\sm|current_state[3]~DUPLICATE_q\ & (!\sm|current_state[1]~DUPLICATE_q\ & \sm|current_state[0]~DUPLICATE_q\))) ) ) ) # ( \dp|yp\(0) & ( !\dp|Add0~25_sumout\ & ( 
-- ((\sm|current_state[1]~DUPLICATE_q\) # (\sm|current_state[3]~DUPLICATE_q\)) # (\sm|current_state[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000011111110111111100000000100000000111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	datab => \sm|ALT_INV_current_state[3]~DUPLICATE_q\,
	datac => \sm|ALT_INV_current_state[1]~DUPLICATE_q\,
	datad => \sm|ALT_INV_current_state[0]~DUPLICATE_q\,
	datae => \dp|ALT_INV_yp\(0),
	dataf => \dp|ALT_INV_Add0~25_sumout\,
	combout => \dp|yp~5_combout\);

-- Location: FF_X30_Y77_N47
\dp|y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|y[0]~feeder_combout\,
	asdata => \dp|yp~5_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|y\(0));

-- Location: LABCELL_X30_Y79_N30
\vga_u0|user_input_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~9_sumout\ = SUM(( !\dp|y\(0) $ (!\dp|x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~10\ = CARRY(( !\dp|y\(0) $ (!\dp|x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~11\ = SHARE((\dp|y\(0) & \dp|x\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(0),
	datad => \dp|ALT_INV_x\(5),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|user_input_translator|Add1~9_sumout\,
	cout => \vga_u0|user_input_translator|Add1~10\,
	shareout => \vga_u0|user_input_translator|Add1~11\);

-- Location: LABCELL_X30_Y79_N33
\vga_u0|user_input_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~13_sumout\ = SUM(( !\dp|y\(1) $ (!\dp|x\(6)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~14\ = CARRY(( !\dp|y\(1) $ (!\dp|x\(6)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~15\ = SHARE((\dp|y\(1) & \dp|x\(6)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000101010100000000000000000101010110101010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(1),
	datad => \dp|ALT_INV_x\(6),
	cin => \vga_u0|user_input_translator|Add1~10\,
	sharein => \vga_u0|user_input_translator|Add1~11\,
	sumout => \vga_u0|user_input_translator|Add1~13_sumout\,
	cout => \vga_u0|user_input_translator|Add1~14\,
	shareout => \vga_u0|user_input_translator|Add1~15\);

-- Location: LABCELL_X30_Y79_N36
\vga_u0|user_input_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~17_sumout\ = SUM(( !\dp|x\(7) $ (!\dp|y\(0) $ (\dp|y\(2))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~18\ = CARRY(( !\dp|x\(7) $ (!\dp|y\(0) $ (\dp|y\(2))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~19\ = SHARE((!\dp|x\(7) & (\dp|y\(0) & \dp|y\(2))) # (\dp|x\(7) & ((\dp|y\(2)) # (\dp|y\(0)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_x\(7),
	datac => \dp|ALT_INV_y\(0),
	datad => \dp|ALT_INV_y\(2),
	cin => \vga_u0|user_input_translator|Add1~14\,
	sharein => \vga_u0|user_input_translator|Add1~15\,
	sumout => \vga_u0|user_input_translator|Add1~17_sumout\,
	cout => \vga_u0|user_input_translator|Add1~18\,
	shareout => \vga_u0|user_input_translator|Add1~19\);

-- Location: LABCELL_X30_Y79_N39
\vga_u0|user_input_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~21_sumout\ = SUM(( !\dp|y\(1) $ (!\dp|y\(3)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~22\ = CARRY(( !\dp|y\(1) $ (!\dp|y\(3)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~23\ = SHARE((\dp|y\(1) & \dp|y\(3)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000101010100000000000000000101010110101010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(1),
	datad => \dp|ALT_INV_y\(3),
	cin => \vga_u0|user_input_translator|Add1~18\,
	sharein => \vga_u0|user_input_translator|Add1~19\,
	sumout => \vga_u0|user_input_translator|Add1~21_sumout\,
	cout => \vga_u0|user_input_translator|Add1~22\,
	shareout => \vga_u0|user_input_translator|Add1~23\);

-- Location: LABCELL_X30_Y79_N42
\vga_u0|user_input_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~25_sumout\ = SUM(( !\dp|y\(4) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~26\ = CARRY(( !\dp|y\(4) $ (!\dp|y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~27\ = SHARE((\dp|y\(4) & \dp|y\(2)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_y\(4),
	datac => \dp|ALT_INV_y\(2),
	cin => \vga_u0|user_input_translator|Add1~22\,
	sharein => \vga_u0|user_input_translator|Add1~23\,
	sumout => \vga_u0|user_input_translator|Add1~25_sumout\,
	cout => \vga_u0|user_input_translator|Add1~26\,
	shareout => \vga_u0|user_input_translator|Add1~27\);

-- Location: LABCELL_X30_Y79_N45
\vga_u0|user_input_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~29_sumout\ = SUM(( !\dp|y\(3) $ (!\dp|y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~30\ = CARRY(( !\dp|y\(3) $ (!\dp|y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~31\ = SHARE((\dp|y\(3) & \dp|y\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(3),
	datac => \dp|ALT_INV_y\(5),
	cin => \vga_u0|user_input_translator|Add1~26\,
	sharein => \vga_u0|user_input_translator|Add1~27\,
	sumout => \vga_u0|user_input_translator|Add1~29_sumout\,
	cout => \vga_u0|user_input_translator|Add1~30\,
	shareout => \vga_u0|user_input_translator|Add1~31\);

-- Location: LABCELL_X30_Y79_N48
\vga_u0|user_input_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~33_sumout\ = SUM(( !\dp|y\(4) $ (!\dp|y\(6)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~34\ = CARRY(( !\dp|y\(4) $ (!\dp|y\(6)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~35\ = SHARE((\dp|y\(4) & \dp|y\(6)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_y\(4),
	datac => \dp|ALT_INV_y\(6),
	cin => \vga_u0|user_input_translator|Add1~30\,
	sharein => \vga_u0|user_input_translator|Add1~31\,
	sumout => \vga_u0|user_input_translator|Add1~33_sumout\,
	cout => \vga_u0|user_input_translator|Add1~34\,
	shareout => \vga_u0|user_input_translator|Add1~35\);

-- Location: LABCELL_X30_Y79_N51
\vga_u0|user_input_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~37_sumout\ = SUM(( \dp|y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~38\ = CARRY(( \dp|y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(5),
	cin => \vga_u0|user_input_translator|Add1~34\,
	sharein => \vga_u0|user_input_translator|Add1~35\,
	sumout => \vga_u0|user_input_translator|Add1~37_sumout\,
	cout => \vga_u0|user_input_translator|Add1~38\,
	shareout => \vga_u0|user_input_translator|Add1~39\);

-- Location: LABCELL_X30_Y79_N54
\vga_u0|user_input_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~5_sumout\ = SUM(( \dp|y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~6\ = CARRY(( \dp|y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~7\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \dp|ALT_INV_y\(6),
	cin => \vga_u0|user_input_translator|Add1~38\,
	sharein => \vga_u0|user_input_translator|Add1~39\,
	sumout => \vga_u0|user_input_translator|Add1~5_sumout\,
	cout => \vga_u0|user_input_translator|Add1~6\,
	shareout => \vga_u0|user_input_translator|Add1~7\);

-- Location: LABCELL_X30_Y79_N57
\vga_u0|user_input_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~1_sumout\ = SUM(( GND ) + ( \vga_u0|user_input_translator|Add1~7\ ) + ( \vga_u0|user_input_translator|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|user_input_translator|Add1~6\,
	sharein => \vga_u0|user_input_translator|Add1~7\,
	sumout => \vga_u0|user_input_translator|Add1~1_sumout\);

-- Location: LABCELL_X30_Y79_N18
\vga_u0|LessThan3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|LessThan3~0_combout\ = ( \dp|y\(5) & ( (\dp|y\(6) & (\dp|y\(3) & \dp|y\(4))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \dp|ALT_INV_y\(6),
	datac => \dp|ALT_INV_y\(3),
	datad => \dp|ALT_INV_y\(4),
	dataf => \dp|ALT_INV_y\(5),
	combout => \vga_u0|LessThan3~0_combout\);

-- Location: LABCELL_X30_Y79_N24
\vga_u0|VideoMemory|auto_generated|decode2|w_anode105w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2) = ( !\vga_u0|LessThan3~0_combout\ & ( (!\vga_u0|writeEn~1_combout\ & (!\vga_u0|writeEn~0_combout\ & (!\vga_u0|user_input_translator|Add1~5_sumout\ & 
-- !\vga_u0|user_input_translator|Add1~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_writeEn~1_combout\,
	datab => \vga_u0|ALT_INV_writeEn~0_combout\,
	datac => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|ALT_INV_LessThan3~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2));

-- Location: FF_X33_Y80_N25
\vga_u0|controller|xCounter[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[3]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[8]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y80_N30
\vga_u0|controller|controller_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~9_sumout\ = SUM(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~10\ = CARRY(( !\vga_u0|controller|yCounter\(2) $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~11\ = SHARE((\vga_u0|controller|yCounter\(2) & \vga_u0|controller|xCounter\(7)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|controller|controller_translator|Add1~9_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~10\,
	shareout => \vga_u0|controller|controller_translator|Add1~11\);

-- Location: LABCELL_X30_Y80_N33
\vga_u0|controller|controller_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~13_sumout\ = SUM(( !\vga_u0|controller|yCounter\(3) $ (!\vga_u0|controller|xCounter[8]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~10\ ))
-- \vga_u0|controller|controller_translator|Add1~14\ = CARRY(( !\vga_u0|controller|yCounter\(3) $ (!\vga_u0|controller|xCounter[8]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( \vga_u0|controller|controller_translator|Add1~10\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~15\ = SHARE((\vga_u0|controller|yCounter\(3) & \vga_u0|controller|xCounter[8]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000101010100000000000000000101010110101010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(3),
	datad => \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~10\,
	sharein => \vga_u0|controller|controller_translator|Add1~11\,
	sumout => \vga_u0|controller|controller_translator|Add1~13_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~14\,
	shareout => \vga_u0|controller|controller_translator|Add1~15\);

-- Location: LABCELL_X30_Y80_N36
\vga_u0|controller|controller_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~17_sumout\ = SUM(( !\vga_u0|controller|yCounter\(4) $ (!\vga_u0|controller|xCounter[9]~DUPLICATE_q\ $ (\vga_u0|controller|yCounter\(2))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~18\ = CARRY(( !\vga_u0|controller|yCounter\(4) $ (!\vga_u0|controller|xCounter[9]~DUPLICATE_q\ $ (\vga_u0|controller|yCounter\(2))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~19\ = SHARE((!\vga_u0|controller|yCounter\(4) & (\vga_u0|controller|xCounter[9]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(2))) # (\vga_u0|controller|yCounter\(4) & ((\vga_u0|controller|yCounter\(2)) # 
-- (\vga_u0|controller|xCounter[9]~DUPLICATE_q\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(4),
	datac => \vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|controller_translator|Add1~14\,
	sharein => \vga_u0|controller|controller_translator|Add1~15\,
	sumout => \vga_u0|controller|controller_translator|Add1~17_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~18\,
	shareout => \vga_u0|controller|controller_translator|Add1~19\);

-- Location: LABCELL_X30_Y80_N39
\vga_u0|controller|controller_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~21_sumout\ = SUM(( !\vga_u0|controller|yCounter\(3) $ (!\vga_u0|controller|yCounter\(5)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( \vga_u0|controller|controller_translator|Add1~18\ ))
-- \vga_u0|controller|controller_translator|Add1~22\ = CARRY(( !\vga_u0|controller|yCounter\(3) $ (!\vga_u0|controller|yCounter\(5)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( \vga_u0|controller|controller_translator|Add1~18\ ))
-- \vga_u0|controller|controller_translator|Add1~23\ = SHARE((\vga_u0|controller|yCounter\(3) & \vga_u0|controller|yCounter\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(3),
	datac => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|controller_translator|Add1~18\,
	sharein => \vga_u0|controller|controller_translator|Add1~19\,
	sumout => \vga_u0|controller|controller_translator|Add1~21_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~22\,
	shareout => \vga_u0|controller|controller_translator|Add1~23\);

-- Location: LABCELL_X30_Y80_N42
\vga_u0|controller|controller_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~25_sumout\ = SUM(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~26\ = CARRY(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~27\ = SHARE((\vga_u0|controller|yCounter\(6) & \vga_u0|controller|yCounter\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000011001100000000000000000011001111001100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(6),
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|controller_translator|Add1~22\,
	sharein => \vga_u0|controller|controller_translator|Add1~23\,
	sumout => \vga_u0|controller|controller_translator|Add1~25_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~26\,
	shareout => \vga_u0|controller|controller_translator|Add1~27\);

-- Location: LABCELL_X30_Y80_N45
\vga_u0|controller|controller_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~29_sumout\ = SUM(( !\vga_u0|controller|yCounter[5]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(7)) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~26\ ))
-- \vga_u0|controller|controller_translator|Add1~30\ = CARRY(( !\vga_u0|controller|yCounter[5]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(7)) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( \vga_u0|controller|controller_translator|Add1~26\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~31\ = SHARE((\vga_u0|controller|yCounter[5]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(7)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|controller_translator|Add1~26\,
	sharein => \vga_u0|controller|controller_translator|Add1~27\,
	sumout => \vga_u0|controller|controller_translator|Add1~29_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~30\,
	shareout => \vga_u0|controller|controller_translator|Add1~31\);

-- Location: LABCELL_X30_Y80_N48
\vga_u0|controller|controller_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~33_sumout\ = SUM(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~34\ = CARRY(( !\vga_u0|controller|yCounter\(6) $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~35\ = SHARE((\vga_u0|controller|yCounter\(6) & \vga_u0|controller|yCounter\(8)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000011001100000000000000000011001111001100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(6),
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~30\,
	sharein => \vga_u0|controller|controller_translator|Add1~31\,
	sumout => \vga_u0|controller|controller_translator|Add1~33_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~34\,
	shareout => \vga_u0|controller|controller_translator|Add1~35\);

-- Location: LABCELL_X30_Y80_N51
\vga_u0|controller|controller_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|controller_translator|Add1~34\,
	sharein => \vga_u0|controller|controller_translator|Add1~35\,
	sumout => \vga_u0|controller|controller_translator|Add1~37_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~38\,
	shareout => \vga_u0|controller|controller_translator|Add1~39\);

-- Location: LABCELL_X30_Y80_N54
\vga_u0|controller|controller_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~2\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~3\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~38\,
	sharein => \vga_u0|controller|controller_translator|Add1~39\,
	sumout => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~2\,
	shareout => \vga_u0|controller|controller_translator|Add1~3\);

-- Location: LABCELL_X30_Y80_N57
\vga_u0|controller|controller_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~5_sumout\ = SUM(( GND ) + ( \vga_u0|controller|controller_translator|Add1~3\ ) + ( \vga_u0|controller|controller_translator|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|controller|controller_translator|Add1~2\,
	sharein => \vga_u0|controller|controller_translator|Add1~3\,
	sumout => \vga_u0|controller|controller_translator|Add1~5_sumout\);

-- Location: LABCELL_X30_Y80_N18
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2) = ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011110000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	datae => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2));

-- Location: LABCELL_X29_Y80_N54
\~GND\ : cyclonev_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: LABCELL_X30_Y77_N42
\dp|x[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[0]~feeder_combout\ = \dp|Add11~13_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \dp|ALT_INV_Add11~13_sumout\,
	combout => \dp|x[0]~feeder_combout\);

-- Location: FF_X30_Y77_N43
\dp|x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[0]~feeder_combout\,
	asdata => \dp|xp~3_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(0));

-- Location: LABCELL_X30_Y77_N36
\dp|x[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[1]~feeder_combout\ = ( \dp|Add11~17_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add11~17_sumout\,
	combout => \dp|x[1]~feeder_combout\);

-- Location: FF_X30_Y77_N37
\dp|x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[1]~feeder_combout\,
	asdata => \dp|xp~4_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(1));

-- Location: LABCELL_X30_Y77_N51
\dp|x[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[2]~feeder_combout\ = ( \dp|Add11~21_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add11~21_sumout\,
	combout => \dp|x[2]~feeder_combout\);

-- Location: LABCELL_X29_Y77_N42
\dp|xp~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~5_combout\ = ( \dp|xp\(2) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initx~0_combout\ & \dp|Add1~21_sumout\)) ) ) # ( !\dp|xp\(2) & ( (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & \dp|Add1~21_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010101110101011101010111010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add1~21_sumout\,
	dataf => \dp|ALT_INV_xp\(2),
	combout => \dp|xp~5_combout\);

-- Location: FF_X30_Y77_N52
\dp|x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[2]~feeder_combout\,
	asdata => \dp|xp~5_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(2));

-- Location: LABCELL_X30_Y77_N57
\dp|x[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[3]~feeder_combout\ = ( \dp|Add11~25_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add11~25_sumout\,
	combout => \dp|x[3]~feeder_combout\);

-- Location: LABCELL_X29_Y77_N45
\dp|xp~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~6_combout\ = (!\sm|WideOr1~0_combout\ & (((\dp|xp\(3))))) # (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & ((\dp|Add1~25_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001001110000010100100111000001010010011100000101001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_xp\(3),
	datad => \dp|ALT_INV_Add1~25_sumout\,
	combout => \dp|xp~6_combout\);

-- Location: FF_X30_Y77_N58
\dp|x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[3]~feeder_combout\,
	asdata => \dp|xp~6_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(3));

-- Location: LABCELL_X30_Y77_N30
\dp|x[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|x[4]~feeder_combout\ = ( \dp|Add11~29_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \dp|ALT_INV_Add11~29_sumout\,
	combout => \dp|x[4]~feeder_combout\);

-- Location: LABCELL_X29_Y77_N36
\dp|xp~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \dp|xp~7_combout\ = ( \dp|xp\(4) & ( (!\sm|WideOr1~0_combout\) # ((!\sm|initx~0_combout\ & \dp|Add1~29_sumout\)) ) ) # ( !\dp|xp\(4) & ( (\sm|WideOr1~0_combout\ & (!\sm|initx~0_combout\ & \dp|Add1~29_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010101110101011101010111010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_WideOr1~0_combout\,
	datab => \sm|ALT_INV_initx~0_combout\,
	datac => \dp|ALT_INV_Add1~29_sumout\,
	dataf => \dp|ALT_INV_xp\(4),
	combout => \dp|xp~7_combout\);

-- Location: FF_X30_Y77_N31
\dp|x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \dp|x[4]~feeder_combout\,
	asdata => \dp|xp~7_combout\,
	sclr => \dp|x[1]~0_combout\,
	sload => \dp|x[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dp|x\(4));

-- Location: M10K_X26_Y79_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a2\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\);

-- Location: FF_X30_Y80_N59
\vga_u0|VideoMemory|auto_generated|address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~5_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1));

-- Location: FF_X30_Y80_N2
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1));

-- Location: FF_X30_Y80_N56
\vga_u0|VideoMemory|auto_generated|address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0));

-- Location: FF_X30_Y80_N8
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0));

-- Location: LABCELL_X30_Y79_N27
\vga_u0|VideoMemory|auto_generated|decode2|w_anode118w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2) = ( !\vga_u0|LessThan3~0_combout\ & ( (!\vga_u0|writeEn~1_combout\ & (!\vga_u0|writeEn~0_combout\ & (!\vga_u0|user_input_translator|Add1~1_sumout\ & \vga_u0|user_input_translator|Add1~5_sumout\))) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_writeEn~1_combout\,
	datab => \vga_u0|ALT_INV_writeEn~0_combout\,
	datac => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|ALT_INV_LessThan3~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2));

-- Location: LABCELL_X31_Y80_N21
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2) = ( \vga_u0|controller|controller_translator|Add1~1_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2));

-- Location: M10K_X38_Y78_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a5\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: LABCELL_X30_Y79_N12
\vga_u0|VideoMemory|auto_generated|decode2|w_anode126w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2) = ( !\vga_u0|writeEn~0_combout\ & ( (!\vga_u0|writeEn~1_combout\ & (\vga_u0|user_input_translator|Add1~1_sumout\ & (!\vga_u0|LessThan3~0_combout\ & !\vga_u0|user_input_translator|Add1~5_sumout\))) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_writeEn~1_combout\,
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	datac => \vga_u0|ALT_INV_LessThan3~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|ALT_INV_writeEn~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2));

-- Location: LABCELL_X31_Y80_N27
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2) = ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ & ( \vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2));

-- Location: M10K_X26_Y80_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a7\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\);

-- Location: LABCELL_X30_Y80_N12
\vga_u0|controller|VGA_R[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_R[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- (((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) # (\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & (((\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0))) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( 
-- (\vga_u0|controller|on_screen~1_combout\ & (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\)))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & (\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & 
-- (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000100000101000000010101000001010001010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\,
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\,
	combout => \vga_u0|controller|VGA_R[0]~0_combout\);

-- Location: M10K_X26_Y78_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a1\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y79_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a4\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\);

-- Location: LABCELL_X30_Y80_N0
\vga_u0|controller|VGA_G[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_G[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|controller|on_screen~1_combout\ & ( \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|controller|on_screen~1_combout\ & ( (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & (\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\)) # 
-- (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100001111110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\,
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	dataf => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	combout => \vga_u0|controller|VGA_G[0]~0_combout\);

-- Location: LABCELL_X31_Y78_N24
\sm|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \sm|WideOr4~0_combout\ = ( \sm|current_state\(0) & ( (!\sm|current_state\(3) & ((\sm|current_state[2]~DUPLICATE_q\))) # (\sm|current_state\(3) & ((!\sm|current_state\(1)) # (!\sm|current_state[2]~DUPLICATE_q\))) ) ) # ( !\sm|current_state\(0) & ( 
-- (\sm|current_state[2]~DUPLICATE_q\) # (\sm|current_state\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001111110011111100111110001111100011111000111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \sm|ALT_INV_current_state\(1),
	datab => \sm|ALT_INV_current_state\(3),
	datac => \sm|ALT_INV_current_state[2]~DUPLICATE_q\,
	dataf => \sm|ALT_INV_current_state\(0),
	combout => \sm|WideOr4~0_combout\);

-- Location: M10K_X26_Y77_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y80_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a6\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y77_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a3\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\);

-- Location: LABCELL_X30_Y80_N6
\vga_u0|controller|VGA_B[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_B[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\)) # 
-- (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\)) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & 
-- (\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010101000000000000010100010000000101010101000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\,
	combout => \vga_u0|controller|VGA_B[0]~0_combout\);

-- Location: LABCELL_X33_Y80_N48
\vga_u0|controller|VGA_HS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (((\vga_u0|controller|xCounter\(0) & \vga_u0|controller|xCounter\(1))) # (\vga_u0|controller|xCounter\(2))) # (\vga_u0|controller|xCounter\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110111111111110011011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(0),
	datab => \vga_u0|controller|ALT_INV_xCounter\(3),
	datac => \vga_u0|controller|ALT_INV_xCounter\(1),
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|VGA_HS1~0_combout\);

-- Location: LABCELL_X33_Y80_N54
\vga_u0|controller|VGA_HS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~1_combout\ = ( \vga_u0|controller|xCounter\(9) & ( \vga_u0|controller|xCounter\(6) & ( ((!\vga_u0|controller|xCounter\(7)) # ((\vga_u0|controller|xCounter\(5) & \vga_u0|controller|VGA_HS1~0_combout\))) # 
-- (\vga_u0|controller|xCounter\(8)) ) ) ) # ( !\vga_u0|controller|xCounter\(9) & ( \vga_u0|controller|xCounter\(6) ) ) # ( \vga_u0|controller|xCounter\(9) & ( !\vga_u0|controller|xCounter\(6) & ( ((!\vga_u0|controller|xCounter\(7)) # 
-- ((!\vga_u0|controller|xCounter\(5) & !\vga_u0|controller|VGA_HS1~0_combout\))) # (\vga_u0|controller|xCounter\(8)) ) ) ) # ( !\vga_u0|controller|xCounter\(9) & ( !\vga_u0|controller|xCounter\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111101010111111111111111111111111101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(5),
	datac => \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	datae => \vga_u0|controller|ALT_INV_xCounter\(9),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|VGA_HS1~1_combout\);

-- Location: FF_X33_Y80_N55
\vga_u0|controller|VGA_HS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_HS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS1~q\);

-- Location: FF_X33_Y80_N46
\vga_u0|controller|VGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_HS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS~q\);

-- Location: LABCELL_X31_Y80_N6
\vga_u0|controller|VGA_VS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~0_combout\ = ( \vga_u0|controller|yCounter\(2) & ( !\vga_u0|controller|yCounter\(4) & ( (!\vga_u0|controller|yCounter\(9) & (\vga_u0|controller|yCounter\(3) & (!\vga_u0|controller|yCounter\(1) $ 
-- (!\vga_u0|controller|yCounter\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000100100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(1),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(0),
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	datae => \vga_u0|controller|ALT_INV_yCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(4),
	combout => \vga_u0|controller|VGA_VS1~0_combout\);

-- Location: LABCELL_X31_Y80_N24
\vga_u0|controller|VGA_VS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~1_combout\ = ( \vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(6)) # ((!\vga_u0|controller|VGA_VS1~0_combout\) # ((!\vga_u0|controller|yCounter[5]~DUPLICATE_q\) # (!\vga_u0|controller|yCounter\(8)))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111101111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\,
	datac => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|VGA_VS1~1_combout\);

-- Location: FF_X31_Y80_N25
\vga_u0|controller|VGA_VS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_VS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS1~q\);

-- Location: FF_X31_Y80_N10
\vga_u0|controller|VGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_VS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS~q\);

-- Location: LABCELL_X33_Y80_N42
\vga_u0|controller|VGA_BLANK1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_BLANK1~0_combout\ = ( \vga_u0|controller|LessThan7~0_combout\ & ( \vga_u0|controller|xCounter[9]~DUPLICATE_q\ & ( (!\vga_u0|controller|xCounter\(8) & !\vga_u0|controller|xCounter\(7)) ) ) ) # ( 
-- \vga_u0|controller|LessThan7~0_combout\ & ( !\vga_u0|controller|xCounter[9]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001000100010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(7),
	datae => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter[9]~DUPLICATE_q\,
	combout => \vga_u0|controller|VGA_BLANK1~0_combout\);

-- Location: FF_X33_Y80_N43
\vga_u0|controller|VGA_BLANK1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_BLANK1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK1~q\);

-- Location: FF_X33_Y80_N34
\vga_u0|controller|VGA_BLANK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_BLANK1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK~q\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;


