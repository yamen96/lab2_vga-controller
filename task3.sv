module task3 (CLOCK_50, 
		 KEY,             
       VGA_R, VGA_G, VGA_B, 
       VGA_HS,             
       VGA_VS,             
       VGA_BLANK,           
       VGA_SYNC,            
       VGA_CLK,
		 LEDR);
  
input CLOCK_50;
input [3:0] KEY;
output [9:0] VGA_R, VGA_G, VGA_B; 
output VGA_HS;             
output VGA_VS;          
output VGA_BLANK;           
output VGA_SYNC;            
output VGA_CLK;
output [9:0] LEDR;
// Some constants that might be useful for you
parameter SCREEN_WIDTH = 160;
parameter SCREEN_HEIGHT = 120;

parameter BLACK = 3'b000;
parameter BLUE = 3'b001;
parameter GREEN = 3'b010;
parameter YELLOW = 3'b110;
parameter RED = 3'b100;
parameter WHITE = 3'b111;

 
 
  // To VGA adapter
  
wire resetn;
wire [7:0] x, ox;
wire [6:0] y, oy;
reg [2:0] colour;
reg plot;
   
   //statemachine to datapath wires
wire initx, inity, loadx, loady, ydone, xdone;
wire init_ox, init_oy, init_crit, load_ox, load_oy, load_crit;
wire [3:0] current_state;
   
	// instantiate VGA adapter 
	
vga_adapter #( .RESOLUTION("160x120"))
    vga_u0 (.resetn(KEY[3]),
	         .clock(CLOCK_50),
			   .colour(colour),
			   .x(x),
			   .y(y),
			   .plot(plot),
			   .VGA_R(VGA_R),
			   .VGA_G(VGA_G),
			   .VGA_B(VGA_B),	
			   .VGA_HS(VGA_HS),
			   .VGA_VS(VGA_VS),
			   .VGA_BLANK(VGA_BLANK),
			   .VGA_SYNC(VGA_SYNC),
			   .VGA_CLK(VGA_CLK));

// Your code to fill the screen goes here.  
task3_stateMachine sm(.CLOCK_50(CLOCK_50), .resetb(KEY[3]), .ydone(ydone), .xdone(xdone), .initx(initx), .inity(inity), .loadx(loadx), .loady(loady), .plot(plot), .colour(colour), 
.init_ox(init_ox), .init_oy(init_oy), .init_crit(init_crit), .load_ox(load_ox), .load_oy(load_oy), .load_crit(load_crit), .ox(ox), .oy(oy), .current_state(current_state));
 
task3_datapath dp(.CLOCK_50(CLOCK_50), .resetb(KEY[3]), .current_state(current_state), .init_ox(init_ox), .init_oy(init_oy), .init_crit(init_crit), .load_ox(load_ox), .load_oy(load_oy), 
.load_crit(load_crit), .initx(initx), .inity(inity), .loadx(loadx), .loady(loady), .xdone(xdone), .ydone(ydone), .x(x), .ox(ox), .y(y), .oy(oy));
endmodule