module task3_stateMachine(CLOCK_50, resetb, ydone, xdone, initx, inity, loadx, loady, plot, colour, 
init_ox, init_oy, init_crit, load_ox, load_oy, load_crit, ox, oy, current_state);

input resetb, ydone, xdone, CLOCK_50;
input [6:0] oy;
input [7:0] ox;

output reg initx, inity, plot, loadx, loady, init_ox, init_oy, init_crit, load_ox, load_oy, load_crit;
output reg [2:0] colour;
output [3:0] current_state;

reg [3:0] current_state, next_state;

//next state logic
always_comb
		case (current_state)
			4'b0000: next_state <= 4'b0010;
 			4'b0001: next_state <= 4'b0010;
 			4'b0010: if (xdone == 0) next_state <= 4'b0010;
 						else if (ydone == 0) next_state <= 4'b0001;
							else next_state <= 4'b0011;
			4'b0011: next_state <= 4'b0100;
			4'b0100: next_state <= 4'b0101;
			4'b0101: next_state <= 4'b0110;
			4'b0110: next_state <= 4'b0111;
			4'b0111: next_state <= 4'b1000;
			4'b1000: next_state <= 4'b1001;
			4'b1001: next_state <= 4'b1010;
			4'b1010: next_state <= 4'b1011;
			4'b1011: next_state <= 4'b1100;
			4'b1100: next_state <= 4'b1101;
			4'b1101: if(oy > ox) next_state <= 4'b1110;
						else next_state <= 4'b0101;
 			default: next_state <= 4'b1110;
		endcase
		
//vDFF
always_ff @(posedge(CLOCK_50) or negedge resetb) begin
	if(resetb == 0)
		current_state = 4'b0000;
	else
 		current_state <= next_state;
end
 	
//output logic
always_comb
	case (current_state)
 		4'b0000: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b11110000000000;
 		4'b0001: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b10110000000000;
 		4'b0010: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00011000000000;
 		4'b0011: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00000000000000;
		4'b0100: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001111111001;
		4'b0101: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b0110: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b0111: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1000: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1001: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1010: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1011: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1100: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000000001;
		4'b1101: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00001000111001;
 		default: {initx,inity,loady,loadx,plot,init_ox,init_oy,init_crit,load_ox,load_oy,load_crit,colour} <= 14'b00000000000000;
	endcase
endmodule