module task3_datapath(CLOCK_50, resetb, current_state, init_ox, init_oy, init_crit, load_ox, load_oy, load_crit, initx, inity, loadx, loady, xdone, ydone, x, ox, y, oy);

	input CLOCK_50, resetb, init_ox, init_oy, init_crit, load_ox, load_oy, load_crit, initx, inity, loadx, loady;
	input [3:0] current_state;
	
	output xdone, ydone;
	output [6:0] y, oy;
	output [7:0] x, ox;
	
	reg  signed [8:0] crit;
	reg xdone, ydone;
	reg [6:0] yp, y, oy;
	reg [7:0] xp, x, ox;
	
	
	//global constants
   parameter R  = 40;
   parameter CX =  80;
   parameter CY =  60; 

	always_ff @(posedge(CLOCK_50))
		begin
			//clear screen datapath
			if (loady == 1) begin
 				if (inity == 1)
 					yp = 0;
 				else
 					yp ++;
			end
			if (loadx == 1) begin
 				if (initx == 1)
 					xp = 0;
 				else 
 					xp ++;
			end
 			ydone <= 0;
 			xdone <= 0;
 			if (yp == 119)
 				ydone <= 1;
 			if (xp == 159)
 				xdone <= 1;
			
			//bresenham circle algorithm datapath
			if(load_oy == 1)
				if(init_oy == 1)
					oy = 0;
				else
					oy++;
					
			if(load_ox == 1) begin
				if(init_ox == 1)
					ox = R;
			end
				
			if(load_crit == 1)
				if(init_crit == 1)
					crit = (1 - R);
				else begin
					if(crit <= 0)
						crit = crit + 2*oy + 1;
					else begin
						ox--;
						crit = crit + 2*(oy - ox) + 1;
					end
				end
				
				
			//output mux for x
			case(current_state)
				4'b0101: x <= CX + ox;
				4'b0110: x <= CX + oy;
				4'b0111: x <= CX - ox;
				4'b1000: x <= CX - oy;
				4'b1001: x <= CX - ox;
				4'b1010: x <= CX - oy;
				4'b1011: x <= CX + ox;
				4'b1100: x <= CX + oy;
				4'b0010: x <= xp;
				default: x <= 0;
            endcase
				
			//output mux for y
			case(current_state)
				4'b0101: y <= CY + oy;
				4'b0110: y <= CY + ox;
				4'b0111: y <= CY + oy;
				4'b1000: y <= CY + ox;
				4'b1001: y <= CY - oy;
				4'b1010: y <= CY - ox;
				4'b1011: y <= CY - oy;
				4'b1100: y <= CY - ox;
				4'b0010: y <= yp;
				default: y <= 0;
            endcase
					
		end
endmodule

