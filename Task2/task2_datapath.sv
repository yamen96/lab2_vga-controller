module task2_datapath(CLOCK_50, resetb, initx, inity, loadx, loady, xdone, ydone, x, y, color);
input resetb, initx, inity, loadx, loady, CLOCK_50;
//outputs
output xdone, ydone;
output [6:0] y;
output [7:0] x;
output [2:0] color;
//regs
reg xdone, ydone;
reg [6:0] y;
reg [7:0] x;
reg [2:0] color;

always_ff @(posedge(CLOCK_50))
	begin
 		if (loady == 1)
 			if (inity == 1)
 				y = 0;
 			else
 				y ++;
		if (loadx == 1)
 			if (initx == 1)
 				x = 0;
 			else 
 				x ++;
 		ydone <= 0;
 		xdone <= 0;
		color = x % 8;
 		if (y == 119)
 			ydone <= 1;
 		if (x == 159)
 			xdone <= 1;
end
			
endmodule