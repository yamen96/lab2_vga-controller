module dp_tb();

//inputs
reg resetb, initx, inity, loadx, loady, CLOCK_50;

//outputs
wire xdone, ydone;
wire [5:0] y;
wire [6:0] x;
wire [2:0] color;

task2_datapath DUT(CLOCK_50, resetb, initx, inity, loadx, loady, xdone, ydone, x, y, color);

initial begin
	CLOCK_50 =0;
	forever begin
		CLOCK_50 = 1; #1;
		CLOCK_50 = 0; #1;
	end
end

initial begin
    loadx = 1; loady =1; #2;
    initx = 1; inity =1; #2; 
    
    $display("initiations");
    assert (x==7'b0000000) $display("PASS"); else $error("FAIL");
    assert (y==6'b000000) $display("PASS"); else $error("FAIL");
    assert (xdone==0) $display("PASS"); else $error("FAIL");
    assert (ydone==0) $display("PASS"); else $error("FAIL");
    $display("end of initiations");
    
end

endmodule
    
    
    
    
